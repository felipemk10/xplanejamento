<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$funcoes = new funcoes;
$login = new login;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));
//---

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
  if ( $_POST['formulario'] == 'login' ) {
    //  Rodando anti-injecgtion nas variáveis.
    $_POST['loginemail'] = $validacoes->anti_injection($_POST['loginemail']);
    $_POST['loginsenha'] = $validacoes->anti_injection($_POST['loginsenha']);
    //---
    
    $msg_formulario = $login->logar($_POST['loginemail'],$_POST['loginsenha']);
  }
}
?>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
    <meta name="description" content="Sistema de Planejamento">
    <meta name="author" content="TemplateMonster">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  </head>

<body class="admin-validation-page sb-l-o sb-r-c onload-check sb-l-m sb-l-disable-animation" data-spy="scroll" data-target="#nav-spy" data-offset="200" style="min-height: 94px;">


  <!-- Start: Main -->
  <div id="main">

          
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout">

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="height: 294px;">
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border panel-system" style="border: 0;">

                <form method="post" action="/" id="admin-form" novalidate="novalidate">

                  <div class="panel-body bg-light alinha-conteudo">
                    <!-- .section-divider -->
                    <div class="section row">
                      <div class="col-md-3">
                        <img class="logo-prefeitura" src="img/logo_prefeitura_home.png" height="150">
                      </div>
                      <div class="col-md-9 top-titulo"><img src="img/logo-planejamento.png"></div>
                    </div>
                    <div class="section row  menu">
                      <?php include_once('includes/menuindex.php'); ?>
                    </div>
                    <div class="section row " style="border: 1px solid margin-left">
                      <div class="col-md-12">
                        <div class="row">
                          <p class="titulo-info">Informativo</p> 
                          <p >Documentação necessária para a emissão de um processo de Alvará e Desmembramento:</p>
                          <p class="titulo-info">Emissão de Habite-se;</p>
                          <ul>
                            <li>Requerimento feito pelo proprietário da edificação;</li>
                            <li>Certidão Negativa de Débitos Municipais do Lote;</li>
                            <li>Certidão Negativa de Débitos Municipais do Contribuinte;</li>
                            <li>Cópia do Alvará de Construção ou Regularização;</li>
                          </ul>
                          <br>
                          <ul>
                            <li><strong>Obs.: O "Habite-se" só será concedido após " Plantio de pelo menos uma árvore nativa... " Resolução do CMMA nº 21/2015 Art. 1º - II.
                            Em ruas pavimentadas, o "Habite-se" só será concedido após a construção das calçadas. ( Lei 068/2001, Capítulo V, Art. 70).</strong></li>
                          </ul>
                          <br>
                          <ul>
                            <li><strong>Obs.: Em Caso de Condomínio: Convenção Registrada</strong></li>
                          </ul>
                          <br>
                          <p class="titulo-info">Emissão de Alvará de Construção, Regularização e Acréscimo de Área;</p>
                          <ul>
                            <li>Título de propriedade ou documento que comprove a justa posse (escritura ou contrato de compra e venda direto com a loteadora do lote em questão;</li>
                            <li>Certidão Negativa de Débitos Municipais do lote;</li>
                            <li>Certidão Negativa do Contribuinte;</li>
                            <li>Cópia dos documentos pessoais do proprietário da obra (CPF e RG) para pessoa física - (CNPJ) e Contrato so para pessoa jurídica;</li>
                            <li>Projeto Arquitetônico da edificação elaborado por profissional habilitado junto ao CREA (Arquiteto, Engenheiro, Técnico de Edificaçães);</li>
                            <li>Memorial Descritivo da construção elaborado por profissional habilitado junto ao CREA (Arquiteto, Engenheiro, Técnico de Edificaçães);</li>
                            <li>Certidão Negativa de Débitos Municipais do Responsável Técnico;</li>
                            <li>Cópia da Anotação de Responsabilidade Técnica (ART) de Autoria do Projeto Arquitetônico;</li>
                            <li>ópia da Anotação de Responsabilidade Técnica (ART) dos projetos e execução dos Complementares - Hidro-sanitária, Elétrico e Estrutural (para obras com 02 ou mais de pavimentos ou obras térreas com mais de 200 mº de área. ou para acréscimos de área que inclua 2º pavimento).</li>
                            <li>Cópia da Anotação de Responsabilidade Técnica (ART) de Execução de obra.</li>
                          </ul>
                          <p class="titulo-info">Emissão de Desmembramento, Remembramento e Redimensionamento</p>
                          <ul>
                            <li>Requerimento feito pelo proprietário do terreno</li>
                            <li>Título de propriedade ou documento que comprove a justa posse (escrita com registro com registro até 3 meses, matrículas atualizada do imóvel - retirada no Cartório de Registro de Imóveis de Barreiras);
                            </li>
                            <li>Cópia dos documentos pessoais do poprietário;</li>
                            <li>ertidão Negativa de Débitos Municipais do Lote;</li>
                            <li>Projeto de Desmembramento/ Remembramento/ Redimensionamento - elaborado por profissional habilitado junto ao CREA (Arquiteto, Engenheiro);</li>
                            <li>Memorial Descritivo do Desmembramento/ Remembramento/ Redimensionamento</li>
                            <li>Cópia da Anotação de Responsabilidade Técnica (ART) de Autoria do Projeto de Desmembrar/ Remembramento/ Redimensionamento, com o comprovante de pagamento;</li>
                            <li>Certidão Negativa de Débitos Municipais do Responsável Técnico;</li>
                          </ul>
                      </div><!-- conteudo-->
                  </div>
                    <!-- end .section row section -->

          </div>
          <?php include('includes/rodapeindex.php'); ?>
        </div>
        <!-- end: .admin-form -->

      </section>
      <!-- End: Content -->
      
    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

 <style>
 .logo-prefeitura {
    padding-top: 45px;
    padding-left:90px;
  }
  .top-titulo {
    font-size: 28px;
  font-weight: 600;
  margin-top: 32px
  }
  .fonte-texto {
    font-size: 13px;
  }

  .espacamento-esquerdo {
    padding-left: 10px;
  }
  .logo-legenda {
    font-size: 15px;
  }

    .menu-login {
      font-size: 11px;
      margin-top: 5px; 
    }
    .profissional {
      padding: 22px;



    }
   .col-md-menu {
    padding: 5px;
   }
   div.main {
    background-color: rgba(211,211,211, 0.5)
   }
   .menu-botao{
    border-left: 1px solid #A9A9A9; 
    margin-left: -30px;
    padding-left: 10px;
   }
   .menu-botao-inf {
    border-left: 1px solid #A9A9A9; 

   }

   .menu {
    border: 1px solid rgba(169,169,169, 1.5); 
    border-radius: 3px;
    padding: 20px;
    font-size: 14px;
    color: black;
    background-color: rgba(211,211,211, 0.2)
   }
   .alinha-conteudo{
    margin-top: 40px;


  }
  .texto-info {
    text-align: justify;
    font-size: 13px;
    margin-top: 15px;
    color: #545353;
  }
  .titulo-info {
    font-size: 25px;
    font-weight: bold;
    color: #545353;
  }

  
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#formlogin").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        loginemail: {
          required: true
        },
        loginsenha: {
          required: true
        }

      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        loginemail: {
          required: 'Digite o e-mail'
        },
        loginsenha: {
          required: 'Digite a senha'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->




</body></html>