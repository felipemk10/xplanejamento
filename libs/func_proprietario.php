<?php
class func_proprietario {
	function manipulacoes($id_pro, $id_cg, $formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
			
		if ( $formulario == 'cadastro' ){
	    	// Cadastra processo
            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_proprietario (
            		id_pro,	
					id_cg
            	) 


            	VALUES (?,?)");	
				$this->sql->bindValue(1, $id_pro);
				$this->sql->bindValue(2, $id_cg);

				$this->sql->execute();

				//return $this->conexao->lastInsertId();

				// Mostrar possíveis erros.
				//print_r($this->sql->errorInfo());

			//	Redirecionamento
			//header("Location: index.php");
		} else if ( $formulario == 'delete' ) {
			$this->sql = $this->conexao->prepare("DELETE FROM processos.processos_proprietario WHERE id_pro = ".$id_pro." and id_cg = ".$id_cg."");
			
			$this->sql->execute();
			
			// Mostrar possíveis erros.
			print_r($this->sql->errorInfo());
		}
	}
}