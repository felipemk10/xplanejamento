<?php
class func_alvarahabitese {
		 
	function manipulacoes(
		$id_pro,
		// 1 = Alvará, 2 = Habite-se
		$tipo,
		$numero,	 
		$id_cg, 
		$ano,
		$id_ah,
		$id_que,
		$substituir,
		$tipo2,
		$data,
		$delete_id_ah,
		$formulario) { 
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;


			
		if ( $formulario == 'cadastro' ){

			if ( $tipo == 1  ) {
				$this->numero = $this->configuracoes->consulta("SELECT numero FROM processos.processos_alvarahabitese WHERE tipo = 1 and tipo2 = 1 ORDER BY id_ah DESC");
				if ( $this->numero->rowCount() > 0 ) {
					$this->numero = $this->numero->fetch();
					$this->numero = $this->numero['numero'];
				} else {
					$this->numero = 0;
				}
			} else if ( $tipo == 2 ) {
				$this->numero = $this->configuracoes->consulta("SELECT numero FROM processos.processos_alvarahabitese WHERE tipo = 2 and tipo2 = 1 ORDER BY id_ah DESC");
				
				if ( $this->numero->rowCount() > 0 ) {
					$this->numero = $this->numero->fetch();
					$this->numero = $this->numero['numero'];
				} else {
					$this->numero = 0;
				}
			} else if ( $tipo == 3 ) {
				$this->numero = $this->configuracoes->consulta("SELECT numero FROM processos.processos_alvarahabitese WHERE tipo = 3 and tipo2 = 1 ORDER BY id_ah DESC");
				if ( $this->numero->rowCount() > 0 ) {
					$this->numero = $this->numero->fetch();
					$this->numero = $this->numero['numero'];
				} else {
					$this->numero = 0;
				}
			}
			
			if ( $numero == 0 ) {
				$numero = $this->numero+1;
			}

			if ( $data == null ) {
				$data = 'now()';
			} else {
				$dia = substr($data, 0,2);
				$mes = substr($data, 3,2);
				$ano = substr($data, 6,4);
				$data = $ano.'-'.$mes.'-'.$dia;
			}
	    	// Cadastra processo
            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_alvarahabitese (
            		id_pro,
					-- 	1 = Alvará, 2 = Habite-se, 3 = Alvará Regularização
					tipo,
					numero,	 
					id_cg, 
					ano,
					id_ah2,
					id_que,
					substituir,
					tipo2,
					datahora
            	) 
            	VALUES (?,?,?,?,?,?,?,?,?,?)");
            		
				$this->sql->bindValue(1, $id_pro);
				$this->sql->bindValue(2, $tipo);
				$this->sql->bindValue(3, $numero);
				$this->sql->bindValue(4, $id_cg);
				$this->sql->bindValue(5, $ano);
				$this->sql->bindValue(6, $id_ah);
				$this->sql->bindValue(7, $id_que);
				$this->sql->bindValue(8, $substituir);
				$this->sql->bindValue(9, $tipo2);
				$this->sql->bindValue(10, $data);

				$this->sql->execute();

				// return $this->conexao->lastInsertId();

				// Mostrar possíveis erros.
				// print_r($this->sql->errorInfo());

			// Redirecionamento
			// header("Location: index.php");
		} else if ( $formulario == 'excluir' ) {
			$this->sql = $this->conexao->prepare("DELETE FROM processos.processos_alvarahabitese WHERE id_ah = $delete_id_ah");
			
			$this->sql->execute();
			
			// Mostrar possíveis erros.
			// print_r($this->sql->errorInfo());
		}
	}
}
