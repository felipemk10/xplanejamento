<?php
class func_usuario {
		 
	function manipulacoes(
		$id_cg, 
		$nome,	 
		$email,	 
		$endereco,	 
		$bairro,	 
		$cidade,
		$estado,	 
		$telefone,	 
		$cpfcnpj,	 
		$creacau,
		//	1 = usuario, 2 = cadastro do sistema
		$tipo, 
		$formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;

			
		if ( $formulario == 'cadastro' ){
			if ( $this->consulta = $this->configuracoes->consulta("SELECT email FROM geral.cg WHERE email = '".$email."'")->rowCount() > 0 and $tipo == 1 ) {
				return 'E-mail '.$email.' já está em uso';
			} else {
				$this->sql = $this->conexao->prepare("INSERT INTO geral.cg (
            		nome,	 
					email,	 
					endereco,	 
					bairro,	 
					cidade,
					estado,
					telefone,	 
					cpfcnpj,	 
					creacau,
					tipo,
					datahora
            	) 


            	VALUES (?,?,?,?,?,?,?,?,?,?,now())");	
				$this->sql->bindValue(1, $nome);
				$this->sql->bindValue(2, $email);
				$this->sql->bindValue(3, $endereco);
				$this->sql->bindValue(4, $bairro);
				$this->sql->bindValue(5, $cidade);
				$this->sql->bindValue(6, $estado);
				$this->sql->bindValue(7, $telefone);
				$this->sql->bindValue(8, $cpfcnpj);
				$this->sql->bindValue(9, $creacau);
				$this->sql->bindValue(10, $tipo);

				$this->sql->execute();

				return $this->conexao->lastInsertId();
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			}
				

			//	Redirecionamento
			//header("Location: index.php");
		} else if ( $formulario == 'alteracao' ) {
			$this->sql = $this->conexao->prepare("UPDATE geral.cg SET 
					nome = '".$nome."',	 
					email = '".$email."',	 
					endereco = '".$endereco."',	 
					bairro = '".$bairro."',	 
					cidade = ".$cidade.",
					estado = '".$estado."',
					telefone = '".$telefone."',	 
					cpfcnpj = '".$cpfcnpj."',	 
					creacau = '".$creacau."',
					tipo = $tipo
				WHERE 
					id_cg = $id_cg");
			$this->sql->execute();
			
			// Mostrar possíveis erros.
			print_r($this->sql->errorInfo());
		} else if ( $formulario == 'desativa' ) {
			$this->sql = $this->conexao->prepare("UPDATE geral.cg SET ativo = false WHERE id_cg = ".$id_cg."");
			
			$this->sql->execute();
			
			// Mostrar possíveis erros.
			print_r($this->sql->errorInfo());
		} else if ( $formulario == 'ativa' ) {
			$this->sql = $this->conexao->prepare("UPDATE geral.cg SET ativo = true WHERE id_cg = ".$id_cg."");
			
			$this->sql->execute();
			
			// Mostrar possíveis erros.
			print_r($this->sql->errorInfo());
		}
	}
}
