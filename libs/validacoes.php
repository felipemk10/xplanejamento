<?php
class validacoes {
	//	Remover simbolos dos valores dos campos cpf e cnpj.
	function simbolos($numero) {
		$numero = str_replace( '.', '', $numero);
		$numero = str_replace( '-', '', $numero);
		$numero = str_replace( '/', '', $numero);
		
		return $numero;
	}

	//	Verifica se a url solicitada possui caractere especiais.
	function detectar_caractere($url) {
		$classe_formatacoes = new formatacoes;
		if ( strpos($url,'.') > 0 ) {
			echo 'Não deve conter ponto na URL.';	
			exit();
		}
		
		$url = $classe_formatacoes->retira_simbolos($url);
		
		if (!preg_match('/^[a-zA-Z0-9-\.]{1,31}$/i', $url)) {
			return false;
		} else {
			switch ($url) {
				case 'boletos':
					return 2;
					break;
				case 'cadastro_usuario':
					return 2;
					break;
				case 'css':
					return 2;
					break;
				case 'colaboradores':
					return 2;
					break;
				case 'empresas':
					return 2;
					break;
				case 'js':
					return 2;
					break;
				case 'operadores':
					return 2;
					break;
				case 'usuarios':
					return 2;
					break;
				case 'conexoes':
					return 2;
					break;
				case 'config':
					return 2;
					break;
				case 'img':
					return 2;
					break;
				case 'paginas':
					return 2;
					break;
				case 'sms':
					return 2;
					break;
				default:
					return true;
					break;
			}
		}
	}
	function datas_diferenca($data_inicial,$data_final,$tipo) {
		// Usa a função strtotime() e pega o timestamp das duas datas:
		$this->time_inicial = strtotime($data_inicial);
		$this->time_final = strtotime($data_final);
		
		// Calcula a diferença de segundos entre as duas datas:
		$this->diferenca = $this->time_final - $this->time_inicial; // 19522800 segundos
		
		// Calcula a diferença de dias
		return (INT)floor( $this->diferenca / (60 * 60 * 24));
	}

	function redirecionar_pg($procedimento) {
		if ( $procedimento == 'erro404' ) {
			header("Location: http://apisuporte.com/");
		}
	}
	//	Calcula a diferença entre datas.
	function diferenca_datas($d1, $d2, $type='', $sep='-') {
		$this->d1 = explode($sep, $d1);
		$this->d2 = explode($sep, $d2);
		switch ($type) {
			case 'A':
			$this->X = 31536000;
			break;
			case 'M':
			$this->X = 2592000;
			break;
			case 'D':
			$this->X = 86400;
			break;
			case 'H':
			$this->X = 3600;
			break;
			case 'MI':
			$this->X = 60;
			break;
			default:
			$this->X = 1;
		}
		return floor( ( ( mktime(0, 0, 0, $this->d2[1], $this->d2[2], $this->d2[0]) - mktime(0, 0, 0, $this->d1[1], $this->d1[2], $this->d1[0])  ) / $this->X ) );
	}
	//	Detectar artigos em termos
	function detectar_artigo($tipo,$termo) {
		$this->termo	=	strtolower($termo);	
		
		if ( $tipo == 'sem_romanos' ) {
			switch ($this->termo) {
				case 'da':
					return true;
					 break;
			    case 'de':
					return true;
					 break;
			    case 'do':
					return true;
					break;
			    case 'das':
					return true;
					break;
			    case 'dos':
					return true;
					break;
			    case 'a':
					return true;
					break;
			    case 'e':
					return true;
					break;
			    case 'i':
					return true;
					break;
			    case 'o':
					return true;
					break;
			    case 'u':
					return true;
					break;
			    case 'as':
			    	return true;
					break;
			    case 'os':
					return true;
					break;
			    case 'em':
					return true;
					break;
			    case 'para':
			    	return true;
					break;
			    case 'pra':
					return true;
					break;
			}
		} else if ( $tipo == 'com_romanos' ) {
			switch ($this->termo) {
				case 'da':
					return true;
					 break;
			    case 'de':
					return true;
					 break;
			    case 'do':
					return true;
					break;
			    case 'das':
					return true;
					break;
			    case 'dos':
					return true;
					break;
			    case 'a':
					return true;
					break;
			    case 'e':
					return true;
					break;
			    case 'o':
					return true;
					break;
			    case 'u':
					return true;
					break;
			    case 'as':
			    	return true;
					break;
			    case 'os':
					return true;
					break;
			    case 'em':
					return true;
					break;
			    case 'para':
			    	return true;
					break;
			    case 'pra':
					return true;
					break;
			}
		}
	}
	//	Evitar SQL injection
	function anti_injection($sql) {
		//Remove palavras que contenham sintaxe sql
		$this->sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i", '',$sql);
		$this->sql = addslashes($this->sql);//Adiciona barras invertidas a uma string
		$this->sql = get_magic_quotes_gpc() ? stripslashes($this->sql) : $this->sql;
		//$sql = function_exists('mysql_real_escape_string') ? mysql_real_escape_string($sql) : mysql_escape_string($sql);
		return $this->sql;
	}
	//---
	function isValidDateTime($dateTime) {
	   if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
	      if (checkdate($matches[2], $matches[3], $matches[1]))
	      {
	         return true;
	      }
	   }
	   return false;
	}
	//---
	function data($data){
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_empresas_validacoes = new empresas_validacoes;
	
		//-- Data atual --
		$this->year  = date("Y");
		$this->month = date("n");
		$this->day   = date("j");
		
		//-- Data enviada --
		$this->ano	=	substr($data,0,4);
		$this->mes 	=	substr($data,5,2);
		$this->dia	=	substr($data,7,2);
		
		//Validação
		$this->dt_nascimento = mktime(0,0,0,$this->mes,$this->dia,$this->ano);
		if( (date("Y",$this->dt_nascimento)>$this->year) || ($this->dia > cal_days_in_month(CAL_GREGORIAN, $this->mes, $this->ano)) ){
			return $this->classe_empresas_validacoes->mensagens_padrao(22);
			return false;
		} else {
			return true;
		}
	}
	//---
	function email($email){
		$this->conta		=	"/^[a-zA-Z0-9\._-]+@";
		$this->domino		=	"[a-zA-Z0-9\._-]+.";
		$this->extensao 	=	"([a-zA-Z]{2,4})$/";
		
		$this->pattern = $this->conta.$this->domino.$this->extensao;
		
		if (preg_match($this->pattern, $email)) {
			return true;
		} else {
			return false;
		}
	}
	//---
	function url($url) {
		if(!filter_var($this->url, FILTER_VALIDATE_URL)) {
			return false;
		} else {
			return true;	
		}
	}
	//---
	function cpf($cpf) {
		if ( $cpf == '00000000000' ) {
			return false;
		}
		if((!is_numeric($cpf)) or (strlen($cpf) <> 11))
		{
			return false;
		}
		else
		{
			if ( ($cpf == '11111111111') || ($cpf == '22222222222') ||
			($cpf == '33333333333') || ($cpf == '44444444444') ||
			($cpf == '55555555555') || ($cpf == '66666666666') ||
			($cpf == '77777777777') || ($cpf == '88888888888') ||
			($cpf == '99999999999') || ($cpf == '00000000000') )
			{
				return true;
			}
			else
			{
				$this->cpf_dv = substr($cpf, 9,2);
			}
		}
		for($this->i=0; $this->i<=8; $this->i++)
		{
			$this->digito[$this->i] = substr($cpf, $this->i,1);
		}
		$this->posicao = 10;
		$this->soma = 0;
		for($this->i=0; $this->i<=8; $this->i++)
		{
			$this->soma = $this->soma + $this->digito[$this->i] * $this->posicao;
			$this->posicao = $this->posicao - 1;
		}
		$this->digito[9] = $this->soma % 11;
		if($this->digito[9] < 2)
		{
			$this->digito[9] = 0;
		}
		else
		{
			$this->digito[9] = 11 - $this->digito[9];
		}
		$this->posicao = 11;
		$this->soma = 0;
		for ($this->i=0; $this->i<=9; $this->i++)
		{
			$this->soma = $this->soma + $this->digito[$this->i] * $this->posicao;
			$this->posicao = $this->posicao - 1;
		}
		$this->digito[10] = $this->soma % 11;
		if ($this->digito[10] < 2)
		{
			$this->digito[10] = 0;
		}
		else
		{
			$this->digito[10] = 11 - $this->digito[10];
		}
		$this->dv = $this->digito[9] * 10 + $this->digito[10];
		if ($this->dv != $this->cpf_dv)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	//---
	function cnpj($cnpj) { 
	    $cnpj = preg_replace("@[./-]@", "", $cnpj);
		if ( $cnpj == '00000000000000' ) {
			return false;
		}
		if (strlen ($cnpj) <> 14 or !is_numeric ($cnpj))
		{
		return 0;
		}
		$this->j = 5;
		$this->k = 6;
		$this->soma1 = "";
		$this->soma2 = "";
		
		for ($this->i = 0; $this->i < 13; $this->i++)
		{
		$this->j = $this->j == 1 ? 9 : $this->j;
		$this->k = $this->k == 1 ? 9 : $this->k;
		$this->soma2 += ($cnpj{$this->i} * $this->k);
		
		if ($this->i < 12)
		{ 
		$this->soma1 += ($cnpj{$this->i} * $this->j); 
		} 
		$this->k--;
		$this->j--; 
		} 
		
		$this->digito1 = $this->soma1 % 11 < 2 ? 0 : 11 - $this->soma1 % 11;
		$this->digito2 = $this->soma2 % 11 < 2 ? 0 : 11 - $this->soma2 % 11; 
		return (($cnpj{12} == $this->digito1) and ($cnpj{13} == $this->digito2));
	}
	//---
	function cep($cep) {
		//	Máscara (xxxxx-xxx).
		if (!preg_match("/^([0-9]{2})\.?([0-9]{3})-?([0-9]{3})$/", $cep)) { 
			return false;
		} else {
			return true;
		}
	}
	
}