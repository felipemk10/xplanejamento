<?php

# Informa qual o conjunto de caracteres será usado.
header('Content-Type: text/html; charset=utf-8', true);

//	Iniciando sessões.
session_start();
//---

function __autoload($classe) {
	$diretorios = array('libs/');
    foreach ( $diretorios as $diretorio ) {
        if ( file_exists($diretorio.$classe.'.php') ) { 
        	include_once $diretorio.$classe.'.php';
        	break;
        }
    }
}