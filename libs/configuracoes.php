<?php
class configuracoes {
	//	Tempo de execusão de script zerado, assim não haverá o erro maximum 30 seconds
	function tempo_execucao($tempo) {
		return set_time_limit($tempo);
	}
	
	function imprimir_data() {
		return date("Y-m-d H:i:s");
	}
	
	//	Modo on-line ou off-line.
	function url_acesso() {
		if ( $_SERVER['SERVER_NAME'] == '13.58.238.67' ) {
			return 'http://13.58.238.67/xplanejamento/';
		} else {
			return 'http://localhost:8080/xplanejamento/';
		}
	}
	
	function url_atual() {
		$this->config_protocolo    = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false) ? 'http' : 'https';
		$this->config_host         = $_SERVER['HTTP_HOST'];
		$this->config_script       = $_SERVER['SCRIPT_NAME'];
		$this->config_parametros   = $_SERVER['QUERY_STRING'];
		return $this->config_urlatual     = $this->config_protocolo . '://' . $this->config_host . $this->config_script . '?' . $this->config_parametros;
	}
	function consulta($consulta) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
		
		$this->conexao = new conexao;
		$valor = $this->conexao->query($consulta);
		if (!$valor) {
	        print_r($this->conexao->errorInfo());
	    } else {
		    return $valor;
	    }
	}
}
