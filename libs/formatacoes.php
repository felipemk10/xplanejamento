<?php

// * @author Andre Lourenco Pedroso - alp.pedroso@gmail.com
// *
// * @date 15 de Janeiro de 2007
// *
 
/**
 *
 * - Manipulacao de Data ou Hora.
 *
 * 		Operacoes: soma DIA, MES ,ANO, HORA, MINUTOS, SEGUNDOS.
 *		Formatos :
 *			Data: 15/01/2007
 *			Hora: 10:35:00
 * 		Para subtrair, basta passar um valor negativo:
 * 		Ex:
 * 			$obj->somaDia(-10);
 *
 * - Calcula diferenca entre duas datas.
 *
 * 		Operacoes: difDataHora.
 *		Formatos :
 *			Data: 15/01/2007 10:35:00
 * 		E necessario passar duas datas como parametro e o tipo de retorno desejado:
 * 		Ex:
 * 			$obj->difDataHora($dataMenor,$dataMaior,"m");
 *
 */
class FuncDataHora
{
	private $data;
	private $hora;
 
	function FuncDataHora($data="",$hora="")
	{
		if($hora=="")
		{
			$hora = date("H:i:s");
		}
		if($data=="")
		{
			$data = date("Y-m-d");
		}
		else if ($this->validaData($data,"d"))
		{
			die ("Padrao de data ($data) invalido! - Padrao = 15/01/2007");
		}
		$this->data = explode("/",$data);
		$this->hora = explode(":",$hora);
	}
	private function validaData($data,$op)
	{
		switch($op)
		{
			case "d": // Padrao: 15/01/2007
				$er = "/(([0][1-9]|[1-2][0-9]|[3][0-1])-([0][1-9]|[1][0-2])-([0-9]{4}))/";
				if(preg_match($er,$data))
				{
					return 0;
				}
				else
				{
					return 1;
				}
				break;
			case "dh": // Padrao 15/01/2007 10:30:00
				$er = "/(([0][1-9]|[1-2][0-9]|[3][0-1])-([0][1-9]|[1][0-2])-([0-9]{4})*)/";
				if(preg_match($er,$data))
				{
					return 0;
				}
				else
				{
					return 1;
				}
				break;
		}
	}
	// DATA
	public function somaDia($dias=1)
	{
		$this->FuncDataHora(strftime("%Y-%m-%d", mktime(0, 0, 0, $this->data[1], $this->data[0]+$dias, $this->data[2])),"");
		return $this->data;
	}
	public function somaMes($meses=1)
	{
		$this->FuncDataHora(strftime("%Y-%m-%d", mktime(0, 0, 0, $this->data[1]+$meses, $this->data[0], $this->data[2])),"");
		return $this->data;
	}
	public function somaAno($anos=1)
	{
		$this->FuncDataHora(strftime("%Y-%m-%d", mktime (0, 0, 0, $this->data[1], $this->data[0], $this->data[2]+$anos)),"");
		return $this->data;
	}
	public function getData()
	{
		return $this->data[0]."-".$this->data[1]."-".$this->data[2];
	}
	// HORA
	public function somaSegundo($segundos=1)
	{
		$this->FuncDataHora("",strftime("%H:%M:%S",mktime($this->hora[0],$this->hora[1],$this->hora[2]+$segundos, 0, 0, 0)));
		return $this->hora;
	}
	public function somaMinuto($minutos=1)
	{
		$this->FuncDataHora("",strftime("%H:%M:%S",mktime($this->hora[0],$this->hora[1]+$minutos,$this->hora[2], 0, 0, 0)));
		return $this->hora;
	}
	public function somaHora($horas=1)
	{
		$this->FuncDataHora("",strftime("%H:%M:%S",mktime($this->hora[0]+$horas,$this->hora[1],$this->hora[2], 0, 0, 0)));
		return $this->hora;
	}
	public function getHora()
	{
		return $this->hora[0].":".$this->hora[1].":".$this->hora[2];
	}
 
 
	/**
	 *
	 * Retorna diferença entre as datas em Dias, Horas ou Minutos
	 * Function difDataHora(data menor, [data maior],[dias horas minutos segundos])
	 *
	 * Formato 04/05/2006 12:00:00
	 *
	 * Chame a funcao com o valor NULL como 'data maior' para 'data maior' = data atual.
	 *
	 * Formatacao do retorno [dias horas minutos segundos]:
	 *
	 * "s": Segundos
	 * "m": Minutos
	 * "H": Horas
	 * "h": Horas arredondada
	 * "D": Dias
	 * "d": Dias arredontados
	 *
	 * Original: Gambiarra.com.br Bozo@gambiarra.com.br
	 *
	 * Modificado: Andre Lourenco Pedroso alp.pedroso@gmail.com
	 * Data 15/01/2007 10:00
	 */
	public function difDataHora($datamenor,$datamaior="",$tipo="")
	{	
		if($this->validaData($datamenor,"dh"))
		{
			die ("data errada - $datamenor");
		}
		if($datamaior==""){
			echo $datamaior = date("Y-m-d H:i:s");
		}
		if($tipo==""){
			$tipo = "h";
		}
		list ($anomenor, $mesmenor,$diamenor, $horamenor, $minutomenor, $segundomenor) = preg_split("/[-: ]/",$datamenor);
		list ($anomaior, $mesmaior, $diamaior, $horamaior, $minutomaior, $segundomaior) = preg_split("/[-: ]/",$datamaior);
		return mktime($horamaior,$minutomaior,$segundomaior,$mesmaior,$diamaior, $anomaior).' = '.mktime($horamenor,$minutomenor,$segundomenor,$mesmenor,$diamenor, $anomenor);
		$segundos =	mktime($horamaior,$minutomaior,$segundomaior,$mesmaior,$diamaior, $anomaior)-mktime($horamenor,$minutomenor,$segundomenor,$mesmenor,$diamenor, $anomenor);
 		switch($tipo){
			case "s": // Segundo
				$diferenca = $segundos;
				break;
			case "M": // Minuto
				$diferenca = $segundos/60;
			case "m": // Minuto Arrendondado
				$diferenca = round($segundos/60);
				break;
			case "H": // Hora
				$diferenca = $segundos/3600;
				break;
			case "h": // Hora Arredondada
				$diferenca = round($segundos/3600);
				break;
			case "D": // Dia
				$diferenca = $segundos/86400;
				break;
			case "d": // Dia Arredondado
				$diferenca = round($segundos/86400);
				break;
		}
		return $diferenca;
	}
}

//	-----------

class formatacoes {
	function formatar_data($separacao,$data) {
		$this->dia = substr($data,8,2);
        $this->mes = substr($data,5,2);
        $this->ano = substr($data,0,4);
        return $this->dia.$separacao.$this->mes.$separacao.$this->ano;
	}
	function formatar_datahora($separacao,$data) {
		$this->dia = substr($data,8,2);
        $this->mes = substr($data,5,2);
        $this->ano = substr($data,0,4);

        $this->hora = substr($data,11,5);
        return $this->dia.$separacao.$this->mes.$separacao.$this->ano.' - '.$this->hora;
	}
	function retira_acentos($str, $enc = "UTF-8") {
		$this->acentos = array(
		'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
		'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
		'C' => '/&Ccedil;/',
		'c' => '/&ccedil;/',
		'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
		'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
		'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
		'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
		'N' => '/&Ntilde;/',
		'n' => '/&ntilde;/',
		'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
		'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
		'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
		'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
		'Y' => '/&Yacute;/',
		'y' => '/&yacute;|&yuml;/',
		'a.' => '/&ordf;/',
		'o.' => '/&ordm;/');
	
		return preg_replace($this->acentos, array_keys($this->acentos), htmlentities($str,ENT_NOQUOTES, $enc));
	}
	//	Retirar os simbolos da string.
	function retira_simbolos($string) {
		$this->string = trim($string);
	    $this->string = strtolower($this->string);
	
	    $this->string = str_replace( '-', '', $this->string);
	    $this->string = str_replace( '\\', '', $this->string);
	    $this->string = str_replace( '/', '', $this->string);
	    $this->string = str_replace( '´', '', $this->string);
	    $this->string = str_replace( '`', '', $this->string);
	    $this->string = str_replace( '~', '', $this->string);
	    $this->string = str_replace( '^', '', $this->string);
	    $this->string = str_replace( '¨', '', $this->string);
	    
	    $this->string = str_replace( '!', '', $this->string);
	    $this->string = str_replace( '@', '', $this->string);
	    $this->string = str_replace( '#', '', $this->string);
	    $this->string = str_replace( '$', '', $this->string);
	    $this->string = str_replace( '%', '', $this->string);
	    $this->string = str_replace( '¨', '', $this->string);
	    $this->string = str_replace( '*', '', $this->string);
	    $this->string = str_replace( '(', '', $this->string);
	    $this->string = str_replace( ')', '', $this->string);
	    $this->string = str_replace( '_', '', $this->string);
	    $this->string = str_replace( '+', '', $this->string);
	    $this->string = str_replace( '=', '', $this->string);
	    $this->string = str_replace( '{', '', $this->string);
	    $this->string = str_replace( '[', '', $this->string);
	    $this->string = str_replace( '}', '', $this->string);
	    $this->string = str_replace( ']', '', $this->string);
	    $this->string = str_replace( '|', '', $this->string);
	    $this->string = str_replace( '<', '', $this->string);
	    $this->string = str_replace( '>', '', $this->string);
	    $this->string = str_replace( '.', '', $this->string);
	    $this->string = str_replace( ':', '', $this->string);
	    $this->string = str_replace( ';', '', $this->string);
	    $this->string = str_replace( '?', '', $this->string);
	    $this->string = str_replace( '¹', '', $this->string);
	    $this->string = str_replace( '²', '', $this->string);
	    $this->string = str_replace( '³', '', $this->string);
	    $this->string = str_replace( '£', '', $this->string);
	    $this->string = str_replace( '¢', '', $this->string);
	    $this->string = str_replace( '¬', '', $this->string);
	    $this->string = str_replace( '§', '', $this->string);
	    $this->string = str_replace( 'ª', '', $this->string);
	    $this->string = str_replace( 'º', '', $this->string);
	    $this->string = str_replace( '"', '', $this->string);
	    $this->string = str_replace( "'", '', $this->string);
	    return $this->string;
	}
	
	//	Criptografia padrão.
	function criptografia($valor, $tipo, $acao){
		if ( $tipo == 'base64' ) {
			if ( $acao == 'encode' ) {
				return base64_encode(base64_encode(base64_encode('apisuporte'.$valor)));
			} else if ( $acao == 'decode' ) {
				return substr(base64_decode(base64_decode(base64_decode($valor))),10,11);
			}
		}
	}

	function numerodouble($modo,$numero) {
		if ( empty($numero) ) {
			echo 0;
		} else {
			if ( $modo == 'ajuste' ) {
				return str_replace( ',', '.', $numero);
			} else if ( $modo == 'imprimir' ) {
				return str_replace( '.', ',', $numero);
			}
		}
	}

	function mask($val, $mask) {
	 $maskared = '';
	 $k = 0;
	 for($i = 0; $i<=strlen($mask)-1; $i++)
	 {
	   if($mask[$i] == '#')
	   {
	      if(isset($val[$k]))
	       $maskared .= $val[$k++];
	   }
	   else
	 {
	     if(isset($mask[$i]))
	     $maskared .= $mask[$i];
	     }
	 }
	 return $maskared;
	}
}