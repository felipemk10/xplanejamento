<?php
class func_pavimentacao {
	function manipulacoes($id_pav, $id_alv, $tipo, $area, $formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
		//	tipo: pat = Pavimento Terreo, sbc = área sub solo a contruir, are = área Existente,  sbe = área sub solo existente, adk = área Deck, apa = área Piscina 
		$tipo = $this->validacoes->anti_injection($tipo);

		

		if ( $formulario != 'delete' and $tipo != 'pat' and 
			$formulario != 'delete' and $tipo != 'sbc' and 
			$formulario != 'delete' and $tipo != 'are' and 
			$formulario != 'delete' and $tipo != 'sbe' and 
			$formulario != 'delete' and $tipo != 'adk' and 
			$formulario != 'delete' and $tipo != 'apa' ) {
			return 'Tipo inválido';
		} else if ( $formulario != 'delete' and empty($area) ) {
			return 'Preencha a área do terreno';
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_pavimentacao (
	            		id_alv,	 
						area,	 
						tipo,
						datahora
	            	) 


	            	VALUES (?,?,?,now())");	
					$this->sql->bindValue(1, $id_alv);
					$this->sql->bindValue(2, $area);
					$this->sql->bindValue(3, $tipo);
					
					$this->sql->execute();

					return $this->conexao->lastInsertId();

					// Mostrar possíveis erros.
					print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos_pavimentacao
	            		SET 
		            		id_alv	 		=	$id_alv,
							area	 		=	$area,
							tipo	=	'".$tipo."'
							
	            		WHERE id_pav = ".$id_pav."");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($this->sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$this->sql = $this->conexao->prepare("DELETE FROM processos.processos_pavimentacao WHERE id_pav = $id_pav");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			}
		}
	}
}