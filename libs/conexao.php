<?php
/*
	Então fazendo uso desta classe toda vez que eu quiser fazer conexão com o banco eu uso o método implementado getInstance(), pois ele é quem vai me retornar uma conexão existente ou criá-la para mim.
	
	Veja que eu coloquei direto alí os valores, senhas e talls.. pra conectar certo. Então, porém tanto pra php quanto pra java - falando de web - o ideal é centralizar todas as configurações em um único arquivo, para o site. Em java se faz uso do arquivo de configuração web.xml, já no php este arquivo de configuração geral é mais conhecido como config.
	
	É nada mais nada menos que um arquivo com extensão .php que contem todas as configurações necessárias para o site.
*/

class conexao extends PDO { 
	private $dsn = 'pgsql:host=localhost;port=5432;dbname=xplanejamento';
	private $user = 'postgres';
	private $password = '1qRKRTpWtVc0';
	
	function __construct() {
		try {
			if ( $this->handle == null ) {
				$dbh = parent::__construct( $this->dsn , $this->user , $this->password);
				$this->handle = $dbh;
				return $this->handle;
			}
		} catch ( PDOException $e ) {
			echo 'Connection failed: ' . $e->getMessage( );
			return false;
		}
	}
		
	function __destruct() {
		$this->handle = NULL;
	}
}
