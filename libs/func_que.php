<?php
class func_que {
	function manipulacoes($id_que, $id_con, $areacontruir, $areaexistente, $areausoexclusivo, $areacomumproporcional, $tipoprocesso, $formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
		//	tipoprocesso: c = Construção, R = Regularização, A = Acréscimo de área
		$tipoprocesso = $this->validacoes->anti_injection($tipoprocesso);

		

		if ( $formulario != 'delete' and $tipoprocesso != 'c' and $tipoprocesso != 'r' and $tipoprocesso != 'a' ) {
			return 'Tipo processo inválido';
		} else if ( $formulario != 'delete' and $areacontruir < 0 ) {
			return 'Preencha a área a construir';
		} else if ( $formulario != 'delete' and $areaexistente < 0 ) {
			return 'Preencha a área existente';
		} else if ( $formulario != 'delete' and $areausoexclusivo < 0 ) {
			return 'Preencha a área de uso exclusivo';
		} else if ( $formulario != 'delete' and $areacomumproporcional < 0 ) {
			return 'Preencha a área comum proporcional';
		} else {
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_que (
	            		id_con,	 
						areacontruir,	
						areaexistente,	
						areausoexclusivo,	
						areacomumproporcional,	
						tipoprocesso
	            	) 


	            	VALUES (?,?,?,?,?,?)");	
					$this->sql->bindValue(1, $id_con);
					$this->sql->bindValue(2, $areacontruir);
					$this->sql->bindValue(3, $areaexistente);
					$this->sql->bindValue(4, $areausoexclusivo);
					$this->sql->bindValue(5, $areacomumproporcional);
					$this->sql->bindValue(6, $tipoprocesso);
					
					$this->sql->execute();

					//return $this->conexao->lastInsertId();

					// Mostrar possíveis erros.
					print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos_que
	            		SET 
		            		id_con 					= $id_con,	 
							areacontruir			= ".(double)$areacontruir.",
							areaexistente			= ".(double)$areaexistente.",
							areausoexclusivo		= ".(double)$areausoexclusivo.",
							areacomumproporcional	= ".(double)$areacomumproporcional.",
							tipoprocesso			= '".$tipoprocesso."'
							
	            		WHERE id_que = ".$id_que."");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$this->sql = $this->conexao->prepare("DELETE FROM processos.processos_que WHERE id_que = $id_que");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			}
		}
	}
}