<?php
class func_usuario_sis {
		 
	function manipulacoes(
		$id_cg, 
		$senha,	 
		$tipousuario,	 
		$departamento,	 
		$cargofuncao,
		$tipoprofissional,
		$formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;


			
		if ( $formulario == 'cadastro' ){
	    	// Cadastra processo
            $this->sql = $this->conexao->prepare("INSERT INTO geral.usuarios (
            		id_cg,	 
					senha,
					tipousuario,
					departamento,	 
					cargofuncao,
					tipoprofissional
            	) 


            	VALUES (?,?,?,?,?,?)");	
				$this->sql->bindValue(1, $id_cg);
				$this->sql->bindValue(2, md5($senha));
				$this->sql->bindValue(3, $tipousuario);
				$this->sql->bindValue(4, $departamento);
				$this->sql->bindValue(5, $cargofuncao);
				$this->sql->bindValue(6, $tipoprofissional);

				$this->sql->execute();

				return $this->conexao->lastInsertId();

				// Mostrar possíveis erros.
				//print_r($this->sql->errorInfo());

			//	Redirecionamento
			//header("Location: index.php");
		} else if ( $formulario == 'alteracao' ) {
			$this->sql = "UPDATE geral.usuarios SET";

			if ( !empty($senha) ) {
				$this->sql = $this->sql." senha = '".md5($senha)."',";
			}
			
			if ( !empty($tipoprofissional) ) {
				$vartipoprofissional = ',tipoprofissional = '.$tipoprofissional.' ';
			}
			$this->sql = $this->sql."
					departamento	= '".$departamento."',
					cargofuncao 	= '".$cargofuncao."'
					".$vartipoprofissional."

				WHERE 
					id_cg = $id_cg";
			
			$this->sql = $this->conexao->prepare($this->sql);

			$this->sql->execute();
			
			// Mostrar possíveis erros.
			print_r($this->sql->errorInfo());
		} else if ( $formulario == 'desativa' ) {
			$this->sql = $this->conexao->prepare("UPDATE geral.cg SET ativo = false WHERE id_cg = ".$id_cg."");
			
			$this->sql->execute();
			
			// Mostrar possíveis erros.
			//print_r($this->sql->errorInfo());
		}
	}
}
