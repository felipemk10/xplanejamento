<?php
class func_redimensionamento {
	function manipulacoes($id_red, $id_pro, $areatotalterreno, $sa_qtdlotes, $sp_qtdlotes, $descricaolotes, $requerente, $requerentetelefone, $tipo, $formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
		$descricaolotes = $this->validacoes->anti_injection($descricaolotes);
		$requerente = $this->validacoes->anti_injection($requerente);
		$requerentetelefone = $this->validacoes->anti_injection($requerentetelefone);


		if ( empty($sa_qtdlotes) ) {
			return 'Preencha a quantidade dos lotes - Situação Atual';
		} else if ( empty($sp_qtdlotes) ) {
			return 'Preencha a quantidade dos lotes - Situação Pretendida';
		} else if ( empty($descricaolotes) ) {
			return 'Digite a descrição dos lotes';
		} else if ( empty($requerente) ) {
			return 'Digite o nome do requerente';
		} else if ( empty($requerentetelefone) ) {
			return 'Preencha a área de uso exclusivo';
		} else if ( empty($areatotalterreno) ) {
			return 'Preencha a área comum proporcional';
		} else if ( $tipo < 1 and $tipo > 2 ) {
			return 'Tipo de Redimensionamento inválido';
		} else {
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_redimensionamento (
	            		id_pro,	 
						areatotalterreno,	
						requerente,	
						requerentetelefone,	
						descricaolotes,
						sa_qtdlotes,
						sp_qtdlotes,
						tipo,
						datahora
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,now())");	
					$this->sql->bindValue(1, $id_pro);
					$this->sql->bindValue(2, $areatotalterreno);
					$this->sql->bindValue(3, $requerente);
					$this->sql->bindValue(4, $requerentetelefone);
					$this->sql->bindValue(5, $descricaolotes);
					$this->sql->bindValue(6, $sa_qtdlotes);
					$this->sql->bindValue(7, $sp_qtdlotes);
					$this->sql->bindValue(8, $tipo);
					
					$this->sql->execute();

					//return $this->conexao->lastInsertId();

					// Mostrar possíveis erros.
					print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos_redimensionamento
	            		SET 
		            		id_pro 					= $id_pro,	 
							areatotalterreno		= $areatotalterreno,
							requerente				= '".$requerente."',
							requerentetelefone		= '".$requerentetelefone."',
							descricaolotes			= '".$descricaolotes."',
							sa_qtdlotes				= $sa_qtdlotes,
							sp_qtdlotes				= $sp_qtdlotes,
							tipo  					= $tipo
							
	            		WHERE id_red = ".$id_red."");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$this->sql = $this->conexao->prepare("DELETE FROM processos.processos_redimensionamento WHERE id_red = $id_red");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
		}
		}
	}
}