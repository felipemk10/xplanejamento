<?php
class func_alvara {
	function manipulacoes($id_alv,$id_pro,$id_cg, $finalidadeobra, $areaterreno, $situacaoterreno, $desmembramento, $desmembramento_entregue, $taxapermeabilidade, $formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
		//	Rodando anti-injection nas variáveis.
		
		// finalidadeobra: re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial 
		$finalidadeobra = $this->validacoes->anti_injection($finalidadeobra);
		// situacaoterreno: m = meio de quadra, e = esquina
		$situacaoterreno = $this->validacoes->anti_injection($situacaoterreno); 
		//---
		

		if ( $finalidadeobra != 're' and $finalidadeobra != 'co' and $finalidadeobra != 'mi' and $finalidadeobra != 'is' and $finalidadeobra != 'ga' and $finalidadeobra != 'id' ) {
			return 'Finalidade de Obra inválida';
		} else if ( $situacaoterreno != 'm' and $situacaoterreno != 'e' ) {
			return 'Situação do Terreno inválida';
		} else if ( isset($desmembramento) and empty($desmembramento_entregue) ) {
      	  	return 'Informe a opção de Entregue'; 
		} else if ( empty($areaterreno) ) {
			return 'Preencha a área do terreno';
		/*} else if ( empty($taxapermeabilidade) ) {
        	return 'Preencha a Taxa de permeabilidade';*/
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_alvara (
	            		id_pro,	 
						id_cg,	 
						areaterreno,	 
						situacaoterreno,	 
						desmembramento,
						desmembramento_entregue,
						finalidadeobra,
						taxapermeabilidade,
						datahora
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,now())");	
					$this->sql->bindValue(1, $id_pro);
					$this->sql->bindValue(2, $id_cg);
					$this->sql->bindValue(3, $areaterreno);
					$this->sql->bindValue(4, $situacaoterreno);
					$this->sql->bindValue(5, $desmembramento);
					$this->sql->bindValue(6, $desmembramento_entregue);
					$this->sql->bindValue(7, $finalidadeobra);
					$this->sql->bindValue(8, $taxapermeabilidade);
					
					$this->sql->execute();

					return $this->conexao->lastInsertId();

					// Mostrar possíveis erros.
					print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos_alvara
	            		SET 
		            		id_pro	 		=	$id_pro,
							id_cg	 		=	$id_cg,
							areaterreno	 	=	$areaterreno,
							situacaoterreno	=	'".$situacaoterreno."',
							desmembramento	=	'".$desmembramento."',
							desmembramento_entregue	=	'".$desmembramento_entregue."',
							finalidadeobra	=	'".$finalidadeobra."',
							taxapermeabilidade = $taxapermeabilidade

	            		WHERE id_alv = ".$id_alv."");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			}
		}
	}
}