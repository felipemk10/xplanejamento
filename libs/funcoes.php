<?php

class funcoes {
	function consulta($consulta) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
		
		$this->conexao = new conexao;
		return $this->conexao->query($consulta);
	}
}