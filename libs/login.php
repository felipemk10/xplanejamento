<?php
class login {
	function logar($email,$senha) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
		//	Rodando anti-injection nas variáveis.
		$email = $this->validacoes->anti_injection($email);
		$senha = $this->validacoes->anti_injection($senha);
		//---
		

		if ( empty($email) ) {
			return 'Digite seu e-mail.';
		} else if ( !$this->validacoes->email($email) ) {
			return 'Digite seu e-mail corretamente.';
		} else if ( empty($senha) ) {
			return 'Digite sua senha.';
		} else {
			//	Criptografando a variável.
			$senha = md5($senha);
			
			if ( (INT)$this->formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode') == 0 and !empty($email) and !empty($senha) ) {
				
				// consultando se o usuário enviou a senha correta
			    $this->sql_usuario = $this->conexao->query("SELECT cg.id_cg, cg.email, usuarios.senha FROM geral.cg
		        INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg and usuarios.ativo = true
		        WHERE cg.email = '".$email."' and usuarios.senha = '".$senha."' and usuarios.ativo = true and cg.tipo = 1");

			    //  Verifica se encontra o usuário administrador e grava as sessões correspondentes.
			    if ( $this->sql_usuario->rowCount() == 1 ) {	
			    	$this->linha_usu = $this->sql_usuario->fetch();
			        
			        $_SESSION['id_usuario'] = $this->formatacoes->criptografia($this->linha_usu['id_cg'],'base64','encode');
			        $_SESSION["sessiontime"] = time();
		            
		            
		            $http_client_ip       = $_SERVER['HTTP_CLIENT_IP'];
					$http_x_forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
					$remote_addr          = $_SERVER['REMOTE_ADDR'];
					 
					/* VERIFICO SE O IP REALMENTE EXISTE NA INTERNET */
					if(!empty($http_client_ip)){
					    $ip = $http_client_ip;
					    /* VERIFICO SE O ACESSO PARTIU DE UM SERVIDOR PROXY */
					} elseif(!empty($http_x_forwarded_for)){
					    $ip = $http_x_forwarded_for;
					} else {
					    /* CASO EU NÃO ENCONTRE NAS DUAS OUTRAS MANEIRAS, RECUPERO DA FORMA TRADICIONAL */
					    $ip = $remote_addr;
					}

		            $this->sql = $this->conexao->prepare("INSERT INTO logs.log_acesso (id_cg, ip, datahora) VALUES (?,?,now())");	
						$this->sql->bindValue(1, $this->linha_usu['id_cg']);
						$this->sql->bindValue(2, $ip);
						$this->sql->execute();

					
					//	Redirecionando para o sistema
					header("Location: listar_processos.php");
			    } else {
					return 'Poxa, e-mail e/ou senha está incorreto.<br />Tente desta vez diferente!';
				}
			} else {
				return 'Poxa, e-mail e/ou senha está incorreto.<br />Tente desta vez diferente!';
			}
		}
	}
}