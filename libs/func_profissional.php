<?php
class func_profissional {
	function manipulacoes($id_pro, $id_cg, $tipoprofissional, $tipo, $formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		
		//	Rodando anti-injection nas variáveis.
		
		//	e = engenheiro, a = arquiteto
		$tipoprofissional = $this->validacoes->anti_injection($tipoprofissional);
		//	a = autor, r = responsável técnico
		$tipo = $this->validacoes->anti_injection($tipo); 
		//---
		
		
		if ( $tipoprofissional != 'e' and $tipoprofissional != 'a' ) {
			return 'Tipo profissional inválido';
		} else if ( $tipo != 'a' and $tipo != 'r' ) {
			return 'Tipo inválido';
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos_profissional (
	            		id_pro,	
						id_cg,
						tipoprofissional,	
						tipo
	            	) 


	            	VALUES (?,?,?,?)");	
					$this->sql->bindValue(1, $id_pro);
					$this->sql->bindValue(2, $id_cg);
					$this->sql->bindValue(3, $tipoprofissional);
					$this->sql->bindValue(4, $tipo);
					
					$this->sql->execute();

					return $this->conexao->lastInsertId();

					// Mostrar possíveis erros.
					print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos_profissional
	            		SET 
		            		id_pro = $id_pro,	
							id_cg = $id_cg,
							tipoprofissional = '".$tipoprofissional."',	
							tipo = '".$tipo."'

	            		WHERE id_pro = ".$id_pro." and id_cg = ".$id_cg."");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());
			}
		}
	}
}