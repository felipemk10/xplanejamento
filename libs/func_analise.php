<?php
class func_analise {
	function manipulacoes(
			$id_ana,
			$id_cg,
			$id_pro,
			$tipo,
			$situacaoprojeto,
			$obsgerais,
			$descricao_img1,	 
			$descricao_img2,	 
			$descricao_img3,	 
			$descricao_img4,	 
			$descricao_img5,
			$valor1,	 
			$valor2,	 
			$valor3,	 
			$valor4,	 
			$valor5,	 
			$valor6,	 
			$valor7,	 
			$valor8,	 
			$valor9,	 
			$valor10,	 
			$valor11,	 
			$valor12,	 
			$valor13,	 
			$valor14,	 
			$valor15,	 
			$valor16,	 
			$valor17,	 
			$valor18,	 
			$valor19,	 
			$valor20,	 
			$valor21,	 
			$valor22,	 
			$valor23,	 
			$valor24,	 
			$valor25,	 
			$valor26,	 
			$valor27,	 
			$valor28,	 
			$valor29,	 
			$valor30,	 
			$valor31,	 
			$valor32,	 
			$valor33,	 
			$valor34,	 
			$valor35,	 
			$valor36,	 
			$valor37,	 
			$valor38,	 
			$valor39,	 
			$valor40,	 
			$valor41,	 
			$valor42,	 
			$valor43,	 
			$valor44,	 
			$valor45,	 
			$valor46,	 
			$valor47,	 
			$valor48,	 
			$valor49,	 
			$valor50,
			$formulario
		) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		$this->func_log_analise = new func_log_analise;
		
		//	Rodando anti-injection nas variáveis.
		
		// ca = cadastro, po = protocolo, fi = fiscalização, aa = análise alvará 
		$tipo = $this->validacoes->anti_injection($tipo);
		// sa = A ser analisado, ea = em análise, in = insuficiente - pendende de documentos, re = reprovado, aa = aprovado - encaminhado para elaboração alvará, ab = aprovado - boleto disponível, pe = Processo finalizado entregue 
		$status_processo = $this->validacoes->anti_injection($status_processo);
		
		$obsgerais = $this->validacoes->anti_injection($obsgerais);
		$descricao_img1 = $this->validacoes->anti_injection($descricao_img1);
		$descricao_img2 = $this->validacoes->anti_injection($descricao_img2);
		$descricao_img3 = $this->validacoes->anti_injection($descricao_img3);
		$descricao_img4 = $this->validacoes->anti_injection($descricao_img4);
		$descricao_img5 = $this->validacoes->anti_injection($descricao_img5);
		//---
		
		

		if ( $tipo != 'ca' and $tipo != 'ch' and $tipo != 'po' and $tipo != 'fi' and $tipo != 'aa'  ) {
			return 'Análise inválida';
		} else if ( $situacaoprojeto < 1 and $situacaoprojeto > 12  ) {
			return 'Status inválido';
		} else {
			if ( $formulario == 'cadastro' ){
	    		// Cadastra análise
				$this->sql = $this->conexao->prepare("INSERT INTO processos.processos_analise (
            		id_pro,
            		tipo,
            		obsgerais,
            		id_cg,
					situacaoprojeto,

					descricao_img1,
					descricao_img2,
					descricao_img3,
					descricao_img4,
					descricao_img5,

            		valor1,	 
					valor2,	 
					valor3,	 
					valor4,	 
					valor5,	 
					valor6,	 
					valor7,	 
					valor8,	 
					valor9,	 
					valor10,	 
					valor11,	 
					valor12,	 
					valor13,	 
					valor14,	 
					valor15,	 
					valor16,	 
					valor17,	 
					valor18,	 
					valor19,	 
					valor20,	 
					valor21,	 
					valor22,	 
					valor23,	 
					valor24,	 
					valor25,	 
					valor26,	 
					valor27,	 
					valor28,	 
					valor29,	 
					valor30,	 
					valor31,	 
					valor32,	 
					valor33,	 
					valor34,	 
					valor35,	 
					valor36,	 
					valor37,	 
					valor38,	 
					valor39,	 
					valor40,	 
					valor41,	 
					valor42,	 
					valor43,	 
					valor44,	 
					valor45,	 
					valor46,	 
					valor47,	 
					valor48,	 
					valor49,	 
					valor50,
					
            		datahora

            	)

            	VALUES (
            	?,?,?,

            	?,?,

            	?,?,?,?,?,

            	?,?,?,?,?,?,?,?,?,?,
            	?,?,?,?,?,?,?,?,?,?,
            	?,?,?,?,?,?,?,?,?,?,
            	?,?,?,?,?,?,?,?,?,?,
            	?,?,?,?,?,?,?,?,?,?,
            	
            	now())");

				$this->sql->bindValue(1,$id_pro);
				$this->sql->bindValue(2,$tipo);
				$this->sql->bindValue(3,$obsgerais);

				$this->sql->bindValue(4,$id_cg);
				$this->sql->bindValue(5,$situacaoprojeto);


				$this->sql->bindValue(6,$descricao_img1);	 
				$this->sql->bindValue(7,$descricao_img2); 
				$this->sql->bindValue(8,$descricao_img3); 
				$this->sql->bindValue(9,$descricao_img4);	 
				$this->sql->bindValue(10,$descricao_img5);

				$this->sql->bindValue(11,$valor1);	 
				$this->sql->bindValue(12,$valor2);	 
				$this->sql->bindValue(13,$valor3);	 
				$this->sql->bindValue(14,$valor4);	 
				$this->sql->bindValue(15,$valor5);	 
				$this->sql->bindValue(16,$valor6);	 
				$this->sql->bindValue(17,$valor7);	 
				$this->sql->bindValue(18,$valor8);	 
				$this->sql->bindValue(19,$valor9);	 
				$this->sql->bindValue(20,$valor10);	 
				$this->sql->bindValue(21,$valor11);	 
				$this->sql->bindValue(22,$valor12);	 
				$this->sql->bindValue(23,$valor13);	 
				$this->sql->bindValue(24,$valor14);	 
				$this->sql->bindValue(25,$valor15);	 
				$this->sql->bindValue(26,$valor16);	 
				$this->sql->bindValue(27,$valor17);	 
				$this->sql->bindValue(28,$valor18);	 
				$this->sql->bindValue(29,$valor19);	 
				$this->sql->bindValue(30,$valor20);	 
				$this->sql->bindValue(31,$valor21);	 
				$this->sql->bindValue(32,$valor22);	 
				$this->sql->bindValue(33,$valor23);	 
				$this->sql->bindValue(34,$valor24);	 
				$this->sql->bindValue(35,$valor25);	 
				$this->sql->bindValue(36,$valor26);	 
				$this->sql->bindValue(37,$valor27);	 
				$this->sql->bindValue(38,$valor28);	 
				$this->sql->bindValue(39,$valor29);	 
				$this->sql->bindValue(40,$valor30);	 
				$this->sql->bindValue(41,$valor31);	 
				$this->sql->bindValue(42,$valor32);	 
				$this->sql->bindValue(43,$valor33);	 
				$this->sql->bindValue(44,$valor34);	 
				$this->sql->bindValue(45,$valor35);	 
				$this->sql->bindValue(46,$valor36);	 
				$this->sql->bindValue(47,$valor37);	 
				$this->sql->bindValue(48,$valor38);	 
				$this->sql->bindValue(49,$valor39);	 
				$this->sql->bindValue(50,$valor40);	 
				$this->sql->bindValue(51,$valor41);	 
				$this->sql->bindValue(52,$valor42);	 
				$this->sql->bindValue(53,$valor43);	 
				$this->sql->bindValue(54,$valor44);	 
				$this->sql->bindValue(55,$valor45);	 
				$this->sql->bindValue(56,$valor46);	 
				$this->sql->bindValue(57,$valor47);	 
				$this->sql->bindValue(58,$valor48);	 
				$this->sql->bindValue(59,$valor49);	 
				$this->sql->bindValue(60,$valor50);
				

				$this->sql->execute();
				
				$id_ana = $this->conexao->lastInsertId();

				//	Impede que a situação exclusiva da parte fiscal altere as informações do processo.
				if ( $situacaoprojeto == 12 ) {
					$situacaoprojeto = 2;
        		}
        		
        		$this->sql = $this->conexao->prepare("UPDATE processos.processos
                  SET 
                  situacaoprojeto = '".$situacaoprojeto."'
                  
                  WHERE id_pro = ".$id_pro."");

        		$this->sql->execute();

				// Mostrar possíveis erros.
				print_r($this->sql->errorInfo());

				//	Registrando log da análise
				$this->func_log_analise->manipulacoes(
					$id_ana,
					$id_cg,
					$id_pro,
					$tipo,
					$situacaoprojeto,
					$obsgerais,
					$descricao_img1,	 
					$descricao_img2,	 
					$descricao_img3,	 
					$descricao_img4,	 
					$descricao_img5,
					$valor1,	 
					$valor2,	 
					$valor3,	 
					$valor4,	 
					$valor5,	 
					$valor6,	 
					$valor7,	 
					$valor8,	 
					$valor9,	 
					$valor10,	 
					$valor11,	 
					$valor12,	 
					$valor13,	 
					$valor14,	 
					$valor15,	 
					$valor16,	 
					$valor17,	 
					$valor18,	 
					$valor19,	 
					$valor20,	 
					$valor21,	 
					$valor22,	 
					$valor23,	 
					$valor24,	 
					$valor25,	 
					$valor26,	 
					$valor27,	 
					$valor28,	 
					$valor29,	 
					$valor30,	 
					$valor31,	 
					$valor32,	 
					$valor33,	 
					$valor34,	 
					$valor35,	 
					$valor36,	 
					$valor37,	 
					$valor38,	 
					$valor39,	 
					$valor40,	 
					$valor41,	 
					$valor42,	 
					$valor43,	 
					$valor44,	 
					$valor45,	 
					$valor46,	 
					$valor47,	 
					$valor48,	 
					$valor49,	 
					$valor50);
				//---
				
				return $id_ana;

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos_analise
            		SET 
            		id_cg = $id_cg,
					tipo = '".$tipo."',
					situacaoprojeto = $situacaoprojeto,
					obsgerais = '".$obsgerais."',
					descricao_img1 = '".$descricao_img1."',
					descricao_img2 = '".$descricao_img2."', 
					descricao_img3 = '".$descricao_img3."',
					descricao_img4 = '".$descricao_img4."', 
					descricao_img5 = '".$descricao_img5."', 
					valor1 = '".$valor1."',	 
					valor2 = '".$valor2."',	 
					valor3 = '".$valor3."',	 
					valor4 = '".$valor4."',	 
					valor5 = '".$valor5."',	 
					valor6 = '".$valor6."',	 
					valor7 = '".$valor7."',	 
					valor8 = '".$valor8."',	 
					valor9 = '".$valor9."',	 
					valor10 = '".$valor10."',	 
					valor11 = '".$valor11."',	 
					valor12 = '".$valor12."',	 
					valor13 = '".$valor13."',	 
					valor14 = '".$valor14."',	 
					valor15 = '".$valor15."',	 
					valor16 = '".$valor16."',	 
					valor17 = '".$valor17."',	 
					valor18 = '".$valor18."',	 
					valor19 = '".$valor19."',	 
					valor20 = '".$valor20."',	 
					valor21 = '".$valor21."',	 
					valor22 = '".$valor22."',	 
					valor23 = '".$valor23."',	 
					valor24 = '".$valor24."',	 
					valor25 = '".$valor25."',	 
					valor26 = '".$valor26."',	 
					valor27 = '".$valor27."',	 
					valor28 = '".$valor28."',	 
					valor29 = '".$valor29."',	 
					valor30 = '".$valor30."',	 
					valor31 = '".$valor31."',	 
					valor32 = '".$valor32."',	 
					valor33 = '".$valor33."',	 
					valor34 = '".$valor34."',	 
					valor35 = '".$valor35."',	 
					valor36 = '".$valor36."',	 
					valor37 = '".$valor37."',	 
					valor38 = '".$valor38."',	 
					valor39 = '".$valor39."',	 
					valor40 = '".$valor40."',	 
					valor41 = '".$valor41."',	 
					valor42 = '".$valor42."',	 
					valor43 = '".$valor43."',	 
					valor44 = '".$valor44."',	 
					valor45 = '".$valor45."',	 
					valor46 = '".$valor46."',	 
					valor47 = '".$valor47."',	 
					valor48 = '".$valor48."',
					valor49 = '".$valor49."',
					valor50 = '".$valor50."',
					datahora = now()

					WHERE id_ana = $id_ana");
				$this->sql->execute();
				
				//	Impede que a situação exclusiva da parte fiscal altere as informações do processo.
				if ( $situacaoprojeto == 12 ) {
					$situacaoprojeto = 2;
        		}
				$this->sql = $this->conexao->prepare("UPDATE processos.processos
                  SET 
                  situacaoprojeto = '".$situacaoprojeto."'
                  
                  WHERE id_pro = ".$id_pro."");

        		$this->sql->execute();

				//	Registrando log da análise
				$this->func_log_analise->manipulacoes(
					$id_ana,
					$id_cg,
					$id_pro,
					$tipo,
					$situacaoprojeto,
					$obsgerais,
					$descricao_img1,	 
					$descricao_img2,	 
					$descricao_img3,	 
					$descricao_img4,	 
					$descricao_img5,
					$valor1,	 
					$valor2,	 
					$valor3,	 
					$valor4,	 
					$valor5,	 
					$valor6,	 
					$valor7,	 
					$valor8,	 
					$valor9,	 
					$valor10,	 
					$valor11,	 
					$valor12,	 
					$valor13,	 
					$valor14,	 
					$valor15,	 
					$valor16,	 
					$valor17,	 
					$valor18,	 
					$valor19,	 
					$valor20,	 
					$valor21,	 
					$valor22,	 
					$valor23,	 
					$valor24,	 
					$valor25,	 
					$valor26,	 
					$valor27,	 
					$valor28,	 
					$valor29,	 
					$valor30,	 
					$valor31,	 
					$valor32,	 
					$valor33,	 
					$valor34,	 
					$valor35,	 
					$valor36,	 
					$valor37,	 
					$valor38,	 
					$valor39,	 
					$valor40,	 
					$valor41,	 
					$valor42,	 
					$valor43,	 
					$valor44,	 
					$valor45,	 
					$valor46,	 
					$valor47,	 
					$valor48,	 
					$valor49,	 
					$valor50);
				//---
			}
		}
	}
}