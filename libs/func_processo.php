<?php
class func_processo {
	function manipulacoes($id_pro,$tipoprocesso,$situacaoprojeto,$endereco,$bairro,$quadra,$lote,$numero,$cidade,$estado,$formulario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once('libs/autoload.php');
	
		//	Iniciando classses.
		$this->conexao = new conexao;
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->configuracoes = new configuracoes;
		

		
		// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
		$tipoprocesso = (int)$tipoprocesso;
		

		$endereco 	= $this->validacoes->anti_injection($endereco);
		$bairro 	= $this->validacoes->anti_injection($bairro);
		$quadra 	= $this->validacoes->anti_injection($quadra);
		$lote 		= $this->validacoes->anti_injection($lote);
		$numero 	= (int)$numero;
		$cidade 	= $this->validacoes->anti_injection($cidade);
		$estado 	= $this->validacoes->anti_injection($estado);
		//---
		
		/* Legenda 'Situação Projeto'
		  1 - A ser analisado,
		  2 - Em análise,
		  3 - Pendência de documentos ou correção, 
		  
		  4 - Processo não permitido ou reprovado na análise/vistoria, 
		  5 - Processo encaminhado à procuradoria - Dúvida na vistoria,
		  6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria, 
		  
		  7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto,
		  8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga, 
		  9 - Aprovado - Decreto assinado - para entregar,
		  10 - Aprovado - Processo finalizado e decreto entregue, 

		  11 - Aprovado
		*/

		if ( $tipoprocesso < 1 or $tipoprocesso > 5 ) {
			return 'Processo inválido.';
		} else if ( $situacaoprojeto < 1 and $situacaoprojeto > 11 ) {
			return 'Situação do projeto inválido';
		} else if ( empty($endereco) ) {
			return 'Preencha o endereço';
		} else if ( $numero == 0 and empty($quadra) and empty($lote) ) {
			return 'Preencha quadra/lote ou o número do local';
		} else if ( $numero == 0 and empty($quadra) ) {
			return 'Preencha a quadra';
		} else if ( $numero == 0 and empty($lote) ) {
			return 'Preencha a lote';
		} else if ( empty($bairro) ) {
			return 'Preencha o bairro';
		} else if ( empty($estado) ) {
			return 'Escolha o estado';
		} else if ( empty($cidade) ) {
			return 'Escolha a cidade';
		} else {
			
			$this->sql_usuario = $this->conexao->query("SELECT cg.id_cg FROM geral.cg WHERE cg.id_cg = ".$this->formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode')." and cg.ativo = true");

	    	$this->linha_usu = $this->sql_usuario->fetch();

		    if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = $this->conexao->prepare("INSERT INTO processos.processos (
	            		tipoprocesso, 
	            		situacaoprojeto, 
	            		endereco,
	            		bairro,
	            		quadra,
	            		lote,
	            		numero,
	            		cidade,
	            		estado,
	            		id_cg,
	            		datahora,
	            		ativo
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,?,?,now(),true)");	
					$this->sql->bindValue(1, $tipoprocesso);
					$this->sql->bindValue(2, $situacaoprojeto);
					$this->sql->bindValue(3, $endereco);
					$this->sql->bindValue(4, $bairro);
					$this->sql->bindValue(5, $quadra);
					$this->sql->bindValue(6, $lote);
					$this->sql->bindValue(7, $numero);
					$this->sql->bindValue(8, $cidade);
					$this->sql->bindValue(9, $estado);
					$this->sql->bindValue(10, $this->linha_usu['id_cg']);
					
					$this->sql->execute();

					$this->id_pro = $this->conexao->lastInsertId();

					// Cadastra análise
					$this->sql = $this->conexao->prepare("INSERT INTO processos.processos_analise (
	            		id_pro,
	            		id_cg,
	            		tipo,
	            		situacaoprojeto,
	            		datahora
	            	)

	            	VALUES (?,?,?,?,now())");	
					$this->sql->bindValue(1, $this->id_pro);
					$this->sql->bindValue(2, $this->formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'));
					$this->sql->bindValue(3, 'ca');
					$this->sql->bindValue(4, 1);
					
					$this->sql->execute();
					
					return $this->id_pro;

					// Mostrar possíveis erros.
					print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = $this->conexao->prepare("UPDATE processos.processos
	            		SET 
	            		tipoprocesso = ".$tipoprocesso.", 
	            		endereco = '".$endereco."',
	            		bairro = '".$bairro."',
	            		quadra = '".$quadra."',
	            		lote = '".$lote."',
	            		numero = ".$numero.",
	            		cidade = ".$cidade.",
	            		estado = '".$estado."',
	            		id_cg = ".$this->formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode')."

	            		WHERE id_pro = ".$id_pro."");
				$this->sql->execute();
				
				return $id_pro;

				// Mostrar possíveis erros.
				//print_r($this->sql->errorInfo());
			}
		}
	}
}