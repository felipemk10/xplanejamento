<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$funcoes = new funcoes;
$login = new login;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));
//---

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
  if ( $_POST['formulario'] == 'login' ) {
    //  Rodando anti-injecgtion nas variáveis.
    $_POST['loginemail'] = $validacoes->anti_injection($_POST['loginemail']);
    $_POST['loginsenha'] = $validacoes->anti_injection($_POST['loginsenha']);
    //---
    
    $msg_formulario = $login->logar($_POST['loginemail'],$_POST['loginsenha']);
  }
}
?>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
    <meta name="description" content="Sistema de Planejamento">
    <meta name="author" content="TemplateMonster">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  </head>

<body class="admin-validation-page sb-l-o sb-r-c onload-check sb-l-m sb-l-disable-animation" data-spy="scroll" data-target="#nav-spy" data-offset="200" style="min-height: 94px;">


  <!-- Start: Main -->
  <div id="main">

    
      
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout">

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="height: 294px;">
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border panel-system" style="border: 0;">

                <form method="post" action="/" id="admin-form" novalidate="novalidate">

                  <div class="panel-body bg-light alinha-conteudo">
                    <!-- .section-divider -->
                    <div class="section row">
                      <div class="col-md-3">
                        <img class="logo-prefeitura" src="img/logo_prefeitura_home.png" height="150">
                      </div>
                      <div class="col-md-9 top-titulo"><img src="img/logo-planejamento.png"></div>
                    </div>
                    <div class="section row  menu">
                      <?php include_once('includes/menuindex.php'); ?>
                    </div>
                    <div class="section row " style="border: 1px solid margin-left">
                      <div class="col-md-12">
                        <div class=" section row">
                          <div class="row section">
                            <div class="col-md-4 titulo-info">Arquivos informativos</div>
                          </div>
                          <div class="row section">
                            <div class="col-md-12 ">
                              <table class="fonte-texto">
                                <tr>
                                  <td><strong style="font-size:14px;">Lei Ciclovia</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/lei_ciclovia.pdf">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Lei Calçadas</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/lei_calcadas.pdf">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Lei Iluminação Led</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/lei_iluminacao_led.pdf">Baixar
                                    </a></button>
                                  </td>
                                </tr>
                              </table>
                              <br /><br />
                              <table>
                                <tr>
                                  <td><strong style="font-size:14px;">Resolução n°21.2015(MEIO AMBIENTE)</strong></td>
                                  <td colspan="4" class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/Resoluçãon21.2015-MEIO-AMBIENTE.pdf">Baixar
                                    </a></button>
                                  </td>
                                </tr>
                              </table>
                              <br /><br />
                              <table class="fonte-texto">
                                <tr>
                                  <td><strong style="font-size:14px;">Carimbo Padrão</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/carimbo_padrao.dwg">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Código de Obras</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/LEI68-01codigodeobrascamara.pdf">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Plano Diretor Urbano</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/PLANODIRETORLEI791-2017.rar">Baixar
                                    </a></button>
                                  </td>
                                </tr>
                              </table>
                             
                            </div><!-- Col-md-12 -->
                          </div><!-- Section row -->

                          <hr />

                          <p class="titulo-info">Requerimentos</p> 
                          <p>Nesta sessão você encontra uma série de requerimentos disponíveis para download.</p>
                          <hr />
                          <div class="row section">
                            <div class="col-md-4 titulo-info">Pessoa Física</div>
                          </div>
                          <div class="row section">
                            <div class="col-md-12 ">
                              <table class="fonte-texto">
                                <tr>
                                  <td><strong style="font-size:14px;">Desmembramento</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/desmembramento_pf.doc">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Remebramento e Desmenbramento</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/remembramento_desmembramento_pf.doc">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Remembramento</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/remembramento_pf.doc">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Habite-se</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/habitese_pf.doc">Baixar
                                    </a></button>
                                  </td>
                                </tr>
                              </table>
                            </div><!-- Col-md-12 -->
                          </div><!-- Section row -->
                          <div class="section row titulo-info">
                            <div class="col-md-12">Pessoa Jurídica</div>
                          </div>
                          <div class="section row">
                            <div class="col-md-12">
                              <table class="fonte-texto">
                                <tr>
                                  <td><strong style="font-size:14px;">Desmembramento</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/desmembramento_pj.doc">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Remebramento e Desmenbramento</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;" target="blank" href="docs/10.2018/remembramento_desmembramento_pj.doc">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Remembramento</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;"target="blank" href="docs/10.2018/remembramento_pj.doc">Baixar
                                    </a></button>
                                  </td>
                                  <td class="espacamento-esquerdo"><strong style="font-size:14px;">Habite-se</strong></td>
                                  <td class="espacamento-esquerdo">
                                    <button type="button" class="btn btn-xs btn-success"><a style="padding:20px;color:white;"target="blank" href="docs/10.2018/habitese_pj.doc">Baixar
                                    </a></button>
                                  </td>
                                </tr>
                              </table>
                          </div>


                    </div><!-- conteudo-->
                  </div>
                    <!-- end .section row section -->

          </div>
          <?php include('includes/rodapeindex.php'); ?>
        </div>
        <!-- end: .admin-form -->

      </section>
      <!-- End: Content -->
      
    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

 <style>
 .logo-prefeitura {
    padding-top: 45px;
    padding-left:90px;
  }
  .top-titulo {
    font-size: 28px;
  font-weight: 600;
  margin-top: 32px
  }
  .fonte-texto {
    font-size: 13px;
  }

  .espacamento-esquerdo {
    padding-left: 10px;
  }
  .logo-legenda {
    font-size: 15px;
  }

    .menu-login {
      font-size: 11px;
      margin-top: 5px; 
    }
    .profissional {
      padding: 22px;



    }
   .col-md-menu {
    padding: 5px;
   }
   div.main {
    background-color: rgba(211,211,211, 0.5)
   }
   .menu-botao{
    border-left: 1px solid #A9A9A9; 
    margin-left: -30px;
    padding-left: 10px;
   }
   .menu-botao-inf {
    border-left: 1px solid #A9A9A9; 

   }

   .menu {
    border: 1px solid rgba(169,169,169, 1.5); 
    border-radius: 3px;
    padding: 20px;
    font-size: 14px;
    color: black;
    background-color: rgba(211,211,211, 0.2)
   }
   .alinha-conteudo{
    margin-top: 40px;


  }
  .texto-info {
    text-align: justify;
    font-size: 13px;
    margin-top: 15px;
    color: #545353;
  }
  .titulo-info {
    font-size: 25px;
    font-weight: bold;
    color: #545353;
  }

  
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->




</body></html>