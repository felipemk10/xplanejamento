<?php
	//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
	require_once('libs/autoload.php');
	
	//	Iniciando classes.
	$formatacoes = new formatacoes;
	//---
	
	$id_usuario = (INT)$formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode');

	if ( $id_usuario > 0 ) {
		//	Apagando as sessões.
		session_unset();
		header("Location: index.php");
	}