<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

$id_pro = (int)$_GET['id_pro'];
$id_ah = (int)$_GET['id_ah'];

// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

if ( $id_pro > 0 ) {
    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();

      $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
        id_pro,
        --  1 = Alvará, 2 = Habite-se, 3 Alvará Regularização
        tipo,
        numero,   
        id_cg,
        id_que,
        ano,
        substituir,
        datahora

        FROM 

        processos.processos_alvarahabitese 

        WHERE processos_alvarahabitese.id_ah = $id_ah");
      
      
      $linha_alvarahabitese = $consulta_alvarahabitese->fetch();
}
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
    <meta name="author" content="TemplateMonster">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  </head>
  <body class="impressao">
    <br>
      <div class="section row">
        <div class="col-xs-offset-8 col-xs-2 print-no-view">
          <button type="button" onclick="window.print();">Imprimir</button>
        </div>
      </div>
      <div class="section row">
        <div class="col-xs-offset-2 col-xs-8 titulo">ALVARÁ DE LICENÇA PARA <?php
          if ( $linha2['tipoprocesso'] == 1 ) {
            echo 'CONSTRUÇÃO';
          } else if ( $linha2['tipoprocesso'] == 2 ) {
            echo 'REGULARIZAÇÃO DE OBRAS';
          } else if ( $linha2['tipoprocesso'] == 3 ) {
            echo 'ACRÉSCIMO DE ÁREAS';
          } else if ( $linha2['tipoprocesso'] == 4 ) {
            $con_que = $configuracoes->consulta("SELECT 
                  id_que,   
                  areacontruir,   
                  areaexistente,  
                  areausoexclusivo,   
                  areacomumproporcional,  
                  tipoprocesso

                  FROM 
                  processos.processos_que

                  WHERE processos_que.id_que = ".$linha_alvarahabitese['id_que']."");
            $linha_que = $con_que->fetch();

            if ( $linha_que['tipoprocesso'] == 'c' ) {
              echo 'CONSTRUÇÃO';
            } else if ( $linha_que['tipoprocesso'] == 'r' ) {
              echo 'REGULARIZAÇÃO';
            } else if ( $linha_que['tipoprocesso'] == 'a' ) {
              echo 'ACRÉSCIMO DE ÁREAS';
            }
          } else if ( $linha2['tipoprocesso'] == 5 ) {
            echo 'REDIMENSIONAMENTO';
          }
        ?></div>
      </div>
      <div class="section row">
        <div class="c col-xs-2 text-right subtitulo">Número:</div>
        <div class="col-xs-3 text-left campo-texto"><h4><?php if ( $linha2['tipoprocesso'] == 2 or $linha_que['tipoprocesso'] == 'r' ) { echo 'R.'; } echo $linha_alvarahabitese['numero'].' / '.$linha_alvarahabitese['ano']; ?></h4></div>
      </div>
        <?php 
          if ( (int)$linha_alvarahabitese['substituir'] > 0 ) {

            $substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$linha_alvarahabitese['substituir']." and tipo = ".$linha_alvarahabitese['tipo']."  ORDER BY id_ah DESC")->fetch(); ?>
          <br />
          <div class="section row">
            <div class="col-xs-offset-1 col-xs-6 subtitulo ">Este documento substitui o alvará existente de número</div>
          </div>
          <div class="section row">
            <div class="c col-xs-2 text-right subtitulo" style="font-size: 11px;">Número:</div>
            <div class="col-xs-3 text-left campo-texto" style="font-size: 11px;"><strong><?php if ( $linha2['tipoprocesso'] == 2 ) { echo 'R.'; } echo $linha_alvarahabitese['substituir'].' / '.$substituto['ano']; ?></strong></div>
            <br>
          </div>
          <br />
        <?php } ?>
        
      <div class="c col-xs-2 text-right subtitulo">Data de emissão:</div>
      <div class="col-xs-3 text-left campo-texto"><h4><?php echo $formatacoes->formatar_data('/',$linha_alvarahabitese['datahora']); ?></h4></div>
        
      <br>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-3 subtitulo "></div>
      </div>
      </div>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-3 subtitulo ">Dados do proprietário</div>
      </div>
      
      <div class="section row">
      <?php 
      //  Listando informações sobre os proprietários.
      $con_listagem_proprietario = $configuracoes->consulta("SELECT 
        cg.nome,
        cg.cpfcnpj

        FROM 
        processos.processos_proprietario

        INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

        WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

        foreach ( $con_listagem_proprietario as $listagem_proprietario ) {
      ?>
      <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Nome:</div>
      <div class="col-xs-6 text-left campo-texto"><?php echo strtoupper($listagem_proprietario['nome']); ?></div>   
      <div class="col-xs-1 text-right subtitulo"><?php 
        if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
          echo 'CPF';
        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
          echo 'CNPJ';
        }
        ?>:</div>
      <div class="col-xs-2 text-left campo-texto"><?php 

        if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
          echo $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'###.###.###-##');
        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
          echo $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'##.###.###/####-##');
        }

        ?></div>   
      <?php } ?>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Dados do projeto</div>
        <br>
      </div>
      <div class="section row">
      <?php 
        //  Listando informações sobre autor.
        $con_listagem_autor = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

        foreach ( $con_listagem_autor as $listagem_autor ) { ?>
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Autoria:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo strtoupper($listagem_autor['nome']); ?></div>
        <div class="col-xs-1 text-right subtitulo"><?php if ( $listagem_autor['tipoprofissional'] == 'e' ) { echo 'CREA'; } else if ( $listagem_autor['tipoprofissional'] == 'a' ) { echo 'CAU'; } ?></div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_autor['creacau']; ?></div> 

      <?php } ?>
      </div>
      <div class="section row">
      <?php 
        //  Listando informações sobre autor.
        $con_listagem_resptecnico = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");

        foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) { ?>
        <div class="col-xs-2 text-right subtitulo">Resposável Técnico:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo strtoupper($listagem_resptecnico['nome']); ?></div>
        <div class="col-xs-1 text-right subtitulo"><?php if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { echo 'CREA'; } else if ( $listagem_resptecnico['tipoprofissional'] == 'a' ) { echo 'CAU'; } ?></div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_resptecnico['creacau']; ?></div>  
      <?php } ?>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Endereço:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo $linha2['endereco']; ?></div>
        <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
        <div class="col-xs-1 text-right subtitulo">Quadra:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha2['quadra']; ?></div>
        <?php } ?>
      </div>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Bairro:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo $linha2['bairro']; ?></div>
        <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
        <div class="col-xs-1 text-right subtitulo">Lote:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha2['lote']; ?></div>
        <?php } ?>
      </div>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Cidade:</div>
        <div class="col-xs-6 text-left campo-texto">Luís Eduardo Magalhães - BA</div>
        <?php if ( $linha2['numero'] > 0 ) { ?>
        <div class="col-xs-1 text-right subtitulo">Número:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha2['numero']; ?></div>
        <?php } ?>
      </div>
      <div class="section row">
      <?php 
        if ( $tipoprocesso >= 1 and $tipoprocesso <= 3 ) { 
          include_once('includes/impressao/info_alvara.php');
        } else if ( $tipoprocesso == 4 ) { 
          include_once('includes/impressao/info_condominio.php');
        }
      ?>
      <br />
      <div class="section row">
        <div class="col-md-offset-2 col-md-6 nota">
          Após a análise dos elementos apresentados e estando os mesmos de acordo com a legislação pertinente em vigor, a Prefeitura Munícipal de Luís Eduardo Magalhães, considera como APROVADO este processo de Solicitação de Alvará.          
        </div>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <div class="assinatura col-xs-offset-3 col-xs-6 text-center"></div>

      <div class="section row text-center subtitulo">
        Secretaria de Planejamento, Orçamento e Gestão
      </div>
  </body>
    <style>
   @media print {
    .campo-texto {
      font-size: 10px!important;
      font-weight: normal !important;
    }
    .titulo, .subtitulo {
      font-size: 10px!important;
      font-weight: bold !important;
    }

    .titulo {
      font-size: 15px!important;
      font-weight: bold !important;
    }

    .impressao {
      padding-top: 10px;
    }

    .nota {
      border-style: none!important;
      font-size: 10px!important;
      text-align:left;
    }

    .conteudo-1{border-style: none!important;}
    .campo-texto { border-style: none!important;}
    .print-no-view {display: none!important; }
    .assinatura { border-bottom: 1px solid black;
      border-bottom: 1px solid black; }
    .text-area {
      width: 540px!important;
      height: 50px!important;
  }
  }
      .text-area {
      width: 1060px;
      height: 100px;

    }

    .impressao{
    padding-left: 60px;
    background: #fff;
    color: #393939;
    font-size: 16px;


    }
    .alinha-espaco {
      padding-top: -1000px;
    }

    .titulo {
      font-size: 15px;
      font-weight: 900;
    }

    .titulo-sec {
        font-weight: 600;
        font-size: 12px;
        margin-left: 64px;
    }
    .subtitulo {
        font-size: 14px;
        font-weight: bold;
        margin-top: 10px; 
    }
    .assinatura { border-bottom: 1px solid black;}

    .campo-texto {
      font-size: 14px;
      border-bottom: #666 1px solid;
      margin-top: 10px;  
    }
    .text-area {
      border: 1px solid black;
    }
    .nota {
      font-size:13px;
      text-align:left;
    } 
    </style>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <script src="js/functions.js"></script>
    <script type="text/javascript">
      function ap30(){ 
          if ( $('input[name=checkocultdetalhes]').is(':checked')) {
              $("#camposocultosmaisdetana").fadeIn(500);
            }else{
              $("#camposocultosmaisdetana").css("display","none");
            } 
        }
        function apobs(){ 
          if ( $('input[name=checkocultobs]').is(':checked')) {
              $("#boxobs").fadeIn(500);
            }else{
              $("#boxobs").css("display","none");
            } 
        }
    </script>
</html>