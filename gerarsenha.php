<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao            = new conexao;
$configuracoes      = new configuracoes;
$validacoes         = new validacoes;
$formatacoes        = new formatacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
//$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));
//---

if (filter_var($_GET["email"], FILTER_VALIDATE_EMAIL)) {
  $_POST["email"] = $_GET["email"];
}

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $email = $validacoes->anti_injection($_POST["email"]);

    if ( empty($email) ) {
      $msg_formulario = 'Digite seu e-mail';
    } else if ( $consulta = $configuracoes->consulta("SELECT cg.email FROM geral.cg 
      INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg
      WHERE cg.email = '".$email."' and cg.tipo = 1 and usuarios.ativo = true")->rowCount() == 0 ) {
      $msg_formulario = 'E-mail não está em uso';
    } else {
      $id_usuario = $configuracoes->consulta("SELECT cg.id_cg FROM geral.cg 
      INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg
      WHERE cg.email = '".$email."' and cg.tipo = 1 and usuarios.ativo = true")->fetch();

      $sql = $conexao->prepare("UPDATE geral.recuperasenha SET 
          linkativo = false
        WHERE 
          id_cg = ".$id_usuario['id_cg']."");
      $sql->execute();

      $sql = $conexao->prepare("INSERT INTO geral.recuperasenha (
          id_cg,  
          linkativo,
          data) 


              VALUES (?,?,now())"); 
        $sql->bindValue(1, $id_usuario['id_cg']);
        $sql->bindValue(2, t);
        
        $sql->execute();

      $hash = $conexao->lastInsertId();
      // Mostrar possíveis erros.
      //print_r($sql->errorInfo());

      require 'vendor/autoload.php';

      $resources = new \Mailjet\Resources();

      $api1 = 'f74843b63a568c5d7116cff5d971eb45';
      $api2 = '06fed94a47fa0688ef15fd36453ec365';

      $mj = new \Mailjet\Client($api1, $api2);
      $body = [
          'FromEmail' => "nao-responda@valleteclab.com",
          'FromName' => "Prefeitura de Luís Eduardo Magalhães - XPlanejamento",
          'Subject' => 'Assistência de conta do Planejamento, Orçamento e Gestão - XPlanejamento',
          'Html-part' => '[ E-mail resposta, por favor não responda esse e-mail ] <br />
        Prezado(a) Senhor(a),<br />
        <br /><br />
        <strong>Redefinição de Senha</strong><br />
        Para gerar uma senha para sua conta clique no link abaixo.<br /> 
        <a href="http://http://35.196.41.183/xplanejamento/novasenha.php?hash='.$formatacoes->criptografia($hash,'base64','encode').'">Por favor, clique aqui para redefinir sua conta </a>',
          'Recipients' => [
              [
                  'Email' => "".$email.""
              ]
          ]
      ];
      $response = $mj->post($resources::$Email, ['body' => $body]);
      $response->success() && var_dump($response->getData());
      ?>
        <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript">
          $(function () {
            setTimeout(function(){
                  alert("E-mail enviado para <?php echo $email; ?>. Para voltar à sua conta, siga as instruções que enviamos para seu endereço de e-mail");
                  window.open('index.php','_self');
              }, 1000);
            });
        </script>
      <?php
    }

}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="external-page external-alt sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content" class="animated fadeIn">

        <div class="admin-form theme-info mw500" style="margin-top: 10%;" id="login">
          <div class="row mb15 table-layout">

            <div class="col-xs-6 pln">
              <a href="dashboard.html" title="Return to Dashboard">
                <img src="img/vale.jpeg" title="XPlanejamento Logo" class="img-responsive w250">
              </a>
            </div>
          </div>

          <div class="panel">
            <form method="post" action="" id="resetar">
              <div class="panel-body p15">
                <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <i class="fa fa-info pr10"></i>Digite seu <b>e-mail</b> e as instruções serão enviadas para você!
                </div>
              </div>
              <!-- end .form-body section -->
              <div class="panel-footer p25 pv15">
                <div class="section mn">
                  <div class="smart-widget sm-right smr-80">
                    <label for="email" class="field prepend-icon">
                      <input type="text" name="email" id="email" class="gui-input" placeholder="Seu endereço de e-mail" value="<?php echo $_POST["email"]; ?>">
                      <label for="email" class="field-icon">
                        <i class="fa fa-envelope-o"></i>
                      </label>
                    </label>
                    <button type="submit" for="email" class="button">Solicitar</button>
                  </div>
                  <?php if ( !empty($msg_formulario) ) { ?>
                  <div class="panel-footer clearfix" style="text-align:center;">
                    <a href="#" class="btn btn-danger mb10 mr5 notification" data-note-style="danger"><?php echo $msg_formulario; ?></a>
                  </div>
                  <?php } ?>
                  <!-- end .smart-widget section -->

                </div>
                <!-- end section -->
        
              </div>
              <!-- end .form-footer section -->

            </form>

          </div>

        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    //xDemo.init();

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#resetar").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        email: {
          required: true,
          email: true
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
       email: {
          required: 'Informe um e-mail válido',
          email: 'Informe um e-mail válido'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>
