<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$funcoes = new funcoes;
$validacoes = new validacoes;
$func_usuario = new func_usuario;
$func_usuario_sis = new func_usuario_sis;

$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear','cad_cliente');


if ( $_POST['formulario'] == 'ok' ) {
  
  $nome = $validacoes->anti_injection($_POST["nome"]);
  $email = $validacoes->anti_injection($_POST["email"]);
  $endereco = $validacoes->anti_injection($_POST["endereco"]);
  $bairro = $validacoes->anti_injection($_POST["bairro"]);
  $cidade = (int)$_POST["cidade"];
  $estado = $validacoes->anti_injection($_POST["estado"]);
  $telefone = str_replace( ' ', '', $formatacoes->retira_simbolos($_POST["telefone"]));
  $cpfcnpj = $formatacoes->retira_simbolos($_POST["cpfcnpj"]);

  $senha = $validacoes->anti_injection($_POST["senha"]);  
  $tipousuario = 'cl';  
  $tipoprofissional = (int)$_POST["tipoprofissional"];

  $id_usuario = $func_usuario->manipulacoes(
    0, 
    $nome,   
    $email,  
    $endereco,   
    $bairro,   
    $cidade,
    $estado,   
    $telefone,   
    $cpfcnpj,  
    '',
     1,
    'cadastro');

  if ( (int)$id_usuario > 0 ) { 
    $func_usuario_sis-> manipulacoes(
      $id_usuario, 
      $senha,  
      $tipousuario,  
      '',   
      '',
      $tipoprofissional,
      'cadastro');

    require 'vendor/autoload.php';

    $resources = new \Mailjet\Resources();

    $api1 = 'f74843b63a568c5d7116cff5d971eb45';
    $api2 = '06fed94a47fa0688ef15fd36453ec365';

    $mj = new \Mailjet\Client($api1, $api2);
    $body = [
        'FromEmail' => "nao-responda@valleteclab.com",
        'FromName' => "Prefeitura de Luís Eduardo Magalhães - XPlanejamento",
        'Subject' => 'Cadastro de Profissional',
        'Html-part' => '[ E-mail resposta, por favor não responda esse e-mail ] <br />
      Prezado(a) Senhor(a),<br />
      <br /><br />
      Seu cadastro foi registrado com sucesso!<br /> 
      Seu cadastro será análisado, aguarde nosso contato para poder efetuar login no sistema.<br /> 
      www.luiseduardomagalhaes.ba.gov.br/planejamento <br />
      <br /><br />
      Agradecemos o contato!',
        'Recipients' => [
            [
                'Email' => "".$email.""
            ]
        ]
    ];
    $response = $mj->post($resources::$Email, ['body' => $body]);
    $response->success() && var_dump($response->getData());

    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Cadastro concluído com sucesso! Você será redirecionado para fazer seu primeiro acesso!');
              window.open('login.php','_self');
          }, 1000);
        });
    </script>
    <?php
  }
}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia" />
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="admin-validation-page sb-l-o sb-r-c onload-check sb-l-m sb-l-disable-animation" data-spy="scroll" data-target="#nav-spy" data-offset="200" style="min-height: 94px;">


  <!-- Start: Main -->
  <div id="main">

    

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout">

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="height: 294px;">
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border panel-system" style="border: 0;">

                

                  <div class="panel-body bg-light alinha-conteudo">
                    <!-- .section-divider -->
                    <div class="section row">
                      <div class="col-md-3">
                        <img class="logo-prefeitura" src="img/logo_prefeitura_home.png" height="150">
                      </div>
                      <div class="col-md-9 top-titulo"><img src="img/logo-planejamento.png"></div>
                    </div>
                    <div class="section row  menu">
                      <?php include_once('includes/menuindex.php'); ?>
                    </div>
                  </div>
                  <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <form method="post" id="admin-form">
                <input type="hidden" name="formulario" value="ok">
                <div class="panel-body bg-light">

                    <div class="section-divider mt20 mb40">
                      <span> Dados pessoais </span>
                    </div>

                    <?php if ( $_POST['formulario'] == 'ok' and $id_usuario == 0 ) { ?>
                    <div class="panel-footer clearfix" style="text-align:center;">
                      <a href="#" class="btn btn-danger mb10 mr5 notification" data-note-style="danger"><?php echo $id_usuario; ?></a>
                    </div>
                    <?php } ?>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->  
                   
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="nome" class="field prepend-icon">
                          <input type="text" name="nome" id="nome" class="gui-input" placeholder="Nome - Obrigatório">
                          <label for="nome" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="email" class="field prepend-icon">
                          <input type="email" name="email" id="email" class="gui-input" placeholder="E-mail - Obrigatório">
                          <label for="email" class="field-icon">
                            <i class="fa fa-envelope"></i>
                          </label>
                        </label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-6">
                        <label for="repitaemail" class="field prepend-icon">
                          <input type="repitaemail" name="repitaemail" id="repitaemail" class="gui-input" placeholder="Repita o e-mail">
                          <label for="repitaemail" class="field-icon">
                            <i class="fa fa-envelope"></i>
                          </label>
                        </label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-3">
                        <label for="cpf" class="field">CPF:
                        <input name="cpfcnpj" id="cpfcnpj" class="gui-input" type="text" placeholder="Obrigatório"></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-3">
                          <label for="">Tipo profissional:</label>
                          <label class="field select">
                            <select id="tipoprofissional" name="tipoprofissional">
                              <option value="">Selecione</option>
                              <option value="1">Engenheiro</option>
                              <option value="2">Técnico</option>
                              <option value="3">Arquiteto</option>
                            </select>
                            <i class="arrow"></i>
                          </label>
                      </div>
                    </div>
                  <div class="section-divider col-md-12"><span> Dados de Endereço/Contato</span></div>
                  <div class="section row">    
                      <div class="col-md-3">
                        <label ">Estado atual:</label>
                        <label for="estado" class="field select">
                          <select id="cod_estados" name="estado">
                            <option value="0">Selecione</option>

                            <?php 
                              $con_listagem_estados = $configuracoes->consulta("SELECT codigouf, uf FROM geral.estados");

                              foreach ( $con_listagem_estados as $listagem_estados ) { 
                            ?>
                                <option value="<?php echo $listagem_estados['uf']; ?>"><?php echo $listagem_estados['uf']; ?></option>
                            <?php } ?>
                          </select>
                          <i class="arrow"></i>
                        </label>
                    </div>
                    <div class="carregando" style="display: none;">carregando cidades...</div>
                    <div class="col-md-4">
                      <label for="">Cidade atual:</label>
                      <label for="cidade" class="field select">
                        <select id="cod_cidades" name="cidade">
                        </select>
                        <i class="arrow"></i>
                      </label>
                    </div>
                  </div> 
                  <div class="section row">
                      <div class="col-md-4">  
                        <label class="field" for="endereco">Endereço
                        <input name="endereco" id="endereco" class="gui-input" type="text" placeholder="Obrigatório"></label>
                      </div>
                      <div class="col-md-3">
                        <label class="field" for="bairro">Bairro
                        <input name="bairro" id="bairro" class="gui-input" type="text" placeholder="Obrigatório"></label>
                      </div>
                      <div class="col-md-2">
                        <label class="field" for="telefone">Telefone
                        <input type="tel" name="telefone" id="telefone" class="gui-input phone-group" placeholder="Obrigatório" aria-invalid="false" ></label>

                      </div>
                  </div>
                  
                  <div class="section-divider col-md-12"><span> Dados de Usuário</span></div>
                  <div id="boxalterarsenha">
                    <div class="section row">
                      <div class="col-md-3">
                        <label class="field" for="senha">Senha:
                        <input name="senha" id="senha" class="gui-input" type="password"></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-3">
                        <label class="field" for="repitasenha">Repita a senha:
                        <input name="repitasenha" class="gui-input" type="password"></label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer text-left">
                <button type="submit" class="button btn-success">Cadastrar</button>
              </div>              
            </div>
            <?php include('includes/rodapeindex.php'); ?>
          </div>
        </div>
        <!-- end: .admin-form -->

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  .logo-prefeitura {
    padding-top: 45px;
    padding-left:90px;
  }
  .top-titulo {
    font-size: 28px;
  font-weight: 600;
  margin-top: 32px
  }
  .logo-legenda {
    font-size: 15px;
  }
  .button-direita {
  
  }

    .menu-login {
      font-size: 11px;
      margin-top: 5px; 
    }
    .profissional {
      padding: 22px;



    }
   .col-md-menu {
    padding: 5px;
   }
   div.main {
    background-color: rgba(211,211,211, 0.5)
   }
   .menu-botao{
    border-left: 1px solid #A9A9A9; 
    margin-left: -30px;
    padding-left: 10px;
   }
   .menu-botao-inf {
    border-left: 1px solid #A9A9A9; 

   }

   .menu {
    border: 1px solid rgba(169,169,169, 1.5); 
    border-radius: 3px;
    padding: 20px;
    font-size: 14px;
    color: black;
    background-color: rgba(211,211,211, 0.2)
   }
   .alinha-conteudo{
    margin-top: 40px;


  }
  .texto-info {
    text-align: justify;
    font-size: 13px;
    margin-top: 15px;
    color: #545353;
  }
  .titulo-info {
    font-size: 25px;
    font-weight: bold;
    color: #545353;
  }

  
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/jquery.maskedinput.min.js"></script>
  <script src="js/mascaras.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    $(function(){
        $('#cod_estados').change(function(){
          if ( $(this).val() ) {
            $('#cod_cidades').hide();
            $('.carregando').show();
            $.getJSON('municipios.ajax.php?cod_estado=',{cod_estado: $(this).val(), ajax: 'true'}, function(j){
              var options = '<option value=""></option>'; 
              for (var i = 0; i < j.length; i++) {
                  options += '<option value="' + j[i].cod_municipio + '">' + j[i].nome + '</option>';
              }

              $('#cod_cidades').html(options).show();
              $('.carregando').hide();
            });
          } else {
              $('#cod_cidades').html('<option value="">â€“ Escolha um estado â€“</option>');
          }
        });
    });
  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    $("#cpfcnpj").mask("999.999.999-99");
    $("#telefone").mask("(99) 99999-9999");

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        nome: {
          required: true
        },
        email: {
          required: true,
          email: true
        },
        repitaemail: {
          required: true,
          equalTo: '#email'
        },
        cpfcnpj: {
          required: true
        },
        tipoprofissional: {
          required: true
        },
        estado: {
          required: true
        },
        cidade: {
          required: true
        },
        endereco: {
          required: true
        },
        bairro: {
          required: true
        },
        tipousuario: {
          required: true
        },
        telefone: {
          require_from_group: [1, ".phone-group"]
        },
        senha: {
          required: function(element) { 
            if ($('input[name=alterarsenha]').is(':checked')){
              required: true
            }
            
          }
        },
        repitasenha: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#senha'
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        nome: {
          required: 'Informe o seu nome'
        },
        email: {
          required: 'Informe um e-mail válido',
          email: 'Informe um e-mail válido'
        },
        repitaemail: {
          required: 'Insira o mesmo e-mail informado acima',
          equalTo: 'E-mails não estão iguais'
        },
        cpfcnpj: {
          required: 'Informe seu CPF'
        },
        tipoprofissional: {
          required: 'Informe o tipo de profissional'
        },
        estado: {
          required: 'Informe o Estado'
        },
        cidade: {
          required: 'Informe a Cidade'
        },
        endereco: {
          required: 'Informe seu endereço'
        },
        bairro: {
          required: 'Informe o bairro onde mora'
        },
        tipousuario: {
          required: 'Informe seu Tipo de Usuário'
        },
        telefone: {
          require_from_group: 'Informe seu telefone'
        },
        senha: {
          required: 'Informe sua senha'
        },
        repitasenha: {
          required: 'Insira a mesma senha informada acima',
          equalTo: 'Senhas não estão iguais'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>