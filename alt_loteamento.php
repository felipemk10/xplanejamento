<?php
if(!$tipoprocesso) $tipoprocesso = NULL;//Irving
if(!$tipousuarioLogado) $tipousuarioLogado = NULL;//Irving
if(!$usuarioBI) $usuarioBI = NULL;//Irving

//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require 'vendor/autoload.php';
session_start();

$autenticacao   = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios       = new \Biblioteca\usuarios;

$manipuladores  = new Biblioteca\manipuladores;

$processos      = new \Biblioteca\processos;

$tipousuarioLogado    = $usuarios->getUsuarioLogado($manipuladores->criptografia($_SESSION['id_usuario'] ,'base64','decode'))->tipousuario;

if ( $_GET['id_pro'] > 0 ) {
  //  Listando informações sobre o processo.
  $consulta = "SELECT 
    processos.id_pro,
    processos.endereco,
    processos.quadra,
    processos.lote,
    processos.numero,
    processos.bairro,
    processos.cidade,
    processos.estado,
    processos.tipoprocesso,
    processos.datahora, 
    processos.situacaoprojeto 

    FROM 

    processos.processos 

    INNER JOIN processos.processos_proprietario ON processos_proprietario.id_pro = processos.id_pro
    INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

    WHERE processos.id_pro = ".$_GET['id_pro']." and processos.ativo = true";

    if ( $tipousuarioLogado == 'cl' ) {
      $consulta = $consulta." and processos.id_cg = ".$manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode')."";
    }
    //  Listando processos.
    $consulta = $processos->consulta($consulta);

    $linha = $consulta->fetch();
}

?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
    <meta name="description" content="Sistema de Planejamento">
    <meta name="author" content="uasgeek">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  </head>
  <body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

    <!-- Start: Main -->
    <div id="main">

      <!-- Start: Header -->
      <?php include('includes/header2.php'); ?>
      <!-- End: Header -->
      <!-- Start: Sidebar -->
      <?php include('includes/sidebar.php'); ?>
      <!-- End: Sidebar -->

      <!-- Start: Content-Wrapper -->
      <section id="content_wrapper">
        
        <!-- Start: Topbar -->
        <header id="topbar" class="alt">
          <div class="topbar-left">
            <ol class="breadcrumb">
              <li class="crumb-icon">
                <a href="dashboard.html">
                  <span class="glyphicon glyphicon-home"></span>
                </a>
              </li>
              <li class="crumb-active">
                <a href="dashboard.html">Cadastro de Processos</a>
              </li>
              <li class="crumb-trail">
                <a href="dashboard.html">Alvará de Loteamento</a>
              </li>
            </ol>
          </div>
        </header>
        <!-- End: Topbar -->      <!-- Begin: Content section -->
        <section id="content" class="table-layout animated fadeIn">
          <!-- begin: .tray-center -->
          <div class="tray tray-center">
            <!-- Begin: Content Header -->
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border panel-system">

                <form method="post" id="admin-form" action="javascript: formularios_dinamicos('#admin-form', 'backend/alt_loteamento.php?tipoprocesso=<?php echo $_GET['tipoprocesso']; ?>', '#carregando', '#formResult', 's', '');">
                  <input type="hidden" name="form" value="alteracao" />
                  <input type="hidden" name="id_pro" value="<?php echo $linha->id_pro; ?>" />

                  <div class="panel-body bg-light">

<?php //if ( $tipousuarioLogado == 'ad' ) { ?>
                    <!--button type="button" class="button btn-primary" onclick="pg_modal('modals/alt_processos.php?id_pro=<?php //echo (int)$_GET['id_pro']; ?>&tipoprocesso=<?php //echo (int)$_GET['tipoprocesso']; ?>','');">Alterar tipo do processo</button-->
<?php //} ?>
                    <div class="section-divider mt20 mb40">
                      <span> Dados do Proprietário </span>
                    </div>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->
<?php 
  //  Listando informações sobre os proprietários.
  $con_listagem_proprietario = $processos->consulta("SELECT 
    cg.id_cg,
    cg.nome,
    cg.email,
    cg.cpfcnpj

    FROM 
    processos.processos_proprietario

    INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

    WHERE processos.processos_proprietario.id_pro = ".$_GET['id_pro']." ORDER BY processos.processos_proprietario.id_pro ASC"); 

  $listagem_proprietario = $con_listagem_proprietario->fetch(); 

  $proprietario1 = $listagem_proprietario->cpfcnpj;
?>

                    <input type="hidden" name="IDProprietario" id="IDProprietario" value="<?php echo $listagem_proprietario->id_cg; ?>" /> 
                        
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="nomeProprietario" class="field prepend-icon">
                          <input type="text" name="nomeProprietario" id="nomeProprietario" class="gui-input" placeholder="Nome" value="<?php echo $listagem_proprietario->nome; ?>" />
                          <label for="nomeProprietario" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div>
                      <!-- end section -->
                    </div>
                    <!-- end .section row section -->

                    <div class="section row">
                      <div class="col-md-6">
                        <label for="emailProprietario" class="field prepend-icon">
                          <input type="email" name="emailProprietario" id="emailProprietario" class="gui-input" placeholder="E-mail" value="<?php echo $listagem_proprietario->email; ?>" />
                          <label for="emailProprietario" class="field-icon">
                            <i class="fa fa-envelope"></i>
                          </label>
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-2">   
                        <label class="option">
                          <input name="tipopessoa" id="pessoasim" type="radio" <?php if ( strlen($listagem_proprietario->cpfcnpj) == 11 ) { ?>checked="checked"<?php } ?> onclick="ap100();" />
                          <span class="radio"></span>Física
                        </label>
                      </div>
                      <div class="col-md-2">
                        <label class="option">
                          <input name="tipopessoa" id="pessoanao" type="radio" <?php if ( strlen($listagem_proprietario->cpfcnpj) == 14 ) { ?>checked="checked"<?php } ?> onclick="ap110();" />
                          <span class="radio"></span>Jurídica
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="cpfcnpjProprietario" class="field prepend-icon">
                          <input type="input" name="cpfcnpjProprietario" id="cpfcnpjProprietario" class="gui-input" placeholder="CPF/CNPJ" value="<?php echo $listagem_proprietario->cpfcnpj; ?>" />
                          <label for="cpfcnpjProprietario" class="field-icon">
                            <i class="fa fa-check"></i>
                          </label>
                        </label>
                      </div>
                    </div>
                    <div class="section row ">
                      <div class="col-md-4">  
                        <label for="endereco" class="field">Endereço
                          <input name="endereco" id="endereco" class="gui-input col-md-6" type="text" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->endereco; ?>" <?php }?> />
                        </label>
                      </div>
<?php
  //  Procedimentos específicos para cada empresa.
  $sFile = "loteamentos.json";

  if (file_exists($sFile)) { 
    $json_str = file_get_contents($sFile);
  } else {
    echo 'Não achou o arquivo de loteamentos.';
    exit;
  }

  // faz o parsing da string, criando o array "loteamentos"
  $jsonObj = json_decode($json_str);
  $loteamentos = $jsonObj->loteamentos;

  //Irving - Ordenação dos loteamentos puxados de loteamentos.json
  $lot_nome = array_column($loteamentos, 'nome');
  array_multisort($lot_nome, SORT_ASC, $loteamentos);

  //navega pelos elementos do array, imprimindo cada empregado
  foreach ( $loteamentos as $e ) 
    if ( $e->ativo == 'S' && $e->nome == $linha->bairro ) $checkbox = 'lista'; 
?>

                      <div class="col-md-4">
                        <label>Bairro</label>
                        <label for="bairro" class="field select">
                          <input name="inputbairro" <?php if($checkbox!='lista'){ echo 'value="'.$linha->bairro.'" id="bairro"';} else echo 'id="inputbairro" style="display: none;"'; ?> class="gui-input col-md-6" >
                          <select name="selectbairro" <?php if($checkbox=='lista'){ echo 'id="bairro"';} else echo 'id="selectbairro" style="display: none;"'; ?> >
                            <option value=""></option>
                            <ul>
<?php
  //navega pelos elementos do array, imprimindo cada bairro
  foreach ( $loteamentos as $e ) { 
    if ( $e->ativo == 'S' ) {
      if( $e->nome == $linha->bairro ){
        echo "<option value='".$e->nome."' selected >".$e->nome."</option>";   
      }
      else echo "<option value='".$e->nome."'>".$e->nome."</option>";
    }
  } 
?>
                            </ul>
                          </select>
                          <input id="campobairro" name="campobairro" type="checkbox" <?php if($checkbox=='lista') echo 'checked'; ?> onchange="listabairros('muda_checkbox');" /> lista
                        </label>
                      </div>






                      <div class="col-md-1">
                        <label for="quadra" class="field">Quadra
                          <input name="quadra" id="quadra" class="gui-input col-md-6" type="text"<?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->quadra; ?>" <?php }?> />
                        </label>
                      </div>
                      <div class="col-md-1">
                        <label for="lote" class="field">Lote
                          <input name="lote" id="lote" class="gui-input col-md-6" type="text" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->lote; ?>" <?php }?> />
                        </label>
                      </div>
                      <div class="col-md-1">
                        <label for="numero" class="field">Nº
                          <input name="numero" id="numero" class="gui-input col-md-6" type="text" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="15" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->numero; ?>" <?php }?> />
                        </label>
                      </div>                    
                    </div> <!-- fim da row -->
                    <div class="section row">    
                      <div class="col-md-3">
                        <label>Estado:</label>
                        <label for="estado" class="field select">
                          <select id="cod_estados" name="estado">
<?php 
  $con_listagem = $processos->consulta("SELECT codigouf, nome FROM geral.estados");
  while ( $estado = $con_listagem->fetch() ) { 
?>
                            <option value="<?php echo $estado->codigouf; ?>" <?php if($estado->codigouf==$linha->estado) echo "selected";?> ><?php echo $estado->nome; ?></option>
<?php 
  } 
?>
                          </select>
                          <i class="arrow"></i>
                        </label>
                      </div>
                      <div class="carregando" style="display: none;">carregando cidades...</div>
                      <div class="col-md-3">
                        <label>Cidade:</label>
                        <label for="cidade" class="field select">
                          <select id="cod_cidades" name="cidade">
<?php //Irving --- SQL corrigido
  $con_listagem = $processos->consulta("SELECT codigo, geral.municipios.nome as nome FROM geral.municipios INNER JOIN geral.estados ON municipios.uf=estados.uf WHERE codigouf=".$linha->estado.";");
  while ( $cidade = $con_listagem->fetch()) { 
?>
                            <option value="<?php echo $cidade->codigo; ?>" <?php if($cidade->codigo==$linha->cidade) echo "selected";?> ><?php echo $cidade->nome; ?></option>
<?php 
  } 
?>
                          </select>
                          <i class="arrow"></i>
                        </label>
                      </div>  
                    </div><!-- fim da row -->
<?php 
  //  Listando informações sobre os proprietários.
  $con_listagem_proprietario = $processos->consulta("SELECT 
    cg.id_cg,
    cg.nome,
    cg.email,
    cg.cpfcnpj

    FROM 
    processos.processos_proprietario

    INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

    WHERE processos.processos_proprietario.id_pro = ".$_GET['id_pro']." ORDER BY processos.processos_proprietario.id_pro ASC OFFSET 1"); 
?>
                    
                    <div class="section row col-md-12">
                      <label class="option ">
                        <input name="addproprietario" type="checkbox" id="addproprietario" <?php if ( $con_listagem_proprietario->rowCount() > 0 ) { ?>checked="checked"  <?php } ?> onclick="ap20();" />
                        <span class="checkbox"></span>Adicionar proprietários
                      </label>
                    </div> <!-- fim da row -->
                    <div id="campomaisproprietario" style="<?php if ( $con_listagem_proprietario->rowCount() == 0 ) { ?>display: none;<?php } ?>">
                      <div class="enblobacampos31">                           
                        <div id="textProprietario">
                        <hr /><hr />
<?php foreach ( $con_listagem_proprietario as $listagem_proprietario ) { ?>
                          <input type="hidden" name="IDProprietarioAdd[]" id="IDProprietarioAdd[]" value="<?php echo $listagem_proprietario->id_cg; ?>" /> 
                          <div class="section row">
                            <div class="col-md-6">
                              <label for="nomeProprietarioAdd[]" class="field prepend-icon">
                                <input type="text" name="nomeProprietarioAdd[]" id="nomeProprietarioAdd[]" class="gui-input" placeholder="Nome" value="<?php echo $listagem_proprietario->nome; ?>" />
                                <label for="nomeProprietarioAdd[]" class="field-icon">
                                  <i class="fa fa-user"></i>
                                </label>
                              </label>
                            </div>
                            <!-- end section -->
                          </div>
                          <!-- end .section row section -->
                          <div class="section row">
                            <div class="col-md-6">
                              <label for="emailProprietarioAdd[]" class="field prepend-icon">
                                <input type="email" name="emailProprietarioAdd[]" id="emailProprietarioAdd[]" class="gui-input" placeholder="E-mail" value="<?php echo $listagem_proprietario->email; ?>" />
                                <label for="emailProprietarioAdd[]" class="field-icon">
                                  <i class="fa fa-envelope"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                          <div class="section row">
                            <div class="col-md-6">
                              <label for="cpfcnpjProprietarioAdd[]" class="field prepend-icon">
                                <input type="input" name="cpfcnpjProprietarioAdd[]" id="cpfcnpjProprietarioAdd[]" class="gui-input" placeholder="CPF/CNPJ" value="<?php echo $listagem_proprietario->cpfcnpj; ?>" />
                                <label for="cpfcnpjProprietarioAdd[]" class="field-icon">
                                  <i class="fa fa-check"></i>
                                </label>
                              </label>
                            </div>
                          </div>
                          <hr />
<?php } ?>
                        </div>
                      </div> <!-- FIM DE ENGLOBACAMPOS31 -->
                      <!-- Irving - nova função criada para adicionar proprietários-->
                      <button type="button" class="button btn-primary" onclick="addInputProprietarioNovo();">Adicionar proprietários</button>
                    </div> 
                    <div class="section-divider col-md-12"><span>Dados do Projeto</span></div>
<?php 
  //  Listando informações sobre autor.
  $con_listagem_autor = $processos->consulta("SELECT 
    cg.id_cg,
    cg.nome,
    cg.creacau,
    processos_profissional.tipoprofissional

    FROM 
    processos.processos_profissional

    INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

    WHERE processos.processos_profissional.id_pro = ".$_GET['id_pro']." and processos.processos_profissional.tipo = 'a'");

  $listagem_autor = $con_listagem_autor->fetch();
?>
                    <input type="hidden" name="id_autoria" value="<?php echo $listagem_autor->id_cg; ?>" />
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="autoria" class="field prepend-icon">
                          <input type="text" name="autoria" id="autoria" class="gui-input" placeholder="Autoria" value="<?php echo $listagem_autor->nome; ?>" />
                          <label for="autoria" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div> <!-- end section -->
                    </div> <!-- end .section row section -->
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="autoria_creacau" class="field prepend-icon">
                          <input type="text" name="autoria_creacau" id="autoria_creacau" class="gui-input" placeholder="CREA/CAU" value="<?php echo $listagem_autor->creacau; ?>" />
                          <label for="autoria_creacau" class="field-icon">
                            <i class="fa fa-check"></i>
                          </label>
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-2">    
                        <label class="option">
                          <input name="autoria_profissional" type="radio" value="e" <?php if ( $listagem_autor->tipoprofissional == 'e' ) { ?>checked="checked"<?php } ?> />
                          <span class="radio"></span>Engenheiro
                        </label>
                      </div>
                      <div class="col-md-2">
                        <label class="option">
                          <input name="autoria_profissional" type="radio" value="a" <?php if ( $listagem_autor->tipoprofissional == 'a' ) { ?>checked="checked"<?php } ?> />
                          <span class="radio"></span>Arquiteto
                        </label>
                      </div>
                    </div><!-- fim da row -->
                    <hr />                  
<?php 
  //  Listando informações sobre responsável técnico.
  $con_listagem_resptecnico = $processos->consulta("SELECT 
    cg.id_cg,
    cg.nome,
    cg.creacau,
    processos_profissional.tipoprofissional

    FROM 
    processos.processos_profissional

    INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

    WHERE processos.processos_profissional.id_pro = ".$_GET['id_pro']." and processos.processos_profissional.tipo = 'r'");

  $listagem_resptecnico = $con_listagem_resptecnico->fetch();
?>
                    <div class="section row col-md-12">
                      <label class="option">
                        <input type="checkbox" name="resp_open" <?php if ($con_listagem_resptecnico->rowCount() > 0) { ?>checked="checked"<?php } ?> onclick="ap19();" />
                        <span class="checkbox"></span>Responsável Técnico
                      </label>
                    </div> <!-- fim da row -->
                    <input type="hidden" name="id_resptecnico" value="<?php echo $listagem_resptecnico->id_cg; ?>" />
                    <div id="camporesptec" style="<?php if ($con_listagem_resptecnico->rowCount() == 0) { ?> display:none;<?php } ?>">
                      <div class="section row">
                        <div class="col-md-6">
                          <label for="resptecnico" class="field prepend-icon">
                            <input type="text" name="resptecnico" id="resptecnico" class="gui-input" placeholder="Resp. Técnico" value="<?php echo $listagem_resptecnico->nome; ?>" />
                            <label for="resptecnico" class="field-icon">
                              <i class="fa fa-user"></i>
                            </label>
                          </label>
                        </div> <!-- end section -->
                      </div> <!-- end .section row section -->
                      <div class="section row">
                        <div class="col-md-6">
                          <label for="resptecnico_creacau" class="field prepend-icon">
                            <input type="text" name="resptecnico_creacau" id="resptecnico_creacau" class="gui-input" placeholder="CREA/CAU" value="<?php echo $listagem_resptecnico->creacau; ?>" />
                            <label for="resptecnico_creacau" class="field-icon">
                              <i class="fa fa-check"></i>
                            </label>
                          </label>
                        </div>
                      </div>
                      <div class="section row">
                        <div class="col-md-2"> 
                          <label class="option">
                            <input name="resptecnico_profissional" type="radio" value="e" <?php if ( $listagem_resptecnico->tipoprofissional == 'e' ) { ?>checked="checked"<?php } ?> />
                            <span class="radio"></span>Engenheiro
                          </label>
                        </div> 
                        <div class="col-md-2">  
                          <label class="option">
                            <input name="resptecnico_profissional" type="radio" value="a" <?php if ( $listagem_resptecnico->tipoprofissional == 'a' ) { ?>checked="checked"<?php } ?> />
                            <span class="radio"></span>Arquiteto
                          </label>
                        </div>
                      </div><!-- fim da row -->
                    </div>
                    <div class="section-divider col-md-12">
                      <span>Dados do Loteamento</span>
                    </div>
<?php 
  //  Listando informações sobre o loteamento.
  $con_listagem_loteamento = $processos->consulta("SELECT 
    processos_loteamento.id_lot,
    processos_loteamento.id_pro,
    processos_loteamento.id_cg,
    processos_loteamento.nome,
    processos_loteamento.matricula,
    processos_loteamento.areatotal,
    processos_loteamento.arealotes,
    processos_loteamento.quantlotes,
    processos_loteamento.areaviaspublicas,
    processos_loteamento.areaverde,
    processos_loteamento.areaequipamentopublico,
    processos_loteamento.ativo

    FROM 
    processos.processos_loteamento

    INNER JOIN geral.cg ON cg.id_cg = processos_loteamento.id_cg                                    

    WHERE processos.processos_loteamento.id_pro = ".$_GET['id_pro']."");

  $listagem_loteamento = $con_listagem_loteamento->fetch();
  $id_lot = $listagem_loteamento->id_lot;
  $lot_ativo = $listagem_loteamento->ativo;
  $somaATM = $listagem_loteamento->areaviaspublicas + $listagem_loteamento->areaverde + $listagem_loteamento->areaequipamentopublico;
?>
                    <input type="hidden" name="id_lot" value="<?php echo $listagem_loteamento->id_lot; ?>" />
                    <div class="section row">
                      <div class="col-md-4">  
                        <label for="loteamentoproposto" class="field"> Loteamento Proposto
                          <input name="loteamentoproposto" id="loteamentoproposto" class="gui-input col-md-6" type="text" value="<?php echo $listagem_loteamento->nome; ?>" />
                        </label>
                      </div>
                      <div class="col-md-2">
                        <label for="matricula" class="field">Número de Matrícula
                          <input name="matricula" id="matricula" class="gui-input col-md-6" type="text" value="<?php echo $listagem_loteamento->matricula; ?>" />
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-2">  
                        <label for="areatotal" class="field">Área Total (m²)
                          <input name="areatotal" id="areatotal" class="gui-input" type="text" value="<?php echo $manipuladores->numeroarea($listagem_loteamento->areatotal); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" onchange="Areas(this);" maxlength="15" />
                        </label>
                      </div>
                      <div class="col-md-2">  
                        <label for="arealotes" class="field">Área de Lotes (m²)
                          <input name="arealotes" id="arealotes" class="gui-input" type="text" value="<?php echo $manipuladores->numeroarea($listagem_loteamento->arealotes); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" onchange="Areas(this);" maxlength="15" />
                        </label>
                      </div>
                      <div class="col-md-2">
                        <label for="quantidade_lotes" class="field">Quantidade de Lotes
                          <input name="quantidade_lotes" id="quantidade_lotes" class="gui-input col-md-6" type="text" value="<?php echo $manipuladores->numerodouble('imprimir',$listagem_loteamento->quantlotes); ?>" onkeypress="mascaraInteiro();" onkeyup="mascaraInteiro();" />
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-2">  
                        <label for="areaviaspublicas" class="field">Área Vias Públ.(m²)
                          <input name="areaviaspublicas" id="areaviaspublicas" class="gui-input" type="text" value="<?php echo $manipuladores->numeroarea($listagem_loteamento->areaviaspublicas); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" onchange="Areas(this);" maxlength="15" placeholder=">= 15%" />
                        </label>
                      </div>
                      <div class="col-md-2"> 
                        <label for="areaverde" class="field">Áreas Verdes (m²)
                          <input name="areaverde" id="areaverde" class="gui-input" type="text"  value="<?php echo $manipuladores->numeroarea($listagem_loteamento->areaverde); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" onchange="Areas(this);" maxlength="15" placeholder=">= 10%" />
                        </label>
                      </div>
                      <div class="col-md-4">  
                        <label for="areaequipamentopublico" class="field">Área de Equipamentos Públicos (m²)
                          <input name="areaequipamentopublico" id="areaequipamentopublico" class="gui-input" type="text" value="<?php echo $manipuladores->numeroarea($listagem_loteamento->areaequipamentopublico); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" onchange="Areas(this);" maxlength="15" width="20" placeholder=">= 5%" />
                        </label>
                      </div>
                      <div class="col-md-3">  
                        <label for="ATM" class="field">Área Transferência Municipal (m²)
                          <input name="ATM" id="ATM" class="gui-input" type="text" value="<?php echo $manipuladores->numeroarea($somaATM); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" onchange="Areas(this);" maxlength="15" disabled="disabled" placeholder="ATM >= 35%" disabled="disabled" />
                        </label>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <label class="option">
                        <input type="checkbox" name="lot_ativo" id="lot_ativo" <?php if ($lot_ativo) { ?>checked="checked"<?php } ?> />
                        <span class="checkbox"></span>Termo de Verificação de Obras
                      </label>
                    </div> <!-- fim da row -->
                  </div>
                  <div class="panel-footer text-left">
                    <button type="submit" class="button btn-success">Atualizar</button>
                  </div> <!-- end: panel-body -->
                </div> <!-- end: admin-form .tab-content -->
              </div> <!-- end: panel-class -->
            </div> <!-- end: div class admin-form -->
          </div> <!-- end: tray-center  -->
        </section> <!-- end: Content section -->
      </section> <!-- end: Content-Wraper section -->  
    </div> <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->
    <style>
      /* demo page styles */
      body { min-height: 2300px; }
      
      .content-header b,
      .admin-form .panel.heading-border:before,
      .admin-form .panel .heading-border:before {
        transition: all 0.7s ease;
      }
      /* responsive demo styles */
      @media (max-width: 800px) {
        .admin-form .panel-body { padding: 18px 12px; }
      }
    </style>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <!-- jQuery Validate Plugin-->
    <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
    <!-- jQuery Validate Addon -->
    <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
    <!-- Theme Javascript -->
    <script src="assets/js/utility/utility.js"></script>
    <script src="assets/js/demo/demo.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/mascaras.js"></script>
    <script src="js/functions.js"></script>
    <script type="text/javascript">

<?php if ( strlen($proprietario1) == 11 ) { ?>
      $("#cpfcnpjProprietario").mask("999.999.999-99");
<?php } else if ( strlen($proprietario1) == 14 ) { ?>
      $("#cpfcnpjProprietario").mask("99.999.999/9999-99");
<?php } ?>

    </script>
    <script type="text/javascript">

      $(function(){ //Irving - função chamada para popular seleção de cidades após escolher estado
        $('#cod_estados').change(function(){
          if ( $(this).val() ) {
            $('#cod_cidades').hide();
            $('.carregando').show();
            $.getJSON('municipios.ajax.php?cod_estado=',{cod_estado: $(this).val(), ajax: 'true'}, function(j){
              var options = ''; 
              for (var i = 0; i < j.length; i++) {
                  options += '<option value="' + j[i].cod_municipio + '">' + j[i].nome + '</option>';
              }

              $('#cod_cidades').html(options).show();
              $('.carregando').hide();
              listabairros('muda_cidade');
            });
          } else {
              $('#cod_cidades').html('<option value=""> Escolha um estado </option>');
          }
        });

        $('#cod_cidades').change(function(){
          listabairros('muda_cidade');
        });
      });

      jQuery(document).ready(function() {
        "use strict";    
        Core.init();// Init Theme Core     
        Demo.init();// Init Demo JS

        /* @custom validation method (smartCaptcha) 
        ------------------------------------------------------------------ */
        $.validator.methods.smartCaptcha = function(value, element, param) {
          return value == param;
        };

        $("#admin-form").validate({
        /* @validation states + elements 
        ------------------------------------------- */

          errorClass: "state-error",
          validClass: "state-success",
          errorElement: "em",

          /* @validation rules 
          ------------------------------------------ */
          rules: {
            nome_proprietario: {
              required: true
            },
            email_proprietario: {
              required: true
            },
            cpfcnpj_proprietario: {
              required: true
            },
            autoria: {
              required: true
            },
            autoria_creacau: {
              required: true
            },
            autoria_profissional: {
              required: true
            },
            resptecnico: {
              required: function(element) { 
                if ($('input[name=resp_open]').is(':checked')){
                  required: true
                }
              }
            },
            resptecnico_creacau: {
              required: function(element) { 
                if ($('input[name=resp_open]').is(':checked')){
                  required: true
                }
              }
            },
            resptecnico_profissional: {
              required: function(element) { 
                if ($('input[name=resp_open]').is(':checked')){
                  required: true
                }
              }
            },
            endereco: {
              required: true
            },
            bairro: {
              required: true
            },
            quadra: {
              required: true
            },
            lote: {
              required: true
            },
            numero: {
              required: true
            },
            finalidadeobra: {
              required: true
            },
            areaterreno: {
              required: true
            },
            situacaoterreno: {
              required: true
            },
            area_construir: {
              required: true
            },
            desmembramento_entregue: {
              required: function(element) { 
                if ($('input[name=desmembramento]').is(':checked')){
                  required: true
                }
              }
            },
            taxapermeabilidade: {
              required: true
            },
            tipopessoa: {
              required: true
            }
          },

          /* @validation error messages 
          ---------------------------------------------- */
          messages: {
            nome_proprietario: {
              required: 'Digite o nome do Proprietário'
            },
            email_proprietario: {
              required: 'Digite o e-mail do Proprietário'
            },
            cpfcnpj_proprietario: {
              required: 'Digite o CPF/CNPJ do Proprietário'
            },
            autoria: {
              required: 'Digite o nome do autor'
            },
            autoria_creacau: {
              required: 'Digite o CREA/CAU do autor'
            },
            autoria_profissional: {
              required: 'Escolha o tipo de profissional do autor'
            },
            resptecnico: {
              required: 'Digite o nome do Resp. Técnico'
            },
            resptecnico_creacau: {
              required: 'Digite o CREA/CAU do Resp. Técnico'
            },
            resptecnico_profissional: {
              required: 'Escolha o tipo de profissional do Resp. Técnico'
            },
            endereco: {
              required: 'Digite o endereço'
            },
            bairro: {
              required: 'Digite o bairro'
            },
            quadra: {
              required: 'Digite a quadra'
            },
            lote: {
              required: 'Digite o lote'
            },
            numero: {
              required: 'Digite o número'
            },
            finalidadeobra: {
              required: 'Escolha a finalidade da obra'
            },
            areaterreno: {
              required: 'Preencha a área do terreno'
            },
            situacaoterreno: {
              required: 'Situação do terreno está inválida'
            },
            area_construir: {
              required: 'Preencha a área do terreno à construir'
            },
            desmembramento_entregue: {
              required: 'Informe a opção de Entregue'
            },
            taxapermeabilidade: {
              required: 'Informe a Taxa de permeabilidade'
            },
            tipopessoa: {
              required: 'Informe se o Proprietário é pessoa física ou jurídica'
            }

          },

          /* @validation highlighting + error placement  
          ---------------------------------------------------- */
          highlight: function(element, errorClass, validClass) {
            $(element).closest('.field').addClass(errorClass).removeClass(validClass);
          },
          unhighlight: function(element, errorClass, validClass) {
            $(element).closest('.field').removeClass(errorClass).addClass(validClass);
          },
          errorPlacement: function(error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
              element.closest('.option-group').after(error);
            } else {
              error.insertAfter(element.parent());
            }
          }
        });

        // Cache several DOM elements
        var pageHeader = $('.content-header').find('b');
        var adminForm = $('.admin-form');
        var options = adminForm.find('.option');
        var switches = adminForm.find('.switch');
        var buttons = adminForm.find('.button');
        var Panel = adminForm.find('.panel');

        // Form Skin Switcher
        $('#skin-switcher a').on('click', function() {
          var btnData = $(this).data('form-skin');

          $('#skin-switcher a').removeClass('item-active');
          $(this).addClass('item-active')

          adminForm.each(function(i, e) {
            var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
            var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
            $(e).removeClass(skins).addClass('theme-' + btnData);
            Panel.removeClass(panelSkins).addClass('panel-' + btnData);
            pageHeader.removeClass().addClass('text-' + btnData);
          });

          $(options).each(function(i, e) {
            if ($(e).hasClass('block')) {
              $(e).removeClass().addClass('block mt15 option option-' + btnData);
            } else {
              $(e).removeClass().addClass('option option-' + btnData);
            }
          });

          // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
          $('body').find('.ui-slider').each(function(i, e) {
            $(e).addClass('slider-primary');
          });

          $(switches).each(function(i, ele) {
            if ($(ele).hasClass('switch-round')) {
              if ($(ele).hasClass('block')) {
                $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
              } else {
                $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
              }
            } else {
              if ($(ele).hasClass('block')) {
                $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
              } else {
                $(ele).removeClass().addClass('switch switch-' + btnData);
              }
            }
          });
          buttons.removeClass().addClass('button btn-' + btnData);
        });

        setTimeout(function() {
          adminForm.addClass('theme-primary');
          Panel.addClass('panel-primary');
          pageHeader.addClass('text-primary');

          $(options).each(function(i, e) {
            if ($(e).hasClass('block')) {
              $(e).removeClass().addClass('block mt15 option option-primary');
            } else {
              $(e).removeClass().addClass('option option-primary');
            }
          });

          // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
          $('body').find('.ui-slider').each(function(i, e) {
            $(e).addClass('slider-primary');
          });

          $(switches).each(function(i, ele) {
            if ($(ele).hasClass('switch-round')) {
              if ($(ele).hasClass('block')) {
                $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
              } else {
                $(ele).removeClass().addClass('switch switch-round switch-primary');
              }
            } else {
              if ($(ele).hasClass('block')) {
                $(ele).removeClass().addClass('block mt15 switch switch-primary');
              } else {
                $(ele).removeClass().addClass('switch switch-primary');
              }
            }
          });
          buttons.removeClass().addClass('button btn-primary');
        }, 800);
      });
    </script> <!-- END: PAGE SCRIPTS -->
  </body>
</html>