<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$funcoes = new funcoes;
$validacoes = new validacoes;
$func_analise = new func_analise;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
//  Função para includes reconhecer a página e modificar sua exibição.
$aprovacaologs = true;
$id_pro = (int)$_GET['id_pro'];
// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

if ( $id_pro > 0 ) {
  //  Listando informações sobre o processo.
  $consulta = $configuracoes->consulta("SELECT 
    processos.id_pro,
    processos.endereco,
    processos.quadra,
    processos.lote,
    processos.numero,
    processos.bairro,
    processos.cidade,
    processos.estado,
    processos.tipoprocesso,
    processos.datahora, 
    processos.situacaoprojeto 

    FROM 

    processos.processos 

    WHERE processos.id_pro = $id_pro and processos.ativo = true");
    $linha2 = $consulta->fetch();

  //  Listando informações sobre responsável técnico.
  $con_listagem_redimensionamento = $configuracoes->consulta("SELECT 
    processos_redimensionamento.id_red,
    processos_redimensionamento.tipo,
    processos_redimensionamento.areatotalterreno,
    processos_redimensionamento.sa_qtdlotes,  
    processos_redimensionamento.sp_qtdlotes,
    processos_redimensionamento.descricaolotes,
    processos_redimensionamento.requerente,
    processos_redimensionamento.requerentetelefone

    FROM 
    processos.processos_redimensionamento

    WHERE processos.processos_redimensionamento.id_pro = $id_pro");

    $listagem_redimensionamento = $con_listagem_redimensionamento->fetch();

    $id_red = $listagem_redimensionamento['id_red'];
    // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
}

if ( $_POST['formulario'] == 'ok' ) {
  $id_ana = (int)$_POST['id_ana'];

  $situacaoprojeto = $_POST['situacaoprojeto'];
  $obsgerais = $_POST['obsgerais'];
  $valor1 = $_POST['valor1'];
  $valor2 = $_POST['valor2'];
  $valor3 = $_POST['valor3'];
  $valor4 = $_POST['valor4'];
  $valor5 = $_POST['valor5'];
  $valor6 = $_POST['valor6'];
  $valor7 = $_POST['valor7'];
  $valor8 = $_POST['valor8'];

  if ( $id_ana > 0 ) {
    $formulario = 'alteracao';
  } else {
    $formulario = 'cadastro';
  } 
  

  $id_ana2 = $func_analise->manipulacoes(
      $id_ana,
      $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'),
      $id_pro,
      'ch',
      $situacaoprojeto,
      $obsgerais,
      $descricao_img1,   
      $descricao_img2,   
      $descricao_img3,   
      $descricao_img4,   
      $descricao_img5,
      $valor1,   
      $valor2,   
      $valor3,   
      $valor4,   
      $valor5,   
      $valor6,   
      $valor7,   
      $valor8,   
      f,   
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,
      $formulario
    );

    if ( $id_ana == 0 ) {
      $id_ana = $id_ana2;
    }

    //  Pega nome e e-mail do cliente e usuário.
    $tusuario = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos

      INNER JOIN geral.cg ON cg.id_cg = processos.id_cg
      WHERE processos.id_pro = $id_pro");
    $tusuario = $tusuario->fetch();

    $tautor = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos_analise

      INNER JOIN geral.cg ON cg.id_cg = processos_analise.id_cg
      WHERE processos_analise.id_ana = $id_ana");
    $tautor = $tautor->fetch();

    //--Procedimento envio de e-mail (usando Mailjet)--

    if ( $tipoprocesso == 1 ) {
      $descr_tipoprocesso = 'Alvará de Construção';
    } else if ( $tipoprocesso == 2 ) {
      $descr_tipoprocesso = 'Alvará de Regularização de Obras';
    } else if ( $tipoprocesso == 3 ) {
      $descr_tipoprocesso = 'Alvará de Acréscimo de Área';
    } else if ( $tipoprocesso == 4 ) {
      $descr_tipoprocesso = 'Condomínio Edilício';
    } else if ( $tipoprocesso == 5 ) {
      if ( $listagem_redimensionamento['tipo'] == 1 ) {
        $descr_tipoprocesso = 'Redimensionamento - Remembramento';
      } else if ( $listagem_redimensionamento['tipo'] == 2 ) {
        $descr_tipoprocesso = 'Redimensionamento - Desmembramento';
      }
    }

    /*
      1 - A ser analisado = white, 
      2 - Em análise = #ddd, 
      3 - Pendência de documentos ou correção = #c4d79b, 
      4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
      5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
      6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
      7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
      8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
      9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
      10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
      11 - Aprovado = #eada6d, 
      12 - Dispensar Vistoria
    */

    if ( $situacaoprojeto == 1 ) {
      $descr_situacaoprojeto = 'A ser analisado '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 2 ) {
      $descr_situacaoprojeto = 'Documentos checados e encaminhados para vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 3 ) {
      $descr_situacaoprojeto = 'Insuficiente - Pendência de Documentos ou correção - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 4 ) {
      $descr_situacaoprojeto = 'Reprovado - Processo não permitido ou reprovado na análise/vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 5 ) {
      $descr_situacaoprojeto = 'Processo encaminhado à procuradoria - Dúvida na vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 6 ) {
      $descr_situacaoprojeto = 'Processo encaminhado ao departamento imobiliário - Dúvida na vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 7 ) {
       $descr_situacaoprojeto = 'Aprovado - Pendente de pagamento de taxa para elaboração do decreto - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 8 ) {
      $descr_situacaoprojeto = 'Aprovado - Fazer decreto e pegar assinatura - com taxa paga - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 9 ) {
      $descr_situacaoprojeto = 'Aprovado - Decreto assinado - para entregar - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 10 ) {
      $descr_situacaoprojeto = 'Aprovado - Processo finalizado e decreto entregue'.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 11 ) {
      $descr_situacaoprojeto = 'Aprovado'.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 12 ) {
      $descr_situacaoprojeto = 'Documentos encaminhados para análise - Vistoria dispensada - '.date('d/m/Y H:i');
    }

    require 'vendor/autoload.php';

    $resources = new \Mailjet\Resources();

    $api1 = 'f74843b63a568c5d7116cff5d971eb45';
    $api2 = '06fed94a47fa0688ef15fd36453ec365';

    $mj = new \Mailjet\Client($api1, $api2);
    $body = [
        'FromEmail' => "nao-responda@valleteclab.com",
        'FromName' => "Prefeitura de Luís Eduardo Magalhães - XPlanejamento",
        'Subject' => 'Checagem de Processo [ '.$descr_tipoprocesso.' ]',
        'Html-part' => '[ E-mail resposta, por favor não responda esse e-mail ] <br />
          Prezado(a) Senhor(a),<br />
          <br /><br />
          Status de processo de <strong>'.$descr_tipoprocesso.'</strong>, Protocolo: '.$id_pro.'<br /> 
          <strong>Situação do projeto:</strong><br />
          '.$descr_situacaoprojeto.'
          <br />
          <strong>Observações</strong><br />
          '.$obsgerais.'
          <br />
          <br />
          www.luiseduardomagalhaes.ba.gov.br/planejamento <br />
          <br /><br />
          Agradecemos o contato!',
        'Recipients' => [
            [
                'Email' => "".$tusuario['email'].""
            ]
        ]
    ];
    $response = $mj->post($resources::$Email, ['body' => $body]);
    $response->success() && var_dump($response->getData());
    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Procedimento efetuado com sucesso!');
              window.open('listar_processos.php?tipoprocesso=5','_self');
          }, 1000);
        });
    </script>
    <?php
}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia" />
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a>Checagem</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">
      
                <form method="post" id="admin-form">
                <input type="hidden" name="formulario" value="ok">
                  <div class="panel-body bg-light">

            <?php include_once('includes/info_redimensionamento.php'); ?>

            <div class="section-divider col-md-12"><span>Checagem - Consta nos documentos entregues para análise:</span></div>
            
                    <!-- .section-divider -->
                    <!-- inicio formulário -->
                    <div class="section row">
                      <div class="col-md-12">
                        <strong>Consta nos documentos entregues para análise:</strong>
                      </div>
                    </div>

                    <?php 
                      //  Listando informações sobre responsável técnico.
                      $con_listagem_analise = $configuracoes->consulta("SELECT 
                        processos_analise.id_ana,
                        processos_analise.id_pro,
                        processos_analise.tipo,
                        processos_analise.situacaoprojeto,
                        processos_analise.obsgerais,
                        processos_analise.valor1,
                        processos_analise.valor2,
                        processos_analise.valor3,
                        processos_analise.valor4,
                        processos_analise.valor5,
                        processos_analise.valor6,
                        processos_analise.valor7,
                        processos_analise.valor8
                                               

                        FROM 
                        processos.processos_analise

                        WHERE processos.processos_analise.id_pro = $id_pro and tipo = 'ch'");

                        $listagem_analise = $con_listagem_analise->fetch();

                        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
                    ?>
                    <input type="hidden" name="id_ana" value="<?php echo $listagem_analise['id_ana']; ?>">
                    <div class="section row">
                      <div class="col-md-6">Projeto de Edificações (Arquitetônico) - 1 (uma)cópia para aprovação;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option field option-primary">
                                <input name="valor1" type="radio" value="t"<?php if ( $listagem_analise['valor1'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option field option-primary">
                                <input name="valor1" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor1'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div> 
                    </div>
                     <div class="section row">
                      <div class="col-md-6">Memorial Descritivo da Obra - 1 (uma) cópia para aprovação;;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor2" type="radio" value="t"<?php if ( $listagem_analise['valor2'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor2" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor2'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-12">
                        <em>*Após a análise do projeto e consequente aprovação do mesmo, o requerente deverá providenciar 3 cópias do projeto e memorial;</em>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Título de propriedade ou documento que comprove a justa posse (escritura ou contrato de compra e venda);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor3" type="radio" value="t"<?php if ( $listagem_analise['valor3'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor3" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor3'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div> 
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Certidão negativa do proprietário (imóvel);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor4" type="radio" value="t"<?php if ( $listagem_analise['valor4'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor4" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor4'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Cópia de documentos pessoais do proprietário (RG e CPF);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor5" type="radio" value="t"<?php if ( $listagem_analise['valor5'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor5" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor5'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div> 
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Cópia da(s) ART de autoria do projeto e responsabilidade técnica;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor6" type="radio" value="t"<?php if ( $listagem_analise['valor6'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor6" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor6'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Memorial Descritivo da Obra - 1 (uma) cópia para aprovação;;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor7" type="radio" value="t"<?php if ( $listagem_analise['valor7'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor7" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor7'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div> 
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Certidão negativa do profissional autor do projeto e responsável técnico (contribuinte);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input name="valor8" type="radio" value="t"<?php if ( $listagem_analise['valor8'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input name="valor8" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor8'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-12">
                        <strong>Situação do projeto:</strong>
                      </div>
                    </div>
                    <div class="option-group field"> 
                      <div class="section row">
                          <div class="col-md-3"> 
                              <label class="option option-primary">
                              <input name="situacaoprojeto" type="radio" value="2" <?php if ( $listagem_analise['situacaoprojeto'] == 2 ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Encaminhar para vistoria</label>
                          </div>
                      </div>
                      <div class="section row">
                          <div class="col-md-5">                       
                              <label class="option option-primary">
                              <input name="situacaoprojeto" type="radio" value="3" <?php if ( $listagem_analise['situacaoprojeto'] == 3 ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Pendência de documentos ou correção</label>
                          </div>
                      </div>
                    </div><br /><br />
                    <div class="section" id="spy3">
                      <label for="comment" class="field prepend-icon">
                        <textarea class="gui-textarea" id="obsgerais" name="obsgerais"><?php echo $listagem_analise['obsgerais']; ?></textarea>
                        <label for="comment" class="field-icon">
                          <i class="fa fa-comments"></i>
                        </label>
                      </label>
                    </div>

                    <div class="panel-footer text-left">
                      <button type="submit" class="button btn-success">Atualizar</button>
                    </div>
                    </form>
            </div>
            <div id="sidebar-right-tab2" class="tab-pane"></div>
            <div id="sidebar-right-tab3" class="tab-pane"></div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    function ap30(){ 
      if ( $('input[name=checkocultdetalhes]').is(':checked')) {
        $("#camposocultosmaisdetana").fadeIn(500);
      }else{
        $("#camposocultosmaisdetana").css("display","none");
      } 
    }
    function aplogs($acionador,$objeto){ 
      if ( $('input[name='+$acionador+']').is(':checked')) {
        $("#"+$objeto).fadeIn(500);
      }else{
        $("#"+$objeto).css("display","none");
      } 
    }
    function apfotos(){ 
      if ( $('input[name=checkocultofotos]').is(':checked')) {
        $("#boxfotos").fadeIn(500);
      }else{
        $("#boxfotos").css("display","none");
      } 
    }
  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        valor1: {
          required: true
        },
        valor2: {
          required: true
        },
        valor3: {
          required: true
        },
        valor4: {
          required: true
        },
        valor5: {
          required: true
        },
        valor6: {
          required: true
        },
        valor7: {
          required: true
        },
        valor8: {
          required: true
        },
        status: {
          required: true
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        valor1: {
          required: 'Marque uma opção'
        },
        valor2: {
          required: 'Marque uma opção'
        },
        valor3: {
          required: 'Marque uma opção'
        },
        valor4: {
          required: 'Marque uma opção'
        },
        valor5: {
          required: 'Marque uma opção'
        },
        valor6: {
          required: 'Marque uma opção'
        },
        valor7: {
          required: 'Marque uma opção'
        },
        valor8: {
          required: 'Marque uma opção'
        },
        status: {
          required: 'Defina a situação do projeto'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>