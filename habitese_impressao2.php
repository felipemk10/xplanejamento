<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

$id_pro = (int)$_GET['id_pro'];
$id_ah = (int)$_GET['id_ah'];

// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

if ( $id_pro > 0 ) {
  //  Listando informações sobre o processo.
  $consulta = $configuracoes->consulta("SELECT 
    processos_analise.id_ana,
    processos_analise.id_pro,
    processos_analise.tipo,
    processos_analise.situacaoprojeto,
    processos_analise.obsgerais,
    processos_analise.descricao_img1,
    processos_analise.descricao_img2,
    processos_analise.descricao_img3,
    processos_analise.descricao_img4,
    processos_analise.descricao_img5
                           

    FROM 
    processos.processos_analise

    WHERE processos.processos_analise.id_pro = $id_pro and tipo = 'fi'");
    $linha = $consulta->fetch();

    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();

      $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
        id_pro,
        -- 1 = Alvará, 2 = Habite-se
        tipo,
        numero,   
        id_cg,
        ano,
        id_ah2,
        id_que,
        substituir,
        datahora

        FROM 

        processos.processos_alvarahabitese 

        WHERE  processos_alvarahabitese.id_pro = $id_pro and processos_alvarahabitese.id_ah = $id_ah");
      
      
      $linha_alvarahabitese = $consulta_alvarahabitese->fetch();

      //  Listando informações sobre responsável técnico.
      $con_listagem_alvara = $configuracoes->consulta("SELECT 
        processos_alvara.id_alv,
        processos_alvara.finalidadeobra,
        processos_alvara.areaterreno,
        processos_alvara.situacaoterreno,
        processos_alvara.desmembramento,
        processos_alvara.taxapermeabilidade

        FROM 
        processos.processos_alvara

        WHERE processos.processos_alvara.id_pro = $id_pro");

        $listagem_alvara = $con_listagem_alvara->fetch();

        $id_alv = $listagem_alvara['id_alv'];
        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial

        /* Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
          pat = a construir
          sbc = área sub solo a construir
          are = área Existente
          sbe = área sub solo existente
        */

        if ( $tipoprocesso != 4 ) { 
          $con_listagem_pavimento = $configuracoes->consulta("SELECT 
            processos_pavimentacao.id_pav,
            processos_pavimentacao.area,
            processos_pavimentacao.tipo

            FROM 
            processos.processos_pavimentacao

            WHERE processos.processos_pavimentacao.id_alv = $id_alv 
            and (processos.processos_pavimentacao.tipo = 'pat' 
            or processos.processos_pavimentacao.tipo = 'sbc'
            or processos.processos_pavimentacao.tipo = 'are' 
            or processos.processos_pavimentacao.tipo = 'sbe') 
            ORDER BY processos.processos_pavimentacao.id_pav ASC");

          $total_contruir = 0;
          $total_pat = 0;
          $total_sbc = 0;
          $total_are = 0;
          $total_sbe = 0;

          $total_patare_terreo = 0;
          $i = 0;
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
              
              $total_contruir = $total_contruir + $listagem_pavimento['area'];

              if ( $listagem_pavimento['tipo']  == 'pat' ) {
                $total_pat = $total_pat+$listagem_pavimento['area'];
                if ( $i == 0 ) {
                  $total_patare_terreo = $listagem_pavimento['area'];
                  $i++;
                }
              }
              if (  $listagem_pavimento['tipo']  == 'sbc' ) {
                $total_sbc = $total_sbc+$listagem_pavimento['area'];
              }
              if (  $listagem_pavimento['tipo']  == 'are' ) {
                $total_are = $total_are+$listagem_pavimento['area'];
                 if ( $i == 1 ) {
                  $total_patare_terreo = $total_patare_terreo + $listagem_pavimento['area'];
                  $i++;
                }
              }
              if (  $listagem_pavimento['tipo']  == 'sbe' ) {
                $total_sbe = $total_sbe+$listagem_pavimento['area'];
              }
          }

          // Como a variável é usada em outras funções abaixo, criei uma nova para não perder o valor afim de colocar o mesmo no subtotal no rodapé do relatório.
          $total_pat2 = $total_pat;
        }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
    <meta name="description" content="TemplateMonster - A Responsive HTML5 Admin UI Framework">
    <meta name="author" content="TemplateMonster">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
  </head>
  <body class="impressao" style="font-size: 12px;">
    <br>
    <br>
    <br>
      <div class="section row">
        <div class="col-xs-offset-8 print-no-view">
          <div class="col-xs-2"></div>
          <button type="button" onclick="window.print();">Imprimir</div>
          </button>
        </div>
      </div>
      <div class="section row">
        <div class="col-xs-12 text-center"><br><br><br>
          <span class="titulo">SECRETARIA DE PLANEJAMENTO, ORÇAMENTO E GESTÃO</span>
        </div>
      </div><!-- fim da section row -->
      <br>
      <br>
      <br><div class="section row">
      <div class="col-xs-10 col-xs-offset-1">
        <div class="section row conteudo-1 ">
          <div class="col-xs-1 pull-left">
            <label class="select">
              <select id="estado" name="estado">
                <option value="">Selecione</option>
                <option>Habite-se </option>
                <option>Habite-se parcial</option>
              </select>
              <i class="arrow"></i>
            </label>
          </div>
        </div>
        <br>
        <br>
        <div class="section row">
          <div class="col-xs-9 border-xs">
            <strong>Número</strong>
          </div><!--t Título do campo -->
          <div class="col-xs-3 border-xs">
            <strong>Data</strong>
          </div><!--t Título do campo -->
        </div>
        <div class="section row" style="font-size: 18px;">
          <div class="col-xs-9 border-xs">
            <strong><?php echo $linha_alvarahabitese['numero'].' / '.$linha_alvarahabitese['ano']; ?></strong>
          </div><!-- conteudo texto -->
          <div class="col-xs-3 border-xs">
            <strong><?php echo $formatacoes->formatar_data('/',$linha_alvarahabitese['datahora']); ?></strong>
          </div><!--Conteúdo texto -->
        </div><!-- Fim bloco -->

        <?php 
          if ( (int)$linha_alvarahabitese['substituir'] > 0 ) {

            $substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$linha_alvarahabitese['substituir']." and tipo = ".$linha_alvarahabitese['tipo']."")->fetch(); ?>
          <br />
          <div class="section row">
            <div class="col-xs-12 border-xs">
              <strong>Este documento substitui o habite-se existente de número</strong>
            </div><!--t Título do campo -->
          </div>
          <div class="section row">
            <div class="col-xs-12 border-xs" style="font-size: 11px;">

              <strong>Número: <?php echo $linha_alvarahabitese['substituir'].' / '.$substituto['ano']; ?></strong>
            </div><!-- conteudo texto -->
          </div>
          <br />
        <?php } ?>

        <?php 
      //  Listando informações sobre os proprietários.
      $con_listagem_proprietario = $configuracoes->consulta("SELECT 
        cg.nome,
        cg.cpfcnpj

        FROM 
        processos.processos_proprietario

        INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

        WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

        foreach ( $con_listagem_proprietario as $listagem_proprietario ) { 
        ?>
        <div class="section row">
          <div class="col-xs-9 border-xs">
            <strong>Nome</strong>
          </div><!--t Título do campo-1 -->
          <div class="col-xs-3 border-xs">
            <strong><?php 
              if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
                echo 'CPF';
              } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
                echo 'CNPJ';
              }
        ?></strong>
          </div><!--t Título do campo-2 -->
        </div>
        <div class="section row">
          <div class="col-xs-9 border-xs">
            <?php echo strtoupper($listagem_proprietario['nome']); ?>
          </div><!-- conteudo texto-1 -->
          <div class="col-xs-3 border-xs">
            <?php 
              if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
                echo $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'###.###.###-##');
              } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
                echo $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'##.###.###/####-##');
              }
            ?>
          </div><!--Conteúdo texto-2 -->
        </div><!-- Fim bloco -->
        <?php } ?>
        
        <br>
        <br>
         <div class="section row">
          <div class="col-xs-6 border-xs">
            <strong>Endereço</strong>
          </div><!--t Título do campo-1 -->
          <div class="col-xs-2 border-xs">
            <strong>Quadra</strong>
          </div><!--t Título do campo-2 -->
            <div class="col-xs-2 border-xs">
            <strong>Número</strong>
          </div><!--t Título do campo-3 -->
            <div class="col-xs-2 border-xs">
            <strong>Lote</strong>
          </div><!--t Título do campo-4-->
        </div>
        <div class="section row">
          <div class="col-xs-6 border-xs">
            <?php echo $linha2['endereco']; ?>
          </div><!-- conteudo texto-1 -->
          <div class="col-xs-2 border-xs">
            <?php echo $linha2['quadra']; ?>
          </div><!--Conteúdo texto-2 -->
          <div class="col-xs-2 border-xs">
            <?php echo $linha2['numero']; ?>
          </div><!--Conteúdo texto-3 -->
          <div class="col-xs-2 border-xs">
            <?php echo $linha2['lote']; ?>
          </div><!--Conteúdo texto-4 -->
        </div><!-- Fim bloco -->
        <div class="section row">
          <div class="col-xs-9 border-xs">
            <strong>Bairro</strong>
          </div><!--t Título do campo-1 -->
          <div class="col-xs-3 border-xs">
            <strong>Cidade</strong>
          </div><!--t Título do campo-2 -->
        </div>
        <div class="section row">
          <div class="col-xs-9 border-xs">
            <?php echo $linha2['bairro']; ?>
          </div><!-- conteudo texto-1 -->
          <div class="col-xs-3 border-xs">
            Luís Eduardo Magalhães - BA
          </div><!--Conteúdo texto-2 -->
        </div><!-- Fim bloco -->
        <br>
        <br>


        <?php if ( $tipoprocesso >= 1 and $tipoprocesso <= 3 ) { 
            include_once('includes/impressao/habitese_alvara.php');
          } else if ( $tipoprocesso == 4 ) { 
            include_once('includes/impressao/habitese_condominio.php');
          } else if ( $tipoprocesso == 5 ) { 
            //include_once('includes/impressao/info_redimensionamento.php');
          }
        ?>

            <?php /*A edifição que tem como finalidade o uso <strong>RESIDENCIAL EM CONDOMÍNIO</strong>  , totalizando uma área construída de <<strong>64,86 m²</strong>,  além de estar em conformidade com a legislação municipal vigente, encontra-se de acordo com o(s) Alvará(s)
            de <strong>REGULARIZAÇÃO</strong>  , número(s) <strong>00051 / 2017</strong>*/ ?>
            <br>
            <br>
            <label class="option option-primary">Observações
              <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
              <div id="boxobs" style="display: none;">
                <span class="checkbox"></span></label>
                <br>
                <br>
                <textarea class="text-area" cols="112" rows="5"></textarea>
                <br>
                <br>
              </div>
          
          </div><!-- conteudo texto -->
        </div><!-- Fim bloco -->
        <br>
        <br>
        <br>
        <div class="section row border-xs ">
          <div class="col-xs-12 text-center padding-rodape">
            <div class="section row">
              <div class=" assinatura col-xs-offset-3 col-xs-6"></div>
            </div>
          </div>
          <div class="section row text-center ">
            <strong><!--Secretaria de Planejamento, Orçamento e Gestão--></strong>
          </div>
        </div>
        <br>
        <br>
        <br>
       </div><!-- fim da div conteudo -->        
      </div><!-- fim da row -->

      
        </body>
    <style>

    @media print {
      body {font-size: 11px!important;}
    .conteudo-1{border-style: none!important;}
    .border-xs { border-style: none!important;}
    .print-no-view {display: none!important; }
    .assinatura { border-bottom: 1px solid black!important;
      border-bottom: 1px solid black; }
    .text-area {
      width: 620px!important;
      height: 50px!important;
    }
    }
    .padding-rodape{
      padding: 30px;
    }

    .text-area {
      width: 1060px;
      height: 100px;
    }

    .assinatura { border-bottom: 1px solid black!important;}

    .border-xs {
      border: 1px solid rgb(211,211,211);
      padding: 10px;  

    }
    .conteudo-1 {
      background-color: rgba(211,211,211, 0.3);
      border: 1px solid rgb(211,211,211);
     
      border-radius: 5px;
      padding: 10px;

    }

    .impressao{
    background: #fff;
    color: #393939;
    font-size: 15px;



    }

    .titulo {
      font-weight: 600;
      font-size: 15px;

      
    }

    .titulo-sec {
        font-weight: 600;
        font-size: 12px;
        margin-left: 64px;

    }
    .subtitulo {
        font-size: 12px;
        font-weight: bold;
        margin-top: 10px; 


    }

    .campo-texto {

      font-size: 11px;
      border-bottom: #666 1px solid;
      margin-top: 10px;  
    }

    .nota {
      font-size: 13px;
    } 
    </style>
      <!-- jQuery -->
      <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
      <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
      <script src="js/functions.js"></script>
      <script type="text/javascript">
        function apobs(){ 
          if ( $('input[name=checkocultobs]').is(':checked')) {
              $("#boxobs").fadeIn(500);
            }else{
              $("#boxobs").css("display","none");
            } 
        }
      </script>
</html>