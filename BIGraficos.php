<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require 'vendor/autoload.php';
session_start();

$autenticacao       = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

$manipuladores  = new Biblioteca\manipuladores;

$usuarios           = new \Biblioteca\usuarios;
$tipousuarioLogado  = $usuarios->getUsuarioLogado($manipuladores->criptografia($_SESSION['id_usuario'] ,'base64','decode'))->tipousuario;

$processos      = new \Biblioteca\processos;

$processos->setTipoProcesso($_GET['tipoprocesso']);
$processos->setSituacaoProjeto((int)$_GET['situacaoprojeto']);

$tipoprocesso       = $processos->getTipoProcesso();
$situacaoprojeto    = $processos->getSituacaoProjeto();

$tipousuarioLogado  = $usuarios->getUsuarioLogado($manipuladores->criptografia($_SESSION['id_usuario'] ,'base64','decode'))->tipousuario;

$usuarioBI          = $usuarios->getUsuarioLogado($manipuladores->criptografia($_SESSION['id_usuario'] ,'base64','decode'))->bi;

// Parâmetros.
$dia30anterior = date('Y-m-d', strtotime('-30 days', strtotime(date('Y-m-d'))));

if($_GET[ano_periodo]) $ano_periodo = $_GET[ano_periodo];//Irving
else $ano_periodo = date('Y');//Irving
?>
<!DOCTYPE html>
<html>

<head>
 <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

  <!-- Datatables Editor Addon CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/extensions/Editor/css/dataTables.editor.css">

  <!-- Datatables ColReorder Addon CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

  <link rel="stylesheet" href="css/simplePagination.css" />
</head>

<body class="charts-highcharts-page" data-spy="scroll" data-target="#nav-spy" data-offset="100">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header2.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="dashboard.html">Dashboard</a>
            </li>
            <li class="crumb-trail">
              <a href="dashboard.html">Dashboard</a>
            </li>
          </ol>
        </div>
        <div class="topbar-right">
          <div class="ib topbar-dropdown">
            <label for="topbar-multiple" class="control-label fs16">Reporting Period:</label>
            <select id="topbar-multiple" class="hidden">
              <optgroup label="Filter By:">
                <option value="1-1">Last 30 Days</option>
                <option value="1-2" selected="selected">Last 60 Days</option>
                <option value="1-3">Last Year</option>
              </optgroup>
            </select>
          </div>
          <div class="ml30 ib va-m" id="toggle_sidemenu_r">
            <a href="#" class="pl5">
              <i class="fa fa-align-right fs17"></i>
            </a>
          </div>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray320" data-tray-height="match">

          <h4 class="mt5 mb20"> Gŕaficos e Relatórios -<small>"BI"</small></h4>

          <div id="nav-spy">
            <ul class="nav tray-nav" data-smoothscroll="-70" data-spy="affix" data-offset-top="200">
              <li>
                <a href="#pchart3">
                  <span class="fa fa-line-chart fa-lg"></span> Emissões de Alvará e Habite-se</a>
              </li>
              <li>
                <a href="#pchart8">
                  <span class="fa fa-bar-chart fa-lg"></span> Processos aprovados</a>
              </li>
              <li>
                <a href="#pchart9">
                  <span class="fa fa-line-chart fa-lg"></span> Processos cadastrados</a>
              </li>
            </ul>
          </div>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

          <!-- Filterable Column Chart -->
          <div class="panel" id="pchart3">
            <div class="panel-heading"><center>
              <span class="panel-title text-info fw600">
                <i class="fa fa-pencil hidden"></i>Emissões de Alvará e Habite-se</span>
              </center>
            </div>
<!--Irving - 13/02/2020-->
<form action="" method="get" id="form_ano">
<b>Ano: </b><input class="btn-primary" type="number" id="ano_periodo" value='<?php echo $ano_periodo; ?>' name="ano_periodo" min="2012" max="<?php echo date('Y'); ?>">
<input class="btn btn-sm btn-primary" type="submit" value="Mudar"></input> 
</form>
<!-- --- -->
            <div class="panel-menu hidden">
              <div class="chart-legend" data-chart-id="#high-line">
                <button type="button" data-chart-id="0" class="legend-item btn btn-sm btn-primary mr10">Data 1</button>
                <button type="button" data-chart-id="1" class="legend-item btn btn-info btn-sm">Data 2</button>
              </div>
            </div>
            <div class="panel-body pn">
              <div id="high-line" style="width: 100%; height: 275px; margin: 0 auto"></div>

              <div class="p15 pt5 mt15 br-t">
                <div class="table-responsive">
                  <table class="table mbn admin-form fs13 table-legend" data-chart-id="#high-line">
                    <tbody>
                      <tr>
                        <td>
                          <label class="switch switch-custom block mbn">
                            <input type="checkbox" class="legend-switch" name="features" id="s1" value="0">
                            <label for="s1" data-on="ON" data-off="OFF"></label>
                            <span></span>
                          </label>
                        </td>
                        <td class="fs15 fw600 text-left">
                          Alvarás
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="switch switch-custom block mbn">
                            <input type="checkbox" class="legend-switch" name="features" id="s2" value="1">
                            <label for="s2" data-on="ON" data-off="OFF"></label>
                            <span></span>
                          </label>
                        </td>
                        <td class="fs15 fw600 text-left">
                          Alvarás de regularização
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <label class="switch switch-custom block mbn">
                            <input type="checkbox" class="legend-switch" name="features" id="s3" value="2">
                            <label for="s3" data-on="ON" data-off="OFF"></label>
                            <span></span>
                          </label>
                        </td>
                        <td class="fs15 fw600 text-left">
                          Habite-se
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <!-- Stats Top Graph Bot -->
          <div class="panel" id="pchart8">
            <div class="panel-heading">
              <center><span class="panel-title text-info fw600">
                <i class="fa fa-pencil hidden"></i> Processos aprovados</span>
            </div></center>
            <div class="panel-menu">
              <div class="chart-legend" data-chart-id="#high-line3">
                <a type="button" data-chart-id="0" class="legend-item btn btn-sm btn-primary mr10">Data 1</a>
                <a type="button" data-chart-id="1" class="legend-item btn btn-sm btn-primary mr10">Data 2</a>
                <a type="button" data-chart-id="2" class="legend-item btn btn-sm btn-primary mr10">Data 2</a>
                <a type="button" data-chart-id="3" class="legend-item btn btn-sm btn-primary mr10">Data 2</a>
                <a type="button" data-chart-id="4" class="legend-item btn btn-sm btn-primary mr10">Data 2</a>
              </div>
            </div>
            <div class="panel-body pn">
              <div id="high-line3" style="width: 100%; height: 210px; margin: 0 auto"></div>
            </div>
          </div>

          <!-- Column Graph -->
          <div class="panel" id="pchart9">
            <div class="panel-heading">
              <span class="panel-title text-info fw600">
                <i class="fa fa-pencil hidden"></i> Processos cadastrados</span>
            </div>
            <div class="panel-menu pn bg-dark">
              <ul class="nav nav-justified text-center fw600 chart-legend" data-chart-id="#high-column3">
                <li>
                  <a href="#" class="legend-item active" data-chart-id="0"> Construção </a>
                </li>
                <li class="br-l">
                  <a href="#" class="legend-item active" data-chart-id="1"> Regularização </a>
                </li>
                <li class="br-l">
                  <a href="#" class="legend-item active" data-chart-id="2"> Condomínio </a>
                </li>
                <li class="br-l">
                  <a href="#" class="legend-item active" data-chart-id="3"> Acrés. Àrea </a>
                </li>
                <li class="br-l">
                  <a href="#" class="legend-item active" data-chart-id="4"> Redimensionamento </a>
                </li>
              </ul>

            </div>
            <div class="panel-body pbn">
              <div id="high-column3" style="width: 100%; height: 400px; margin: 0 auto"></div>
            </div>
          </div>

        </div>
        <!-- end: .tray-center -->

      </section>
      <!-- End: Content -->

    </section>

    <!-- Start: Right Sidebar -->
    <aside id="sidebar_right" class="nano affix">

      <!-- Start: Sidebar Right Content -->
      <div class="sidebar-right-content nano-content">

        <div class="tab-block sidebar-block br-n">
          <ul class="nav nav-tabs tabs-border nav-justified hidden">
            <li class="active">
              <a href="#sidebar-right-tab1" data-toggle="tab">Tab 1</a>
            </li>
            <li>
              <a href="#sidebar-right-tab2" data-toggle="tab">Tab 2</a>
            </li>
            <li>
              <a href="#sidebar-right-tab3" data-toggle="tab">Tab 3</a>
            </li>
          </ul>
          <div class="tab-content br-n">
            <div id="sidebar-right-tab1" class="tab-pane active">

              <h5 class="title-divider text-muted mb20"> Server Statistics
                <span class="pull-right"> 2013
                  <i class="fa fa-caret-down ml5"></i>
                </span>
              </h5>
              <div class="progress mh5">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 44%">
                  <span class="fs11">DB Request</span>
                </div>
              </div>
              <div class="progress mh5">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 84%">
                  <span class="fs11 text-left">Server Load</span>
                </div>
              </div>
              <div class="progress mh5">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 61%">
                  <span class="fs11 text-left">Server Connections</span>
                </div>
              </div>

              <h5 class="title-divider text-muted mt30 mb10">Traffic Margins</h5>
              <div class="row">
                <div class="col-xs-5">
                  <h3 class="text-primary mn pl5">132</h3>
                </div>
                <div class="col-xs-7 text-right">
                  <h3 class="text-success-dark mn">
                    <i class="fa fa-caret-up"></i> 13.2% </h3>
                </div>
              </div>

              <h5 class="title-divider text-muted mt25 mb10">Database Request</h5>
              <div class="row">
                <div class="col-xs-5">
                  <h3 class="text-primary mn pl5">212</h3>
                </div>
                <div class="col-xs-7 text-right">
                  <h3 class="text-success-dark mn">
                    <i class="fa fa-caret-up"></i> 25.6% </h3>
                </div>
              </div>

              <h5 class="title-divider text-muted mt25 mb10">Server Response</h5>
              <div class="row">
                <div class="col-xs-5">
                  <h3 class="text-primary mn pl5">82.5</h3>
                </div>
                <div class="col-xs-7 text-right">
                  <h3 class="text-danger mn">
                    <i class="fa fa-caret-down"></i> 17.9% </h3>
                </div>
              </div>

              <h5 class="title-divider text-muted mt40 mb20"> Server Statistics
                <span class="pull-right text-primary fw600">USA</span>
              </h5>


            </div>
            <div id="sidebar-right-tab2" class="tab-pane"></div>
            <div id="sidebar-right-tab3" class="tab-pane"></div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->


  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Sparkline Local -->
  <script src="vendor/plugins/sparkline/jquery.sparkline.min.js"></script>

  <!-- Charts JS -->
  <script src="vendor/plugins/highcharts/highcharts.js"></script>
  <script src="vendor/plugins/circles/circles.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script src="assets/js/demo/charts/highcharts.js"></script>

  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    Demo.init();

    


    'use strict'; 
    /*! main.js - v0.1.1
    * http://templatemonster.com/
    * Copyright (c) 2015 Admin Designs;*/


    /* Demo theme functions. Required for
     * Settings Pane and misc functions */
    var demoHighCharts = function () {

            // Define chart color patterns
            var highColors = [bgWarning, bgPrimary, bgInfo, bgAlert,
                bgDanger, bgSuccess, bgSystem, bgDark
            ];

            // Color Library we used to grab a random color
            var sparkColors = {
                "primary": [bgPrimary, bgPrimaryLr, bgPrimaryDr],
                "info": [bgInfo, bgInfoLr, bgInfoDr],
                "warning": [bgWarning, bgWarningLr, bgWarningDr],
                "success": [bgSuccess, bgSuccessLr, bgSuccessDr],
                "alert": [bgAlert, bgAlertLr, bgAlertDr]
            };


            // Sparklines Demo
            var demoSparklines = function() {

                 var sparkLine = $('.inlinesparkline');
                 // Init Sparklines
                 if (sparkLine.length) {

                    var sparklineInit = function() {
                            $('.inlinesparkline').each(function(i, e) {
                                var This = $(this);
                                var Color = sparkColors["primary"];
                                var Height = '35';
                                var Width = '70%';
                                This.children().remove();
                                // default color is "primary"
                                // Color[0] = default shade
                                // Color[1] = light shade
                                // Color[2] = dark shade
                                //alert('hi')
                                // User assigned color and height, else default
                                var userColor = This.data('spark-color');
                                var userHeight = This.data('spark-height');
                                if (userColor) {
                                    Color = sparkColors[userColor];
                                }
                                if (userHeight) {
                                    Height = userHeight;
                                }
                                $(e).sparkline('html', {
                                    type: 'line',
                                    width: Width,
                                    height: Height,
                                    enableTagOptions: true,
                                    lineColor: Color[2], // Also tooltip icon color
                                    fillColor: Color[1],
                                    spotColor: Color[0],
                                    minSpotColor: Color[0],
                                    maxSpotColor: Color[0],
                                    highlightSpotColor: bgWarningDr,
                                    highlightLineColor: bgWarningLr
                                });
                            });
                    }

                    // Refresh Sparklines on Resize
                    var refreshSparklines;

                    $(window).resize(function(e) {
                        clearTimeout(refreshSparklines);
                        refreshSparklines = setTimeout(sparklineInit, 500);
                    });

                    sparklineInit();
                }

            }// End Sparklines Demo


            // Circle Graphs Demo
            var demoCircleGraphs = function() {
                var infoCircle = $('.info-circle');
                if (infoCircle.length) {
                    // Color Library we used to grab a random color
                    var colors = {
                        "primary": [bgPrimary, bgPrimaryLr,
                            bgPrimaryDr
                        ],
                        "info": [bgInfo, bgInfoLr, bgInfoDr],
                        "warning": [bgWarning, bgWarningLr,
                            bgWarningDr
                        ],
                        "success": [bgSuccess, bgSuccessLr,
                            bgSuccessDr
                        ],
                        "alert": [bgAlert, bgAlertLr, bgAlertDr]
                    };
                    // Store all circles
                    var circles = [];
                    infoCircle.each(function(i, e) {
                        // Define default color
                        var color = ['#DDD', bgPrimary];
                        // Modify color if user has defined one
                        var targetColor = $(e).data(
                            'circle-color');
                        if (targetColor) {
                            var color = ['#DDD', colors[
                                targetColor][0]]
                        }
                        // Create all circles
                        var circle = Circles.create({
                            id: $(e).attr('id'),
                            value: $(e).attr('value'),
                            radius: $(e).width() / 2,
                            width: 14,
                            colors: color,
                            text: function(value) {
                                var title = $(e).attr('title');
                                if (title) {
                                    return '<h2 class="circle-text-value">' + value + '</h2><p>' + title + '</p>' 
                                } 
                                else {
                                    return '<h2 class="circle-text-value mb5">' + value + '</h2>'
                                }
                            }
                        });
                        circles.push(circle);
                    });

                    // Add debounced responsive functionality
                    var rescale = function() { 
                        infoCircle.each(function(i, e) {
                            var getWidth = $(e).width() / 2;
                            circles[i].updateRadius(
                                getWidth);
                        });
                        setTimeout(function() {
                            // Add responsive font sizing functionality
                            $('.info-circle').find('.circle-text-value').fitText(0.4);
                        },50);
                    } 
                    var lazyLayout = _.debounce(rescale, 300);
                    $(window).resize(lazyLayout);
                  
                }

            } // End Circle Graphs Demo



            // High Charts Demo
            var demoHighCharts = function() {


                // Column Charts
                var demoHighColumns = function() {

                     var column1 = $('#high-column');

                     if (column1.length) {
                        // Column Chart 1
                        $('#high-column').highcharts({
                            credits: false,
                            colors: highColors,
                            chart: {
                                backgroundColor: 'transparent',
                                type: 'column',
                                padding: 0,
                                margin: 0,
                                marginTop: 10
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                lineWidth: 0,
                                tickLength: 0,
                                minorTickLength: 0,
                                title: {
                                    text: null
                                },
                                labels: {
                                    enabled: false
                                }
                            },
                            yAxis: {
                                gridLineWidth: 0,
                                title: {
                                    text: null
                                },
                                labels: {
                                    enabled: false
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    groupPadding: 0.05,
                                    pointPadding: 0.25,
                                    borderWidth: 0
                                }
                            },
                            series: [{
                                name: 'Behance',
                                data: [30]
                            }, {
                                name: 'Twitter',
                                data: [60]
                            }, {
                                name: 'Facebook',
                                data: [90]
                            }, {
                                name: 'Dribble',
                                data: [120]
                            }]
                        });
                     }
                        
                     var column3 = $('#high-column3');
                     
                     if (column3.length) {

                        // Column Chart3
                        $('#high-column3').highcharts({
                            credits: false,
                            colors: highColors,
                            chart: {
                                type: 'column',
                                padding: 0,
                                spacingTop: 10,
                                marginTop: 100,
                                marginLeft: 30,
                                marginRight: 30
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: '',
                                style: {
                                    fontSize: 20,
                                    fontWeight: 600
                                }
                            },
                            subtitle: {
                                text: 'Dados dos últimos 30 dias',
                                style: {
                                    color: '#AAA'
                                }
                            },
                            xAxis: {
                                lineWidth: 0,
                                tickLength: 0,
                                title: {
                                    text: null
                                },
                                labels: {
                                    enabled: false
                                },
                            },
                            yAxis: {
                                gridLineWidth: 0,
                                title: {
                                    text: null
                                },
                                labels: {
                                    enabled: false
                                }
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:10px"></span><table>',
                                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    '<td style="padding:0"> <b>{point.y}</b></td></tr>',
                                footerFormat: '</table>',
                                shared: true,
                                useHTML: true
                            },
                            plotOptions: {
                                column: {
                                    colorByPoint: true,
                                    colors: [bgPrimary, bgPrimary,
                                        bgInfo, bgInfo
                                    ],
                                    groupPadding: 0,
                                    pointPadding: 0.24,
                                    borderWidth: 0
                                }
                            },

                            <?php

                              $consulta = $processos->consulta("SELECT count(*) as total, tipoprocesso FROM processos.processos Where datahora >= '".$dia30anterior."' group by tipoprocesso ORDER BY tipoprocesso ASC");
                              $dados = array();
                              foreach ( $consulta as $listagem ) {
                                array_push($dados, $listagem->total);
                              }

                            ?>


                            series: [{
                                visible: true,
                                name: 'Construção',
                                data: [<?php echo $dados[0]; ?>]
                            }, {
                                visible: true,
                                name: 'Regularização',
                                data: [<?php echo $dados[1]; ?>]
                            }, {
                                visible: true,
                                name: 'Condomínio',
                                data: [<?php echo $dados[2]; ?>]
                            }, {
                                visible: true,
                                name: 'Acrés. Àrea',
                                data: [<?php echo $dados[3]; ?>]
                            }, {
                                visible: true,
                                name: 'Redimensionamento',
                                data: [<?php echo $dados[4]; ?>]
                            }],
                            dataLabels: {
                                enabled: true,
                                rotation: 0,
                                color: '#AAA',
                                align: 'center',
                                x: 0,
                                y: -8,
                            }
                        });
                    }


                } // End High Columns

                var demoHighBars = function() {

                     var bars1 = $('#high-bars');
                     
                     if (bars1.length) {

                        // Bar Chart 1
                        $('#high-bars').highcharts({
                            colors: [bgWarning,bgPrimary,bgInfo],
                            credits: false,
                            legend: {
                                enabled: false,
                                y: -5,
                                verticalAlign: 'top',
                                useHTML: true
                            },
                            chart: {
                                spacingLeft: 30,
                                type: 'bar',
                                marginBottom: 0,
                                marginTop: 0
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                showEmpty: false,
                                tickLength: 80,
                                lineColor: '#EEE',
                                tickColor: '#EEE',
                                offset: 1,
                                categories: ['TV', 'Radio'],
                                title: {
                                    text: null
                                },
                                labels: {
                                    align: 'right',
                                }
                            },
                            yAxis: {
                                min: 0,
                                gridLineWidth: 0,
                                showEmpty: false,
                                title: {
                                    text: null
                                },
                                labels: {
                                    enabled: false,
                                }
                            },
                            tooltip: {
                                valueSuffix: ' millions'
                            },
                            plotOptions: {
                                bar: {}
                            },
                            series: [{
                                id: 0,
                                name: 'Viewers',
                                data: [100, 100]
                            }, {
                                id: 1,
                                name: 'Women',
                                data: [36, 55]
                            }, {
                                id: 2,
                                name: 'Men',
                                data: [65, 45]
                            }]
                        });
                    }
                }

                var demoHighLines = function() {

                    var line1 = $('#high-line');
                     
                    if (line1.length) {

                        // High Line 1
                        $('#high-line').highcharts({
                            credits: false,
                            colors: highColors,
                            chart: {
                                type: 'column',
                                zoomType: 'x',
                                panning: true,
                                panKey: 'shift',
                                marginRight: 50,
                                marginTop: -5,
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                gridLineColor: '#EEE',
                                lineColor: '#EEE',
                                tickColor: '#EEE',
                                categories: ['Jan', 'Fev', 'Mar', 'Abr',
                                    'Mai', 'Jun', 'Jul', 'Ago',
                                    'Set', 'Out', 'Nov', 'Dez'
                                ]
                            },
                            yAxis: {
                                min: -2,
                                tickInterval: 5,
                                gridLineColor: '#EEE',
                                title: {
                                    text: 'Quantidade',
                                    style: {
                                        color: bgInfo,
                                        fontWeight: '600'
                                    }
                                }
                            },
                            plotOptions: {
                                spline: {
                                    lineWidth: 3,
                                },
                                area: {
                                    fillOpacity: 0.2
                                }
                            },
                            legend: {
                                enabled: false,
                            },
                            <?php
                              $consulta = $processos->consulta("select to_char(datahora,'Mon') as mon,
                                     extract(year from datahora) as yyyy,
                                     tipo as tipo,
                                     count(*) as total
                              from processos.processos_alvarahabitese

                              WHERE  EXTRACT(year FROM datahora ) = $ano_periodo
                              group by 1,2,3");

                              $dados = array();
                              foreach ( $consulta as $listagem ) {
                                if ( $listagem->tipo == 1  ) {
                                  $dados['alvara'][$listagem->mon] += $listagem->total;
                                } else if ( $listagem->tipo == 3  ) {
                                  $dados['alvarareg'][$listagem->mon] += $listagem->total;
                                } else if ( $listagem->tipo == 2 ) {
                                  $dados['habite'][$listagem->mon] += $listagem->total;
                                }
                              }
                            ?>
                            series: [{
                                name: 'Alvará',
                                data: [<?php echo $dados['alvara']['Jan']; ?>, 
                                        <?php echo $dados['alvara']['Feb']; ?>, 
                                        <?php echo $dados['alvara']['Mar']; ?>, 
                                        <?php echo $dados['alvara']['Apr']; ?>, 
                                        <?php echo $dados['alvara']['May']; ?>, 
                                        <?php echo $dados['alvara']['Jun']; ?>, 
                                        <?php echo $dados['alvara']['Jul']; ?>, 
                                        <?php echo $dados['alvara']['Aug']; ?>, 
                                        <?php echo $dados['alvara']['Sep']; ?>, 
                                        <?php echo $dados['alvara']['Oct']; ?>, 
                                        <?php echo $dados['alvara']['Nov']; ?>, 
                                        <?php echo $dados['alvara']['Dec']; ?>]
                            }, {
                                name: 'Alvará Regularização',
                                data: [<?php echo $dados['alvarareg']['Jan']; ?>, 
                                        <?php echo $dados['alvarareg']['Feb']; ?>, 
                                        <?php echo $dados['alvarareg']['Mar']; ?>, 
                                        <?php echo $dados['alvarareg']['Apr']; ?>, 
                                        <?php echo $dados['alvarareg']['May']; ?>, 
                                        <?php echo $dados['alvarareg']['Jun']; ?>, 
                                        <?php echo $dados['alvarareg']['Jul']; ?>, 
                                        <?php echo $dados['alvarareg']['Aug']; ?>, 
                                        <?php echo $dados['alvarareg']['Sep']; ?>, 
                                        <?php echo $dados['alvarareg']['Oct']; ?>, 
                                        <?php echo $dados['alvarareg']['Nov']; ?>, 
                                        <?php echo $dados['alvarareg']['Dec']; ?>]
                            }, {
                                name: 'Habite-se',
                                data: [<?php echo $dados['habite']['Jan']; ?>, 
                                        <?php echo $dados['habite']['Feb']; ?>, 
                                        <?php echo $dados['habite']['Mar']; ?>, 
                                        <?php echo $dados['habite']['Apr']; ?>, 
                                        <?php echo $dados['habite']['May']; ?>, 
                                        <?php echo $dados['habite']['Jun']; ?>, 
                                        <?php echo $dados['habite']['Jul']; ?>, 
                                        <?php echo $dados['habite']['Aug']; ?>, 
                                        <?php echo $dados['habite']['Sep']; ?>, 
                                        <?php echo $dados['habite']['Oct']; ?>, 
                                        <?php echo $dados['habite']['Nov']; ?>, 
                                        <?php echo $dados['habite']['Dec']; ?>]
                            }]
                        });

                    }

                    var line2 = $('#high-line2');
                     
                    if (line2.length) {

                        // High Line 2
                        $('#high-line2').highcharts({
                            credits: false,
                            colors: highColors,
                            chart: {
                                type: 'column',
                                zoomType: 'x',
                                panning: true,
                                panKey: 'shift',
                                marginRight: 50,
                                marginTop: -5,
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                gridLineColor: '#EEE',
                                lineColor: '#EEE',
                                tickColor: '#EEE',
                                categories: ['Jan', 'Fev', 'Mar', 'Abr',
                                    'Mai', 'Jun', 'Jul', 'Ago',
                                    'Set', 'Out', 'Nov', 'Dez'
                                ]
                            },
                            yAxis: {
                                min: -2,
                                tickInterval: 5,
                                gridLineColor: '#EEE',
                                title: {
                                    text: 'Quantidade',
                                    style: {
                                        color: bgInfo,
                                        fontWeight: '600'
                                    }
                                }
                            },
                            plotOptions: {
                                spline: {
                                    lineWidth: 3,
                                },
                                area: {
                                    fillOpacity: 0.2
                                }
                            },
                            legend: {
                                enabled: false,
                            },
                            
                            series: [{
                                name: 'Yahoo',
                                data: [7.0, 6, 9, 14, 18, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                            }, {
                                name: 'CNN',
                                data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                            }, {
                                visible: false,
                                name: 'Yahoo',
                                data: [1, 5, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                            }, {
                                visible: false,
                                name: 'Facebook',
                                data: [3, 1, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
                            }, {
                                visible: false,
                                name: 'Facebook',
                                data: [7.0, 6, 9, 14, 18, 21.5, 25.2, 26.5, 23.3, 18.3,13.9, 9.6]
                            }]
                        });

                    }

                    var line3 = $('#high-line3');
                     
                    if (line3.length) {

                        // High Line 3
                        $('#high-line3').highcharts({
                            credits: false,
                            colors: highColors,
                            chart: {
                                backgroundColor: '#f9f9f9',
                                className: 'br-r',
                                type: 'line',
                                zoomType: 'x',
                                panning: true,
                                panKey: 'shift',
                                marginTop: 25,
                                marginRight: 1,
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                gridLineColor: '#EEE',
                                lineColor: '#EEE',
                                tickColor: '#EEE',
                                categories: ['Jan', 'Fev', 'Mar', 'Abr',
                                    'Mai', 'Jun', 'Jul', 'Ago',
                                    'Set', 'Out', 'Nov', 'Dez'
                                ]
                            },
                            yAxis: {
                                min: 0,
                                tickInterval: 5,
                                gridLineColor: '#EEE',
                                title: {
                                    text: 'Quantidade',
                                }
                            },
                            plotOptions: {
                                spline: {
                                    lineWidth: 3,
                                },
                                area: {
                                    fillOpacity: 0.2
                                }
                            },
                            legend: {
                                enabled: true,
                            },

                            <?php

                              $consulta = $processos->consulta("select to_char(datahora,'Mon') as mon,
                                     extract(year from datahora) as yyyy,
                                     tipoprocesso as tipo,
                                     count(*) as Total
                              from processos.processos

                              WHERE situacaoprojeto = 11 and EXTRACT(year FROM datahora ) = ".$ano_periodo." 
                              group by 1,2,3");

                              $dados = array();
                              foreach ( $consulta as $listagem ) {
                                if ( $listagem->tipo == 1  ) {
                                  $dados['construcao'][$listagem->mon] += $listagem->total;
                                } else if ( $listagem->tipo == 2  ) {
                                  $dados['regularizacao'][$listagem->mon] += $listagem->total;
                                } else if ( $listagem->tipo == 3 ) {
                                  $dados['acresarea'][$listagem->mon] += $listagem->total;
                                } else if ( $listagem->tipo == 4 ) {
                                  $dados['condominio'][$listagem->mon] += $listagem->total;
                                } else if ( $listagem->tipo == 5 ) {
                                  $dados['redimensionamento'][$listagem->mon] += $listagem->total;
                                } 
                              }
                            
                            ?>

                            series: [{
                                name: 'Construção',
                                data: [<?php if ( !empty($dados['construcao']['Jan']) ) { echo $dados['construcao']['Jan']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Feb']) ) { echo $dados['construcao']['Feb']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Mar']) ) { echo $dados['construcao']['Mar']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Apr']) ) { echo $dados['construcao']['Apr']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['May']) ) { echo $dados['construcao']['May']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Jun']) ) { echo $dados['construcao']['Jun']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Jul']) ) { echo $dados['construcao']['Jul']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Aug']) ) { echo $dados['construcao']['Aug']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Sep']) ) { echo $dados['construcao']['Sep']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Oct']) ) { echo $dados['construcao']['Oct']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Nov']) ) { echo $dados['construcao']['Nov']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['construcao']['Dec']) ) { echo $dados['regularizacao']['Dec']; } else { echo 'null'; } ?>]
                            }, {
                                name: 'Regularização',
                                data: [<?php if ( !empty($dados['regularizacao']['Jan']) ) { echo $dados['regularizacao']['Jan']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Feb']) ) { echo $dados['regularizacao']['Feb']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Mar']) ) { echo $dados['regularizacao']['Mar']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Apr']) ) { echo $dados['regularizacao']['Apr']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['May']) ) { echo $dados['regularizacao']['May']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Jun']) ) { echo $dados['regularizacao']['Jun']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Jul']) ) { echo $dados['regularizacao']['Jul']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Aug']) ) { echo $dados['regularizacao']['Aug']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Sep']) ) { echo $dados['regularizacao']['Sep']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Oct']) ) { echo $dados['regularizacao']['Oct']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Nov']) ) { echo $dados['regularizacao']['Nov']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['regularizacao']['Dec']) ) { echo $dados['regularizacao']['Dec']; } else { echo 'null'; } ?>]
                            }, {
                                name: 'Acŕes. Àrea',
                                data: [<?php if ( !empty($dados['acresarea']['Jan']) ) { echo $dados['acresarea']['Jan']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Feb']) ) { echo $dados['acresarea']['Feb']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Mar']) ) { echo $dados['acresarea']['Mar']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Apr']) ) { echo $dados['acresarea']['Apr']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['May']) ) { echo $dados['acresarea']['May']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Jun']) ) { echo $dados['acresarea']['Jun']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Jul']) ) { echo $dados['acresarea']['Jul']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Aug']) ) { echo $dados['acresarea']['Aug']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Sep']) ) { echo $dados['acresarea']['Sep']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Oct']) ) { echo $dados['acresarea']['Oct']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Nov']) ) { echo $dados['acresarea']['Nov']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['acresarea']['Dec']) ) { echo $dados['acresarea']['Dec']; } else { echo 'null'; } ?>]
                            }, {
                                name: 'Condomínio',
                                data: [<?php if ( !empty($dados['condominio']['Jan']) ) { echo $dados['condominio']['Jan']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Feb']) ) { echo $dados['condominio']['Feb']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Mar']) ) { echo $dados['condominio']['Mar']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Apr']) ) { echo $dados['condominio']['Apr']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['May']) ) { echo $dados['condominio']['May']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Jun']) ) { echo $dados['condominio']['Jun']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Jul']) ) { echo $dados['condominio']['Jul']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Aug']) ) { echo $dados['condominio']['Aug']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Sep']) ) { echo $dados['condominio']['Sep']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Oct']) ) { echo $dados['condominio']['Oct']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Nov']) ) { echo $dados['condominio']['Nov']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['condominio']['Dec']) ) { echo $dados['condominio']['Dec']; } else { echo 'null'; } ?>]
                            }, {
                                name: 'Redimensionamento',
                                data: [<?php if ( !empty($dados['redimensionamento']['Jan']) ) { echo $dados['redimensionamento']['Jan']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Feb']) ) { echo $dados['redimensionamento']['Feb']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Mar']) ) { echo $dados['redimensionamento']['Mar']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Apr']) ) { echo $dados['redimensionamento']['Apr']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['May']) ) { echo $dados['redimensionamento']['May']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Jun']) ) { echo $dados['redimensionamento']['Jun']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Jul']) ) { echo $dados['redimensionamento']['Jul']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Aug']) ) { echo $dados['redimensionamento']['Aug']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Sep']) ) { echo $dados['redimensionamento']['Sep']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Oct']) ) { echo $dados['redimensionamento']['Oct']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Nov']) ) { echo $dados['redimensionamento']['Nov']; } else { echo 'null'; } ?>, 
                                        <?php if ( !empty($dados['redimensionamento']['Dec']) ) { echo $dados['redimensionamento']['Dec']; } else { echo 'null'; } ?>]
                            }]
                        });

                    }

                } // End High Line Charts Demo

                // Pie Charts
                var demoHighPies = function() { 

                    var pie1 = $('#high-pie');
                     
                    if (pie1.length) {

                        // Pie Chart1
                        $('#high-pie').highcharts({
                            credits: false,
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false
                            },
                            title: {
                                text: null
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    center: ['30%', '50%'],
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: false
                                    },
                                    showInLegend: true
                                }
                            },
                            colors: highColors,
                            legend: {
                                x: 90,
                                floating: true,
                                verticalAlign: "middle",
                                layout: "vertical",
                                itemMarginTop: 10
                            },
                            series: [{
                                type: 'pie',
                                name: 'Porcentagem',
                                data: [
                                    ['Construção', 35.0],
                                    ['Regularização', 36.8],
                                    ['Acŕes. Àrea', 36.8],
                                    ['Condominio', 18.5],
                                    ['Redimensionamento', 18.5],
                                ]
                            }]
                        });
                    }
                } // End High Pie Charts Demo

                // Demo High Area Charts
                var demoHighAreas = function() {

                    var area1 = $('#high-area');
                     
                    if (area1.length) {

                        // Area 1
                        $('#high-area').highcharts({
                            colors: [bgWarning, bgPrimary, bgInfo],
                            credits: false,
                            chart: {
                                type: 'areaspline',
                                spacing: 0,
                                margin: -10
                            },
                            title: {
                                text: null
                            },
                            legend: {
                                enabled: false
                            },
                            xAxis: {
                                allowDecimals: false,
                                tickColor: '#EEE',
                                labels: {
                                    formatter: function() {
                                        return this.value; // clean, unformatted number for year
                                    }
                                }
                            },
                            yAxis: {
                                title: {
                                    text: null
                                },
                                gridLineColor: 'transparent',
                                labels: {
                                    enabled: false,
                                }
                            },
                            plotOptions: {
                                areaspline: {
                                    fillOpacity: 0.25,
                                    marker: {
                                        enabled: true,
                                        symbol: 'circle',
                                        radius: 2,
                                        states: {
                                            hover: {
                                                enabled: true
                                            }
                                        }
                                    }
                                }
                            },
                            series: [{
                                id: 0,
                                name: 'USA',
                                data: [150, 260, 80, 100, 150,200, 240]
                            }, {
                                id: 1,
                                name: 'Russia',
                                data: [10, 20, 40, 120, 240, 180, 160]
                            }, {
                                id: 2,
                                name: 'China',
                                data: [60, 100, 180, 110, 100, 20, 40]
                            }]
                        });
                    }
                }

                // Init Chart Types
                demoHighColumns();
                demoHighLines();
                demoHighBars();
                demoHighPies();
                demoHighAreas();

            } // End Demo HighCharts


            // High Charts Demo
            var demoHighChartMenus = function() {

               // Create custom menus for charts associated
               // with the ".chart-legend" element
               var chartLegend = $('.chart-legend');
                     
                if (chartLegend.length) {

                    $('.chart-legend').each(function(i, ele) {
                        var legendID = $(ele).data('chart-id');
                        $(ele).find('a.legend-item').each(function(
                            i, e) {
                            var This = $(e);
                            var itemID = This.data(
                                'chart-id');
                            // Use ID of menu to find what chart it belongs to
                            // Then use ID of its child menu items to find out what
                            // data on the chart it is connected to
                            var legend = $(legendID).highcharts()
                                .series[itemID];
                            // pull legend name from chart and populate menu buttons
                            var legendName = legend.name;
                            This.html(legendName);
                            // assign click handler which toggles legend data 
                            This.click(function(e) {
                                if (This.attr(
                                    'href')) {
                                    e.preventDefault();
                                }
                                if (legend.visible) {
                                    legend.hide();
                                    This.toggleClass(
                                        'active'
                                    );
                                } else {
                                    legend.show();
                                    This.toggleClass(
                                        'active'
                                    );
                                }
                            });
                        });
                    });
                }

                // Create custom menus for table charts
                var tableLegend = $('.table-legend');
                     
                if (tableLegend.length) {

                    $('.table-legend').each(function(i, e) {
                        var legendID = $(e).data('chart-id');
                        $(e).find('input.legend-switch').each(
                            function(i, e) {
                                var This = $(e);
                                var itemID = This.val();
                                // Use ID of menu to find what chart it belongs to
                                // Then use ID of its child menu items to find out what
                                // data on the chart it is connected to
                                var legend = $(legendID).highcharts()
                                    .series[itemID];
                                // pull legend name from chart and populate menu buttons
                                var legendName = legend.name;
                                This.html(legendName);
                                // Toggle checkbox state based on series visability
                                if (legend.visible) {
                                    This.attr('checked', true);
                                } else {
                                    This.attr('checked', false);
                                }
                                // assign click handler which toggles legend data 
                                This.on('click', function(i, e) {
                                    if (legend.visible) {
                                        legend.hide();
                                        This.attr(
                                            'checked',
                                            false);
                                    } else {
                                        legend.show();
                                        This.attr(
                                            'checked',
                                            true);
                                    }
                                });
                        });
                    });
                }

            } // End Demo HighChart Menus


      return {
            init: function () {

                // Init Demo Charts 
                demoSparklines();
                demoCircleGraphs();
                demoHighCharts();
                demoHighChartMenus();

            }
      } 
    }();

    // This page contains more Initilization Javascript than normal.
    // As a result it has its own js page. See charts.js for more info
    demoHighCharts.init();
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>

</html>