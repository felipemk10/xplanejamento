<form method="post" id="admin-form" action="javascript: formularios_dinamicos('#admin-form', 'backend/removerFoto.php', '#carregando', '#formResult', 's', '');">
<input type="hidden" name="id_pro" value="<?php echo $_GET['id_pro']; ?>">
<input type="hidden" name="id_foto" value="<?php echo $_GET['id_foto']; ?>">
<div class="modal-dialog modal-lg">

  <div class="modal-content">
  
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"></span></button>
      <h1 class="modal-title">Deseja remover a foto <strong><?php echo $_GET['id_foto']; ?></strong></h4>
    </div>
    
    <div class="modal-body">
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Sim</button>
      </div>
  </div><!--/.modal-content-->
</div><!-- /.modal-dialog-->
</form>