<form method="post" id="admin-form" action="javascript: formularios_dinamicos('#admin-form', 'backend/alt_processos.php', '#carregando', '#formResult', 's', '');">
<input type="hidden" name="id_pro" value="<?php echo $_GET['id_pro']; ?>">
<div class="modal-dialog modal-lg">

  <div class="modal-content">
  
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"></span></button>
      <h1 class="modal-title">Alteração tipo do processo <strong><?php echo $_GET['id_pro']; ?></strong></h4>
    </div>
    
    <div class="modal-body">
    
    
      <div class="panel-body bg-light">

        <div class="section row">
          <div class="col-md-3" align="center" style="text-align: center;">
              <label class="select">
                <select id="tipoprocesso" name="tipoprocesso">
                  <option <?php if ( $_GET['tipoprocesso'] == 1 ) { ?>selected<?php } ?> value="1">Alvará de Construção</option>
                  <option <?php if ( $_GET['tipoprocesso'] == 2 ) { ?>selected<?php } ?> value="2">Alvará de Regularização</option>
                  <option <?php if ( $_GET['tipoprocesso'] == 3 ) { ?>selected<?php } ?> value="3">Alvará de Acréscimo</option>
                  <option <?php if ( $_GET['tipoprocesso'] == 6 ) { ?>selected<?php } ?> value="6">Alvará de Projeto</option>
                </select>
                <i class="arrow"></i>
              </label>
          </div>
        </div>

      </div>
       
       
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-primary">Alterar</button>
      </div>

    
  </div><!--/.modal-content-->
</div><!-- /.modal-dialog-->
</form>