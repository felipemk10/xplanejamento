<?php
 //  Listando informações sobre responsável técnico.
  $con_listagem_redimensionamento = $configuracoes->consulta("SELECT 
    processos_redimensionamento.id_red,
    processos_redimensionamento.tipo,
    processos_redimensionamento.areatotalterreno,
    processos_redimensionamento.sa_qtdlotes,  
    processos_redimensionamento.sp_qtdlotes,
    processos_redimensionamento.descricaolotes,
    processos_redimensionamento.requerente,
    processos_redimensionamento.requerentetelefone

    FROM 
    processos.processos_redimensionamento

    WHERE processos.processos_redimensionamento.id_pro = $id_pro");

    $listagem_redimensionamento = $con_listagem_redimensionamento->fetch();

    $id_red = $listagem_redimensionamento['id_red'];
?>                    


<div class="section-divider mt20 mb40">
                      <span> Dados do Processo</span>
                    </div>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->
                    
                
                  
                    <div class="section row">
                      <div class="col-md-4">
                        <a style="color:white;" target="_blank" href="protocolo_impressao.php?id_pro=<?php echo $linha2['id_pro']; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>"><button type="button" class="btn btn-sm btn-system btn-block" style="width:150px;">Imprimir Protocolo</button></a>
                        <br /><br />
                        <label class="field-icon">Protocolo / ID: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['id_pro']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Finalidade do Processo: <span style="border-bottom: #666 1px solid;"><?php 
                              if ( $listagem_redimensionamento['tipo'] == 1 ) {
                                echo 'Remembramento';
                              } else if ( $listagem_redimensionamento['tipo'] == 2 ) {
                                echo 'Desmembramento';
                              }

                         ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Dados do Proprietário:</strong></label>
                      </div>
                    </div>
                    <?php 
                    //  Listando informações sobre os proprietários.
                    $con_listagem_proprietario = $configuracoes->consulta("SELECT 
                      cg.id_cg,
                      cg.nome,
                      cg.email,
                      cg.cpfcnpj

                      FROM 
                      processos.processos_proprietario

                      INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

                      WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

                      foreach ( $con_listagem_proprietario as $listagem_proprietario ) { 
                      ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Nome: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_proprietario['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php 
        if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
          echo 'CPF';
        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
          echo 'CNPJ';
        }
        ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_proprietario['cpfcnpj']; ?></span></label>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Dados do Projeto:</strong></label>
                      </div>
                    </div>
                    
                    <div class="section row">
                      <?php 
                        //  Listando informações sobre autor.
                        $con_listagem_autor = $configuracoes->consulta("SELECT 
                          cg.id_cg,
                          cg.nome,
                          cg.creacau,
                          processos_profissional.tipoprofissional

                          FROM 
                          processos.processos_profissional

                          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

                        foreach ( $con_listagem_autor as $listagem_autor ) {
                      ?>
                      <div class="col-md-5">
                        <label class="field-icon">Autoria: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_autor['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php if ( $listagem_autor['tipoprofissional'] == 'e' ) { echo 'CREA'; } else { echo 'CAU'; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_autor['creacau']; ?></span></label>
                      </div>
                      <?php } ?>
                    </div>

                    <?php 
                        //  Listando informações sobre Resp. Técnico.
                        $con_listagem_resptecnico = $configuracoes->consulta("SELECT 
                          cg.id_cg,
                          cg.nome,
                          cg.creacau,
                          processos_profissional.tipoprofissional

                          FROM 
                          processos.processos_profissional

                          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");

                        if ( $con_listagem_resptecnico->rowCount() > 0 ) {
                      ?>
                    <div class="section row">
                      <?php foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) { ?>
                      <div class="col-md-5">
                        <label class="field-icon">Resp. Técnico: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_resptecnico['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { echo 'CREA'; } else { echo 'CAU'; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_resptecnico['creacau']; ?></span></label>
                      </div>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <hr />
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Endereço: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['endereco']; ?></span></label>
                      </div>
                    </div>
                    <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Quadra: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['quadra']; ?></span></label>
                      </div>
                      <div class="col-md-4">
                        <label class="field-icon">Lote: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['lote']; ?></span></label>
                      </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( $linha2['numero'] > 0 ) { ?>
                      <div class="section row">
                        <div class="col-md-3">
                          <label class="field-icon">Número: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['numero']; ?></span></label>
                        </div>
                      </div>
                    <?php } ?>
                    

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Bairro: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['bairro']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Cidade: Luís Eduardo Magalhães</label>
                      </div>
                    </div>
                    <hr />
                    
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Objeto / Situação Atual:</strong></label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Quantidade de Lotes: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_redimensionamento['sa_qtdlotes']; ?> Unidades</span></label>
                      </div>
                    </div>
                    
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Área do Terreno: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_redimensionamento['areatotalterreno']; ?> m<sup>2</sup></span></label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Objeto / Situação Pretendida:</strong></label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Quantidade de Lotes: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_redimensionamento['sp_qtdlotes']; ?> Unidades</span></label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Descrição dos Lotes: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_redimensionamento['descricaolotes']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Requerente: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_redimensionamento['requerente']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Telefone: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_redimensionamento['requerentetelefone']; ?></span></label>
                      </div>
                    </div>

                    <?php if ( $aprovacaologs ) { ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultofotos" id="checkocultofotos" onclick="apfotos();">
                          <span class="checkbox"></span>Fotos
                        </label>
                      </div>
                    </div>
                    
                    <div id="boxfotos" style="display: none;">
                      <table class="table table-hover" style="font-size: 12px;">
                        <tbody>
                          <?php if (file_exists('vistoria/'.$id_pro.'_1.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_1.jpg" target="_blank">Visualizar Imagem 1</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_2.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_2.jpg" target="_blank">Visualizar Imagem 2</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_3.jpg" target="_blank">Visualizar Imagem 3</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_4.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_4.jpg" target="_blank">Visualizar Imagem 4</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_5.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_5.jpg" target="_blank">Visualizar Imagem 5</a>
                          <?php } ?><br /><br />
                        </tbody>
                        <footer>
                          <tr>
                            <th></th>
                            <th></th>
                          </tr>
                        </footer>
                      </table>
                    </div>
                    
                    <?php include('logs.php'); } ?>
                    
                    <div class="section row">
                      <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultdetalhes" id="checkocultdetalhes" onclick="ap30();">
                          <span class="checkbox"></span>Detalhes
                        </label>
                      </div>
                    </div>
                    <div id="camposocultosmaisdetana" style="display: none;">
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Data de Emissão: <span style="border-bottom: #666 1px solid;"><?php echo $formatacoes->formatar_datahora('/',$linha2['datahora']); ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon">Origem do Cadastro: <span style="border-bottom: #666 1px solid;"><?php 
                          //  Verificando a origem do cadastro
                          $con_checkorigem = $configuracoes->consulta("SELECT 
                            processos.id_pro

                            FROM 
                            processos.processos

                            INNER JOIN geral.usuarios ON usuarios.id_cg = processos.id_cg

                            WHERE processos.processos.id_pro = $id_pro");

                            if ( $con_checkorigem->rowCount() > 0 ) {
                              echo 'COLABORADOR';
                            } else{
                              echo 'ONLINE';
                            }

                          ?></span></label>
                      </div>
                    </div>
            </div>
