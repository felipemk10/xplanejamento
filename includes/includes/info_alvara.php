                    <div class="section-divider mt20 mb40">
                      <span> Dados do Processo</span>
                    </div>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->
                    
                
                  
                    <div class="section row">
                      <div class="col-md-4">
                        <strong><a target="_blank" href="protocolo_impressao.php?id_pro=<?php echo $linha2['id_pro']; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>"><-IMPRIMIR-></a></strong><br /><br />
                        <label class="field-icon">Protocolo / ID: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['id_pro']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-8">
                        <label class="field-icon">Finalidade do Processo: <span style="border-bottom: #666 1px solid;"><?php 
                              if ( $linha2['tipoprocesso'] == 1 ) {
                                echo 'Alvará de Construção';
                              } else if ( $linha2['tipoprocesso'] == 2 ) {
                                echo 'Alvará de Regularização de Obras';
                              } else if ( $linha2['tipoprocesso'] == 3 ) {
                                echo 'Alvará de Acréscimo de Área';
                              } else if ( $linha2['tipoprocesso'] == 4 ) {
                                echo 'Condomínio Edilício';
                              } else if ( $linha2['tipoprocesso'] == 5 ) {
                                echo 'Redimensionamento';
                              }

                         ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Dados do Proprietário:</strong></label>
                      </div>
                    </div>
                    <?php 
                    //  Listando informações sobre os proprietários.
                    $con_listagem_proprietario = $configuracoes->consulta("SELECT 
                      cg.id_cg,
                      cg.nome,
                      cg.email,
                      cg.cpfcnpj

                      FROM 
                      processos.processos_proprietario

                      INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

                      WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

                      foreach ( $con_listagem_proprietario as $listagem_proprietario ) { 
                      ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Nome: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_proprietario['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon">CNPJ: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_proprietario['cpfcnpj']; ?></span></label>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Dados do Projeto:</strong></label>
                      </div>
                    </div>
                    
                    <div class="section row">
                      <?php 
                        //  Listando informações sobre autor.
                        $con_listagem_autor = $configuracoes->consulta("SELECT 
                          cg.id_cg,
                          cg.nome,
                          cg.creacau,
                          processos_profissional.tipoprofissional

                          FROM 
                          processos.processos_profissional

                          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

                        foreach ( $con_listagem_autor as $listagem_autor ) {
                      ?>
                      <div class="col-md-5">
                        <label class="field-icon">Autoria: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_autor['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php if ( $listagem_autor['tipoprofissional'] == 'e' ) { echo 'CREA'; } else { echo 'CAU'; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_autor['creacau']; ?></span></label>
                      </div>
                      <?php } ?>
                    </div>

                    <?php 
                        //  Listando informações sobre Resp. Técnico.
                        $con_listagem_resptecnico = $configuracoes->consulta("SELECT 
                          cg.id_cg,
                          cg.nome,
                          cg.creacau,
                          processos_profissional.tipoprofissional

                          FROM 
                          processos.processos_profissional

                          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");

                        if ( $con_listagem_resptecnico->rowCount() > 0 ) {
                      ?>
                    <div class="section row">
                      <?php foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) { ?>
                      <div class="col-md-5">
                        <label class="field-icon">Resp. Técnico: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_resptecnico['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { echo 'CREA'; } else { echo 'CAU'; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_resptecnico['creacau']; ?></span></label>
                      </div>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <hr />
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Endereço: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['endereco']; ?></span></label>
                      </div>
                    </div>
                    <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Quadra: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['quadra']; ?></span></label>
                      </div>
                      <div class="col-md-4">
                        <label class="field-icon">Lote: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['lote']; ?></span></label>
                      </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( $linha2['numero'] > 0 ) { ?>
                      <div class="section row">
                        <div class="col-md-3">
                          <label class="field-icon">Número: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['numero']; ?></span></label>
                        </div>
                      </div>
                    <?php } ?>
                    

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Bairro: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['bairro']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Cidade: Luís Eduardo Magalhães</label>
                      </div>
                    </div>
                    <hr />
                    <?php 
                      //  Listando informações sobre responsável técnico.
                      $con_listagem_alvara = $configuracoes->consulta("SELECT 
                        processos_alvara.id_alv,
                        processos_alvara.finalidadeobra,
                        processos_alvara.areaterreno,
                        processos_alvara.situacaoterreno,
                        processos_alvara.desmembramento,
                        processos_alvara.taxapermeabilidade

                        FROM 
                        processos.processos_alvara

                        WHERE processos.processos_alvara.id_pro = $id_pro");

                        $listagem_alvara = $con_listagem_alvara->fetch();

                        $id_alv = $listagem_alvara['id_alv'];
                        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
                    ?>
                    <div class="section row">
                      <div class="col-md-8">
                        <label class="field-icon">Finalidade da Obra: <span style="border-bottom: #666 1px solid;"><?php 
                          if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                            echo 'Residencial';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                            echo 'Comercial';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                            echo 'MISTO';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                            echo 'Institucional';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                            echo 'Galpão';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                            echo 'Industrial';
                          }
                        ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Área do Terreno: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_alvara['areaterreno']; ?> m<sup>2</sup></span></label>
                      </div>
                    </div>
                    <?php 
                      /*  Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
                        pat = Pavimento Terreo 
                        are = área Existente
                        sbe = área sub solo existente
                      */
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area,
                        processos_pavimentacao.tipo

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and (processos.processos_pavimentacao.tipo = 'pat' or processos.processos_pavimentacao.tipo = 'are' or processos.processos_pavimentacao.tipo = 'sbe') 
                        ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      $total_contruir = 0;
                      $total_pat = 0;
                      $total_are = 0;
                      $total_sbe = 0;

                      $total_patare_terreo = 0;
                      $i = 0;
                      foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          
                          $total_contruir = $total_contruir + $listagem_pavimento['area'];

                          if ( $listagem_pavimento['tipo']  == 'pat' ) {
                            $total_pat = $total_pat+$listagem_pavimento['area'];
                            if ( $i == 0 ) {
                              $total_patare_terreo = $listagem_pavimento['area'];
                              $i++;
                            }
                          }
                          if (  $listagem_pavimento['tipo']  == 'are' ) {
                            $total_are = $total_are+$listagem_pavimento['area'];
                             if ( $i == 1 ) {
                              $total_patare_terreo = $total_patare_terreo + $listagem_pavimento['area'];
                              $i++;
                            }
                          }
                          if (  $listagem_pavimento['tipo']  == 'sbe' ) {
                            $total_sbe = $total_sbe+$listagem_pavimento['area'];
                          }
                      }
                    ?>
                    <?php if ( $total_pat > 0 ) { ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon"><?php 
                        if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 ) {
                          echo 'Construir';
                        } else if ( $linha2['tipoprocesso'] == 2 ) {
                          echo 'Regularizar';
                        }
                        ?>: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                      </div>
                    </div>
                    <?php } if ( $total_are > 0 ) { ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Área Existente: <span style="border-bottom: #666 1px solid;"><?php echo $total_are; ?> m<sup>2</sup></span></label>
                      </div>
                    </div>
                    <?php } if ( $total_sbe > 0 ) { ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Área Subsolo Existente: <span style="border-bottom: #666 1px solid;"><?php echo $total_sbe; ?> m<sup>2</sup></span></label>
                      </div>
                    </div>
                    <?php } ?>

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Taxa de Ocupação: <span style="border-bottom: #666 1px solid;"><?php echo round((($total_patare_terreo/$listagem_alvara['areaterreno'])*100),2); ?>%</span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Ind. Utilização: <span style="border-bottom: #666 1px solid;"><?php echo round(($total_contruir/$listagem_alvara['areaterreno']),2); ?></span></label>
                      </div>
                    </div>
                    <?php 
                      //  Destruindo variáveis.
                      unset($total_contruir,$total_pat,$total_are,$total_sbe,$total_patare_terreo,$i); ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Taxa de permeabilidade: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_alvara['taxapermeabilidade']; ?>%</span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Situação do Terreno: <span style="border-bottom: #666 1px solid;"><?php 
                          if ( $listagem_alvara['situacaoterreno'] == 'm' ) {
                            echo 'MEIO DE QUADRA';
                          } else if ( $listagem_alvara['situacaoterreno'] == 'e' ) {
                            echo 'ESQUINA';
                          }
                        ?></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Desmembramento: 
                        <?php if ( $listagem_alvara['desmembramento'] == true ) {
                                echo '<span style="border-bottom: #666 1px solid;">Sim</span>';
                              } else {
                                echo '<span style="border-bottom: #666 1px solid;">Não</span>';
                              } ?></label>
                      </div>
                      <?php if ( $listagem_alvara['desmembramento'] == true ) { ?>
                      <div class="col-md-4">
                        <label class="field-icon">Entregue: 
                        <?php if ( $listagem_alvara['desmembramento_entregue'] == true ) {
                                echo '<span style="border-bottom: #666 1px solid;">Sim</span>';
                              } else {
                                echo '<span style="border-bottom: #666 1px solid;">Não</span>';
                              } ?></label>
                      </div>
                      <?php } ?>
                    </div>

                    <?php if ( $aprovacaologs ) { ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultofotos" id="checkocultofotos" onclick="apfotos();">
                          <span class="checkbox"></span>Fotos
                        </label>
                      </div>
                    </div>
                    
                    <div id="boxfotos" style="display: none;">
                      <table class="table table-hover" style="font-size: 12px;">
                        <tbody>
                          <?php if (file_exists('vistoria/'.$id_pro.'_1.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_1.jpg" target="_blank">Visualizar Imagem 1</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_2.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_2.jpg" target="_blank">Visualizar Imagem 2</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_3.jpg" target="_blank">Visualizar Imagem 3</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_4.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_4.jpg" target="_blank">Visualizar Imagem 4</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_5.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_5.jpg" target="_blank">Visualizar Imagem 5</a>
                          <?php } ?><br /><br />
                        </tbody>
                        <footer>
                          <tr>
                            <th></th>
                            <th></th>
                          </tr>
                        </footer>
                      </table>
                    </div>
                    
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultologs" id="checkocultologs" onclick="aplogs();">
                          <span class="checkbox"></span>Logs
                        </label>
                      </div>
                    </div>
                    
                    <div id="boxlogs" style="display: none;">
                      <table class="table table-hover" style="font-size: 12px;">
                        <thead>
                        <tr>
                          <th></th>
                          <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        //  Listando informações sobre as unidades.
                        $con_listagem_log = $configuracoes->consulta("SELECT 
                          log_analise.datahora,   
                          log_analise.situacaoprojeto,   
                          log_analise.obsgerais,
                          log_analise.tipo,
                          cg.nome

                          FROM 
                          logs.log_analise

                          LEFT JOIN geral.cg ON cg.id_cg = log_analise.id_cg

                          WHERE logs.log_analise.id_pro = $id_pro ORDER BY logs.log_analise.datahora ASC");
                          $log_id = 0;
                          foreach ( $con_listagem_log as $listagem_log ) { 
                            $log_id++;

                            /*
                              1 - A ser analisado = white, 
                              2 - Em análise = #ddd, 
                              3 - Pendência de documentos ou correção = #c4d79b, 
                              4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
                              5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
                              6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
                              7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
                              8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
                              9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
                              10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
                              11 - Aprovado = #eada6d, 
                              12 - Dispensar Vistoria
                            */
                              if ( $listagem_log['situacaoprojeto'] == 1 ) {
                                $log_situacao = "A ser analisado";
                              } else if ( $listagem_log['situacaoprojeto'] == 2 ) {
                                $log_situacao = "Em análise";
                              } else if ( $listagem_log['situacaoprojeto'] == 3 ) {
                                $log_situacao = "Pendência de documentos ou correção";
                              } else if ( $listagem_log['situacaoprojeto'] == 4 ) {
                                $log_situacao = "Processo não permitido ou reprovado na análise/vistoria";
                              } else if ( $listagem_log['situacaoprojeto'] == 5 ) {
                                $log_situacao = "Processo encaminhado à procuradoria - Dúvida na vistoria";
                              } else if ( $listagem_log['situacaoprojeto'] == 6 ) {
                                $log_situacao = "Processo encaminhado ao departamento imobiliário - Dúvida na vistoria";
                              } else if ( $listagem_log['situacaoprojeto'] == 7 ) {
                                $log_situacao = "Aprovado - Pendente de pagamento de taxa para elaboração do decreto";
                              } else if ( $listagem_log['situacaoprojeto'] == 8 ) {
                                $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                              } else if ( $listagem_log['situacaoprojeto'] == 9 ) {
                                $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                              } else if ( $listagem_log['situacaoprojeto'] == 10 ) {
                                $log_situacao = "Aprovado - Processo finalizado e decreto entregue";
                              } else if ( $listagem_log['situacaoprojeto'] == 11 ) {
                                $log_situacao = "Aprovado";
                              } else if ( $listagem_log['situacaoprojeto'] == 12 ) {
                                $log_situacao = "Vistoria dispensada - Encaminhada para análise";
                              } else if ( $listagem_log['situacaoprojeto'] == 13 ) {
                                $log_situacao = 'Documentos retirados';
                              } else if ( $listagem_log['situacaoprojeto'] == 14 ) {
                                $log_situacao = 'Documentos entregues';
                              }
                            ?>
                              
                              <tr style="border-top:1px solid green; ">
                                <td align="left">Tipo:</td>
                                <td align="left"><?php 
                                  if ( $listagem_log['tipo'] == 'ch' ) {
                                    echo 'Checagem';
                                  } else if ( $listagem_log['tipo'] == 'po' ) {
                                    echo 'Protocolo';
                                  } else if ( $listagem_log['tipo'] == 'fi' ) {
                                    echo 'Fiscalização';
                                  } else if ( $listagem_log['tipo'] == 'aa' ) {
                                    echo 'Análise';
                                  }

                                 ?></td>
                              </tr>
                              <tr>
                                <td align="left"><?php echo $log_id; ?><sup>o</sup> Etapa:</td>
                                <td align="left"><?php echo $formatacoes->formatar_datahora('/',$listagem_log['datahora']); ?></td>
                              </tr>
                              <tr>
                                <td align="left">Situação:</td>
                                <td align="left"><?php echo $log_situacao; ?></td>
                              </tr>
                              <tr>
                                <td align="left">Autor:</td>
                                <td align="left"><?php echo $listagem_log['nome']; ?></td>
                              </tr>
                              <tr>
                                <td align="left">Observações:</td>
                                <td align="left"><?php echo $listagem_log['obsgerais']; ?></td>
                              </tr>
                              
                          <?php } unset($log_id); ?>
                          </tbody>

                          <footer>
                            <tr>
                              <th></th>
                              <th></th>
                            </tr>
                          </footer>
                        </table>
                    </div>
                    <?php } ?>
                    
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultdetalhes" id="checkocultdetalhes" onclick="ap30();">
                          <span class="checkbox"></span>Detalhes
                        </label>
                      </div>
                    </div>
                    <div id="camposocultosmaisdetana" style="display: none;">
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Data de Emissão: <span style="border-bottom: #666 1px solid;"><?php echo $formatacoes->formatar_datahora('/',$linha2['datahora']); ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon">Origem do Cadastro: <span style="border-bottom: #666 1px solid;">
                        <?php 
                          //  Verificando a origem do cadastro
                          $con_checkorigem = $configuracoes->consulta("SELECT 
                            processos.id_pro

                            FROM 
                            processos.processos

                            INNER JOIN geral.usuarios ON usuarios.id_cg = processos.id_cg

                            WHERE processos.processos.id_pro = $id_pro");

                            if ( $con_checkorigem->rowCount() > 0 ) {
                              echo 'COLABORADOR';
                            } else{
                              echo 'ONLINE';
                            }

                          ?></span></label>
                      </div>
                    </div>
                    <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'pat' ORDER BY processos.processos_pavimentacao.id_pav ASC");
                        $n_pav = 0;
                        $total_pat = 0; ?>

                        <?php if ( $con_listagem_pavimento->rowCount() > 0 ) { ?>
                          <div class="section row">
                            <div class="col-md-2">
                              <label class="field-icon"><strong>Área a Construir</strong></label>
                            </div>
                          </div>
                    <?php 
                        
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo'; } else { echo $n_pav; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                          
                        <?php } ?>
                        <div class="section row">
                          <div class="col-md-5">
                            <label class="field-icon">Total: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                          </div>
                        </div>
                        <?php } ?>
                    <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbc' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="field-icon"><strong>Área Subsolo à Construir</strong></label>
                      </div>
                    </div>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Pavimento <?php echo $n_pav;?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                        <?php } ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Total: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                        <?php } ?>

                      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'are' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="field-icon"><strong>Área Existente</strong></label>
                      </div>
                    </div>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo'; } else { echo $n_pav; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                          
                        <?php } ?>
                        <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Total: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                        <?php } ?>
                      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbe' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="field-icon"><strong>Área Sub Solo Existente</strong></label>
                      </div>
                    </div>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Pavimento <?php echo $n_pav;?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                          
                        <?php } ?>
                        <div class="section row">
                          <div class="col-md-5">
                            <label class="field-icon">Total: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                          </div>
                        </div>
                        <?php } ?>  
                      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'adk' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="field-icon"><strong>Área Deck</strong></label>
                      </div>
                    </div>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Pavimento <?php echo $n_pav;?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                          
                        <?php } ?>
                        <div class="section row">
                          <div class="col-md-5">
                            <label class="field-icon">Total: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                          </div>
                        </div>
                        <?php } ?>
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'apa' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="field-icon"><strong>Área Piscina</strong></label>
                      </div>
                    </div>
                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                            <div class="col-md-5">
                              <label class="field-icon">Pavimento <?php echo $n_pav;?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></span></label>
                            </div>
                          </div>
                          
                        <?php } ?>
                        <div class="section row">
                          <div class="col-md-5">
                            <label class="field-icon">Total: <span style="border-bottom: #666 1px solid;"><?php echo $total_pat; ?> m<sup>2</sup></span></label>
                          </div>
                        </div>
                        <?php } ?>
            </div>