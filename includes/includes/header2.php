<div id="carregando" class="bs-component" style="display: none; left: 45%; position: fixed; top: 40%; z-index: 1000;">
  <div class="alert alert-info alert-dismissable">
  <i class="fa fa-info pr10"></i>
  <a href="#" class="alert-link">Aguarde</a>, estamos enviados as informações.</div>
</div>

<div id="formResultAdd" class="bs-component" style="display: none; left: 45%; position: fixed; top: 40%; z-index: 1000; width: 300px;" >
  <div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" onclick="$('#formResultAdd').hide();">×</button>
    <strong><div id="formResult" style="display: none;"></div></strong>
  </div>
</div>

<header class="navbar navbar-fixed-top navbar-shadow bg-system">
      <div class="navbar-branding">
        <a class="navbar-brand" href="listar_processos.php">
          <b>XPlanejamento</b>
        </a>
        <span id="toggle_sidemenu_l" class="fa fa-align-justify"></span>
      </div>
      <ul class="nav navbar-nav navbar-left">
        <li class="hidden-xs">
          <a class="request-fullscreen toggle-active" href="#">
            <span class="imoon imoon-expand fs16"></span>
          </a>
        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown menu-merge">
          <div class="navbar-btn btn-group">
            <div class="dropdown-menu dropdown-persist w350 animated animated-shorter fadeIn" role="menu">
              <div class="panel mbn">
                
		                <div class="panel-body panel-scroller scroller-navbar pn scroller scroller-active">
		                  <div class="scroller-content">
		                    <div class="tab-content br-n pn">
		                      
		                      <div id="nav-tab2" class="tab-pane chat-widget" role="tabpanel"></div>
		                      <div id="nav-tab3" class="tab-pane scroller-nm" role="tabpanel"></div>
		                    </div>
		                  </div>
		              	</div>
		          	</div>
		        </div>
		  	</div>
		</li>
		      <li class="dropdown menu-merge">
          <div class="navbar-btn btn-group">
            
            
          </div>
        </li>
        <li class="menu-divider hidden-xs">
          <i class="fa fa-circle"></i>
        </li>
        <li class="dropdown menu-merge">
          <a href="#" class="dropdown-toggle fw700 p15" data-toggle="dropdown">
            <img class="mw30 br64">
            <span class="hidden-xs pl15 pr5">
            <?php 
              //  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
              require 'vendor/autoload.php';

              session_start();
            
              $manipuladores = new \Biblioteca\manipuladores();

              echo $usuarios->getUsuarioLogado($manipuladores->criptografia($_SESSION['id_usuario'] ,'base64','decode'))->nome;
            ?></span>
            <span class="fa fa-angle-down hidden-xs"></span>
          </a>
          <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
            <li class="dropdown-header clearfix">
               <li class="list-group-item">
             	 <a href="configuracao.php" class="animated animated-short fadeInUp">
               	 <span class="fa fa-gear"></span>Configurações</a>
            	</li>
	            <li class="dropdown-footer">
	              <a href="sair.php" class="">Sair</a>
	            </li>
         	</ul>
        </li>
      </ul>
    </header>
<div class="modal fade" id="myModal">
  
</div><!-- /.modal -->