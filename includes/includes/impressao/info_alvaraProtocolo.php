<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Finalidade da Obra</div>
        <?php 
          //  Listando informações sobre responsável técnico.
          $con_listagem_alvara = $configuracoes->consulta("SELECT 
            processos_alvara.id_alv,
            processos_alvara.finalidadeobra,
            processos_alvara.areaterreno,
            processos_alvara.situacaoterreno,
            processos_alvara.desmembramento,
            processos_alvara.taxapermeabilidade

            FROM 
            processos.processos_alvara

            WHERE processos.processos_alvara.id_pro = $id_pro");

            $listagem_alvara = $con_listagem_alvara->fetch();

            $id_alv = $listagem_alvara['id_alv'];
            // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
        ?>
        <div class="col-xs-3 text-left campo-texto"><?php 
                          if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                            echo 'Residencial';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                            echo 'Comercial';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                            echo 'MISTO';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                            echo 'Institucional';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                            echo 'Galpão';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                            echo 'Industrial';
                          }
                        ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_alvara['areaterreno']; ?> m<sup>2</sup></div>
      </div>
      <?php 
        /*  Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
          pat = Pavimento Terreo 
          are = área Existente
          sbe = área sub solo existente
        */
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area,
          processos_pavimentacao.tipo

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and (processos.processos_pavimentacao.tipo = 'pat' or processos.processos_pavimentacao.tipo = 'are' or processos.processos_pavimentacao.tipo = 'sbe') 
          ORDER BY processos.processos_pavimentacao.id_pav ASC");

        $total_contruir = 0;
        $total_pat = 0;
        $total_are = 0;
        $total_sbe = 0;

        $total_patare_terreo = 0;
        $i = 0;
        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            
            $total_contruir = $total_contruir + $listagem_pavimento['area'];

            if ( $listagem_pavimento['tipo']  == 'pat' ) {
              $total_pat = $total_pat+$listagem_pavimento['area'];
              if ( $i == 0 ) {
                $total_patare_terreo = $listagem_pavimento['area'];
                $i++;
              }
            }
            if (  $listagem_pavimento['tipo']  == 'are' ) {
              $total_are = $total_are+$listagem_pavimento['area'];
               if ( $i == 1 ) {
                $total_patare_terreo = $total_patare_terreo + $listagem_pavimento['area'];
                $i++;
              }
            }
            if (  $listagem_pavimento['tipo']  == 'sbe' ) {
              $total_sbe = $total_sbe+$listagem_pavimento['area'];
            }
        }

        // Como a variável é usada em outras funções abaixo, criei uma nova para não perder o valor afim de colocar o mesmo no subtotal no rodapé do relatório.
        $total_pat2 = $total_pat;
      ?>
      <?php if ( $total_pat > 0 ) { ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo"><?php 
                        if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 ) {
                          echo 'Construir';
                        } else if ( $linha2['tipoprocesso'] == 2 ) {
                          echo 'Regularizar';
                        }
                        ?>:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
      </div>
      <?php } if ( $total_are > 0 ) { ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Existente:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $total_are; ?> m<sup>2</sup></div>
      </div>
      <?php } if ( $total_sbe > 0 ) { ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Subsolo Existente:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $total_sbe; ?> m<sup>2</sup></div>
      </div>
      <?php } ?>
      
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo round((($total_patare_terreo/$listagem_alvara['areaterreno'])*100),2); ?> %</div>
        <div class="col-xs-2 text-right subtitulo">Ind. Utilização:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo round(($total_contruir/$listagem_alvara['areaterreno']),2); ?></div> 

      </div>
      <br>
      <br>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Logs
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultologs" id="checkocultologs" onclick="aplogs();">
            <span class="checkbox"></span>
        </div>
      </div>
      
      <div id="boxlogs" style="display: none;">
        <table class="table table-hover" style="font-size: 12px;">
          <thead>
          <tr>
            <th></th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <?php 
          //  Listando informações sobre as unidades.
          $con_listagem_log = $configuracoes->consulta("SELECT 
            log_analise.datahora,   
            log_analise.situacaoprojeto,   
            log_analise.obsgerais,
            log_analise.tipo,
            cg.nome

            FROM 
            logs.log_analise

            LEFT JOIN geral.cg ON cg.id_cg = log_analise.id_cg

            WHERE logs.log_analise.id_pro = $id_pro ORDER BY logs.log_analise.datahora ASC");
            $log_id = 0;
            foreach ( $con_listagem_log as $listagem_log ) { 
              $log_id++;

              /*
                1 - A ser analisado = white, 
                2 - Em análise = #ddd, 
                3 - Pendência de documentos ou correção = #c4d79b, 
                4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
                5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
                6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
                7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
                8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
                9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
                10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
                11 - Aprovado = #eada6d, 
                12 - Dispensar Vistoria
              */
                if ( $listagem_log['situacaoprojeto'] == 1 ) {
                  $log_situacao = "A ser analisado";
                } else if ( $listagem_log['situacaoprojeto'] == 2 ) {
                  $log_situacao = "Em análise";
                } else if ( $listagem_log['situacaoprojeto'] == 3 ) {
                  $log_situacao = "Pendência de documentos ou correção";
                } else if ( $listagem_log['situacaoprojeto'] == 4 ) {
                  $log_situacao = "Processo não permitido ou reprovado na análise/vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 5 ) {
                  $log_situacao = "Processo encaminhado à procuradoria - Dúvida na vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 6 ) {
                  $log_situacao = "Processo encaminhado ao departamento imobiliário - Dúvida na vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 7 ) {
                  $log_situacao = "Aprovado - Pendente de pagamento de taxa para elaboração do decreto";
                } else if ( $listagem_log['situacaoprojeto'] == 8 ) {
                  $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                } else if ( $listagem_log['situacaoprojeto'] == 9 ) {
                  $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                } else if ( $listagem_log['situacaoprojeto'] == 10 ) {
                  $log_situacao = "Aprovado - Processo finalizado e decreto entregue";
                } else if ( $listagem_log['situacaoprojeto'] == 11 ) {
                  $log_situacao = "Aprovado";
                } else if ( $listagem_log['situacaoprojeto'] == 12 ) {
                  $log_situacao = "Vistoria dispensada - Encaminhada para análise";
                } else if ( $listagem_log['situacaoprojeto'] == 13 ) {
                  $log_situacao = 'Documentos retirados';
                } else if ( $listagem_log['situacaoprojeto'] == 14 ) {
                  $log_situacao = 'Documentos entregues';
                }
              ?>
                
                <tr style="border-top:1px solid green; ">
                  <td align="left">Tipo:</td>
                  <td align="left"><?php 
                    if ( $listagem_log['tipo'] == 'ch' ) {
                      echo 'Checagem';
                    } else if ( $listagem_log['tipo'] == 'po' ) {
                      echo 'Protocolo';
                    } else if ( $listagem_log['tipo'] == 'fi' ) {
                      echo 'Fiscalização';
                    } else if ( $listagem_log['tipo'] == 'aa' ) {
                      echo 'Análise';
                    }

                   ?></td>
                </tr>
                <tr>
                  <td align="left"><?php echo $log_id; ?><sup>o</sup> Etapa:</td>
                  <td align="left"><?php echo $formatacoes->formatar_datahora('/',$listagem_log['datahora']); ?></td>
                </tr>
                <tr>
                  <td align="left">Situação:</td>
                  <td align="left"><?php echo $log_situacao; ?></td>
                </tr>
                <tr>
                  <td align="left">Autor:</td>
                  <td align="left"><?php echo $listagem_log['nome']; ?></td>
                </tr>
                <tr>
                  <td align="left">Observações:</td>
                  <td align="left"><?php echo $listagem_log['obsgerais']; ?></td>
                </tr>
                
            <?php } unset($log_id); ?>
            </tbody>

            <footer>
              <tr>
                <th></th>
                <th></th>
              </tr>
            </footer>
          </table>
      </div>
      <br>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Detalhes
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultdetalhes" id="checkocultdetalhes" onclick="ap30();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="camposocultosmaisdetana" style="display: none;">
      <?php
        //  Listando informações sobre pavimentacao.
        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'pat' ORDER BY processos.processos_pavimentacao.id_pav ASC");
          $n_pav = 0;
          $total_pat = 0; ?>

          <?php if ( $con_listagem_pavimento->rowCount() > 0 ) { ?>
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Área a construir</div>
            </div>
            <br>
      <?php 
          
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            $n_pav++;
            $total_pat+= $listagem_pavimento['area']; ?>
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo'; } else { echo $n_pav; } ?>: </div>
              <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
            </div>
          <?php } ?>
          <div class="section row">
            <div class="col-xs-2 text-right subtitulo">Total: </div>
            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
          </div>
          <?php } ?><hr />
      <?php
        //  Listando informações sobre pavimentacao.
        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbc' ORDER BY processos.processos_pavimentacao.id_pav ASC");

        if ( $con_listagem_pavimento->rowCount() > 0 ) {
      ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Subsolo à Construir</div>
      </div>
      <br>
      

      <?php
            $n_pav = 0;
            $total_pat = 0;
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            $n_pav++;
            $total_pat+= $listagem_pavimento['area']; ?>
            
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
              <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
            </div>

            <?php } ?>
              <div class="section row">
                <div class="col-xs-2 text-right subtitulo">Total: </div>
                <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
              </div>
            <?php } ?><hr /> 
      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'are' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Existente</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                           <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo'; } else { echo $n_pav; } ?>: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                          </div>

                          <?php } ?>
                            <div class="section row">
                              <div class="col-xs-2 text-right subtitulo">Total: </div>
                              <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                            </div>
                        <?php } ?><hr />
                        
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbe' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Sub Solo Existente</div>
                    </div>
                    <br>

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div>
                        <?php } ?><hr />
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'adk' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Deck</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div>
                        <?php } ?><hr />
                      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'apa' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Piscina</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div>
                        <?php } ?>
      </div>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>
      </div>