<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Finalidade da Obra</div>
        <?php 
          //  Listando informações sobre responsável técnico.
          $con_listagem_condominio = $configuracoes->consulta("SELECT 
            processos_condominio.id_con,
            processos_condominio.finalidadeobra,
            processos_condominio.areaterreno,
            processos_condominio.situacaoterreno,
            processos_condominio.desmembramento

            FROM 
            processos.processos_condominio

            WHERE processos.processos_condominio.id_pro = $id_pro");

            $listagem_condominio = $con_listagem_condominio->fetch();

            $id_con = $listagem_condominio['id_con'];
            // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial            

        ?>
        <div class="col-xs-2 text-left campo-texto"><?php 
                          if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
                            echo 'Residencial em condomínio';
                          } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
                            echo 'Condomínio misto';
                          }
                        ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_condominio['areaterreno']; ?> m<sup>2</sup></div>
      </div>
      
      <?php 
          $con_listagem_que = $configuracoes->consulta("SELECT 
                  id_que,   
                  areacontruir,   
                  areaexistente,  
                  areausoexclusivo,   
                  areacomumproporcional,  
                  tipoprocesso

                  FROM 
                  processos.processos_que

                  WHERE processos.processos_que.id_con = $id_con 
                  ORDER BY processos.processos_que.id_con ASC");

          $total_areacontruir = 0;
          $total_areausoexclusivo = 0;
          $total_areacomumproporcional = 0;

          foreach ( $con_listagem_que as $listagem_que ) {
            $total_areacontruir = $total_areacontruir+$listagem_que['areacontruir'];
            $total_areausoexclusivo = $total_areausoexclusivo+$listagem_que['areausoexclusivo'];
            $total_areacomumproporcional = $total_areacomumproporcional+$listagem_que['areacomumproporcional'];
          }

          $total_areausoexclusivo2      = $total_areausoexclusivo;
          $total_areacomumproporcional2 = $total_areacomumproporcional;
        ?>
        <?php if ( $total_areacontruir > 0 ) { ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Área Comum proporcional:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $total_areacomumproporcional; ?> m<sup>2</sup></div>
        </div>
        <?php } if ( $total_areacomumproporcional > 0 ) { ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo"><?php 
                          if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                            echo 'Construir';
                          } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                            echo 'Regularizar';
                          }
                          ?>:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $total_areacontruir; ?> m<sup>2</sup></div>
        </div>
        <?php } ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round((($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional))*100),2); ?>%</span></div>
          <div class="col-xs-2 text-right subtitulo">Ind. Utilização:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round(($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?></span></div>
        </div>
        <br />
        <br />
        <div class="section row">
          <div class="col-xs-10" style="font-size: 11px;">
            <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>Unidades</th>
                              <th>Área a Construir(m²)</th>
                              <th>Área Existente(m²)</th>
                              <th>Área de Uso Exclusico(m²)</th>
                              <th>Área Comum Proporcional(m²)</th>
                              <th>Fração Ideal</th>
                              <th>Taxa de Ocupação Proporcional (%)</th>
                              <th>Índice de Utilização Proporcional</th>
                              <th>Tipo do Processo(m²)</th>
                              <?php if ( $emissaoalvarahabitese ) { ?><th style="width:300px;"></th><?php } ?>
                            </tr>
                          </thead>
                          <tbody>
                          <?php 
                            //  Listando informações sobre as unidades.
                            $con_listagem_unidades = $configuracoes->consulta("SELECT 
                              id_que,   
                              areacontruir,   
                              areaexistente,  
                              areausoexclusivo,   
                              areacomumproporcional,  
                              tipoprocesso

                              FROM 
                              processos.processos_que

                              WHERE processos.processos_que.id_con = $id_con ORDER BY processos.processos_que.id_con ASC");
                              $n_uni = 0;

                              $total_areacontruir = 0;
                              $total_areaexistente = 0;
                              $total_areausoexclusivo = 0;
                              $total_areacomumproporcional = 0;
                              foreach ( $con_listagem_unidades as $listagem_unidades ) { 
                                $n_uni++; 

                                $total_areacontruir           += $listagem_unidades['areacontruir'];
                                $total_areaexistente          += $listagem_unidades['areaexistente'];
                                $total_fracao                 += round((( ($listagem_unidades['areausoexclusivo']+$listagem_unidades['areacomumproporcional'])  /  ($total_areausoexclusivo2+$total_areacomumproporcional2)  )*100),2);
                                $total_areausoexclusivo       += $listagem_unidades['areausoexclusivo'];
                                $total_areacomumproporcional  += $listagem_unidades['areacomumproporcional'];

                                ?>
                                <input type="hidden" value="<?php echo $n_uni; ?>" name="num_casa[]">
                      
                                <input type="hidden" name="IDunidadesAdd[]" id="IDunidadesAdd[]" value="<?php echo $listagem_unidades['id_que']; ?>">
                                    <tr style="border-bottom: 1px solid #eeeeee;">
                                     <td>Casa <?php echo $n_uni; ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areacontruir'] > 0 ) { echo $listagem_unidades['areacontruir']; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areaexistente'] > 0 ) { echo $listagem_unidades['areaexistente']; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areausoexclusivo'] > 0 ) { echo $listagem_unidades['areausoexclusivo']; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areacomumproporcional'] > 0 ) { echo $listagem_unidades['areacomumproporcional']; ?> m<sup>2</sup><?php } ?></td>
                                        <td><?php echo round((( ($listagem_unidades['areausoexclusivo']+$listagem_unidades['areacomumproporcional'])  /  ($total_areausoexclusivo2+$total_areacomumproporcional2)  )*100),2); ?> %</td>
                                        <td><?php echo round((($listagem_unidades['areacontruir']/$listagem_unidades['areausoexclusivo'])*100),2); ?> %</td>
                                        <td><?php echo round(($listagem_unidades['areacontruir']/$listagem_unidades['areausoexclusivo']),2); ?></td>

                                        <td style="text-align=left;">
                                          <?php if ( $listagem_unidades['tipoprocesso'] == 'c' ) { echo "Construção"; } else if ( $listagem_unidades['tipoprocesso'] == 'r' ) { echo "Regularização"; } else if ( $listagem_unidades['tipoprocesso'] == 'a' ) { echo "Ácrescimo de área"; } ?>
                                        </td>
                                        
                                    </tr>
                                    
                                    
                                    
                              <?php } ?>
                              </tbody>
                            
                              <footer>
                                <tr>
                                  <th>Total do condomínio</th>
                                  <th><?php echo $total_areacontruir; ?> (m²)</th>
                                  <th><?php echo $total_areaexistente; ?> (m²)</th>
                                  <th><?php echo $total_areausoexclusivo; ?> (m²)</th>
                                  <th><?php echo $total_areacomumproporcional; ?> (m²)</th>
                                  <th><?php echo $total_fracao; ?> %</th>

                                  <th><?php echo round(( ($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional))*100),2); ?> %</th>
                                  <th><?php echo round( ($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?></th>
                                  <th></th>

                                  <?php if ( $emissaoalvarahabitese ) { ?><th></th><?php } ?>
                                </tr>
                              </footer>
                            </table>
          </div>                  
        </div><!-- Fim da row -->
        <?php 
          //  Destruindo variáveis.
          unset($total_areacontruir,$total_areacomumproporcional); ?>
      <br>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Logs
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultologs" id="checkocultologs" onclick="aplogs();">
            <span class="checkbox"></span>
        </div>
      </div>
      
      <div id="boxlogs" style="display: none;">
        <table class="table table-hover" style="font-size: 12px;">
          <thead>
          <tr>
            <th></th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <?php 
          //  Listando informações sobre as unidades.
          $con_listagem_log = $configuracoes->consulta("SELECT 
            log_analise.datahora,   
            log_analise.situacaoprojeto,   
            log_analise.obsgerais,
            log_analise.tipo,
            cg.nome

            FROM 
            logs.log_analise

            LEFT JOIN geral.cg ON cg.id_cg = log_analise.id_cg

            WHERE logs.log_analise.id_pro = $id_pro ORDER BY logs.log_analise.datahora ASC");
            $log_id = 0;
            foreach ( $con_listagem_log as $listagem_log ) { 
              $log_id++;

              /*
                1 - A ser analisado = white, 
                2 - Em análise = #ddd, 
                3 - Pendência de documentos ou correção = #c4d79b, 
                4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
                5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
                6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
                7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
                8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
                9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
                10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
                11 - Aprovado = #eada6d, 
                12 - Dispensar Vistoria
              */
                if ( $listagem_log['situacaoprojeto'] == 1 ) {
                  $log_situacao = "A ser analisado";
                } else if ( $listagem_log['situacaoprojeto'] == 2 ) {
                  $log_situacao = "Em análise";
                } else if ( $listagem_log['situacaoprojeto'] == 3 ) {
                  $log_situacao = "Pendência de documentos ou correção";
                } else if ( $listagem_log['situacaoprojeto'] == 4 ) {
                  $log_situacao = "Processo não permitido ou reprovado na análise/vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 5 ) {
                  $log_situacao = "Processo encaminhado à procuradoria - Dúvida na vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 6 ) {
                  $log_situacao = "Processo encaminhado ao departamento imobiliário - Dúvida na vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 7 ) {
                  $log_situacao = "Aprovado - Pendente de pagamento de taxa para elaboração do decreto";
                } else if ( $listagem_log['situacaoprojeto'] == 8 ) {
                  $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                } else if ( $listagem_log['situacaoprojeto'] == 9 ) {
                  $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                } else if ( $listagem_log['situacaoprojeto'] == 10 ) {
                  $log_situacao = "Aprovado - Processo finalizado e decreto entregue";
                } else if ( $listagem_log['situacaoprojeto'] == 11 ) {
                  $log_situacao = "Aprovado";
                } else if ( $listagem_log['situacaoprojeto'] == 12 ) {
                  $log_situacao = "Vistoria dispensada - Encaminhada para análise";
                } else if ( $listagem_log['situacaoprojeto'] == 13 ) {
                  $log_situacao = 'Documentos retirados';
                } else if ( $listagem_log['situacaoprojeto'] == 14 ) {
                  $log_situacao = 'Documentos entregues';
                }
              ?>
                
                <tr style="border-top:1px solid green; ">
                  <td align="left">Tipo:</td>
                  <td align="left"><?php 
                    if ( $listagem_log['tipo'] == 'ch' ) {
                      echo 'Checagem';
                    } else if ( $listagem_log['tipo'] == 'po' ) {
                      echo 'Protocolo';
                    } else if ( $listagem_log['tipo'] == 'fi' ) {
                      echo 'Fiscalização';
                    } else if ( $listagem_log['tipo'] == 'aa' ) {
                      echo 'Análise';
                    }

                   ?></td>
                </tr>
                <tr>
                  <td align="left"><?php echo $log_id; ?><sup>o</sup> Etapa:</td>
                  <td align="left"><?php echo $formatacoes->formatar_datahora('/',$listagem_log['datahora']); ?></td>
                </tr>
                <tr>
                  <td align="left">Situação:</td>
                  <td align="left"><?php echo $log_situacao; ?></td>
                </tr>
                <tr>
                  <td align="left">Autor:</td>
                  <td align="left"><?php echo $listagem_log['nome']; ?></td>
                </tr>
                <tr>
                  <td align="left">Observações:</td>
                  <td align="left"><?php echo $listagem_log['obsgerais']; ?></td>
                </tr>
                
            <?php } unset($log_id); ?>
            </tbody>

            <footer>
              <tr>
                <th></th>
                <th></th>
              </tr>
            </footer>
          </table>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>