<?php
//  Listando informações sobre responsável técnico.
  $con_listagem_redimensionamento = $configuracoes->consulta("SELECT 
    processos_redimensionamento.id_red,
    processos_redimensionamento.tipo,
    processos_redimensionamento.areatotalterreno,
    processos_redimensionamento.sa_qtdlotes,  
    processos_redimensionamento.sp_qtdlotes,
    processos_redimensionamento.descricaolotes,
    processos_redimensionamento.requerente,
    processos_redimensionamento.requerentetelefone

    FROM 
    processos.processos_redimensionamento

    WHERE processos.processos_redimensionamento.id_pro = $id_pro");

    $listagem_redimensionamento = $con_listagem_redimensionamento->fetch();

    $id_red = $listagem_redimensionamento['id_red'];
    // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
?>
<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Tipo:</div>
        <div class="col-xs-3 text-left campo-texto"><?php 
            if ( $listagem_redimensionamento['tipo'] == 1 ) {
              echo 'Remembramento';
            } else if ( $listagem_redimensionamento['tipo'] == 2 ) {
              echo 'Desmembramento';
            } else if ( $listagem_redimensionamento['tipo'] == 0 ) {
              echo 'Redimensionamento';
            }
          ?>
        </div>
      </div>
      <br />
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo"><strong>Objeto / Situação Atual</strong></div>
      </div>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Quantidade de Lotes:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['sa_qtdlotes']; ?> Unidades</div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['areatotalterreno']; ?> Unidades</div>
      </div>
      <br />
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo"><strong>Objeto / Situação Pretendida</strong></div>
      </div>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Quantidade de Lotes:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['sp_qtdlotes']; ?> Unidades</div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Descrição dos Lotes:</div>
        <div class="col-xs-4 text-left campo-texto"><?php echo $listagem_redimensionamento['descricaolotes']; ?></div>
      </div>
      <br />
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Requerentes:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['requerente']; ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Telefone:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['requerentetelefone']; ?></div>
      </div>

      <br>
      <br>
      
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Logs
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultologs" id="checkocultologs" onclick="aplogs();">
            <span class="checkbox"></span>
        </div>
      </div>
      
      <div id="boxlogs" style="display: none;">
        <table class="table table-hover" style="font-size: 12px;">
          <thead>
          <tr>
            <th></th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <?php 
          //  Listando informações sobre as unidades.
          $con_listagem_log = $configuracoes->consulta("SELECT 
            log_analise.datahora,   
            log_analise.situacaoprojeto,   
            log_analise.obsgerais,
            log_analise.tipo,
            cg.nome

            FROM 
            logs.log_analise

            LEFT JOIN geral.cg ON cg.id_cg = log_analise.id_cg

            WHERE logs.log_analise.id_pro = $id_pro ORDER BY logs.log_analise.datahora ASC");
            $log_id = 0;
            foreach ( $con_listagem_log as $listagem_log ) { 
              $log_id++;

              /*
                1 - A ser analisado = white, 
                2 - Em análise = #ddd, 
                3 - Pendência de documentos ou correção = #c4d79b, 
                4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
                5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
                6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
                7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
                8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
                9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
                10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
                11 - Aprovado = #eada6d, 
                12 - Dispensar Vistoria
              */
                if ( $listagem_log['situacaoprojeto'] == 1 ) {
                  $log_situacao = "A ser analisado";
                } else if ( $listagem_log['situacaoprojeto'] == 2 ) {
                  $log_situacao = "Em análise";
                } else if ( $listagem_log['situacaoprojeto'] == 3 ) {
                  $log_situacao = "Pendência de documentos ou correção";
                } else if ( $listagem_log['situacaoprojeto'] == 4 ) {
                  $log_situacao = "Processo não permitido ou reprovado na análise/vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 5 ) {
                  $log_situacao = "Processo encaminhado à procuradoria - Dúvida na vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 6 ) {
                  $log_situacao = "Processo encaminhado ao departamento imobiliário - Dúvida na vistoria";
                } else if ( $listagem_log['situacaoprojeto'] == 7 ) {
                  $log_situacao = "Aprovado - Pendente de pagamento de taxa para elaboração do decreto";
                } else if ( $listagem_log['situacaoprojeto'] == 8 ) {
                  $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                } else if ( $listagem_log['situacaoprojeto'] == 9 ) {
                  $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                } else if ( $listagem_log['situacaoprojeto'] == 10 ) {
                  $log_situacao = "Aprovado - Processo finalizado e decreto entregue";
                } else if ( $listagem_log['situacaoprojeto'] == 11 ) {
                  $log_situacao = "Aprovado";
                } else if ( $listagem_log['situacaoprojeto'] == 12 ) {
                  $log_situacao = "Vistoria dispensada - Encaminhada para análise";
                } else if ( $listagem_log['situacaoprojeto'] == 13 ) {
                  $log_situacao = 'Documentos retirados';
                } else if ( $listagem_log['situacaoprojeto'] == 14 ) {
                  $log_situacao = 'Documentos entregues';
                }
              ?>
                
                <tr style="border-top:1px solid green; ">
                  <td align="left">Tipo:</td>
                  <td align="left"><?php 
                    if ( $listagem_log['tipo'] == 'ch' ) {
                      echo 'Checagem';
                    } else if ( $listagem_log['tipo'] == 'po' ) {
                      echo 'Protocolo';
                    } else if ( $listagem_log['tipo'] == 'fi' ) {
                      echo 'Fiscalização';
                    } else if ( $listagem_log['tipo'] == 'aa' ) {
                      echo 'Análise';
                    }

                   ?></td>
                </tr>
                <tr>
                  <td align="left"><?php echo $log_id; ?><sup>o</sup> Etapa:</td>
                  <td align="left"><?php echo $formatacoes->formatar_datahora('/',$listagem_log['datahora']); ?></td>
                </tr>
                <tr>
                  <td align="left">Situação:</td>
                  <td align="left"><?php echo $log_situacao; ?></td>
                </tr>
                <tr>
                  <td align="left">Autor:</td>
                  <td align="left"><?php echo $listagem_log['nome']; ?></td>
                </tr>
                <tr>
                  <td align="left">Observações:</td>
                  <td align="left"><?php echo $listagem_log['obsgerais']; ?></td>
                </tr>
                
            <?php } unset($log_id); ?>
            </tbody>

            <footer>
              <tr>
                <th></th>
                <th></th>
              </tr>
            </footer>
          </table>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>
      </div>