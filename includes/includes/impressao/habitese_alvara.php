<div class="section row">
          <div class="col-xs-4 border-xs">
            <strong>Alvará</strong>
          </div><!--t Título do campo -->
          <div class="col-xs-4 border-xs">
            <strong>Área total da construção</strong>
          </div><!--t Título do campo -->
          <div class="col-xs-4 border-xs">
            <strong>Finalidade</strong>
          </div><!--t Título do campo -->
        </div>
        <div class="section row">
          <div class="col-xs-4 border-xs">
            <?php
              if ( $linha2['tipoprocesso'] == 1 ) {
                echo 'CONSTRUÇÃO';
              } else if ( $linha2['tipoprocesso'] == 2 ) {
                echo 'REGULARIZAÇÃO DE OBRAS';
              } else if ( $linha2['tipoprocesso'] == 3 ) {
                echo 'ACRÉSCIMO DE ÁREAS';
              } else if ( $linha2['tipoprocesso'] == 4 ) {
                echo 'CONDOMÍNIO EDILÍCIO';
              } else if ( $linha2['tipoprocesso'] == 5 ) {
                echo 'REDIMENSIONAMENTO';
              }
            ?>
          </div><!-- conteudo texto -->
          <div class="col-xs-4 border-xs">
            <?php echo $total_contruir; ?> m<sup>2</sup>
          </div><!--Conteúdo texto -->
          <div class="col-xs-4 border-xs">
            <!--RESIDENCIAL EM CONDOMÍNIO - <strong>UNIDADE: 1</strong>-->

            <?php 

              if ( $id_pro == '1101559' and $listagem_alvara['finalidadeobra'] == 're' ) {
                echo 'RESIDENCIAL EM CONDOMÍNIO';
              } else if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                echo 'RESIDENCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                echo 'COMERCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                echo 'MISTO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                echo 'INSTITUCIONAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                echo 'GALPÃO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                echo 'INDUSTRIAL';
              }
            ?>
          </div><!--Conteúdo texto -->
        </div><!-- Fim bloco -->
          <br>
          <br>
         <div class="section row">
          <div class="col-xs-12 border-xs">
            <strong>Descrição</strong>
          </div><!--t Título do campo -->
        </div><!--fim do bloco -->
        <div class="section row">
          <div class="col-xs-12 border-xs">
            A edifição que tem como finalidade o uso <?php 
              if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                echo 'RESIDENCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                echo 'COMERCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                echo 'MISTO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                echo 'INSTITUCIONAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                echo 'GALPÃO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                echo 'INDUSTRIAL';
              }
            ?>, totalizando uma área construída de <?php echo $total_contruir; ?> m<sup>2</sup>,  além de estar em conformidade com a legislação municipal vigente, encontra-se de acordo com o(s) Alvará(s)
de <?php
              if ( $linha2['tipoprocesso'] == 1 ) {
                echo 'CONSTRUÇÃO';
              } else if ( $linha2['tipoprocesso'] == 2 ) {
                echo 'REGULARIZAÇÃO DE OBRAS';
              } else if ( $linha2['tipoprocesso'] == 3 ) {
                echo 'ACRÉSCIMO DE ÁREAS';
              } else if ( $linha2['tipoprocesso'] == 4 ) {
                echo 'CONDOMÍNIO EDILÍCIO';
              } else if ( $linha2['tipoprocesso'] == 5 ) {
                echo 'REDIMENSIONAMENTO';
              }

              $consulta_alvara_ah = $configuracoes->consulta("SELECT 
              numero,   
              ano
              
              FROM 

              processos.processos_alvarahabitese 

              WHERE processos_alvarahabitese.id_ah = ".$linha_alvarahabitese['id_ah2']."");
              $linha_alvara_ah = $consulta_alvara_ah->fetch();
            
            ?>, número(s) <?php echo $linha_alvara_ah['numero'].' / '.$linha_alvara_ah['ano']; ?>