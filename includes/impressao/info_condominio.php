<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Finalidade da Obra</div>
        <?php 
          //  Listando informações sobre responsável técnico.
          $con_listagem_condominio = $configuracoes->consulta("SELECT 
            processos_condominio.id_con,
            processos_condominio.finalidadeobra,
            processos_condominio.areaterreno,
            processos_condominio.situacaoterreno,
            processos_condominio.desmembramento

            FROM 
            processos.processos_condominio

            WHERE processos.processos_condominio.id_pro = $id_pro");

            $listagem_condominio = $con_listagem_condominio->fetch();

            $id_con = $listagem_condominio['id_con'];
            // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial            

        ?>
        <div class="col-xs-2 text-left campo-texto"><?php 
          if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
            echo 'Residencial em condomínio';
          } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
            echo 'Condomínio misto';
          }
        ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_condominio['areaterreno']; ?> m<sup>2</sup></div>
      </div>
      
      <?php 
        $con_listagem_que = $configuracoes->consulta("SELECT 
                  id_que,   
                  areacontruir,   
                  areaexistente,  
                  areausoexclusivo,   
                  areacomumproporcional,  
                  tipoprocesso

                  FROM 
                  processos.processos_que

                  WHERE processos.processos_que.id_con = $id_con 
                  ORDER BY processos.processos_que.id_con ASC");

          $total_areacontruir           = 0;
          $total_areausoexclusivo       = 0;
          $total_areacomumproporcional  = 0;
          $total_areaexistente          = 0;

          foreach ( $con_listagem_que as $listagem_que ) { 
              $total_areacontruir           = $total_areacontruir+$listagem_que['areacontruir'];
              $total_areausoexclusivo       = $total_areausoexclusivo+$listagem_que['areausoexclusivo'];
              $total_areaexistente          = $total_areaexistente+$listagem_que['areaexistente'];
              $total_areacomumproporcional  = $total_areacomumproporcional+$listagem_que['areacomumproporcional'];
          }
        ?>
        <?php if ( $total_areacontruir > 0 ) { ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Área Comum proporcional:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $total_areacomumproporcional; ?> m<sup>2</sup></div>
        </div>
        <?php } if ( $total_areacomumproporcional > 0 ) { ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo"><?php 
                          if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                            echo 'Construir';
                          } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                            echo 'Regularizar';
                          }
                          ?>:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $total_areacontruir; ?> m<sup>2</sup></div>
        </div>
        <?php } ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round((($total_areacontruir/$listagem_condominio['areaterreno'])*100),2); ?>%</span></div>
          <div class="col-xs-2 text-right subtitulo">Ind. Utilização:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round(($total_areacontruir/$listagem_condominio['areaterreno']),2); ?></span></div>
        </div>
      <br>
      <br>
      
      <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Quantidades de Unidades Exclusivas:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $con_listagem_que->rowCount(); ?></div>
      
          <div class="col-xs-2 text-right subtitulo">Unidade:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo (int)$_GET['unidade']; ?></div>
      </div>
      <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Área a Construir:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $linha_que['areacontruir']; ?> m<sup>2</sup></div>
      
          <div class="col-xs-2 text-right subtitulo">Área de Uso Exclusivo:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $linha_que['areausoexclusivo']; ?> m<sup>2</sup></div>
      </div>
      <?php if ( $linha_que['areaexistente'] > 0 ) { ?>
      <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Área Existente:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $linha_que['areaexistente']; ?> m<sup>2</sup></div>
      </div>
      <?php } ?>
      <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Índice de Utilização Proporcional:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round(($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?></div>
      
          <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação Proporcional:</div>
          <div class="col-xs-2 text-left campo-texto"><?php 

          echo $_GET['tocupacao'];
          //echo round((($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional))*100),2); ?>%</div>
      </div>

      <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Área Comum proporcional:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $linha_que['areacomumproporcional']; ?> m<sup>2</sup></div>
      </div>
<?php 
          //  Destruindo variáveis.
          unset($total_areacontruir,$total_areacomumproporcional); ?>
      <br />
      <br />

      <div class="section row hidden-print">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>

      <div class="section row">
        <div class="col-xs-offset-3 col-xs-4 text-right subtitulo">Subtotal ( <?php 
                        if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                          echo 'Construir';
                        } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                          echo 'Regularizar';
                        }
                        ?> ) :</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha_que['areacontruir']; ?> m<sup>2</sup></div>

        <div class="col-xs-offset-3 col-xs-4 text-right subtitulo">Total ( <?php 
                        if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                          echo 'Construir';
                        } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                          echo 'Regularizar';
                        }

                        if ( $linha_que['areaexistente'] > 0 ) {
                          echo ' + Existente';
                        }
                        ?> ) :</div>
        <div class="col-xs-2 campo-texto"><?php echo $linha_que['areacontruir'] + $linha_que['areaexistente']; ?> m<sup>2</sup></div>
      </div>