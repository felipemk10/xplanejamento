<?php

$con_listagem_alvara = $configuracoes->consulta("SELECT
        processos_alvara.id_alv,
        processos_alvara.finalidadeobra,
        processos_alvara.areaterreno,
        processos_alvara.situacaoterreno,
        processos_alvara.desmembramento,
        processos_alvara.taxapermeabilidade,
        processos_alvara.taxaocupacao

        FROM
        processos.processos_alvara

        WHERE processos.processos_alvara.id_pro = $id_pro");

        $listagem_alvara = $con_listagem_alvara->fetch();

        $id_alv = $listagem_alvara['id_alv'];
        // Legenda finalidadedeobra:  re = residencial, rc = residencial em condominio, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial

/*  Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
pat = Pavimento Terreo
are = área Existente
sbe = área sub solo existente
*/
$con_listagem_pavimento = $configuracoes->consulta("SELECT
processos_pavimentacao.id_pav,
processos_pavimentacao.area,
processos_pavimentacao.tipo

FROM
processos.processos_pavimentacao

WHERE processos.processos_pavimentacao.id_alv = $id_alv and (processos.processos_pavimentacao.tipo = 'pat' or processos.processos_pavimentacao.tipo = 'are' or processos.processos_pavimentacao.tipo = 'sbe')
ORDER BY processos.processos_pavimentacao.id_pav ASC");


/*  Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
  pat = Pavimento Terreo
  are = área Existente
  sbe = área sub solo existente
*/
$con_listagem_pavimento = $configuracoes->consulta("SELECT
  processos_pavimentacao.id_pav,
  processos_pavimentacao.area,
  processos_pavimentacao.tipo

  FROM
  processos.processos_pavimentacao

  WHERE processos.processos_pavimentacao.id_alv = $id_alv and (processos.processos_pavimentacao.tipo = 'pat' or processos.processos_pavimentacao.tipo = 'are' or processos.processos_pavimentacao.tipo = 'sbe' or processos.processos_pavimentacao.tipo = 'sbc')
  ORDER BY processos.processos_pavimentacao.id_pav ASC");

$total_contruir = 0;
$total_pat = 0;
$total_are = 0;
$total_sbe = 0;

$total_sbc = 0;

$total_patare_terreo = 0;
$i = 0;
$i2 = 0;


foreach ( $con_listagem_pavimento as $listagem_pavimento ) {

    $total_contruir = $total_contruir + $listagem_pavimento['area'];

    if ( $listagem_pavimento['tipo']  == 'pat' ) {
      $total_pat = $total_pat+$listagem_pavimento['area'];
      if ( $i == 0 ) {
        $total_patare_terreo += $listagem_pavimento['area'];
        $i++;
      }
    }
    if (  $listagem_pavimento['tipo']  == 'are' ) {
      $total_are = $total_are+$listagem_pavimento['area'];
       if ( $i2 == 0 ) {
        $total_patare_terreo = $total_patare_terreo + $listagem_pavimento['area'];
        $i2++;
      }
    }
    if (  $listagem_pavimento['tipo']  == 'sbe' ) {
      $total_sbe = $total_sbe+$listagem_pavimento['area'];
    }
    if (  $listagem_pavimento['tipo']  == 'sbc' ) {
      $total_sbc = $total_sbc+$listagem_pavimento['area'];
    }
}

// Como a variável é usada em outras funções abaixo, criei uma nova para não perder o valor afim de colocar o mesmo no subtotal no rodapé do relatório.
$total_pat2 = $total_pat;

$html .= "<tr> <!-- FINALIDADE DE OBRA  -->
                <td align='center' colspan='6'>
                        <table align='center' width='300' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                                <tr>
                                        <td width='190' align='right'><b>FINALIDADE DA OBRA:</b></td>
                                        <td align='left'>";
                                        if ( $listagem_alvara['finalidadeobra'] == 'rc' ) {//Irving 13/02/2020
                                            $html .= 'RESIDENCIAL EM CONDOMÍNIO';
                                        } else if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                                                $html .= 'RESIDENCIAL';
                                        } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                                                $html .= 'COMERCIAL';
                                        } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                                                $html .= 'MISTO';
                                        } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                                                $html .= 'INSTITUCIONAL';
                                        } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                                                $html .= 'GALPÃO';
                                        } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                                                $html .= 'INDUSTRIAL';
                                        }
                                        $html .= "</td>
                                        <td></td>
                                        <td></td>
                                </tr>
                        </table>
                </td>
        </tr>";

        $html .= "<tr>
                <td colspan='6'>
                        <table style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
                                <tr>
                                        <td width='170' align='right'><b>ÁREA DO TERRENO:</b></td>
                                        <td width='70'>";
                                        $html .= number_format(($listagem_alvara['areaterreno']), 2, '.', '');
                                        $html .= " m²</td>";

                                        if ( $total_pat > 0 ) {
                                                $html .= "<td width='200' align='right'><b>";
                                                if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 or $linha2['tipoprocesso'] == 6 ) {
                                                  $html .= 'CONSTRUIR';
                                                } else if ( $linha2['tipoprocesso'] == 2 ) {
                                                  $html .= 'REGULARIZAR';
                                                }
                                                $html .= ":</b></td>
                                                <td width='70'>";
                                                $html .= number_format(($total_pat+$total_sbc), 2, '.', '');
                                                $html .= " m²</td>";
                                        }
                                $html .="</tr>
                                <tr>";
                                        if ( $total_are > 0 ) {
                                                $html .= "<td width='140' align='right'><b>";
                                                $html .= "ÁREA EXISTENTE:";
                                                $html .= "</b></td>
                                                <td width='70'>";
                                                $html .= number_format(($total_are), 2, '.', '');
                                                $html .= " m²</td>";
                                        }
                                        if ( $total_sbe > 0 ) {
                                                $html .= "<td width='140' align='right'><b>";
                                                $html .= "ÁREA SUBSOLO EXISTENTE:";
                                                $html .= "</b></td>
                                                <td width='70'>";
                                                $html .= number_format(($total_sbe), 2, '.', '');
                                                $html .= " m²</td>";
                                        }
                                $html .="</tr>
                                <tr>";
                                        $html .= "<td width='140' align='right'><b>";
                                        $html .= "TAXA OCUPAÇÃO:";
                                        $html .= "</b></td>
                                        <td width='70'>";
                                        if ( !empty($_GET['to'])  ) {
                                            $html .= $_GET['to'];
                                        } else if ( $listagem_alvara['taxaocupacao'] > 0 ) {
                                            $html .= $listagem_alvara['taxaocupacao'];
                                        } else {
                                            $html .= number_format((($total_patare_terreo/$listagem_alvara['areaterreno'])*100), 2, '.', '');
                                        }
                                        $html .= "%</td>";


                                        $html .= "<td width='140' align='right'><b>";
                                        $html .= "IND. UTILIZAÇÃO:";
                                        $html .= "</b></td>
                                        <td width='70'>";
                                        $html .= round(($total_contruir/$listagem_alvara['areaterreno']),2);
                                        $html .= "</td>";

                                $html .=" </tr>
                        </table>
                </td>
        </tr>";

        $html .= "<tr>
                        <td colspan='6'>
                        <table align='center'>
                        <tr>
                        <td align='left'>";

                                //  Listando informações sobre pavimentacao.
                        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                        $con_listagem_pavimento = $configuracoes->consulta("SELECT
                          processos_pavimentacao.id_pav,
                          processos_pavimentacao.area

                          FROM
                          processos.processos_pavimentacao

                          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'pat' ORDER BY processos.processos_pavimentacao.id_pav ASC");
                          $n_pav = 0;
                          $total_pat = 0;
                        if ( $con_listagem_pavimento->rowCount() > 0 ) {
                                $html .= "<table align='left' width='254' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:12px;'>
                                        <tr>
                                                <td colspan='4' width='170' align='center'><b>ÁREA À CONSTRUIR</b></td>
                                        </tr>";
                                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) {
                                                $n_pav++;
                                $total_pat += $listagem_pavimento['area'];

                                                $html .= "<tr>
                                                <td width='30' align='left'></td>
                                                <td width='90' align='right'><b>PAV ";
                                                if ( $n_pav == 1 ) {
                                                        $html .= 'TÉRREO';
                                                } else {
                                                        $html .= $n_pav;
                                                }
                                                $html .= "</b></td>
                                                        <td width='70' align='left'>";
                                                $html .= number_format(($listagem_pavimento['area']), 2, '.', '');
                                                $html .= " m²</td>
                                                        </tr>";
                                        }
                                        $html .= "
                                        <tr>
                                                <td width='22' align='left'></td>
                                                <td width='10' align='right'><b>TOTAL</b></td>
                                                <td width='40' align='left'>";
                                                $html .= number_format(($total_pat), 2, '.', '');
                                                $html .= " m²</td>
                                        </tr>
                                </table>";
                        }

                        $html .= "</td>
                        <td>";
                                //  Listando informações sobre pavimentacao.
                        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                        $con_listagem_pavimento = $configuracoes->consulta("SELECT
                          processos_pavimentacao.id_pav,
                          processos_pavimentacao.area

                          FROM
                          processos.processos_pavimentacao

                          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbc' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                        if ( $con_listagem_pavimento->rowCount() > 0 ) {
                                $html .= "<table align='left' width='254' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:12px;'>
                                        <tr>
                                                <td colspan='4' width='170' align='center'><b>ÁREA SUBSOLO À CONSTRUIR</b></td>
                                        </tr>";
                                        $n_pav = 0;
                                $total_pat = 0;
                                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) {
                                                $n_pav++;
                                $total_pat += $listagem_pavimento['area'];

                                                $html .= "<tr>
                                                <td width='30' align='left'></td>
                                                <td width='10' align='right'><b>PAV ";
                                                $html .= $n_pav;
                                                $html .= "</b></td>
                                                        <td width='40' align='left'>";
                                                $html .= number_format(($listagem_pavimento['area']), 2, '.', '');
                                                $html .= " m²</td>
                                                        </tr>";
                                        }
                                        $html .= "
                                        <tr>
                                                <td width='22' align='left'></td>
                                                <td width='10' align='right'><b>TOTAL</b></td>
                                                <td width='40' align='left'>";
                                                $html .= number_format(($total_pat), 2, '.', '');
                                                $html .= " m²</td>
                                        </tr>
                                </table>";
                        }
                        $html .= "</td>
                        <td>";
                        //  Listando informações sobre pavimentacao.
                        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                        $con_listagem_pavimento = $configuracoes->consulta("SELECT
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'are' ORDER BY processos.processos_pavimentacao.id_pav ASC");
                        if ( $con_listagem_pavimento->rowCount() > 0 ) {
                                $html .= "<table align='left' width='254' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:12px;'>
                                        <tr>
                                                <td colspan='4' width='170' align='center'><b>ÁREA EXISTENTE</b></td>
                                        </tr>";
                                        $n_pav = 0;
                        $total_pat = 0;
                                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) {
                                                $n_pav++;
                                $total_pat += $listagem_pavimento['area'];

                                                $html .= "<tr>
                                                <td width='30' align='left'></td>
                                                <td width='90' align='right'><b>PAV ";
                                                if ( $n_pav == 1 ) {
                                                        $html .= 'TÉRREO';
                                                } else {
                                                        $html .= $n_pav;
                                                }
                                                $html .= "</b></td>
                                                        <td width='70' align='left'>";
                                                $html .= number_format(($listagem_pavimento['area']), 2, '.', '');
                                                $html .= " m²</td>
                                                        </tr>";
                                        }
                                        $html .= "
                                        <tr>
                                                <td width='22' align='left'></td>
                                                <td width='10' align='right'><b>TOTAL</b></td>
                                                <td width='40' align='left'>";
                                                $html .= number_format(($total_pat), 2, '.', '');
                                                $html .= " m²</td>
                                        </tr>
                                </table>";
                        }
                        $html .= "</td>
                        </tr>
                        <tr>
                        <td>";
                          //  Listando informações sobre pavimentacao.
                        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                        $con_listagem_pavimento = $configuracoes->consulta("SELECT
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbe' ORDER BY processos.processos_pavimentacao.id_pav ASC");
                        if ( $con_listagem_pavimento->rowCount() > 0 ) {
                                $html .= "<table align='left' width='254' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:12px;'>
                                        <tr>
                                                <td colspan='4' width='170' align='center'><b>ÁREA SUBSOLO EXISTENTE</b></td>
                                        </tr>";
                                        $n_pav = 0;
                                $total_pat = 0;
                                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) {
                                                $n_pav++;
                                $total_pat += $listagem_pavimento['area'];

                                                $html .= "<tr>
                                                <td width='30' align='left'></td>
                                                <td width='10' align='right'><b>PAV ";
                                                $html .= $n_pav;
                                                $html .= "</b></td>
                                                        <td width='40' align='left'>";
                                                $html .= number_format(($listagem_pavimento['area']), 2, '.', '');
                                                $html .= " m²</td>
                                                        </tr>";
                                        }
                                        $html .= "
                                        <tr>
                                                <td width='22' align='left'></td>
                                                <td width='10' align='right'><b>TOTAL</b></td>
                                                <td width='40' align='left'>";
                                                $html .= number_format(($total_pat), 2, '.', '');
                                                $html .= " m²</td>
                                        </tr>
                                </table>";
                        }
                        $html .= "</td>

                        <td>";
                          //  Listando informações sobre pavimentacao.
                        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                        $con_listagem_pavimento = $configuracoes->consulta("SELECT
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'adk' ORDER BY processos.processos_pavimentacao.id_pav ASC");
                        if ( $con_listagem_pavimento->rowCount() > 0 ) {
                                $html .= "<table align='left' width='254' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:12px;'>
                                        <tr>
                                                <td colspan='4' width='170' align='center'><b>ÁREA DECK</b></td>
                                        </tr>";
                                        $n_pav = 0;
                                $total_pat = 0;
                                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) {
                                                $n_pav++;
                                $total_pat += $listagem_pavimento['area'];

                                                $html .= "<tr>
                                                <td width='30' align='left'></td>
                                                <td width='10' align='right'><b>PAV ";
                                                $html .= $n_pav;
                                                $html .= "</b></td>
                                                        <td width='40' align='left'>";
                                                $html .= number_format(($listagem_pavimento['area']), 2, '.', '');
                                                $html .= " m²</td>
                                                        </tr>";
                                        }
                                        $html .= "
                                        <tr>
                                                <td width='22' align='left'></td>
                                                <td width='10' align='right'><b>TOTAL</b></td>
                                                <td width='40' align='left'>";
                                                $html .= number_format(($total_pat), 2, '.', '');
                                                $html .= " m²</td>
                                        </tr>
                                </table>";
                        }
                        $html .= "</td>
                                <td>";
                                //  Listando informações sobre pavimentacao.
                        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                        $con_listagem_pavimento = $configuracoes->consulta("SELECT
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'apa' ORDER BY processos.processos_pavimentacao.id_pav ASC");
                        if ( $con_listagem_pavimento->rowCount() > 0 ) {
                                $html .= "<table align='left' width='254' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:12px;'>
                                        <tr>
                                                <td colspan='4' width='170' align='center'><b>ÁREA PISCINA</b></td>
                                        </tr>";
                                        $n_pav = 0;
                                $total_pat = 0;
                                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) {
                                                $n_pav++;
                                $total_pat += $listagem_pavimento['area'];

                                                $html .= "<tr>
                                                <td width='30' align='left'></td>
                                                <td width='10' align='right'><b>PAV ";
                                                $html .= $n_pav;
                                                $html .= "</b></td>
                                                        <td width='40' align='left'>";
                                                $html .= number_format(($listagem_pavimento['area']), 2, '.', '');
                                                $html .= " m²</td>
                                                        </tr>";
                                        }
                                        $html .= "
                                        <tr>
                                                <td width='22' align='left'></td>
                                                <td width='10' align='right'><b>TOTAL</b></td>
                                                <td width='40' align='left'>";
                                                $html .= number_format(($total_pat), 2, '.', '');
                                                $html .= " m²</td>
                                        </tr>
                                </table>";
                        }
                        $html .= "</tr>
                        </table>
                        </td>
                        </tr>";
// Observações
$html .= "<tr>
                <td align='center' colspan='6'>
                    <table align='center' width='400' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='300' align='right'><b>OBS:</b></td>
                                <td align='left' width='300' style='font-size:20px;'>";
                                $html .= $_GET['obs'];
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

if ( $_GET['temp'] == 'ok' ) {
    $temp = "<tr>
                    <td align='right' width='455'><strong>ÁREA DE CADA UNIDADE:</strong></td>
                    <td align='left'>44,50 m²</td>
                </tr>
                <tr>
                    <td align='right' width='455'><strong>ÁREA A CONSTRUIR(176 UNIDADES):</strong></td>
                    <td align='left'>7.832,00 m²</td>
                </tr>
                <tr>
                    <td align='right' width='455'><strong>ÁREA COMUM:</strong></td>
                    <td align='left'>507,62 m²</td>
                </tr>
                <tr>
                    <td colspan='2'><hr /></td>
                </tr>";
}

$rodape = "<table align='center' width='800' style='border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                ".$temp."
                <!--<tr>
                    <td align='right' width='455'><strong>SUBTOTAL (";
                    if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 or $linha2['tipoprocesso'] == 6 ) {
                      $rodape .= 'CONSTRUIR';
                    } else if ( $linha2['tipoprocesso'] == 2 ) {
                      $rodape .= 'REGULARIZAR';
                    }
                    $rodape .= "):</strong></td>
                    <td align='left'>";
                    $rodape .= $total_pat2;
                    $rodape .= " m²</td>
                </tr>-->
                <tr>
                    <td align='right'><strong>TOTAL (";
                    if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 or $linha2['tipoprocesso'] == 6 ) {
                      $rodape .= 'CONSTRUIR';
                    } else if ( $linha2['tipoprocesso'] == 2 ) {
                      $rodape .= 'REGULARIZAR';
                    }

                    if ( $total_are > 0 ) {
                      $rodape .= ' + EXISTENTE';
                    }
                    /* Remoção solicitada pelo Vando.
                    if ( $total_sbc > 0 ) {
                      $rodape .= ' + SUBSOLO À CONSTRUIR';
                    }*/
                    $rodape .= "):</strong></td>
                    <td align='left'>";
                    $rodape .= number_format(($total_pat2 + $total_are + $total_sbc), 2, '.', '');
                    $rodape .= " m²</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;padding-bottom:8px;font-size:11px;'>Após a análise dos elementos apresentados e estando os mesmos de acordo com a legislação pertinente em vigor, a Prefeitura Munícipal de Luís Eduardo Magalhães, considera como APROVADO este processo de Solicitação de Alvará.</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;'>__________________________________________________</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-bottom:20px;font-size:11px;'>
                                <strong>Secretaria de Planejamento, Orçamento e Gestão</strong>
                    </td>
                </tr>
        </table>";
