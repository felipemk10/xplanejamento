<div class="section row">
          <div class="col-xs-4 border-xs">
            <strong>Alvará</strong>
          </div><!--t Título do campo -->
          <div class="col-xs-4 border-xs">
            <strong>Área total da construção</strong>
          </div><!--t Título do campo -->
          <div class="col-xs-4 border-xs">
            <strong>Finalidade</strong>
          </div><!--t Título do campo -->
        </div>
        <div class="section row">
          <div class="col-xs-4 border-xs">
            <?php
              //  Listando informações sobre responsável técnico.
              $con_listagem_condominio = $configuracoes->consulta("SELECT 
                processos_condominio.id_con,
                processos_condominio.finalidadeobra,
                processos_condominio.areaterreno,
                processos_condominio.situacaoterreno,
                processos_condominio.desmembramento

                FROM 
                processos.processos_condominio

                WHERE processos.processos_condominio.id_pro = $id_pro");

                $listagem_condominio = $con_listagem_condominio->fetch();

                $id_con = $listagem_condominio['id_con'];
                // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial  

              $con_que = $configuracoes->consulta("SELECT 
                  id_que,   
                  areacontruir,   
                  areaexistente,  
                  areausoexclusivo,   
                  areacomumproporcional,  
                  tipoprocesso

                  FROM 
                  processos.processos_que

                  WHERE processos_que.id_que = ".$linha_alvarahabitese['id_que']."");
            $linha_que = $con_que->fetch();

              if ( $linha_que['tipoprocesso'] == 'c' ) {
                echo 'CONSTRUÇÃO';
              } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                echo 'REGULARIZAÇÃO';
              } else if ( $linha_que['tipoprocesso'] == 'a' ) {
                echo 'ACRÉSCIMO DE ÁREAS';
              }

            ?>
          </div><!-- conteudo texto -->
          <div class="col-xs-4 border-xs">
            <?php echo $linha_que['areacontruir'] + $linha_que['areaexistente']; ?> m<sup>2</sup>
          </div><!--Conteúdo texto -->
          <div class="col-xs-4 border-xs">
            <?php 
              if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
                echo 'RESIDENCIAL EM CONDOMÍNIO - ';
              } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
                echo 'CONDOMÍNIO MISTO - ';
              }
              echo '<strong>UNIDADE: '.(int)$_GET['unidade'].'</strong>';
            ?>
          </div><!--Conteúdo texto -->
        </div><!-- Fim bloco -->
          <br>
          <br>
         <div class="section row">
          <div class="col-xs-12 border-xs">
            <strong>Descrição</strong>
          </div><!--t Título do campo -->
        </div><!--fim do bloco -->
        <div class="section row">
          <div class="col-xs-12 border-xs">
            A edifição que tem como finalidade o uso <?php 
              if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
                echo '<strong>RESIDENCIAL EM CONDOMÍNIO</strong>';
              } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
                echo '<strong>CONDOMÍNIO MISTO</strong>';
              }
            ?>, totalizando uma área construída de <?php echo $linha_que['areacontruir']; ?> m<sup>2</sup>,  além de estar em conformidade com a legislação municipal vigente, encontra-se de acordo com o(s) Alvará(s)
de <?php
              if ( $linha_que['tipoprocesso'] == 'c' ) {
                echo '<strong>CONSTRUÇÃO</strong>';
              } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                echo '<strong>REGULARIZAÇÃO</strong>';
              } else if ( $linha_que['tipoprocesso'] == 'a' ) {
                echo '<strong>ACRÉSCIMO DE ÁREAS</strong>';
              }

              $consulta_alvara_ah = $configuracoes->consulta("SELECT 
              numero,   
              ano
              
              FROM 

              processos.processos_alvarahabitese 

              WHERE processos_alvarahabitese.id_ah = ".$linha_alvarahabitese['id_ah2']."");
              $linha_alvara_ah = $consulta_alvara_ah->fetch();
            ?>, número(s) <?php if ( $linha2['tipoprocesso'] == 2 ) { echo 'R.'; } echo $linha_alvara_ah['numero'].' / '.$linha_alvara_ah['ano']; ?>