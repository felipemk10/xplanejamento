<?php
//  Listando informações sobre responsável técnico.
  $con_listagem_redimensionamento = $configuracoes->consulta("SELECT 
    processos_redimensionamento.id_red,
    processos_redimensionamento.tipo,
    processos_redimensionamento.areatotalterreno,
    processos_redimensionamento.sa_qtdlotes,  
    processos_redimensionamento.sp_qtdlotes,
    processos_redimensionamento.descricaolotes,
    processos_redimensionamento.requerente,
    processos_redimensionamento.requerentetelefone

    FROM 
    processos.processos_redimensionamento

    WHERE processos.processos_redimensionamento.id_pro = $id_pro");

    $listagem_redimensionamento = $con_listagem_redimensionamento->fetch();

    $id_red = $listagem_redimensionamento['id_red'];
    // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
?>
<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Tipo:</div>
        <div class="col-xs-3 text-left campo-texto"><?php 
            if ( $listagem_redimensionamento['tipo'] == 1 ) {
              echo 'Remembramento';
            } else if ( $listagem_redimensionamento['tipo'] == 2 ) {
              echo 'Desmembramento';
            } else if ( $listagem_redimensionamento['tipo'] == 0 ) {
              echo 'Redimensionamento';
            }
          ?>
        </div>
      </div>
      <br />
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo"><strong>Objeto / Situação Atual</strong></div>
      </div>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Quantidade de Lotes:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['sa_qtdlotes']; ?> Unidades</div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['areatotalterreno']; ?> Unidades</div>
      </div>
      <br />
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo"><strong>Objeto / Situação Pretendida</strong></div>
      </div>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Quantidade de Lotes:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['sp_qtdlotes']; ?> Unidades</div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Descrição dos Lotes:</div>
        <div class="col-xs-4 text-left campo-texto"><?php echo $listagem_redimensionamento['descricaolotes']; ?></div>
      </div>
      <br />
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Requerentes:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['requerente']; ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Telefone:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_redimensionamento['requerentetelefone']; ?></div>
      </div>

      <br>
      <br>
      
      <?php include('logsImpressao.php'); ?>
      
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>
      </div>