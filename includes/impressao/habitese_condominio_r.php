<?php

$html .= "<tr> <!-- ALVARÁ  -->
                <td align='center' colspan='6'>
                    <table align='center' width='300' style='padding-top:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='190' align='right'><b>ALVARÁ:</b></td>
                                <td align='left' width='300'>";
                                //  Listando informações sobre responsável técnico.
                                  $con_listagem_condominio = $configuracoes->consulta("SELECT 
                                    processos_condominio.id_con,
                                    processos_condominio.finalidadeobra,
                                    processos_condominio.areaterreno,
                                    processos_condominio.situacaoterreno,
                                    processos_condominio.desmembramento

                                    FROM 
                                    processos.processos_condominio

                                    WHERE processos.processos_condominio.id_pro = $id_pro");

                                    $listagem_condominio = $con_listagem_condominio->fetch();

                                    $id_con = $listagem_condominio['id_con'];
                                    // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial  

                                  $con_que = $configuracoes->consulta("SELECT 
                                      id_que,   
                                      areacontruir,   
                                      areaexistente,  
                                      areausoexclusivo,   
                                      areacomumproporcional,  
                                      tipoprocesso

                                      FROM 
                                      processos.processos_que

                                      WHERE processos_que.id_que = ".$linha_alvarahabitese['id_que']."");
                                $linha_que = $con_que->fetch();

                                  if ( $linha_que['tipoprocesso'] == 'c' ) {
                                    $html .= 'CONSTRUÇÃO';
                                  } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                                    $html .= 'REGULARIZAÇÃO';
                                  } else if ( $linha_que['tipoprocesso'] == 'a' ) {
                                    $html .= 'ACRÉSCIMO DE ÁREAS';
                                  }
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

$html .= "<tr> <!-- FINALIDADE DE OBRA  -->
                <td align='center' colspan='6'>
                    <table align='center' width='300' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='190' align='right'><b>FINALIDADE DA OBRA:</b></td>
                                <td align='left' width='300'>";
                                if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
                                    $html .= 'RESIDENCIAL EM CONDOMÍNIO - ';
                                } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
                                    $html .= 'CONDOMÍNIO MISTO - ';
                                }
                                    $html .= '<strong>UNIDADE: '.(int)$_GET['unidade'].'</strong>';
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

$html .= "<tr>
                <td align='center' colspan='6'>
                    <table align='center' width='400' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='300' align='right'><b>ÁREA TOTAL DA CONSTRUÇÃO:</b></td>
                                <td align='left' width='300' style='font-size:20px;'>";
                                $html .= '<b>'.($linha_que['areacontruir'] + $linha_que['areaexistente']).'</b>';
                                $html .= " m²</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

// Observações
$html .= "<tr>
                <td align='center' colspan='6'>
                    <table align='center' width='400' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='300' align='right'><b>OBS:</b></td>
                                <td align='left' width='300' style='font-size:20px;'>";
                                $html .= $_GET['obs'];
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";


$rodape = "<table align='center' width='800' style='border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;padding-bottom:8px;'>
                    A edifição que tem como finalidade o uso <b>";
              if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                $rodape .= 'RESIDENCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                $rodape .= 'COMERCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                $rodape .= 'MISTO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                $rodape .= 'INSTITUCIONAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                $rodape .= 'GALPÃO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                $rodape .= 'INDUSTRIAL';
              }
            $rodape .= "</b>, totalizando uma área construída de <b>";
            $rodape .= $linha_que['areacontruir']+$linha_que['areaexistente']; 
            $rodape .= " m²</b>,  além de estar em conformidade com a legislação municipal vigente, encontra-se de acordo com o(s) Alvará(s)
de <b>";
              if ( $linha2['tipoprocesso'] == 1 ) {
                $rodape .= 'CONSTRUÇÃO';
              } else if ( $linha2['tipoprocesso'] == 2 ) {
                $rodape .= 'REGULARIZAÇÃO DE OBRAS';
              } else if ( $linha2['tipoprocesso'] == 3 ) {
                $rodape .= 'ACRÉSCIMO DE ÁREAS';
              } else if ( $linha2['tipoprocesso'] == 4 ) {
                $rodape .= 'CONDOMÍNIO';
              } else if ( $linha2['tipoprocesso'] == 5 ) {
                $rodape .= 'REDIMENSIONAMENTO';
              }
              
              $consulta_alvara_ah = $configuracoes->consulta("SELECT 
              numero,   
              ano
              
              FROM 

              processos.processos_alvarahabitese 

              WHERE processos_alvarahabitese.id_ah = ".$linha_alvarahabitese['id_ah2']."");
              $linha_alvara_ah = $consulta_alvara_ah->fetch();
            
            $rodape .= "</b>, número(s) ";
            if ( $linha2['tipoprocesso'] == 2 ) { 
                $rodape .= "R."; 
            } 
            $rodape .= $linha_alvara_ah['numero'].' / '.$linha_alvara_ah['ano'];
                    $rodape .= "</td>
                </tr>
                <tr>
                    <td align='center' colspan='2'><br /><br /><br /><br /><br /></td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;'>__________________________________________________</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-bottom:20px;font-size:12px;'>
                                <strong>Secretaria de Planejamento, Orçamento e Gestão</strong>
                    </td>
                </tr>
        </table>";
