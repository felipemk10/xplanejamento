<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Finalidade da Obra</div>
        <?php 
          //  Listando informações sobre responsável técnico.
          $con_listagem_condominio = $configuracoes->consulta("SELECT 
            processos_condominio.id_con,
            processos_condominio.finalidadeobra,
            processos_condominio.areaterreno,
            processos_condominio.situacaoterreno,
            processos_condominio.desmembramento

            FROM 
            processos.processos_condominio

            WHERE processos.processos_condominio.id_pro = $id_pro");

            $listagem_condominio = $con_listagem_condominio->fetch();

            $id_con = $listagem_condominio['id_con'];
            // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial            

        ?>
        <div class="col-xs-2 text-left campo-texto"><?php 
                          if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
                            echo 'Residencial em Condomínio';
                          } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
                            echo 'Condomínio Misto';
                          }
                        ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_condominio['areaterreno']; ?> m<sup>2</sup></div>
      </div>
      
      <?php 
          $con_listagem_que = $configuracoes->consulta("SELECT 
                  id_que,   
                  areacontruir,   
                  areaexistente,  
                  areausoexclusivo,   
                  areacomumproporcional,  
                  tipoprocesso

                  FROM 
                  processos.processos_que

                  WHERE processos.processos_que.id_con = $id_con 
                  ORDER BY processos.processos_que.id_con ASC");

          $total_areacontruir           = 0;
          $total_areaexistente          = 0;
          $total_areausoexclusivo       = 0;
          $total_areacomumproporcional  = 0;

          $pav1_areaconstruir           = 0;
          $pav1_areausuexclusivo        = 0;
          // Variável utilizada para auxiliar o script a pegar somente o valor do primeiro pavimento de casa unidade.
          $id_anterior                  = 0;

          foreach ( $con_listagem_que as $listagem_que ) {
            $total_areacontruir = $total_areacontruir+$listagem_que['areacontruir'];
            $total_areausoexclusivo = $total_areausoexclusivo+$listagem_que['areausoexclusivo'];
            $total_areacomumproporcional = $total_areacomumproporcional+$listagem_que['areacomumproporcional'];

            //  Listando informações sobre as unidades pavimentos.
            $con_listagem_unidades_pavimentos = $configuracoes->consulta("SELECT 
              id_que,   
              areacontruir,   
              areaexistente,  
              areausoexclusivo,   
              areacomumproporcional

              FROM 
              processos.processos_que_pavimentos

              WHERE processos.processos_que_pavimentos.id_que = ".$listagem_que['id_que']." ORDER BY processos.processos_que_pavimentos.id_que ASC");
              
              

              foreach ( $con_listagem_unidades_pavimentos as $listagem_unidades_pavimentos ) { 
                if ( $id_anterior != $listagem_que['id_que'] ) {
                    $pav1_areaconstruir     += $listagem_unidades_pavimentos['areacontruir'];
                    $pav1_areausuexclusivo  = $listagem_unidades_pavimentos['areausoexclusivo'];
                    $id_anterior = $listagem_que['id_que'];
                }

                $total_areacontruir           += $listagem_unidades_pavimentos['areacontruir'];
                $total_areaexistente          += $listagem_unidades_pavimentos['areaexistente'];
                $total_areausoexclusivo       += $listagem_unidades_pavimentos['areausoexclusivo'];
                $total_areacomumproporcional  += $listagem_unidades_pavimentos['areacomumproporcional'];
              }
          }

          $total_areausoexclusivo2      = $total_areausoexclusivo;
          $total_areacomumproporcional2 = $total_areacomumproporcional;
        ?>
        <?php if ( $total_areacontruir > 0 ) { ?>
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Área Comum Proporcional:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $total_areacomumproporcional; ?> m<sup>2</sup></div>
        </div>
        <?php } ?>
        <!--<div class="section row">
          <div class="col-xs-2 text-right subtitulo"><?php 
                          if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                            echo 'Construir';
                          } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                            echo 'Regularizar';
                          }
                          ?>:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo $total_areacontruir; ?> m<sup>2</sup></div>
        </div>-->
        <div class="section row">
          <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round((($pav1_areaconstruir/($listagem_condominio['areaterreno']))*100),2); ?>%</span></div>
          <div class="col-xs-2 text-right subtitulo">Ind. Utilização:</div>
          <div class="col-xs-2 text-left campo-texto"><?php echo round(($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?></span></div>
        </div>
        <br />
        <br />
        <div class="section row">
          <div class="col-xs-10" style="font-size: 11px;">
            <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>Unidades</th>
                              <th>Área a Construir(m²)</th>
                              <th>Área Existente(m²)</th>
                              <th>Área de Uso Exclusico(m²)</th>
                              <th>Área Comum Proporcional(m²)</th>
                              <th>Fração Ideal</th>
                              <th>Taxa de Ocupação Proporcional (%)</th>
                              <th>Índice de Utilização Proporcional</th>
                              <th>Tipo do Processo(m²)</th>
                              <?php if ( $emissaoalvarahabitese ) { ?><th style="width:300px;"></th><?php } ?>
                            </tr>
                          </thead>
                         <tbody>
                          <?php 
                            //  Listando informações sobre as unidades.
                            $con_listagem_unidades = $configuracoes->consulta("SELECT 
                              id_que,   
                              areacontruir,   
                              areaexistente,  
                              areausoexclusivo,   
                              areacomumproporcional,  
                              tipoprocesso

                              FROM 
                              processos.processos_que

                              WHERE processos.processos_que.id_con = $id_con ORDER BY processos.processos_que.id_que ASC");
                              $n_uni = 0;

                              $total_areacontruir = 0;
                              $total_areaexistente = 0;
                              $total_areausoexclusivo = 0;
                              $total_areacomumproporcional = 0;
                              foreach ( $con_listagem_unidades as $listagem_unidades ) { 
                                $n_uni++; 

                                //  Listando informações sobre as unidades pavimentos.
                                $con_listagem_unidades_pavimentos = $configuracoes->consulta("SELECT 
                                  id_que,   
                                  areacontruir,   
                                  areaexistente,  
                                  areausoexclusivo,   
                                  areacomumproporcional

                                  FROM 
                                  processos.processos_que_pavimentos

                                  WHERE processos.processos_que_pavimentos.id_que = ".$listagem_unidades['id_que']." ORDER BY processos.processos_que_pavimentos.id_que ASC");
                                  
                                  $total_areacontruir           = 0;
                                  $total_areaexistente          = 0;
                                  $total_areausoexclusivo       = 0;
                                  $total_areacomumproporcional  = 0;

                                  $pav1_areaconstruir           = 0;
                                  $pav1_areausuexclusivo        = 0;

                                  foreach ( $con_listagem_unidades_pavimentos as $listagem_unidades_pavimentos ) { 
                                    if ( $pav1_areaconstruir == 0 and $pav1_areausuexclusivo == 0 ) {
                                      $pav1_areaconstruir = $listagem_unidades_pavimentos['areacontruir'];
                                      $pav1_areausuexclusivo = $listagem_unidades_pavimentos['areausoexclusivo'];
                                    }

                                    $total_areacontruir           += $listagem_unidades_pavimentos['areacontruir'];
                                    $total_areaexistente          += $listagem_unidades_pavimentos['areaexistente'];
                                    $total_fracao                 += round((( ($listagem_unidades_pavimentos['areausoexclusivo']+$listagem_unidades_pavimentos['areacomumproporcional'])  /  ($total_areausoexclusivo2+$total_areacomumproporcional2)  )*100),2);
                                    $total_areausoexclusivo       += $listagem_unidades_pavimentos['areausoexclusivo'];
                                    $total_areacomumproporcional  += $listagem_unidades_pavimentos['areacomumproporcional'];
                                  }

                                  // Totais do rodapé
                                  $total_areacontruir3            += $total_areacontruir;
                                  $total_areaexistente3           += $total_areaexistente;
                                  $total_areausoexclusivo3        += $total_areausoexclusivo;
                                  $total_areacomumproporcional3   += $total_areacomumproporcional;
                                ?>
                                <input type="hidden" value="<?php echo $n_uni; ?>" name="num_casa[]">
                      
                                <input type="hidden" name="IDunidadesAdd[]" id="IDunidadesAdd[]" value="<?php echo $listagem_unidades['id_que']; ?>">
                                    <tr style="border-bottom: 1px solid #eeeeee;">
                                     <td>Casa <?php echo $n_uni; ?></td>
                                        <td align="left"><?php if ( $total_areacontruir > 0 ) { echo $total_areacontruir; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $total_areaexistente > 0 ) { echo $total_areaexistente; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $total_areausoexclusivo > 0 ) { echo $total_areausoexclusivo; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $total_areacomumproporcional > 0 ) { echo $total_areacomumproporcional; ?> m<sup>2</sup><?php } ?></td>
                                        <td><?php echo $total_fracao; ?> %</td>
                                        <td>
                                          <?php 
                                            $tocupacao = (round((($pav1_areaconstruir/$listagem_condominio['areaterreno'])*100),2));
                                            echo $tocupacao; 
                                          ?> %</td>
                                        <td><?php echo round(($total_areacontruir/$listagem_condominio['areaterreno']),2); ?></td>

                                        <td style="text-align=left;">
                                          <?php if ( $listagem_unidades['tipoprocesso'] == 'c' ) { echo "Construção"; } else if ( $listagem_unidades['tipoprocesso'] == 'r' ) { echo "Regularização"; } else if ( $listagem_unidades['tipoprocesso'] == 'a' ) { echo "Ácrescimo de área"; } ?>
                                        </td>
                                        
                                    </tr>
                                    
                                    <?php 

                                    // Zerando fração para próxima unidade
                                    unset($total_fracao);  

                                    if ( $emissaoalvarahabitese ) { ?>
                                      <tr>
                                        <td colspan="9">
                                        <table class="table">
                                          <tbody>
                                        <?php
                                          $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
                                            id_ah,
                                            id_pro,
                                            -- 1 = Alvará, 2 = Habite-se
                                            tipo,
                                            numero,   
                                            id_cg,
                                            ano,
                                            processos_que.tipoprocesso,
                                            substituir

                                            FROM 

                                            processos.processos_alvarahabitese 

                                            INNER JOIN processos.processos_que ON processos_que.id_que = processos_alvarahabitese.id_que

                                            WHERE processos_alvarahabitese.id_pro = $id_pro and processos_alvarahabitese.id_que = ".$listagem_unidades['id_que']."");
                                            
                                            if ( $consulta_alvarahabitese->rowCount() > 0 ) { ?>

                                            <?php 
                                            $alvara[$n_uni] = false;
                                            $habitese[$n_uni] = false;
                                            foreach ( $consulta_alvarahabitese as $listagem_alvarahabitese ) { 
                                                //  CONSULTANDO DO REGISTRO SUBSTITUIDO
                                                $substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$listagem_alvarahabitese['substituir']." and tipo = ".$listagem_alvarahabitese['tipo']."")->fetch();
                                              ?> 
                                             

                                            <tr>
                                                    <td><?php if ( $listagem_alvarahabitese['tipoprocesso'] == 'r' and $listagem_alvarahabitese['tipo'] != 2 ) { echo 'R.'; } echo $listagem_alvarahabitese['numero'].'/'.$listagem_alvarahabitese['ano']; ?></td>
                                                    <td><a 
                                                    <?php
                                                      if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) { ?>
                                                        href="alvara_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&unidade=<?php echo $n_uni; ?>&tocupacao=<?php echo $tocupacao; ?>&obs=<?php if ( $_GET['condominio_pavimentos'] == 't' ) { ?>&condominio_pavimentos=t<?php } ?>" target="_blank">
                                                      <?php } else if ( $listagem_alvarahabitese['tipo'] == 2 ) { ?>
                                                        href="habitese_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&unidade=<?php echo $n_uni; ?>&th=t&obs=<?php if ( $_GET['condominio_pavimentos'] == 't' ) { ?>&condominio_pavimentos=t<?php } ?>" target="_blank">
                                                        <?php } ?>
                                                      <?php 

                                                      if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) {
                                                        $alvara[$n_uni] = true;
                                                        $id_ah = $listagem_alvarahabitese['id_ah'];
                                                        echo 'Visualizar Alvará'; if ( (int)$listagem_alvarahabitese['substituir'] > 0 ) { echo ' - Substituindo o alvará: '; if ( $listagem_alvarahabitese['tipo'] == 3 ) { echo 'R.'; } echo $listagem_alvarahabitese['substituir'].'/'.$substituto['ano']; }
                                                      } else if ( $listagem_alvarahabitese['tipo'] == 2 ) {
                                                        $habitese[$n_uni] = true;
                                                        echo 'Visualizar Habite-se'; if ( (int)$listagem_alvarahabitese['substituir'] > 0 ) { echo ' - Substituindo o habite-se: '.$listagem_alvarahabitese['substituir'].'/'.$substituto['ano']; }
                                                      }
                                                    ?>
                                                    </a></td>
                                                    <td>
                                                       <form method="post" id="admin-form" action="back_geraralvara.php">
                                                        <input type="hidden" name="form" value="deleteah">
                                                        <input type="hidden" name="delete_id_ah" value="<?php echo (int)$listagem_alvarahabitese['id_ah']; ?>">
                                                        <input type="hidden" name="id_pro" value="<?php echo (int)$_GET['id_pro']; ?>">
                                                        <input type="hidden" name="tipoprocesso" value="<?php echo (int)$_GET['tipoprocesso']; ?>">
                                                        <button type="submit" class="btn-info">Excluir</button>
                                                      </form>
                                                    </td>
                                                  </tr>
                                                  <?php } ?>
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                          <?php }  ?>
                                          
                                        <?php } ?>
                                    
                              <?php } ?>
                              </tbody>
                            
                              <footer>
                                <tr>
                                  <th>Total do condomínio</th>
                                  <th><?php echo $total_areacontruir3; ?> (m²)</th>
                                  <th><?php echo $total_areaexistente3; ?> (m²)</th>
                                  <th><?php echo $total_areausoexclusivo3; ?> (m²)</th>
                                  <th><?php echo $total_areacomumproporcional3; ?> (m²)</th>
                                  <th><!--<?php //echo $total_fracao; ?> %--></th>

                                  <th><!--<?php //echo round(( ($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional))*100),2); ?> %--></th>
                                  <th><!--<?php //echo round( ($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?>--></th>
                                  <th></th>

                                  <?php if ( $emissaoalvarahabitese ) { ?><th></th><?php } ?>
                                </tr>
                              </footer>
                            </table>
          </div>                  
        </div><!-- Fim da row -->
        <?php 
          //  Destruindo variáveis.
          unset($total_areacontruir,$total_areacomumproporcional); ?>
      <br>
      <br>
      <?php include('logsImpressao.php'); ?>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>