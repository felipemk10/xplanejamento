<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Finalidade da Obra</div>
        <?php 
          //  Listando informações sobre responsável técnico.
          $con_listagem_alvara = $configuracoes->consulta("SELECT 
            processos_alvara.id_alv,
            processos_alvara.finalidadeobra,
            processos_alvara.areaterreno,
            processos_alvara.situacaoterreno,
            processos_alvara.desmembramento,
            processos_alvara.taxapermeabilidade

            FROM 
            processos.processos_alvara

            WHERE processos.processos_alvara.id_pro = $id_pro");

            $listagem_alvara = $con_listagem_alvara->fetch();

            $id_alv = $listagem_alvara['id_alv'];
            // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
        ?>
        <div class="col-xs-3 text-left campo-texto"><?php 
                          if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                            echo 'Residencial';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                            echo 'Comercial';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                            echo 'MISTO';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                            echo 'Institucional';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                            echo 'Galpão';
                          } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                            echo 'Industrial';
                          }
                        ?></div>
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Terreno:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_alvara['areaterreno']; ?> m<sup>2</sup></div>
      </div>
      <?php 
        /*  Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
          pat = Pavimento Terreo 
          are = área Existente
          sbe = área sub solo existente
        */
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area,
          processos_pavimentacao.tipo

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and (processos.processos_pavimentacao.tipo = 'pat' or processos.processos_pavimentacao.tipo = 'are' or processos.processos_pavimentacao.tipo = 'sbe') 
          ORDER BY processos.processos_pavimentacao.id_pav ASC");

        $total_contruir = 0;
        $total_pat = 0;
        $total_are = 0;
        $total_sbe = 0;

        $total_patare_terreo = 0;
        $i = 0;
        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            
            $total_contruir = $total_contruir + $listagem_pavimento['area'];

            if ( $listagem_pavimento['tipo']  == 'pat' ) {
              $total_pat = $total_pat+$listagem_pavimento['area'];
              if ( $i == 0 ) {
                $total_patare_terreo = $listagem_pavimento['area'];
                $i++;
              }
            }
            if (  $listagem_pavimento['tipo']  == 'are' ) {
              $total_are = $total_are+$listagem_pavimento['area'];
               if ( $i == 1 ) {
                $total_patare_terreo = $total_patare_terreo + $listagem_pavimento['area'];
                $i++;
              }
            }
            if (  $listagem_pavimento['tipo']  == 'sbe' ) {
              $total_sbe = $total_sbe+$listagem_pavimento['area'];
            }
        }

        // Como a variável é usada em outras funções abaixo, criei uma nova para não perder o valor afim de colocar o mesmo no subtotal no rodapé do relatório.
        $total_pat2 = $total_pat;
      ?>
      <?php if ( $total_pat > 0 ) { ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo"><?php 
                        if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 ) {
                          echo 'Construir';
                        } else if ( $linha2['tipoprocesso'] == 2 ) {
                          echo 'Regularizar';
                        }
                        ?>:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
      </div>
      <?php } if ( $total_are > 0 ) { ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Existente:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $total_are; ?> m<sup>2</sup></div>
      </div>
      <?php } if ( $total_sbe > 0 ) { ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Subsolo Existente:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $total_sbe; ?> m<sup>2</sup></div>
      </div>
      <?php } ?>
      
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo round((($total_patare_terreo/$listagem_alvara['areaterreno'])*100),2); ?> %</div>
        <div class="col-xs-2 text-right subtitulo">Ind. Utilização:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo round(($total_contruir/$listagem_alvara['areaterreno']),2); ?></div> 

      </div>
      <br>
      <br>

      <?php include('logsImpressao.php'); ?>

      <br>
      <br>
      <div class="section row hidden-print">
        <div class="col-xs-2 text-right subtitulo">Detalhes
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultdetalhes" id="checkocultdetalhes" onclick="ap30();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="camposocultosmaisdetana" style="display: none;">
      <?php
        //  Listando informações sobre pavimentacao.
        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'pat' ORDER BY processos.processos_pavimentacao.id_pav ASC");
          $n_pav = 0;
          $total_pat = 0; ?>

          <?php if ( $con_listagem_pavimento->rowCount() > 0 ) { ?>
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Área a construir</div>
            </div>
            <br>
      <?php 
          
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            $n_pav++;
            $total_pat+= $listagem_pavimento['area']; ?>
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo (1)'; } else { echo $n_pav; } ?>: </div>
              <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
            </div>
          <?php } ?>
          <div class="section row">
            <div class="col-xs-2 text-right subtitulo">Total: </div>
            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
          </div><hr />
          <?php } ?>
      <?php
        //  Listando informações sobre pavimentacao.
        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbc' ORDER BY processos.processos_pavimentacao.id_pav ASC");

        if ( $con_listagem_pavimento->rowCount() > 0 ) {
      ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Subsolo à Construir</div>
      </div>
      <br>
      

      <?php
            $n_pav = 0;
            $total_pat = 0;
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            $n_pav++;
            $total_pat+= $listagem_pavimento['area']; ?>
            
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
              <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
            </div>

            <?php } ?>
              <div class="section row">
                <div class="col-xs-2 text-right subtitulo">Total: </div>
                <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
              </div><hr /> 
            <?php } ?>
      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'are' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Existente</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                           <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo'; } else { echo $n_pav; } ?>: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                          </div>

                          <?php } ?>
                            <div class="section row">
                              <div class="col-xs-2 text-right subtitulo">Total: </div>
                              <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                            </div>
                        <?php } ?><hr />
                        
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbe' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Sub Solo Existente</div>
                    </div>
                    <br>

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div><hr />
                        <?php } ?>
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'adk' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Deck</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div><hr />
                        <?php } ?>
                      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'apa' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Piscina</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div>
                        <?php } ?>
      </div>

      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>
      </div>