<?php

$html .= "<tr> <!-- ALVARÁ  -->
                <td align='center' colspan='6'>
                    <table align='center' width='300' style='padding-top:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='190' align='right'><b>ALVARÁ:</b></td>
                                <td align='left' width='300'>";
                                if ( $linha2['tipoprocesso'] == 1 ) {
                                    $html .= 'CONSTRUÇÃO';
                                } else if ( $linha2['tipoprocesso'] == 2 ) {
                                    $html .= 'REGULARIZAÇÃO DE OBRAS';
                                } else if ( $linha2['tipoprocesso'] == 3 ) {
                                    $html .= 'ACRÉSCIMO DE ÁREAS';
                                } else if ( $linha2['tipoprocesso'] == 4 ) {
                                    $html .= 'CONDOMÍNIO EDILÍCIO';
                                } else if ( $linha2['tipoprocesso'] == 5 ) {
                                    $html .= 'REDIMENSIONAMENTO';
                                } else if ( $linha2['tipoprocesso'] == 6 ) {
                                    $html .= 'PROJETO';
                                }
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

$html .= "<tr> <!-- FINALIDADE DE OBRA  -->
                <td align='center' colspan='6'>
                    <table align='center' width='300' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='190' align='right'><b>FINALIDADE DA OBRA:</b></td>
                                <td align='left' width='300'>";
                                if ( ( $id_pro == '1101559' || $id_pro == '1101560' ) and $listagem_alvara['finalidadeobra'] == 're' ) {
                                    $html .= 'RESIDENCIAL EM CONDOMÍNIO';
                                } else if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                                    $html .= 'RESIDENCIAL';
                                } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                                    $html .= 'COMERCIAL';
                                } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                                    $html .= 'MISTO';
                                } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                                    $html .= 'INSTITUCIONAL';
                                } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                                    $html .= 'GALPÃO';
                                } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                                    $html .= 'INDUSTRIAL';
                                }
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

// Descrição muda se houver valor informado via GET.
if ( $_GET['valor_parcial'] > 0 ) {
    $titulo_areatotal   =   'HABITE-SE PARCIAL';
    $total_contruir2     =   $_GET['valor_parcial'];
} else {
    $titulo_areatotal   =   'ÁREA TOTAL DA CONSTRUÇÃO';
    $total_contruir2     =   $total_contruir;
}

$html .= "<tr>
                <td align='center' colspan='6'>
                    <table align='center' width='400' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='300' align='right'><b>".$titulo_areatotal.":</b></td>
                                <td align='left' width='300' style='font-size:20px;'>";
                                $html .= '<b>'.number_format(($total_contruir2), 2, '.', '').'</b>';
                                $html .= " m²</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

// Observações
$html .= "<tr>
                <td align='center' colspan='6'>
                    <table align='center' width='700' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td align='center' style='font-size:16px;'><b>Obs.:</b> ";
                                $html .= $_GET['obs'];
                                $html .= "</td>
                            </tr>
                    </table>
                </td>
        </tr>";


$rodape = "<table align='center' width='800' style='border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;padding-bottom:8px;'>
                    A edifição que tem como finalidade o uso <b>";
              if ( $listagem_alvara['finalidadeobra'] == 're' ) {
                $rodape .= 'RESIDENCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'co' ) {
                $rodape .= 'COMERCIAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'mi' ) {
                $rodape .= 'MISTO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'is' ) {
                $rodape .= 'INSTITUCIONAL';
              } else if ( $listagem_alvara['finalidadeobra'] == 'ga' ) {
                $rodape .= 'GALPÃO';
              } else if ( $listagem_alvara['finalidadeobra'] == 'id' ) {
                $rodape .= 'INDUSTRIAL';
              }
            $rodape .= "</b>, totalizando uma área construída de <b>";
            $rodape .= $total_contruir; 
            $rodape .= " m²</b>,  além de estar em conformidade com a legislação municipal vigente, encontra-se de acordo com o(s) Alvará(s)
de <b>";
              if ( $linha2['tipoprocesso'] == 1 ) {
                $rodape .= 'CONSTRUÇÃO';
              } else if ( $linha2['tipoprocesso'] == 2 ) {
                $rodape .= 'REGULARIZAÇÃO DE OBRAS';
              } else if ( $linha2['tipoprocesso'] == 3 ) {
                $rodape .= 'ACRÉSCIMO DE ÁREAS';
              } else if ( $linha2['tipoprocesso'] == 4 ) {
                $rodape .= 'CONDOMÍNIO EDILÍCIO';
              } else if ( $linha2['tipoprocesso'] == 5 ) {
                $rodape .= 'REDIMENSIONAMENTO';
              } else if ( $linha2['tipoprocesso'] == 6 ) {
                $rodape .= 'PROJETO';
              }

              
              $consulta_alvara_ah = $configuracoes->consulta("SELECT 
              numero,   
              ano
              
              FROM 

              processos.processos_alvarahabitese 

              WHERE processos_alvarahabitese.id_ah = ".$linha_alvarahabitese['id_ah2']."");
              $linha_alvara_ah = $consulta_alvara_ah->fetch();
            
            $rodape .= "</b>, número(s) ";
            if ( $linha2['tipoprocesso'] == 2 ) { 
                $rodape .= "R."; 
            } 
            $rodape .= $linha_alvara_ah['numero'].' / '.$linha_alvara_ah['ano'];
                    $rodape .= "</td>
                </tr>
                <tr>
                    <td align='center' colspan='2'><br /><br /><br /><br /><br /></td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;'>__________________________________________________</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-bottom:20px;font-size:12px;'>
                                <strong>Secretaria de Planejamento, Orçamento e Gestão</strong>
                    </td>
                </tr>
        </table>";
