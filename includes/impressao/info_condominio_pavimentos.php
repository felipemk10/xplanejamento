<?php

//  Listando informações sobre responsável técnico.
$con_listagem_condominio = $configuracoes->consulta("SELECT 
processos_condominio.id_con,
processos_condominio.finalidadeobra,
processos_condominio.areaterreno,
processos_condominio.situacaoterreno,
processos_condominio.desmembramento

FROM 
processos.processos_condominio

WHERE processos.processos_condominio.id_pro = $id_pro");

$listagem_condominio = $con_listagem_condominio->fetch();

$id_con = $listagem_condominio['id_con'];
// Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial

$html .= "<tr> <!-- FINALIDADE DE OBRA  -->
                <td align='center' colspan='6'>
                        <table align='center' width='300' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                                <tr>
                                        <td width='190' align='right'><b>FINALIDADE DA OBRA:</b></td>
                                        <td align='left' width='200'>";
                                        if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
								        	$html .= 'RESIDENCIAL EM CONDOMÍNIO';
								        } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
								        	$html .= 'CONDOMÍNIO MISTO';
								        }
                                        $html .= "</td>
                                        <td></td>
                                        <td></td>
                                </tr>
                        </table>
                </td>
        </tr>";

$con_listagem_que = $configuracoes->consulta("SELECT 
      id_que,   
      areacontruir,   
      areaexistente,  
      areausoexclusivo,   
      areacomumproporcional,  
      tipoprocesso

      FROM 
      processos.processos_que

      WHERE processos.processos_que.id_con = $id_con 
      ORDER BY processos.processos_que.id_con ASC");

$total_areacontruir           = 0;
$total_areausoexclusivo       = 0;
$total_areacomumproporcional  = 0;
$total_areaexistente          = 0;


$total_areacontruir2           = 0;
$total_areaexistente2          = 0;
$total_areausoexclusivo2       = 0;
$total_areacomumproporcional2  = 0;

$pav1_areaconstruir           = 0;
$pav1_areausuexclusivo        = 0;

// Variável utilizada para auxiliar o script a pegar somente o valor do primeiro pavimento de casa unidade.
$id_anterior                  = 0;

foreach ( $con_listagem_que as $listagem_que ) {     
    //  Listando informações sobre as unidades pavimentos.
    $con_listagem_unidades_pavimentos = $configuracoes->consulta("SELECT 
      id_qp,
      id_que,   
      areacontruir,   
      areaexistente,  
      areausoexclusivo,   
      areacomumproporcional

      FROM 
      processos.processos_que_pavimentos

      WHERE processos.processos_que_pavimentos.id_que = ".$listagem_que['id_que']." ORDER BY processos.processos_que_pavimentos.id_que,id_qp ASC");

    //----

      

    foreach ( $con_listagem_unidades_pavimentos as $listagem_unidades_pavimentos ) { 

        $total_areacontruir           = $total_areacontruir+$listagem_unidades_pavimentos['areacontruir'];
        $total_areausoexclusivo       = $total_areausoexclusivo+$listagem_unidades_pavimentos['areausoexclusivo'];
        $total_areaexistente          = $total_areaexistente+$listagem_unidades_pavimentos['areaexistente'];
        $total_areacomumproporcional  = $total_areacomumproporcional+$listagem_unidades_pavimentos['areacomumproporcional'];
        if ( $id_anterior != $listagem_que['id_que'] ) { 
          $pav1_areaconstruir     += $listagem_unidades_pavimentos['areacontruir'];
          $pav1_areaexistente     += $listagem_unidades_pavimentos['areaexistente'];
          $pav1_areausuexclusivo  += $listagem_unidades_pavimentos['areausoexclusivo'];
          $id_anterior            = $listagem_que['id_que'];
        }

        if ( $linha_que['id_que'] == $listagem_unidades_pavimentos['id_que'] ) { 
            
            if ( $pav1_areaconstruir2 == 0 and !$citacao ) { 
              $citacao = true;
              $pav1_areaconstruir2     += $listagem_unidades_pavimentos['areacontruir'];
              $pav1_areaexistente2     += $listagem_unidades_pavimentos['areaexistente'];
              $pav1_areausuexclusivo2  += $listagem_unidades_pavimentos['areausoexclusivo'];
            }

            $total_areacontruir2           += $listagem_unidades_pavimentos['areacontruir'];
            $total_areausoexclusivo2       += $listagem_unidades_pavimentos['areausoexclusivo'];
            $total_areaexistente2          += $listagem_unidades_pavimentos['areaexistente'];
            $total_areacomumproporcional2  += $listagem_unidades_pavimentos['areacomumproporcional'];
        }
    }
}

$html .= "<tr>
	        <td colspan='6'>DADOS DO CONDOMÍNIO
                <table style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
                        <tr>
                            <td width='170' align='right'><b>ÁREA DO TERRENO:</b></td>
                            <td width='70'>";
                            $html .= number_format(($listagem_condominio['areaterreno']), 2, ',', '');
                            $html .= " m²</td>";
                            
                            if ( $total_areacomumproporcional > 0 ) {
                                $html .= "<td width='200' align='right'><b>";
                                $html .= "ÀREA COMUM PROPORCIONAL";
                                $html .= ":</b></td>
                                <td width='70'>"; 
                                $html .= number_format(($total_areacomumproporcional), 2, ',', '');
                                $html .= " m²</td>";
                            }
                        $html .="</tr>
                        <tr>";
                            if ( $total_areacontruir > 0 ) {
                                $html .= "<td width='200' align='right'><b>";
                                if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
		                        	$html .= "CONSTRUIR";
		                        } else if ( $linha_que['tipoprocesso'] == 'r' ) {
		                            $html .= "REGULARIZAR";
		                        }
                                $html .= ":</b></td>
                                <td width='70'>"; 
                                $html .= number_format(($total_areacontruir), 2, ',', '');
                                $html .= " m²</td>";
                            }
                        $html .="</tr>
                        <tr>";
                            $html .= "<td width='140' align='right'><b>";
                            $html .= "TAXA OCUPAÇÃO:";
                            $html .= "</b></td>
                            <td width='70'>"; 
                            $html .= round(((($pav1_areaconstruir+$pav1_areaexistente) / $listagem_condominio['areaterreno'] )*100),2);
                            $html .= "%</td>";
                            
                            
                            $html .= "<td width='140' align='right'><b>";
                            $html .= "IND. UTILIZAÇÃO:";
                            $html .= "</b></td>
                            <td width='70'>"; 
                            $html .= round((($pav1_areaconstruir+$pav1_areaexistente)/$listagem_condominio['areaterreno']),2);
                            $html .= "</td>";                            

                        $html .=" </tr>
                </table>
	        </td>
	</tr>";

//Condomínio
$html .= "<tr>
	<td colspan='6'>DADOS DA UNIDADE
		<table align='center' width='794' style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
			<tr>";
				$html .= "<td width='200' align='right'>";
				$html .= "<b>ÁREA COMUM PROP.:</b>";
				$html .="</td>
				<td width='70'>"; 
				$html .= number_format(($total_areacomumproporcional2), 2, ',', '');
				$html .= " m²";
				$html .= "</td>";
				
				$html .= "<td width='200' align='right'><b>ÁREA A CONSTRUIR:</b></td>
				<td width='40'>";
				$html .=  number_format(($total_areacontruir2), 2, ',', '');
				$html .= " m²</td>";

				$html .="
				<td width='200' align='right'><b>TAXA OCUP. PROP.:</b></td>
				<td width='70'>";
				$html .= round(((($pav1_areaconstruir2+$pav1_areaexistente2) / $pav1_areausuexclusivo2  )*100),2);
				$html .= "%</td>
			</tr>
			<tr>
				<td width='200' align='right'><b>IND. UTILIZAÇÃO PROP.:</b></td>
				<td width='40'>";
				$html .= round((($pav1_areaconstruir2+$pav1_areaexistente2)/$pav1_areausuexclusivo2),2); 
				$html .= " <!--m²--></td>
				
				<td width='200' align='right'><b>UNIDADES EXCUSIVAS:</b></td>
				<td width='40'>";
				$html .= $con_listagem_que->rowCount(); 
				$html .= "</td>
				
				<td width='200' align='right'><b>UNIDADE:</b></td>
				<td width='40'>";
				$html .= (int)$_GET['unidade'];
				$html .= "</td>
			</tr>
			<tr>
				
				<td width='200' align='right'><b>ÁREA DE USO EXCLUSIVO:</b></td>
				<td width='40'>";
				$html .= number_format(($total_areausoexclusivo2), 2, ',', '');
				$html .= " m²</td>";
				
				$html .= "<td width='130' align='right'>";
				if ( $total_areaexistente > 0 ) { 
					$html .= "<b>ÁREA EXISTENTE:</b>";
					$html .="</td>
					<td width='70'>"; 
					
					$html .= number_format(($total_areaexistente2), 2, ',', '');
					$html .= "m²";
					
					$html .= "</td>";
				} else {
					$html .= "</td><td width='70'></td>";
				}
			$html .= "</tr>
		</table>
	</td>
</tr>";
//

// Observações
$html .= "<tr>
                <td align='center' colspan='6'>
                    <table align='center' width='400' style='padding-bottom:8px;border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                            <tr>
                                <td width='300' align='right'><b>OBS:</b></td>
                                <td align='left' width='300' style='font-size:20px;'>";
                                $html .= $_GET['obs'];
                                $html .= "</td>
                                <td></td>
                                <td></td>
                            </tr>
                    </table>
                </td>
        </tr>";

$rodape = "<table align='center' width='800' style='border-style:dashed;border-color:#EEEED1;font-size:13px;'>
                <tr>
                    <td align='right' width='455' style='border-bottom:1px solid black;'><strong>ÁREA À (";
                    if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                        $rodape .= 'CONSTRUIR';
                    } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                        $rodape .= 'REGULARIZAR';
                    }
                    $rodape .= "):</strong></td>
                    <td align='left' style='border-bottom:1px solid black;'>"; 
                    $rodape .= number_format(($total_areacontruir2), 2, ',', '');
                    $rodape .= " m²</td>
                </tr>";
            //  Listando informações sobre as unidades pavimentos.
            $con_listagem_unidades_pavimentos = $configuracoes->consulta("SELECT 
              id_que,   
              areacontruir,   
              areaexistente,  
              areausoexclusivo,   
              areacomumproporcional

              FROM 
              processos.processos_que_pavimentos

              WHERE processos.processos_que_pavimentos.id_que = ".$linha_que['id_que']." ORDER BY processos.processos_que_pavimentos.id_qp ASC");

            //----
            $cont_qp = 0;
            foreach ( $con_listagem_unidades_pavimentos as $listagem_unidades_pavimentos ) { 
                $cont_qp++;
                $rodape .= "<tr>
                            <td align='right' width='455'><strong>PAV ".$cont_qp;
                            if ( $cont_qp == 1 ) {
                                $rodape .= ' (TÉRREO)';
                            } 
                            
                            $rodape .= ":</strong></td>
                            <td align='left'>"; 
                            $rodape .= number_format(($listagem_unidades_pavimentos['areacontruir']), 2, ',', '');
                            $rodape .= " m²</td>
                        </tr>";
            }

$rodape .=       "<tr>
                    <td align='right' width='455' style='border-top:1px solid black;'><strong>SUBTOTAL (";
                    if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                        $rodape .= 'CONSTRUIR';
                    } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                        $rodape .= 'REGULARIZAR';
                    }
                    $rodape .= "):</strong></td>
                    <td align='left' style='border-top:1px solid black;'>"; 
                    $rodape .= number_format(($total_areacontruir2), 2, ',', '');
                    $rodape .= " m²</td>
                </tr>
                <tr>
                    <td align='right'><strong>TOTAL ("; 
                    if ( $linha_que['tipoprocesso'] == 'c' or $linha_que['tipoprocesso'] == 'a' ) {
                    	$rodape .= 'CONSTRUIR';
                    } else if ( $linha_que['tipoprocesso'] == 'r' ) {
                    	$rodape .= 'REGULARIZAR';
                    }

                    if ( $total_areaexistente2 > 0 ) {
                      $rodape .= ' + EXISTENTE';
                    }
                    $rodape .= "):</strong></td>
                    <td align='left'>";
                    $rodape .= number_format(($total_areacontruir2 + $total_areaexistente2), 2, ',', ''); 
                    $rodape .= " m²</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;padding-bottom:8px;font-size:11px;'>Após a análise dos elementos apresentados e estando os mesmos de acordo com a legislação pertinente em vigor, a Prefeitura Munícipal de Luís Eduardo Magalhães, considera como APROVADO este processo de Solicitação de Alvará.</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-top:50px;'>__________________________________________________</td>
                </tr>
                <tr>
                    <td align='center' colspan='2' style='padding-bottom:20px;font-size:11px;'>
                                <strong>Secretaria de Planejamento, Orçamento e Gestão</strong>
                    </td>
                </tr>
        </table>";
