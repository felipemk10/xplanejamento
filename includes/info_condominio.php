                    <div class="section-divider mt20 mb40">
                      <span> Dados do Processo</span>
                    </div>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->
                    
                
                  
                    <div class="section row">
                      <div class="col-md-4">
                        <a style="color:white;" target="_blank" href="protocolo_impressao.php?id_pro=<?php echo $linha2['id_pro']; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>"><button type="button" class="btn btn-sm btn-system btn-block" style="width:150px;">Imprimir Protocolo</button></a>
                        <br /><br />
                        <label class="field-icon">Protocolo / ID: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['id_pro']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Finalidade do Processo: <span style="border-bottom: #666 1px solid;"><?php 
                              if ( $linha2['tipoprocesso'] == 1 ) {
                                echo 'Alvará de Construção';
                              } else if ( $linha2['tipoprocesso'] == 2 ) {
                                echo 'Alvará de Regularização de Obras';
                              } else if ( $linha2['tipoprocesso'] == 3 ) {
                                echo 'Alvará de Acréscimo de Área';
                              } else if ( $linha2['tipoprocesso'] == 4 ) {
                                echo 'Condomínio Edilício';
                              } else if ( $linha2['tipoprocesso'] == 5 ) {
                                echo 'Redimensionamento';
                              }

                         ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Dados do Proprietário:</strong></label>
                      </div>
                    </div>
                    <?php 
                    //  Listando informações sobre os proprietários.
                    $con_listagem_proprietario = $configuracoes->consulta("SELECT 
                      cg.id_cg,
                      cg.nome,
                      cg.email,
                      cg.cpfcnpj

                      FROM 
                      processos.processos_proprietario

                      INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

                      WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

                      foreach ( $con_listagem_proprietario as $listagem_proprietario ) { 
                      ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Nome: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_proprietario['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php 
        if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
          echo 'CPF';
        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
          echo 'CNPJ';
        }
        ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_proprietario['cpfcnpj']; ?></span></label>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon"><strong>Dados do Projeto:</strong></label>
                      </div>
                    </div>
                    
                    <div class="section row">
                      <?php 
                        //  Listando informações sobre autor.
                        $con_listagem_autor = $configuracoes->consulta("SELECT 
                          cg.id_cg,
                          cg.nome,
                          cg.creacau,
                          processos_profissional.tipoprofissional

                          FROM 
                          processos.processos_profissional

                          LEFT JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

                        foreach ( $con_listagem_autor as $listagem_autor ) {
                      ?>
                      <div class="col-md-5">
                        <label class="field-icon">Autoria: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_autor['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php if ( $listagem_autor['tipoprofissional'] == 'e' ) { echo 'CREA'; } else { echo 'CAU'; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_autor['creacau']; ?></span></label>
                      </div>
                      <?php } ?>
                    </div>

                    <?php 
                        //  Listando informações sobre Resp. Técnico.
                        $con_listagem_resptecnico = $configuracoes->consulta("SELECT 
                          cg.id_cg,
                          cg.nome,
                          cg.creacau,
                          processos_profissional.tipoprofissional

                          FROM 
                          processos.processos_profissional

                          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");

                        if ( $con_listagem_resptecnico->rowCount() > 0 ) {
                      ?>
                    <div class="section row">
                      <?php foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) { ?>
                      <div class="col-md-5">
                        <label class="field-icon">Resp. Técnico: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_resptecnico['nome']; ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon"><?php if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { echo 'CREA'; } else { echo 'CAU'; } ?>: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_resptecnico['creacau']; ?></span></label>
                      </div>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <hr />
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Endereço: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['endereco']; ?></span></label>
                      </div>
                    </div>
                    <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Quadra: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['quadra']; ?></span></label>
                      </div>
                      <div class="col-md-4">
                        <label class="field-icon">Lote: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['lote']; ?></span></label>
                      </div>
                    </div>
                    <?php } ?>
                    
                    <?php if ( $linha2['numero'] > 0 ) { ?>
                      <div class="section row">
                        <div class="col-md-3">
                          <label class="field-icon">Número: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['numero']; ?></span></label>
                        </div>
                      </div>
                    <?php } ?>
                    

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Bairro: <span style="border-bottom: #666 1px solid;"><?php echo $linha2['bairro']; ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Cidade: Luís Eduardo Magalhães</label>
                      </div>
                    </div>
                    <hr />
                    <?php 
                      //  Listando informações sobre responsável técnico.
                      $con_listagem_condominio = $configuracoes->consulta("SELECT 
                        processos_condominio.id_con,
                        processos_condominio.finalidadeobra,
                        processos_condominio.areaterreno,
                        processos_condominio.situacaoterreno,
                        processos_condominio.desmembramento

                        FROM 
                        processos.processos_condominio

                        WHERE processos.processos_condominio.id_pro = $id_pro");

                        $listagem_condominio = $con_listagem_condominio->fetch();

                        $id_con = $listagem_condominio['id_con'];
                        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
                    ?>
                    <div class="section row">
                      <div class="col-md-8">
                        <label class="field-icon">Finalidade da Obra: <span style="border-bottom: #666 1px solid;"><?php 
                          if ( $listagem_condominio['finalidadeobra'] == 'rc' ) {
                            echo 'Residencial em condomínio';
                          } else if ( $listagem_condominio['finalidadeobra'] == 'cm' ) {
                            echo 'Condomínio misto';
                          }
                        ?></span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Área do Terreno: <span style="border-bottom: #666 1px solid;"><?php echo $listagem_condominio['areaterreno']; ?> m<sup>2</sup></span></label>
                      </div>
                    </div>
                    <?php 
                      $con_listagem_que = $configuracoes->consulta("SELECT 
                              id_que,   
                              areacontruir,   
                              areaexistente,  
                              areausoexclusivo,   
                              areacomumproporcional,  
                              tipoprocesso

                              FROM 
                              processos.processos_que

                              WHERE processos.processos_que.id_con = $id_con 
                              ORDER BY processos.processos_que.id_con, processos.processos_que.id_que ASC");

                      $total_areacontruir = 0;
                      $total_areausoexclusivo = 0;
                      $total_areacomumproporcional = 0;

                      foreach ( $con_listagem_que as $listagem_que ) {
                        $total_areacontruir = $total_areacontruir+$listagem_que['areacontruir'];
                        $total_areausoexclusivo = $total_areausoexclusivo+$listagem_que['areausoexclusivo'];
                        $total_areacomumproporcional = $total_areacomumproporcional+$listagem_que['areacomumproporcional'];
                      }

                      $total_areausoexclusivo2      = $total_areausoexclusivo;
                      $total_areacomumproporcional2 = $total_areacomumproporcional;
                    ?>
                    <?php if ( $total_areacontruir > 0 ) { ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Área Comum proporcional: <span style="border-bottom: #666 1px solid;"><?php echo $total_areacomumproporcional; ?></span> m<sup>2</sup></label>
                      </div>
                    </div>
                    <?php } if ( $total_areacomumproporcional > 0 ) { ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Construir / Regularizar: <span style="border-bottom: #666 1px solid;"><?php echo $total_areacontruir; ?></span> m<sup>2</sup></label>
                      </div>
                    </div>
                    <?php } ?>

                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Taxa de Ocupação: <span style="border-bottom: #666 1px solid;"><?php echo round((($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional))*100),2); ?>%</span></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Ind. Utilização: <span style="border-bottom: #666 1px solid;"><?php echo round(($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?></span></label>
                      </div>
                    </div>
                    <?php 
                      //  Destruindo variáveis.
                      unset($total_areacontruir,$total_areacomumproporcional); ?>
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Situação do Terreno: <span style="border-bottom: #666 1px solid;"><?php 
                          if ( $listagem_condominio['situacaoterreno'] == 'm' ) {
                            echo 'MEIO DE QUADRA';
                          } else if ( $listagem_condominio['situacaoterreno'] == 'e' ) {
                            echo 'ESQUINA';
                          }
                        ?></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-4">
                        <label class="field-icon">Desmembramento: 
                        <?php if ( $listagem_condominio['desmembramento'] == true ) {
                                echo '<span style="border-bottom: #666 1px solid;">Sim</span>';
                              } else {
                                echo '<span style="border-bottom: #666 1px solid;">Não</span>';
                              } ?></label>
                      </div>
                      <?php if ( $listagem_condominio['desmembramento'] == true ) { ?>
                      <div class="col-md-4">
                        <label class="field-icon">Entregue: 
                        <?php if ( $listagem_condominio['desmembramento_entregue'] == true ) {
                                echo '<span style="border-bottom: #666 1px solid;">Sim</span>';
                              } else {
                                echo '<span style="border-bottom: #666 1px solid;">Não</span>';
                              } ?></label>
                      </div>
                      <?php } ?>
                    </div>

                    <?php if ( $aprovacaologs ) { ?>
                    <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultofotos" id="checkocultofotos" onclick="apfotos();">
                          <span class="checkbox"></span>Fotos
                        </label>
                      </div>
                    </div>
                    
                    <div id="boxfotos" style="display: none;">
                      <table class="table table-hover" style="font-size: 12px;">
                        <tbody>
                          <?php if (file_exists('vistoria/'.$id_pro.'_1.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_1.jpg" target="_blank">Visualizar Imagem 1</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_2.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_2.jpg" target="_blank">Visualizar Imagem 2</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_3.jpg" target="_blank">Visualizar Imagem 3</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_4.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_4.jpg" target="_blank">Visualizar Imagem 4</a>
                          <?php } ?><br /><br />
                          <?php if (file_exists('vistoria/'.$id_pro.'_5.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_5.jpg" target="_blank">Visualizar Imagem 5</a>
                          <?php } ?><br /><br />
                        </tbody>
                        <footer>
                          <tr>
                            <th></th>
                            <th></th>
                          </tr>
                        </footer>
                      </table>
                    </div>
                    
                    <?php include('logs.php'); } ?>

                    <div class="section row">
                      <div class="col-md-2">
                        <label class="option option-primary">
                          <input type="checkbox" name="checkocultdetalhes" id="checkocultdetalhes" onclick="ap30();">
                          <span class="checkbox"></span>Detalhes
                        </label>
                      </div>
                    </div>
                    <div id="camposocultosmaisdetana" style="display: none;">
                    <div class="section row">
                      <div class="col-md-5">
                        <label class="field-icon">Data de Emissão: <span style="border-bottom: #666 1px solid;"><?php echo $formatacoes->formatar_datahora('/',$linha2['datahora']); ?></span></label>
                      </div>
                      <div class="col-md-5">
                        <label class="field-icon">Origem do Cadastro: <span style="border-bottom: #666 1px solid;"><?php 
                          //  Verificando a origem do cadastro
                          $con_checkorigem = $configuracoes->consulta("SELECT 
                            processos.id_pro

                            FROM 
                            processos.processos

                            INNER JOIN geral.usuarios ON usuarios.id_cg = processos.id_cg

                            WHERE processos.processos.id_pro = $id_pro");

                            if ( $con_checkorigem->rowCount() > 0 ) {
                              echo 'COLABORADOR';
                            } else{
                              echo 'ONLINE';
                            }

                          ?></span></label>
                      </div>
                    </div>
                    

                      
              </div>
                      <div class="enblobacampos31" style="font-size: 10px;">                           
                          <div id="text_unidades">

                          <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>Unidades</th>
                              <th>Área a Construir(m²)</th>
                              <th>Área Existente(m²)</th>
                              <th>Área de Uso Exclusico(m²)</th>
                              <th>Área Comum Proporcional(m²)</th>
                              <th>Fração Ideal</th>
                              <th>Taxa de Ocupação Proporcional (%)</th>
                              <th>Índice de Utilização Proporcional</th>
                              <th>Tipo do Processo(m²)</th>
                              <?php if ( $emissaoalvarahabitese ) { ?><th style="width:300px;"></th><?php } ?>
                            </tr>
                          </thead>
                          <tbody>
                          <?php 
                            //  Listando informações sobre as unidades.
                            $con_listagem_unidades = $configuracoes->consulta("SELECT 
                              id_que,   
                              areacontruir,   
                              areaexistente,  
                              areausoexclusivo,   
                              areacomumproporcional,  
                              tipoprocesso

                              FROM 
                              processos.processos_que

                              WHERE processos.processos_que.id_con = $id_con ORDER BY processos.processos_que.id_que ASC");
                              $n_uni = 0;

                              $total_areacontruir = 0;
                              $total_areaexistente = 0;
                              $total_areausoexclusivo = 0;
                              $total_areacomumproporcional = 0;
                              foreach ( $con_listagem_unidades as $listagem_unidades ) { 
                                $n_uni++; 

                                $total_areacontruir           += $listagem_unidades['areacontruir'];
                                $total_areaexistente          += $listagem_unidades['areaexistente'];
                                $total_fracao                 += round((( ($listagem_unidades['areausoexclusivo']+$listagem_unidades['areacomumproporcional'])  /  ($total_areausoexclusivo2+$total_areacomumproporcional2)  )*100),2);
                                $total_areausoexclusivo       += $listagem_unidades['areausoexclusivo'];
                                $total_areacomumproporcional  += $listagem_unidades['areacomumproporcional'];

                                ?>
                                <input type="hidden" value="<?php echo $n_uni; ?>" name="num_casa[]">
                      
                                <input type="hidden" name="IDunidadesAdd[]" id="IDunidadesAdd[]" value="<?php echo $listagem_unidades['id_que']; ?>">
                                    <tr style="border-bottom: 1px solid #eeeeee;">
                                     <td>Casa <?php echo $n_uni; ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areacontruir'] > 0 ) { echo $listagem_unidades['areacontruir']; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areaexistente'] > 0 ) { echo $listagem_unidades['areaexistente']; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areausoexclusivo'] > 0 ) { echo $listagem_unidades['areausoexclusivo']; ?> m<sup>2</sup><?php } ?></td>
                                        <td align="left"><?php if ( $listagem_unidades['areacomumproporcional'] > 0 ) { echo $listagem_unidades['areacomumproporcional']; ?> m<sup>2</sup><?php } ?></td>
                                        <td><?php echo round((( ($listagem_unidades['areausoexclusivo']+$listagem_unidades['areacomumproporcional'])  /  ($total_areausoexclusivo2+$total_areacomumproporcional2)  )*100),2); ?> %</td>
                                        <td><?php 
                                        if ( $tocupacao > 0 ) {
                                          $tocupacao = (round((($listagem_unidades['areacontruir']/$listagem_unidades['areausoexclusivo'])*100),2)-0.01);
                                        } else {
                                          $tocupacao = 0;
                                        }

                                        echo $tocupacao; ?> %</td>
                                        <td><?php echo round(($listagem_unidades['areacontruir']/$listagem_unidades['areausoexclusivo']),2); ?></td>

                                        <td style="text-align=left;">
                                          <?php if ( $listagem_unidades['tipoprocesso'] == 'c' ) { echo "Construção"; } else if ( $listagem_unidades['tipoprocesso'] == 'r' ) { echo "Regularização"; } else if ( $listagem_unidades['tipoprocesso'] == 'a' ) { echo "Ácrescimo de área"; } ?>
                                        </td>
                                        
                                    </tr>
                                    
                                    <?php if ( $emissaoalvarahabitese ) { ?>
                                      <tr>
                                        <td colspan="9">
                                        <table class="table">
                                          <tbody>
                                        <?php
                                          $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
                                            id_ah,
                                            id_pro,
                                            -- 1 = Alvará, 2 = Habite-se
                                            tipo,
                                            numero,   
                                            id_cg,
                                            ano,
                                            processos_que.tipoprocesso,
                                            substituir

                                            FROM 

                                            processos.processos_alvarahabitese 

                                            INNER JOIN processos.processos_que ON processos_que.id_que = processos_alvarahabitese.id_que

                                            WHERE processos_alvarahabitese.id_pro = $id_pro and processos_alvarahabitese.id_que = ".$listagem_unidades['id_que']."");
                                            
                                            if ( $consulta_alvarahabitese->rowCount() > 0 ) { ?>

                                            <?php 
                                            $alvara[$n_uni] = false;
                                            $habitese[$n_uni] = false;
                                            foreach ( $consulta_alvarahabitese as $listagem_alvarahabitese ) { 
                                                //  CONSULTANDO DO REGISTRO SUBSTITUIDO
                                                $substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$listagem_alvarahabitese['substituir']." and tipo = ".$listagem_alvarahabitese['tipo']."")->fetch();
                                              ?> 
                                             

                                            <tr>
                                                    <td><?php if ( $listagem_alvarahabitese['tipoprocesso'] == 'r' and $listagem_alvarahabitese['tipo'] != 2 ) { echo 'R.'; } echo $listagem_alvarahabitese['numero'].'/'.$listagem_alvarahabitese['ano']; ?></td>
                                                    <td><a 
                                                    <?php
                                                      if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) { ?>
                                                        href="alvara_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&unidade=<?php echo $n_uni; ?>&tocupacao=<?php echo $tocupacao; ?>&obs=" target="_blank">
                                                      <?php } else if ( $listagem_alvarahabitese['tipo'] == 2 ) { ?>
                                                        href="habitese_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&unidade=<?php echo $n_uni; ?>&th=t&obs=" target="_blank">
                                                        <?php } ?>
                                                      <?php 

                                                      if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) {
                                                        $alvara[$n_uni] = true;
                                                        $id_ah = $listagem_alvarahabitese['id_ah'];
                                                        echo 'Visualizar Alvará'; if ( (int)$listagem_alvarahabitese['substituir'] > 0 ) { echo ' - Substituindo o alvará: '; if ( $listagem_alvarahabitese['tipo'] == 3 ) { echo 'R.'; } echo $listagem_alvarahabitese['substituir'].'/'.$substituto['ano']; }
                                                      } else if ( $listagem_alvarahabitese['tipo'] == 2 ) {
                                                        $habitese[$n_uni] = true;
                                                        echo 'Visualizar Habite-se'; if ( (int)$listagem_alvarahabitese['substituir'] > 0 ) { echo ' - Substituindo o habite-se: '.$listagem_alvarahabitese['substituir'].'/'.$substituto['ano']; }
                                                      }
                                                    ?>
                                                    </a></td>
                                                    <td>
                                                       <form method="post" id="admin-form" action="back_geraralvara.php">
                                                        <input type="hidden" name="form" value="deleteah">
                                                        <input type="hidden" name="delete_id_ah" value="<?php echo (int)$listagem_alvarahabitese['id_ah']; ?>">
                                                        <input type="hidden" name="id_pro" value="<?php echo (int)$_GET['id_pro']; ?>">
                                                        <input type="hidden" name="tipoprocesso" value="<?php echo (int)$_GET['tipoprocesso']; ?>">
                                                        <button type="submit" class="btn-info">Excluir</button>
                                                      </form>
                                                    </td>
                                                  </tr>
                                                  <?php } ?>
                                                  <tr>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                          <?php }  ?>
                                          <?php if ( $alvara[$n_uni] == false ) { ?>
                                          <form method="post" id="admin-form" action="back_geraralvara.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&tipoprocesso=<?php echo (int)$_GET['tipoprocesso']; ?>">
                                            <input type="hidden" name="form" value="alvarahabitese">
                                            <input type="hidden" name="tipo" value="<?php 
                                              if ( $listagem_unidades['tipoprocesso'] == 'r' ) {
                                                echo 3;
                                              } else {
                                                echo 1;
                                              }
                                            ?>">
                                            <input type="hidden" name="id_que" value="<?php echo $listagem_unidades['id_que']; ?>">
                                            <table  width="800">
                                              <tr>
                                                <td><button type="submit" class="button btn-success">Gerar Alvará</button></td>
                                                <td>
                                                  <div class="col-md-100" style="margin-left: 10px;">
                                                    <label class="option option-primary">
                                                      <input type="checkbox" name="checksub" id="checksub<?php echo $n_uni; ?>" onclick="jschecksub2(<?php echo $n_uni; ?>);" value="true">
                                                      <span class="checkbox"></span>Subs. Alvará
                                                    </label>
                                                  </div>
                                                  <div class="col-md-6 boxsubstituicao<?php echo $n_uni; ?>" style="display: none;">
                                                    <table width="300">
                                                      <tr>
                                                        <td align="left">
                                                          <div class="col-md-8">
                                                            <input type="text" class="gui-input col-md-6" id="numerosubstituicao<?php echo $n_uni; ?>" name="numerosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                                          </div>
                                                        </td>
                                                        <td align="left">
                                                          <div class="col-md-8">
                                                            <input type="text" class="gui-input col-md-6" id="anosubstituicao<?php echo $n_uni; ?>" name="anosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </div>
                                                </td>

                                                <td>
                                                  <div class="col-md-100" style="margin-left: 10px;">
                                                    <label class="option option-primary">
                                                      <input type="checkbox" name="checkbotaogerarmanual" id="checkbotaogerarmanual<?php echo $n_uni; ?>" onclick="jscheckgerarmanual(<?php echo $n_uni; ?>);" value="true">
                                                      <span class="checkbox"></span>Gerar número manual
                                                    </label>
                                                  </div>
                                                </td>
                                                <td>
                                                  <table width="300">
                                                    <tr>
                                                      <td align="left">
                                                        <div class="col-md-8 boxgerarmanual<?php echo $n_uni; ?>" style="display: none;">
                                                          <input type="text" class="gui-input col-md-8" name="numerogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                                        </div>
                                                      </td>
                                                      <td align="left">
                                                        <div class="col-md-8 boxgerarmanual<?php echo $n_uni; ?>" style="display: none;">
                                                          <input type="text" class="gui-input col-md-8" name="anogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                                        </div>
                                                      </td>
                                                      <td width="100">
                                                        Data emissão
                                                        <input type="text" class="gui-input col-md-3" id="dataah" name="dataah" maxlength="10" placeholder="Emissão">
                                                      </td> 
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                          </form>
                                          <?php } if ( $alvara[$n_uni] == true and $habitese[$n_uni] == false ) { ?>
                                          <form method="post" id="admin-form" action="back_geraralvara.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&tipoprocesso=<?php echo (int)$_GET['tipoprocesso']; ?>">
                                            <input type="hidden" name="form" value="alvarahabitese">
                                            <input type="hidden" name="id_ah" value="<?php echo $id_ah; ?>">
                                            <input type="hidden" name="tipo" value="2">
                                            <input type="hidden" name="id_que" value="<?php echo $listagem_unidades['id_que']; ?>">
                                            <table>
                                              <tr>
                                                <td><button type="submit" class="button btn-success">Gerar Habite-se</button></td>
                                                <td>
                                                  <div class="col-md-100" style="margin-left: 10px;">
                                                    <label class="option option-primary">
                                                      <input type="checkbox" name="checksub" id="checksub<?php echo $n_uni; ?>" onclick="jschecksub2(<?php echo $n_uni; ?>);" value="true">
                                                      <span class="checkbox"></span>Subs. Habite-se
                                                    </label>
                                                  </div>
                                                  <div class="col-md-6 boxsubstituicao<?php echo $n_uni; ?>" style="display: none;">
                                                    <table width="300">
                                                      <tr>
                                                        <td align="left">
                                                          <div class="col-md-8">
                                                            <input type="text" class="gui-input col-md-6" id="numerosubstituicao<?php echo $n_uni; ?>" name="numerosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                                          </div>
                                                        </td>
                                                        <td align="left">
                                                          <div class="col-md-8">
                                                            <input type="text" class="gui-input col-md-6" id="anosubstituicao<?php echo $n_uni; ?>" name="anosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </div>
                                                </td>
                                                <td>
                                                  <div class="col-md-100" style="margin-left: 10px;">
                                                    <label class="option option-primary">
                                                      <input type="checkbox" name="checkbotaogerarmanual" id="checkbotaogerarmanual<?php echo $n_uni; ?>" onclick="jscheckgerarmanual(<?php echo $n_uni; ?>);" value="true">
                                                      <span class="checkbox"></span>Número manual
                                                    </label>
                                                  </div>
                                                </td>
                                                <td>
                                                  <table width="300">
                                                    <tr>
                                                      <td align="left">
                                                        <div class="col-md-8 boxgerarmanual<?php echo $n_uni; ?>" style="display: none;">
                                                          <input type="text" class="gui-input col-md-8" name="numerogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                                        </div>
                                                      </td>
                                                      <td align="left">
                                                        <div class="col-md-8 boxgerarmanual<?php echo $n_uni; ?>" style="display: none;">
                                                          <input type="text" class="gui-input col-md-8" name="anogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                                        </div>
                                                      </td>
                                                      <td width="100">
                                                        Data emissão
                                                        <input type="text" class="gui-input col-md-3" id="dataah" name="dataah" maxlength="10" placeholder="Emissão">
                                                      </td> 
                                                    </tr>
                                                  </table>
                                                </td>
                                              </tr>
                                            </table>
                                          </form>
                                          <?php } ?>
                                          
                                        <?php } ?>
                                    
                              <?php } ?>
                              </tbody>
                            
                              <footer>
                                <tr>
                                  <th>Total do condomínio</th>
                                  <th><?php echo $total_areacontruir; ?> (m²)</th>
                                  <th><?php echo $total_areaexistente; ?> (m²)</th>
                                  <th><?php echo $total_areausoexclusivo; ?> (m²)</th>
                                  <th><?php echo $total_areacomumproporcional; ?> (m²)</th>
                                  <th><?php echo $total_fracao; ?> %</th>

                                  <th><?php echo round(( ($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional))*100),2); ?> %</th>
                                  <th><?php echo round( ($total_areacontruir/($total_areausoexclusivo+$total_areacomumproporcional)),2); ?></th>
                                  <th></th>

                                  <?php if ( $emissaoalvarahabitese ) { ?><th></th><?php } ?>
                                </tr>
                              </footer>
                            </table>
                          </div>
                      </div>