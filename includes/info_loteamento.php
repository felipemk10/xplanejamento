<div class="section row">
        <div class="col-xs-2 text-right subtitulo">Finalidade da Obra</div>
        <?php 
          //  Listando informações sobre responsável técnico.
          $con_listagem_loteamento = $configuracoes->consulta("SELECT 
            processos_loteamento.id_lot,
            processos_loteamento.nome,
            processos_loteamento.areatotal,
            processos_loteamento.areaviaspublicas,
            processos_loteamento.areaverde,
            processos_loteamento.areaequipamentopublico,
            processos_loteamento.arealotes,
            processos_loteamento.quantlotes,
            processos_loteamento.matricula,
            processos_loteamento.ativo

            FROM 
            processos.processos_loteamento

            WHERE processos.processos_loteamento.id_pro = $id_pro");

            $listagem_loteamento = $con_listagem_loteamento->fetch();

            $id_lot = $listagem_loteamento['id_lot'];
            // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
        ?>
        LOTEAMENTO
      </div>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área do Loteamento:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo $listagem_loteamento['areatotal']; ?> m<sup>2</sup></div>
      </div>
      
            
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Taxa de Ocupação:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo round((($total_patare_terreo/$listagem_loteamento['areaterreno'])*100),2); ?> %</div>
        <div class="col-xs-2 text-right subtitulo">Ind. Utilização:</div>
        <div class="col-xs-3 text-left campo-texto"><?php echo round(($total_contruir/$listagem_loteamento['areaterreno']),2); ?></div> 

      </div>
      <br>
      <br>
      <div class="section row hidden-print">
        <div class="col-xs-2 text-right subtitulo">Detalhes
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox" name="checkocultdetalhes" id="checkocultdetalhes" onclick="ap30();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="camposocultosmaisdetana" style="display: none;">
      <?php
        //  Listando informações sobre pavimentacao.
        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'pat' ORDER BY processos.processos_pavimentacao.id_pav ASC");
          $n_pav = 0;
          $total_pat = 0; ?>

          <?php if ( $con_listagem_pavimento->rowCount() > 0 ) { ?>
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Área a construir</div>
            </div>
            <br>
      <?php 
          
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            $n_pav++;
            $total_pat+= $listagem_pavimento['area']; ?>
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo (1)'; } else { echo $n_pav; } ?>: </div>
              <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
            </div>
          <?php } ?>
          <div class="section row">
            <div class="col-xs-2 text-right subtitulo">Total: </div>
            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
          </div><hr />
          <?php } ?>
      <?php
        //  Listando informações sobre pavimentacao.
        //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
        $con_listagem_pavimento = $configuracoes->consulta("SELECT 
          processos_pavimentacao.id_pav,
          processos_pavimentacao.area

          FROM 
          processos.processos_pavimentacao

          WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbc' ORDER BY processos.processos_pavimentacao.id_pav ASC");

        if ( $con_listagem_pavimento->rowCount() > 0 ) {
      ?>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Área Subsolo à Construir</div>
      </div>
      <br>
      

      <?php
            $n_pav = 0;
            $total_pat = 0;
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
            $n_pav++;
            $total_pat+= $listagem_pavimento['area']; ?>
            
            <div class="section row">
              <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
              <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
            </div>

            <?php } ?>
              <div class="section row">
                <div class="col-xs-2 text-right subtitulo">Total: </div>
                <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
              </div>
              <hr />
            <?php } ?> 
      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'are' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Existente</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                           <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Pavimento <?php if ( $n_pav == 1 ) { echo 'Térreo'; } else { echo $n_pav; } ?>: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                          </div>

                          <?php } ?>
                            <div class="section row">
                              <div class="col-xs-2 text-right subtitulo">Total: </div>
                              <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                            </div>
                            <hr />
                        <?php } ?>
                        
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'sbe' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Sub Solo Existente</div>
                    </div>
                    <br>

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div>
                          <hr />
                        <?php } ?>
                        <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'adk' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Deck</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div><hr />
                        <?php } ?>
                      <?php
                      //  Listando informações sobre pavimentacao.
                      //  Legenda tipo: pat = Pavimento Terreo, sbc = área sub solo a construir, are = área Existente, sbe = área sub solo existente, adk = área Deck, apa = área Piscina
                      $con_listagem_pavimento = $configuracoes->consulta("SELECT 
                        processos_pavimentacao.id_pav,
                        processos_pavimentacao.area

                        FROM 
                        processos.processos_pavimentacao

                        WHERE processos.processos_pavimentacao.id_alv = $id_alv and processos.processos_pavimentacao.tipo = 'apa' ORDER BY processos.processos_pavimentacao.id_pav ASC");

                      if ( $con_listagem_pavimento->rowCount() > 0 ) {
                    ?>
                    <div class="section row">
                      <div class="col-xs-2 text-right subtitulo">Área Piscina</div>
                    </div>
                    <br>
                    

                    <?php
                          $n_pav = 0;
                          $total_pat = 0;
                        foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
                          $n_pav++;
                          $total_pat+= $listagem_pavimento['area']; ?>
                          <div class="section row">
                          <div class="col-xs-2 text-right subtitulo">Pavimento <?php echo $n_pav; ?>: </div>
                          <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_pavimento['area']; ?> m<sup>2</sup></div>
                        </div>

                        <?php } ?>
                          <div class="section row">
                            <div class="col-xs-2 text-right subtitulo">Total: </div>
                            <div class="col-xs-2 text-left campo-texto"><?php echo $total_pat; ?> m<sup>2</sup></div>
                          </div>
                        <?php } ?>
      </div>
      <div class="section row hidden-print">
        <div class="col-xs-2 text-right subtitulo">Observações
        </div>
          <div class="col-xs-1 pull-left">            
            <input type="checkbox"  name="checkocultobs" id="checkocultobs" onclick="apobs();">
            <span class="checkbox"></span>
        </div>
      </div>
      <div id="boxobs" style="display: none;">
        <div class="section row">
            <textarea name="comment" rows="7" cols="70" class="text-area col-xs-offset-1"></textarea>         
        </div>
      </div>
      </div>
      <div class="section row">
        <div class="col-xs-offset-3 col-xs-4 text-right subtitulo">Subtotal ( <?php 
                        if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 ) {
                          echo 'Construir';
                        } else if ( $linha2['tipoprocesso'] == 2 ) {
                          echo 'Regularizar';
                        }
                        ?> ) :</div>
        <div class="col-xs-2 campo-texto"><?php echo $total_pat2; ?> m<sup>2</sup></div>

        <div class="col-xs-offset-3 col-xs-4 text-right subtitulo">Total ( <?php 
                        if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 3 ) {
                          echo 'Construir';
                        } else if ( $linha2['tipoprocesso'] == 2 ) {
                          echo 'Regularizar';
                        }

                        if ( $total_are > 0 ) {
                          echo ' + Existente';
                        }
                        if ( $total_sbc > 0 ) {
                          echo ' + Sub Solo à Construir';
                        }
                        ?> ) :</div>
        <div class="col-xs-2 campo-texto"><?php echo $total_pat2 + $total_are + $total_sbc; ?> m<sup>2</sup></div>
      </div>
