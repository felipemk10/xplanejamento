<aside id="sidebar_left" class="nano nano-light light affix">
  <!-- Start: Sidebar Left Content -->
  <div class="sidebar-left-content nano-content">
    <!-- Start: Sidebar Menu / Processos-->
    <ul class="nav sidebar-menu">
<?php 
  if ( $tipousuarioLogado != 'bi' ) 
  { 
?>
      <li>
        <a class="accordion-toggle" href="#">
          <span class="glyphicon glyphicon-th-large"></span>
          <span class="sidebar-title">Cadastrar Processos</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
          <li>
            <a href="cad_alvara.php?tipoprocesso=1">Alvará de Construção 
            </a>
          </li>
          <li>
            <a href="cad_alvara.php?tipoprocesso=2">Alvará de Regularização</a>
          </li>
          <li>
            <a href="cad_alvara.php?tipoprocesso=3">Alvará de Acréscimo</a>
           </li>
          <li>
            <a href="cad_condominio.php?tipoprocesso=4">Condomínio</a>
          </li>
          <li>
            <a href="cad_condominio_pavimentos.php?tipoprocesso=4">Condomínio com pavimentos</a>
          </li>
<!--Irving - Adicionado o link para cadastro de loteamentos-->
          <li>
            <a href="cad_loteamento.php?tipoprocesso=7">Loteamento</a>
          </li>
<!----------------------------------------------------------->
          <li>
            <a href="cad_redimensionamento.php?tipoprocesso=5">Redimensionamento</a>
          </li>
        </ul>
      </li>
          
      <li> <!-- Start: Sidebar Menu / Listar Processos-->
        <a class="accordion-toggle" href="#">
          <span class="glyphicon glyphicon-folder-open"></span>
          <span class="sidebar-title">Listar processos</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
          <li>
            <a class="accordion-toggle" href="#">Alvará de Projeto
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=6&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=6&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=6&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=6&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=6&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
          <li>
            <a class="accordion-toggle" href="#">Alvará de Construção
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=1&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=1&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=1&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=1&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=1&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
          <li>
            <a class="accordion-toggle" href="#">Alvará de Regularização
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=2&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=2&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=2&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=2&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=2&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
          <li>
            <a class="accordion-toggle" href="#">Alvará de Acréscimo de Á 
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=3&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=3&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=3&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=3&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=3&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
          <li>
           <a class="accordion-toggle" href="#">Condomínio
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=4&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=4&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=4&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=4&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=4&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
<!-- Irving - listagem de Loteamentos TEMPORARIAMENTE DESATIVADO -->
          <li>
           <a class="accordion-toggle" href="#">Loteamentos
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=7&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=7&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=7&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=7&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=7&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
<!-------------------------------------------------------------------------------------->
          <li>
           <a class="accordion-toggle" href="#">Redimensionamento
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_processos.php?tipoprocesso=5&situacaoprojeto=1">A ser analisado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=5&situacaoprojeto=2">Em estado de Análise</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=5&situacaoprojeto=3">Insuficiente</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=5&situacaoprojeto=4">Reprovado</a>
              </li>
              <li>
                <a href="listar_processos.php?tipoprocesso=5&situacaoprojeto=11">Aprovado</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
<?php 
  } 
  if ( $tipousuarioLogado == 'ad' ) 
  { 
?>
      <li>
        <a class="accordion-toggle" href="#">
          <span class="glyphicon glyphicon-th-large"></span>
          <span class="sidebar-title">Listar Usuários/Clientes</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
          <li>
            <a class="accordion-toggle" href="#">Usuários
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_cgs.php?tipousuario=co">Coordenador</a>
              </li>
              <li>
                <a href="listar_cgs.php?tipousuario=fi">Fiscal</a>
              </li>
              <li>
                <a href="listar_cgs.php?tipousuario=an">Analista</a>
              </li>
              <li>
                <a href="listar_cgs.php?tipousuario=ad">Administrador</a>
              </li>
            </ul>
          </li>
          <li>
            <a class="accordion-toggle" href="#">Clientes
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="listar_cgs.php?tipousuario=cl&tipoprofissional=1">Engenheiro</a>
              </li>
              <li>
                <a href="listar_cgs.php?tipousuario=cl&tipoprofissional=2">Arquiteto</a>
              </li>
              <li>
                <a href="listar_cgs.php?tipousuario=cl&tipoprofissional=3">Técnico</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
<?php 
  } 
  if ( $tipousuarioLogado == 'bi' or $usuarioBI == true ) 
  { 
?>
      <li>
        <a class="accordion-toggle" href="#">
          <span class="glyphicon glyphicon-th-large"></span>
          <span class="sidebar-title">BI</span>
          <span class="caret"></span>
        </a>
        <ul class="nav sub-nav">
          <li>
            <a class="accordion" href="BIGraficos.php">Gráficos
              <span class="caret"></span>
            </a>
          </li>
          <!--
          <li>
            <a class="accordion" href="#">Relatórios
              <span class="caret"></span>
            </a>
          </li>
          -->
        </ul>
      </li>
<?php 
  } 
?>
    </ul>
    <!-- End: Sidebar Menu -->
  </div>
  <!-- End: Sidebar Left Content -->
</aside>