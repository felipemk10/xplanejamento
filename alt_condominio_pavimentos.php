<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require 'vendor/autoload.php';
session_start();

$autenticacao   = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios       = new \Biblioteca\usuarios;

$manipuladores  = new Biblioteca\manipuladores;

$processos      = new \Biblioteca\processos;

if ( $_GET['id_pro'] > 0 ) {
  //  Listando informações sobre o processo.
  $consulta = "SELECT 
    processos.id_pro,
    processos.endereco,
    processos.quadra,
    processos.lote,
    processos.numero,
    processos.bairro,
    processos.cidade,
    processos.estado,
    processos.tipoprocesso,
    processos.datahora, 
    processos.situacaoprojeto 

    FROM 

    processos.processos 

    INNER JOIN processos.processos_proprietario ON processos_proprietario.id_pro = processos.id_pro
    INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

    WHERE processos.id_pro = ".$_GET['id_pro']." and processos.ativo = true";

    if ( $usuarios->getUsuarioLogado($manipuladores->criptografia($_SESSION['id_usuario'] ,'base64','decode'))->tipousuario == 'cl' ) {
      $consulta = $consulta." and processos.id_cg = ".$manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode')."";
    }
    //  Listando processos.
    $consulta = $processos->consulta($consulta);

    $linha = $consulta->fetch();
}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header2.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="dashboard.html">Cadastro de Processos</a>
            </li>
            <li class="crumb-trail">
              <a href="dashboard.html">Alvará de Construção</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <form method="post" id="admin-form" action="javascript: formularios_dinamicos('#admin-form', 'backend/alt_condominio_pavimentos.php?tipoprocesso=<?php echo $_GET['tipoprocesso']; ?>', '#carregando', '#formResult', 's', '');">
                  <input type="hidden" name="form" value="alteracao">
                  <input type="hidden" name="id_pro" value="<?php echo $linha->id_pro; ?>">
                  <div class="panel-body bg-light">

                    <div class="section-divider mt20 mb40">
                      <span> Dados do Proprietário </span>
                    </div>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->
                  <?php 
                    //  Listando informações sobre os proprietários.
                    $con_listagem_proprietario = $processos->consulta("SELECT 
                      cg.id_cg,
                      cg.nome,
                      cg.email,
                      cg.cpfcnpj

                      FROM 
                      processos.processos_proprietario

                      INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

                      WHERE processos.processos_proprietario.id_pro = ".$_GET['id_pro']." ORDER BY processos.processos_proprietario.id_pro ASC"); 

                    $listagem_proprietario = $con_listagem_proprietario->fetch(); 

                    $proprietario1 = $listagem_proprietario->cpfcnpj;
                    ?>

                      <input type="hidden" name="IDProprietario" id="IDProprietario" value="<?php echo $listagem_proprietario->id_cg; ?>"> 
                        
                        <div class="section row" ">
                          <div class="col-md-6">
                            <label for="nomeProprietario" class="field prepend-icon">
                              <input type="text" name="nomeProprietario" id="nomeProprietario" class="gui-input" placeholder="Nome" value="<?php echo $listagem_proprietario->nome; ?>">
                              <label for="nomeProprietario" class="field-icon">
                                <i class="fa fa-user"></i>
                              </label>
                            </label>
                          </div>
                          <!-- end section -->
                        </div>
                        <!-- end .section row section -->

                        <div class="section row">
                          <div class="col-md-6">
                            <label for="emailProprietario" class="field prepend-icon">
                              <input type="email" name="emailProprietario" id="emailProprietario" class="gui-input" placeholder="E-mail" value="<?php echo $listagem_proprietario->email; ?>">
                              <label for="emailProprietario" class="field-icon">
                                <i class="fa fa-envelope"></i>
                              </label>
                            </label>
                          </div>
                        </div>

                        <div class="section row">
                          <div class="col-md-3"> 
                            <label for="tipopessoa" class="select field prepend-icon">  
                              <label class="option">
                                  <input name="tipopessoa" id="pessoasim" type="radio" <?php if ( strlen($listagem_proprietario->cpfcnpj) == 11 ) { ?>checked="checked"<?php } ?> onclick="ap100();">
                                  <span class="radio"></span>Física</label>
                              <label class="option">
                                  <input name="tipopessoa" id="pessoanao" type="radio" <?php if ( strlen($listagem_proprietario->cpfcnpj) == 14 ) { ?>checked="checked"<?php } ?> onclick="ap110();">
                                  <span class="radio"></span>Jurídica</label>
                              </label>
                          </div>
                        </div>

                        <div class="section row">
                          <div class="col-md-6">
                            <label for="cpfcnpjProprietario" class="field prepend-icon">
                              <input type="input" name="cpfcnpjProprietario" id="cpfcnpjProprietario" class="gui-input" placeholder="CPF/CNPJ" value="<?php echo $listagem_proprietario->cpfcnpj; ?>">
                              <label for="cpfcnpjProprietario" class="field-icon">
                                <i class="fa fa-check"></i>
                              </label>
                            </label>
                          </div>
                        </div>
                        <hr />


                  <?php 
                    //  Listando informações sobre os proprietários.
                    $con_listagem_proprietario = $processos->consulta("SELECT 
                      cg.id_cg,
                      cg.nome,
                      cg.email,
                      cg.cpfcnpj

                      FROM 
                      processos.processos_proprietario

                      INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

                      WHERE processos.processos_proprietario.id_pro = ".$_GET['id_pro']." ORDER BY processos.processos_proprietario.id_pro ASC OFFSET 1"); ?>
                    
                   <div class="section row col-md-12">
                      <label class="option ">
                          <input name="addproprietario" type="checkbox" id="addproprietario" <?php if ( $con_listagem_proprietario->rowCount() > 0 ) { ?>checked="checked"<?php } ?> onclick="ap20();">
                          <span class="checkbox"></span>Adicionar proprietários</label>
                    </div> <!-- fim da row -->
                    <div id="campomaisproprietario" style="<?php if ( $con_listagem_proprietario->rowCount() == 0 ) { ?>display: none;<?php } ?>">
                        <button type="button" class="button btn-primary" onclick="addInputProprietario();">Adicionar proprietários</button>
                        <div class="enblobacampos31">                           
                            <div id="textProprietario"><hr />
                              <?php foreach ( $con_listagem_proprietario as $listagem_proprietario ) { ?>
                                  <input type="hidden" name="IDProprietarioAdd[]" id="IDProprietarioAdd[]" value="<?php echo $listagem_proprietario->id_cg; ?>"> 
                                  
                                  <div class="section row" ">
                                    <div class="col-md-6">
                                      <label for="nomeProprietarioAdd[]" class="field prepend-icon">
                                        <input type="text" name="nomeProprietarioAdd[]" id="nomeProprietarioAdd[]" class="gui-input" placeholder="Nome" value="<?php echo $listagem_proprietario->nome; ?>">
                                        <label for="nomeProprietarioAdd[]" class="field-icon">
                                          <i class="fa fa-user"></i>
                                        </label>
                                      </label>
                                    </div>
                                    <!-- end section -->
                                  </div>
                                  <!-- end .section row section -->

                                  <div class="section row">
                                    <div class="col-md-6">
                                      <label for="emailProprietarioAdd[]" class="field prepend-icon">
                                        <input type="email" name="emailProprietarioAdd[]" id="emailProprietarioAdd[]" class="gui-input" placeholder="E-mail" value="<?php echo $listagem_proprietario->email; ?>">
                                        <label for="emailProprietarioAdd[]" class="field-icon">
                                          <i class="fa fa-envelope"></i>
                                        </label>
                                      </label>
                                    </div>
                                  </div>

                                  <div class="section row">
                                    <div class="col-md-6">
                                      <label for="cpfcnpjProprietarioAdd[]" class="field prepend-icon">
                                        <input type="input" name="cpfcnpjProprietarioAdd[]" id="cpfcnpjProprietarioAdd[]" class="gui-input" placeholder="CPF/CNPJ" value="<?php echo $listagem_proprietario->cpfcnpj; ?>">
                                        <label for="cpfcnpjProprietarioAdd[]" class="field-icon">
                                          <i class="fa fa-check"></i>
                                        </label>
                                      </label>
                                    </div>
                                  </div>
                                  <hr />
                                <?php } ?>

                            </div>
                        </div> <!-- FIM DE ENGLOBACAMPOS31 -->
                    </div>
                    
                    <div class="section-divider col-md-12"><span>Dados do Projeto</span></div>

                    <?php 
                      //  Listando informações sobre autor.
                      $con_listagem_autor = $processos->consulta("SELECT 
                        cg.id_cg,
                        cg.nome,
                        cg.creacau,
                        processos_profissional.tipoprofissional

                        FROM 
                        processos.processos_profissional

                        INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                        WHERE processos.processos_profissional.id_pro = ".$_GET['id_pro']." and processos.processos_profissional.tipo = 'a'");

                        $listagem_autor = $con_listagem_autor->fetch();
                    ?>
                    <input type="hidden" name="id_autoria" value="<?php echo $listagem_autor->id_cg; ?>">
                    <div class="section row" ">
                      <div class="col-md-6">
                        <label for="autoria" class="field prepend-icon">
                          <input type="text" name="autoria" id="autoria" class="gui-input" placeholder="Autoria" value="<?php echo $listagem_autor->nome; ?>">
                          <label for="autoria" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div>
                      <!-- end section -->
                    </div>
                    <!-- end .section row section -->

                    <div class="section row">
                      <div class="col-md-6">
                        <label for="autoria_creacau" class="field prepend-icon">
                          <input type="text" name="autoria_creacau" id="autoria_creacau" class="gui-input" placeholder="CREA/CAU" value="<?php echo $listagem_autor->creacau; ?>">
                          <label for="autoria_creacau" class="field-icon">
                            <i class="fa fa-check"></i>
                          </label>
                        </label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-3"> 
                        <label for="autoria_profissional" class="select field prepend-icon">  
                          <label class="option">
                            <input name="autoria_profissional" type="radio" value="e" <?php if ( $listagem_autor->tipoprofissional == 'e' ) { ?>checked="checked"<?php } ?>>
                            <span class="radio"></span>Engenheiro</label>
                          <label class="option">
                            <input name="autoria_profissional" type="radio" value="a" <?php if ( $listagem_autor->tipoprofissional == 'a' ) { ?>checked="checked"<?php } ?>>
                            <span class="radio"></span>Arquiteto</label>
                        </label>
                        </div>
                    </div><!-- fim da row -->
                    <hr />
                    
                    <?php 
                      //  Listando informações sobre responsável técnico.
                      $con_listagem_resptecnico = $processos->consulta("SELECT 
                        cg.id_cg,
                        cg.nome,
                        cg.creacau,
                        processos_profissional.tipoprofissional

                        FROM 
                        processos.processos_profissional

                        INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

                        WHERE processos.processos_profissional.id_pro = ".$_GET['id_pro']." and processos.processos_profissional.tipo = 'r'");

                        $listagem_resptecnico = $con_listagem_resptecnico->fetch();
                    ?>

                    <div class="section row col-md-12">
                      <label class="option">
                          <input type="checkbox" name="resp_open" <?php if ($con_listagem_resptecnico->rowCount() > 0) { ?>checked="checked"<?php } ?> onclick="ap19();">
                          <span class="checkbox"></span>Responsável Técnico</label>
                    </div> <!-- fim da row -->
                    
                    <input type="hidden" name="id_resptecnico" value="<?php echo $listagem_resptecnico->id_cg; ?>">
                    <div id="camporesptec" style="<?php if ($con_listagem_resptecnico->rowCount() == 0) { ?> display:none;<?php } ?>">
                      <div class="section row" ">
                      <div class="col-md-6">
                        <label for="resptecnico" class="field prepend-icon">
                          <input type="text" name="resptecnico" id="resptecnico" class="gui-input" placeholder="Resp. Técnico" value="<?php echo $listagem_resptecnico->nome; ?>">
                          <label for="resptecnico" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div>
                      <!-- end section -->
                    </div>
                    <!-- end .section row section -->

                    <div class="section row">
                      <div class="col-md-6">
                        <label for="resptecnico_creacau" class="field prepend-icon">
                          <input type="text" name="resptecnico_creacau" id="resptecnico_creacau" class="gui-input" placeholder="CREA/CAU" value="<?php echo $listagem_resptecnico->creacau; ?>">
                          <label for="resptecnico_creacau" class="field-icon">
                            <i class="fa fa-check"></i>
                          </label>
                        </label>
                      </div>
                    </div>

                    <div class="section row">
                      <div class="col-md-3"> 
                        <label for="resptecnico_profissional" class="select field prepend-icon">
                          <label class="option">
                                  <input name="resptecnico_profissional" type="radio" value="e" <?php if ( $listagem_resptecnico->tipoprofissional == 'e' ) { ?>checked="checked"<?php } ?>>
                                  <span class="radio"></span>Engenheiro</label>
                        </div> 
                        <div class="col-md-3">  
                          <label class="option">
                                  <input name="resptecnico_profissional" type="radio" value="a" <?php if ( $listagem_resptecnico->tipoprofissional == 'a' ) { ?>checked="checked"<?php } ?>>
                                  <span class="radio"></span>Arquiteto</label>
                        </div>
                        </label>
                    </div><!-- fim da row -->
                    
                    </div>
                  <div class="section-divider col-md-12"><span>Dados de endereço</span></div>   
                   
                    <div class="section row ">
                     
                      <div class="col-md-4">  
                        <label for="endereco" class="field">Endereço
                        <input name="endereco" id="endereco" class="gui-input col-md-6" type="text" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->endereco; ?>" <?php }?>>
                        </label>
                      </div>

                      <div class="col-md-4">
                        <label for="bairro" class="field">Bairro
                        <input name="bairro" id="bairro" class="gui-input col-md-6" type="text" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->bairro; ?>" <?php }?>>
                        </label>
                      </div>
                      
                      <div class="col-md-1">
                        <label for="quadra" class="field">Quadra
                        <input name="quadra" id="quadra" class="gui-input col-md-6" type="text"<?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->quadra; ?>" <?php }?>>
                        </label>
                      </div>
                 
                      <div class="col-md-1">
                        <label for="lote" class="field">Lote
                        <input name="lote" id="lote" class="gui-input col-md-6" type="text" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->lote; ?>" <?php }?>>
                        </label>
                      </div>

                      <div class="col-md-1">
                        <label for="numero" class="field">Nº
                        <input name="numero" id="numero" class="gui-input col-md-6" type="text" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="15" <?php if ( (int)$_GET['id_pro'] > 0 ) { ?>value="<?php echo $linha->numero; ?>" <?php }?>>
                        </label>
                      </div>
                  

                    <!-- Caso queria identico ao sistema   
                    </div>
                    <div class="section row">
                      <div class="col-md-6">                   
                        <label class="option">
                            <input name="checked" type="checkbox">
                            <span class="checkbox"></span>Quadra e Lote</label>
                      </div>      
                      <div class="col-md-6"> 
                        <label class="option">
                          <input name="checked" type="checkbox">
                          <span class="checkbox"></span>Número de residência</label>
                      </div> -->                     
                    </div> <!-- fim da row -->
          
                    <div class="section row">            
                      <div class="col-md-3">
                            <label ">Estado</label>
                            <label class="field select">
                              <select id="estado" name="estado">
                                <option value="ba">Bahia</option>
                              </select>
                              <i class="arrow"></i>
                            </label>
                      </div>
                      <div class="col-md-4">
                            <label for="">Cidade:</label>
                            <label class="field select">
                              <select id="cidade" name="cidade">
                                <option value="2919553">Luís Eduardo Magalhães</option>
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      </div><!-- fim da row -->
                      <div class="section-divider col-md-12"><span>Dados do Terreno</span></div>

                      <?php 
                      //  Listando informações sobre responsável técnico.
                      $con_listagem_condominio = $processos->consulta("SELECT 
                        processos_condominio.id_con,
                        processos_condominio.finalidadeobra,
                        processos_condominio.areaterreno,
                        processos_condominio.situacaoterreno,
                        processos_condominio.desmembramento,
                        processos_condominio.desmembramento_entregue

                        FROM 
                        processos.processos_condominio

                        INNER JOIN geral.cg ON cg.id_cg = processos_condominio.id_cg                                    

                        WHERE processos.processos_condominio.id_pro = ".$_GET['id_pro']."");

                        $listagem_condominio = $con_listagem_condominio->fetch();
                        $id_con = $listagem_condominio->id_con;
                    ?>
                      <input type="hidden" name="id_con" value="<?php echo $listagem_condominio->id_con; ?>">
                      <div class="section row">
                        <div class="col-md-3">
                            <label>Finalidade da Obra:</label>
                            <label for="finalidadeobra" class="select field prepend-icon">
                              <select id="finalidadeobra" name="finalidadeobra">
                                <option value="">Selecione a finalidade</option>
                                <option value="rc" <?php if ( $listagem_condominio->finalidadeobra == 'rc' ) { echo 'selected'; } ?>>Residencial em Condomínio</option>
                                <option value="cm" <?php if ( $listagem_condominio->finalidadeobra == 'cm' ) { echo 'selected'; } ?>>Condomínio misto</option>
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>

                        <div class="col-md-2">  
                          <label for="areaterreno" class="field">Área do terreno (m²)
                          <input name="areaterreno" id="areaterreno" class="gui-input" type="text" value="<?php echo $manipuladores->numerodouble('imprimir',$listagem_condominio->areaterreno); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" maxlength="15">
                          </label>
                        </div>  
                          
                        <div class="col-md-offset-1 col-md-2">
                            <label for="situacaoterreno" class="select field prepend-icon">Situação do Terreno:
                            <label class="option">
                              <input name="situacaoterreno" type="radio" value="m" <?php if ( $listagem_condominio->situacaoterreno == 'm' ) { ?>checked="checked"<?php } ?>>
                               <span class="radio"></span>Meio Esquina</label>
                            <label class="option">
                              <input name="situacaoterreno" type="radio" value="e" <?php if ( $listagem_condominio->situacaoterreno == 'e' ) { ?>checked="checked"<?php } ?>>
                               <span class="radio"></span>Esquina</label>
                               </label>
                          </div><!-- col-md-2 --> 

                        <label class="option">
                              <input type="checkbox" name="desmembramento" onclick="ap9();" value="t" <?php if ( $listagem_condominio->desmembramento == true ) { ?>checked="checked"<?php } ?>>
                              <span class="checkbox"></span>Necessário Desmembramento</label>
                        
                        <div class="col-md-4"><br />
                          <div class="enblobacampos2" style="<?php if ( $listagem_condominio->desmembramento == false ) { ?>display: none;<?php } ?>">
                            <label for="desmembramento_entregue" class="select field prepend-icon">Entregue<br />
                            <label class="option">
                              <input name="desmembramento_entregue" type="radio" value="t" <?php if ( $listagem_condominio->desmembramento == true and $listagem_condominio->desmembramento_entregue == true ) { ?>checked="checked"<?php } ?>>
                               <span class="radio"></span>Sim</label>
                            <label class="option">
                              <input name="desmembramento_entregue" type="radio" value="f" <?php if ( $listagem_condominio->desmembramento == true and $listagem_condominio->desmembramento_entregue == false ) { ?>checked="checked"<?php } ?>>
                               <span class="radio"></span>Não</label>
                              </label>
                            </div><!-- col-md-2 -->
                          </div>

                      </div><!-- fim da row -->
                      <hr />
                      
                      <div class="section row">
                        <div class="col-md-3">
                          <div>Quantidade de Unidades Exclusivas:</div>
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="button btn-primary" onclick="addInput_unidades_condominio();">Adicionar Unidades</button>
                        </div>
                      </div>

                      <div class="row">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Unidades</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                      <div class="enblobacampos31">                           
                        <div id="text_unidades">
                        <?php 
                          //  Listando informações sobre as unidades.
                          $con_listagem_unidades = $processos->consulta("SELECT 
                            id_que,   
                            areacontruir,   
                            areaexistente,  
                            areausoexclusivo,   
                            areacomumproporcional,  
                            tipoprocesso

                            FROM 
                            processos.processos_que

                            WHERE processos.processos_que.id_con = $id_con ORDER BY processos.processos_que.id_con, processos.processos_que.id_que ASC");
                            $n_uni = 0;
                            foreach ( $con_listagem_unidades as $listagem_unidades ) { 
                              $n_uni++; ?>
                              
                              <input type="hidden" value="<?php echo $n_uni; ?>" name="num_casa[]">

                              <table class="table table-hover">
                                <tbody>
                                  <tr>
                                    <td><b>Casa <?php echo $n_uni; ?></b></td>
                                    <td><label class="select">
                                    <select id="tipo_processo_unidade[]" name="tipo_processo_unidade[]">
                                      <option value="">Selecione</option>
                                      <option <?php if ( $listagem_unidades->tipoprocesso == 'c' ) { ?>selected<?php } ?> value="c">Construção</option>
                                      <option <?php if ( $listagem_unidades->tipoprocesso == 'r' ) { ?>selected<?php } ?> value="r">Regularização</option>
                                      <option <?php if ( $listagem_unidades->tipoprocesso == 'a' ) { ?>selected<?php } ?> value="a">Ácrescimo de área</option>
                                    </select>
                                    <i class="arrow"></i>
                                    </label>

                                    <td>
                                        <button type='button' class='button btn-primary' onclick='addInput_pav_condominio(<?php echo $n_uni; ?>);'>Adicionar</button>
                                    </td>

                                    </td></tr>

                                    <tr><td colspan='3'>
                                        <div id='pav_condominio"+valor3+"'>
                                        </div>  
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              
                              <!-- Input para puxar valor afim de editar no banco -->
                              <input type="hidden" name="IDunidadesAdd[]" id="IDunidadesAdd[]" value="<?php echo $listagem_unidades->id_que; ?>">

                              <?php 
                                //  Listando informações sobre as unidades.
                                $con_listagem_unidades_pavimentos = $processos->consulta("SELECT 
                                  id_qp,
                                  id_que,   
                                  areacontruir,   
                                  areaexistente,  
                                  areausoexclusivo,   
                                  areacomumproporcional

                                  FROM 
                                  processos.processos_que_pavimentos

                                  WHERE processos.processos_que_pavimentos.id_que = $listagem_unidades->id_que ORDER BY processos.processos_que_pavimentos.id_qp ASC");
                                  $n_uni2 = 0;

                                  foreach ( $con_listagem_unidades_pavimentos as $listagem_unidades_pavimentos ) { 
                                    $n_uni2++; ?>

                                    <!-- Input para puxar valor afim de editar no banco -->
                                    <input type="hidden" name="IDunidadesPavimentosAdd<?php echo $n_uni; ?>[]" id="IDunidadesPavimentosAdd<?php echo $n_uni; ?>[]" value="<?php echo $listagem_unidades_pavimentos->id_qp; ?>">

                                    <table class='pav_condominio<?php echo $n_uni; ?>'>
                                      <tr>
                                        <td align='center' width='200'>Pav <?php echo $n_uni2; ?></td>
                                        <td >
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <label>Área a Construir(m²):</label> 
                                                 </div>
                                            </div>
                                            <div class='row'>
                                              <div class='col-md-10'>
                                                <input name='area_construir<?php echo $n_uni; ?>[]' id='area_construir<?php echo $n_uni; ?>[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15' value='<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades_pavimentos->areacontruir); ?>'>
                                              </div>
                                            </div>  
                                        </td>
                                        <td>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <label>Área Existente(m²):</label> 
                                                 </div>
                                            </div>
                                            <div class='row'>
                                              <div class='col-md-10'>
                                                <input name='area_existente<?php echo $n_uni; ?>[]' id='area_existente<?php echo $n_uni; ?>[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15' value='<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades_pavimentos->areaexistente); ?>'>
                                              </div>
                                            </div>  
                                        </td>
                                        <td>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <label>Área de Uso Exclusico(m²):</label> 
                                                 </div>
                                            </div>
                                            <div class='row'>
                                              <div class='col-md-10'>
                                                <input name='area_uso_exclusivo<?php echo $n_uni; ?>[]' id='area_uso_exclusivo<?php echo $n_uni; ?>[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15' value='<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades_pavimentos->areausoexclusivo); ?>'>
                                              </div>
                                            </div>  
                                        </td>
                                        <td>
                                            <div class='row'>
                                                <div class='col-md-12'>
                                                    <label>Área Comum Proporcional(m²):</label> 
                                                 </div>
                                            </div>
                                            <div class='row'>
                                              <div class='col-md-10'>
                                                <input name='area_comum_proporcional<?php echo $n_uni; ?>[]' id='area_comum_proporcional<?php echo $n_uni; ?>[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15' value='<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades_pavimentos->areacomumproporcional); ?>'>
                                              </div>
                                            </div>  
                                        </td>
                                      </tr>
                                    </table>
                                <?php } ?>
                                    <div id="pav_condominio<?php echo $n_uni; ?>">
                                      <input type="hidden" value="<?php echo $n_uni2; ?>" name="numero_pav_condominio<?php echo $n_uni; ?>[]" id="numero_pav_condominio<?php echo $n_uni; ?>[]" />
                                    </div>
                                <?php /* 
                                <table class="table table-hover">
                                  <tbody>
                                    <tr>
                                     <td>Casa <?php echo $n_uni; ?></td>
                                        <td><input name="area_construir[]" id="area_construir[]" class="gui-input" type="text" value="<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades->areacontruir); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" maxlength="15"></td>
                                        <td><input name="area_existente[]" id="area_existente[]" class="gui-input" type="text" value="<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades->areaexistente); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" maxlength="15"></td>
                                        <td><input name="area_uso_exclusivo[]" id="area_uso_exclusivo[]" class="gui-input" type="text" value="<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades->areausoexclusivo); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" maxlength="15"></td>
                                        <td><input name="area_comum_proporcional[]" id="area_comum_proporcional[]" class="gui-input" type="text" value="<?php echo $manipuladores->numerodouble('imprimir',$listagem_unidades->areacomumproporcional); ?>" onkeydown="Mascara(this,Area);" onkeypress="Mascara(this,Area);" onkeyup="Mascara(this,Area);" maxlength="15"></td>
                                        <td><label class="select">
                                          <select id="tipo_processo_unidade[]" name="tipo_processo_unidade[]">
                                            <option value="">Selecione</option>
                                            <option <?php if ( $listagem_unidades->tipoprocesso == 'c' ) { ?>selected<?php } ?> value="c">Construção</option>
                                            <option <?php if ( $listagem_unidades->tipoprocesso == 'r' ) { ?>selected<?php } ?> value="r">Regularização</option>
                                            <option <?php if ( $listagem_unidades->tipoprocesso == 'a' ) { ?>selected<?php } ?> value="a">Ácrescimo de área</option>
                                          </select>
                                          <i class="arrow"></i>
                                        </label>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                 */ ?>
                              <?php } ?>
                          </div>
                      </div>

                      <div class="panel-footer text-left">
                        <button type="submit" class="button btn-success">Atualizar</button>
                      </div>
            </div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/jquery.maskedinput.min.js"></script>
  <script src="js/mascaras.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">

    <?php if ( strlen($proprietario1) == 11 ) { ?>
      $("#cpfcnpjProprietario").mask("999.999.999-99");
    <?php } else if ( strlen($proprietario1) == 14 ) { ?>
      $("#cpfcnpjProprietario").mask("99.999.999/9999-99");
    <?php } ?>

    //  AJUSTA O TOTAL DA VARIÁVEL
    var valor5 = <?php if ( (int)$n_uni > 0 ) { echo $n_uni+1; } else { echo 1; } ?>;
    // FIM


  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {


    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        nome_proprietario: {
          required: true
        },
        email_proprietario: {
          required: true
        },
        cpfcnpj_proprietario: {
          required: true
        },
        autoria: {
          required: true
        },
        autoria_creacau: {
          required: true
        },
        autoria_profissional: {
          required: true
        },
        resptecnico: {
          required: function(element) { 
            if ($('input[name=resp_open]').is(':checked')){
              required: true
            }
          }
        },
        resptecnico_creacau: {
          required: function(element) { 
            if ($('input[name=resp_open]').is(':checked')){
              required: true
            }
          }
        },
        resptecnico_profissional: {
          required: function(element) { 
            if ($('input[name=resp_open]').is(':checked')){
              required: true
            }
          }
        },
        endereco: {
          required: true
        },
        bairro: {
          required: true
        },
        quadra: {
          required: true
        },
        lote: {
          required: true
        },
        numero: {
          required: true
        },
        finalidadeobra: {
          required: true
        },
        areaterreno: {
          required: true
        },
        situacaoterreno: {
          required: true
        },
        area_construir: {
          required: true
        },
        desmembramento_entregue: {
          required: function(element) { 
            if ($('input[name=desmembramento]').is(':checked')){
              required: true
            }
          }
        },
        tipopessoa: {
          required: true
        }

      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        nome_proprietario: {
          required: 'Digite o nome do Proprietário'
        },
        email_proprietario: {
          required: 'Digite o e-mail do Proprietário'
        },
        cpfcnpj_proprietario: {
          required: 'Digite o CPF/CNPJ do Proprietário'
        },
        autoria: {
          required: 'Digite o nome do autor'
        },
        autoria_creacau: {
          required: 'Digite o CREA/CAU do autor'
        },
        autoria_profissional: {
          required: 'Escolha o tipo de profissional do autor'
        },
        resptecnico: {
          required: 'Digite o nome do Resp. Técnico'
        },
        resptecnico_creacau: {
          required: 'Digite o CREA/CAU do Resp. Técnico'
        },
        resptecnico_profissional: {
          required: 'Escolha o tipo de profissional do Resp. Técnico'
        },
        endereco: {
          required: 'Digite o endereço'
        },
        bairro: {
          required: 'Digite o bairro'
        },
        quadra: {
          required: 'Digite a quadra'
        },
        lote: {
          required: 'Digite o lote'
        },
        numero: {
          required: 'Digite o número'
        },
        finalidadeobra: {
          required: 'Escolha a finalidade da obra'
        },
        areaterreno: {
          required: 'Preencha a área do terreno'
        },
        situacaoterreno: {
          required: 'Situação do Terreno inválida'
        },
        area_construir: {
          required: 'Preencha a área do terreno à construir'
        },
        desmembramento_entregue: {
          required: 'Informe a opção de Entregue'
        },
        tipopessoa: {
          required: 'Informe se o Proprietário é pessoa física ou jurídica'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>