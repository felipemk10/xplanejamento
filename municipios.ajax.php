<?php
header( 'Cache-Control: no-cache' );
header( 'Content-type: application/xml; charset="utf-8"', true );

//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

//	Iniciando classses.
$conexao = new conexao;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$configuracoes = new configuracoes;

//	Rodando anti-injection nas variáveis.
$cod_estado = strtoupper($validacoes->anti_injection($_GET['cod_estado']));

$con_listagem_municipios = $configuracoes->consulta("SELECT codigo, municipios.nome AS nome FROM geral.municipios INNER JOIN geral.estados ON municipios.uf = estados.uf WHERE codigouf = '".$cod_estado."' ORDER BY municipios.nome");

$municipios = array();

foreach ( $con_listagem_municipios as $listagem_municipios) {
	$municipios[] = array(
		'cod_municipio'	=> $listagem_municipios['codigo'],
		'nome'			=> $listagem_municipios['nome'],
	);
}

echo( json_encode( $municipios ) );