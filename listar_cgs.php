<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require 'vendor/autoload.php';

session_start();

$usuarios     = new \Biblioteca\usuarios();
$func_usuario = new \Biblioteca\func_usuario();

// cl = Cliente, co = Coordenador, an = Analista, fi = Fiscal, ad = Administrador
$usuarios->setTipoUsuario($_GET['tipousuario']);
// 1 = Engenheiro, 2 = Técnico, 3 = Arquiteto
$usuarios->setTipoProfissional((int)$_GET['tipoprofissional']);

if ( $_POST['form'] == 'status' ) {
  if ( $_POST['ativo'] == 'sim' ) { 
    $func_usuario->manipulacoes(
      (int)$_POST['idusuario'], 
      0,   
      0,  
      0,   
      0,   
      0,
      0,   
      0,   
      0,  
      0,
      0, 
      'ativa');
  } else if ( $_POST['ativo'] == 'nao' ) {
    $func_usuario->manipulacoes(
      (int)$_POST['idusuario'], 
      0,   
      0,  
      0,   
      0,   
      0,
      0,   
      0,   
      0,  
      0,
      0, 
      'desativa');
  }
}
?>
<!DOCTYPE html>
<html>

<head>
 <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Datatables CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/media/css/dataTables.bootstrap.css">

  <!-- Datatables Editor Addon CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/extensions/Editor/css/dataTables.editor.css">

  <!-- Datatables ColReorder Addon CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="datatables-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header2.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

            <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="dashboard.html">Listar </a>
            </li>
            <li class="crumb-trail">
              <a href="dashboard.html">
                <?php 
                  // 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Condomínio Edilício, 4 = Redimensionamento
                  if ( $_GET['tipousuario'] == 'ch' ) {
                    echo 'Clientes';
                  } else if ( (int)$_GET['tipoprofissional'] == 2 ) {
                    echo 'Usuários';
                  }
                ?>
              </a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">
                    <div class="panel-body bg-light">
                      <label class="option">
                            <input type="checkbox" name="botaolegenda" onclick="apLegenda();">
                            <span class="checkbox"></span>Legenda - Ocultar/Desocultar</label>
                      <div id="boxlegendas" style="display: none;">
                        <div class="section row">
                          <div class="panel-body bg-light">
                      <!-- .section-divider -->
                      <!-- inicio formulário -->

                      <div class="section row col-md-12"></div>
                
                      <div class="section row">
                       <div class="col-md-1">
                          <button type="button" class="btn btn-default dark">
                              <i class="glyphicon glyphicon-thumbs-up"></i>
                          </button>
                        </div>
                        <div class="col-md-2 leg-list">  
                          <div>Ativar usuário</div>  
                        </div>
                      </div><!-- Legenda -->  

                      <div class="section row">
                         <div class="col-md-1">
                            <button type="button" class="btn btn-default dark">
                                <i class="glyphicon glyphicon-thumbs-down"></i>
                            </button>
                          </div>
                          <div class="col-md-3 leg-list">  
                            <div>Desativar usuário</div>  
                          </div>
                      </div><!-- Legenda -->
                 </div><!-- fim da row -->
                  <div class="section row">
                    <div class="col-md-12 col-md-12-padding">
                      <table >
                        <tr>
                          <td><div class="col-md-1"></div><button type="button" class="leg-sitdr-1"></button></td>
                          <td>Usuário Ativado</td>
                        </tr>
                        <tr>
                          <td><div class="col-md-1"></div><button type="button" class="leg-sitdr-2"></button></td><td>Usuário Desativado</td>
                        </tr>
                      </table>
                    </div>
                  </div>

                      
                  </div><!--  Legenda - Cor -->
</div>

                      </div>
                  </div>

                  <div class="panel-body bg-light">

                    <!-- .section-divider -->
                    <!-- inicio formulário -->
                    <form method="get" enctype="multipart/form-data">

                    
                    <div class="section row">
                      <?php if ( $_GET['tipousuario'] == 'cl' ) { ?>
                        <input type="hidden" name="tipousuario" value="<?php echo $_GET['tipousuario']; ?>">
                        <div class="col-md-offset-5 col-md-3">
                            <label class="select">
                              <select id="tipoprofissional" name="tipoprofissional">
                                <option <?php if ( $tipoprofissional == 1 ) { ?>selected<?php } ?> value="1">Engenheiro</option>
                                <option <?php if ( $tipoprofissional == 2 ) { ?>selected<?php } ?> value="2">Técnico</option>
                                <option <?php if ( $tipoprofissional == 3 ) { ?>selected<?php } ?> value="3">Arquiteto</option>
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      <?php } else { ?>
                        <div class="col-md-offset-5 col-md-3">
                            <label class="select">
                              <select id="tipousuario" name="tipousuario">
                                <option <?php if ( $_GET['tipousuario'] == 'co' ) { ?>selected<?php } ?> value="co">Coordenador</option>
                                <option <?php if ( $_GET['tipousuario'] == 'an' ) { ?>selected<?php } ?> value="an">Analista</option>
                                <option <?php if ( $_GET['tipousuario'] == 'fi' ) { ?>selected<?php } ?> value="fi">Fiscal</option>
                                <option <?php if ( $_GET['tipousuario'] == 'ad' ) { ?>selected<?php } ?> value="ad">Administrador</option>
                              </select>
                              <i class="arrow"></i>
                            </label>
                        </div>
                      <?php } ?>
                      <!--<div class="col-md-3">
                        <input name="busca" id="busca" class="gui-input col-md-6" type="text">
                      </div>-->
                      <button type="submit" class="button btn-success">Buscar</button>
                    </div>
                    </form>
                    <div class="section row">
                        <div class="alert alert-default light ">
                           <i class="fa fa-cog pr10 hidden"></i>
                          <?php echo $usuarios->findAll()[0]; ?> Resultados encontrados
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                     <div class="panel panel-visible" id="spy3">
                       
                       <div class="panel-body pn">
                        <table class="table table-striped table-hover dataTable" id="datatable3" role="grid" aria-describedby="datatable3_info" style="width: 100%;" width="100%" cellspacing="0">
                         <thead>
                           <tr role="row">
                              <th class="sorting_asc" tabindex="0" aria-controls="datatable3" rowspan="1" colspan="1" style="width: 87px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Nome
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="datatable3" rowspan="1" colspan="1" style="width: 114px;" aria-label="Position: activate to sort column ascending">
                              <?php if ( $_GET['tipousuario'] == 'cl' ) { 
                                      echo 'Profissional';
                                    } else { 
                                      echo 'Departamento';
                                    }
                              ?>
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="datatable3" rowspan="1" colspan="1" style="width: 72px;" aria-label="Office: activate to sort column ascending"><?php if ( $_GET['tipousuario'] != 'cl' ) { 
                                      echo 'Cargo/Função';
                                    }
                              ?>
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="datatable3" rowspan="1" colspan="1" style="width: 33px;" aria-label="Age: activate to sort column ascending">E-mail
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="datatable3" rowspan="1" colspan="1" style="width: 79px;" aria-label="Start date: activate to sort column ascending">Data
                              </th>
                              <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 67px;" aria-label="Salary">Ativa/Desativa
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                /*
                                  1 - A ser analisado = white, 
                                  2 - Em análise = #ddd, 
                                  3 - Pendência de documentos ou correção = #c4d79b, 
                                  4 - Processo não permitido ou reprovado na vistoria = #c00000, 
                                  5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
                                  6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
                                  7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
                                  8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
                                  9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
                                  10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
                                  11 - Aprovado = #eada6d, 
                                  12 - Dispensar Vistoria
                                */
                                  foreach ( $usuarios->findAll()[1] as $key => $listagem):
                                    // Cor das legendas
                                    if ( $listagem->ativo == true ) {
                                      $corProcesso = "white;";
                                    } else if ( $listagem->ativo == false ) {
                                      $corProcesso = "#c00000;";
                                      $complementocorprocesso = "color:white;";
                                    }
                            ?>
                              
                            <tr role="row" class="odd">
                              <td class="sorting_1" style="background: <?php echo $corProcesso.$complementocorprocesso;  ?>"><?php echo $listagem->nome; ?></td>
                              <td style="background: <?php echo $corProcesso.$complementocorprocesso;  ?>"><?php 
                                  if ( $_GET['tipousuario'] == 'cl' ) {
                                    if ( $listagem->tipoprofissional == 1 ) {
                                      echo 'Engenheiro';
                                    } else if ( $listagem->tipoprofissional == 2 ) {
                                      echo 'Técnico';
                                    } else if ( $listagem->tipoprofissional == 3 ) {
                                      echo 'Arquiteto';
                                    }
                                  } else {
                                    echo $listagem->departamento;
                                  } 
                              ?></td>
                              <td style="background: <?php echo $corProcesso.$complementocorprocesso;  ?>"><?php if ( $_GET['tipousuario'] != 'cl' ) { echo $listagem->cargofuncao; } ?></td>
                              <td style="background: <?php echo $corProcesso.$complementocorprocesso;  ?>">
                              <?php echo $listagem->email; ?></td>
                              <td style="background: <?php echo $corProcesso.$complementocorprocesso;  ?>"><?php echo $manipuladores->formatar_data('/',$listagem->datahora,''); ?></td>
                              <td style="background: <?php echo $corProcesso.$complementocorprocesso;  ?>">
                                 <div class="bs-component">
                                    <div class="btn-group">
                                        <?php /*
                                        <button type="button" class="btn btn-default dark" <?php if ( (int)$_GET['tipoprofissional'] == 1 or (int)$_GET['tipoprofissional'] == 2 or (int)$_GET['tipoprofissional'] == 3 ) { ?>onclick="window.location.href='alt_alvara.php?id_pro=<?php echo $listagem->id_pro']; ?>&tipoprocesso=<?php echo $listagem->tipoprocesso']; ?>';"<?php } else if ( (int)$_GET['tipoprofissional'] == 4 ) { ?>onclick="window.location.href='alt_condominio.php?id_pro=<?php echo $listagem->id_pro']; ?>&tipoprocesso=<?php echo $listagem->tipoprocesso']; ?>';"<?php } else if ( (int)$_GET['tipoprofissional'] == 5 ) { ?>onclick="window.location.href='alt_redimensionamento.php?id_pro=<?php echo $listagem->id_pro']; ?>&tipoprocesso=<?php echo $listagem->tipoprocesso']; ?>';"<?php } ?>>
                                          <i class="glyphicon glyphicon-cog"></i>
                                        </button>

                                         onclick="window.location.href='alt_redimensionamento.php?tipousuario=<?php echo $_GET['tipousuario']; ?>&tipoprofissional=<?php echo $tipoprofissional; ?>';"
                                        */ ?>
                                        <?php 
                                          //  O usuário não pode desativar a si mesmo
                                          if ( $listagem->id_cg != $manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode') ) { ?>
                                          <?php if ( $listagem->ativo == false ) { ?>
                                          <form method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="form" value="status">
                                            <input type="hidden" name="ativo" value="sim">
                                            <input type="hidden" name="idusuario" value="<?php echo $listagem->id_cg; ?>">
                                            <button type="submit" class="btn btn-default dark">
                                              <i class="glyphicon glyphicon-thumbs-up"></i>
                                            </button>
                                          </form>
                                          <?php } else if ( $listagem->ativo == true ) { ?>
                                          <form method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="form" value="status">
                                            <input type="hidden" name="ativo" value="nao">
                                            <input type="hidden" name="idusuario" value="<?php echo $listagem->id_cg; ?>">
                                            <button type="submit" class="btn btn-default dark">
                                              <i class="glyphicon glyphicon-thumbs-down"></i>
                                            </button>
                                          </form>
                                          <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                              </td>
                            </tr>
                            <?php unset($corProcesso,$complementocorprocesso); endforeach; ?>
                          </tbody>
                        </table>
                       </div>
                      </div>
                      </div>
                    </div>
                  </div>            
                </div>
            </div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside> 
    <!-- End: Right Sidebar -->



  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->
  <style type="text/css">

  .col-md-12-padding {
      font-size: 10px;
      width: 90%;
      padding: 0px 0 0px 54px;

    }

    
  .leg-sitdr-1, .leg-sitdr-2, .leg-sitdr-3, .leg-sitdr-4, .leg-sitdr-5, .leg-sitdr-6, .leg-sitdr-7, .leg-sitdr-8, .leg-sitdr-9,  
  .leg-sitdr-10, .leg-sitdr-11  {
    font-size: 5px;
    width: 25px;
    height: 23px;

  }
  
  .leg-sitdr-1 {
    background-color: white; 
    /*Legenda em estado de análise*/

  }
  .leg-sitdr-2 {
    background-color: #c00000; 
    /*Legenda Pendência de documentos ou correção*/

  }
  

  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>
  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Datatables -->
  <script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

  <!-- Datatables Tabletools addon -->
  <script src="vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

  <!-- Datatables ColReorder addon -->
  <script src="vendor/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
    function apLegenda(){ 
        if ( $('input[name=botaolegenda]').is(':checked')) {
            $("#boxlegendas").fadeIn(500);
          }else{
            $("#boxlegendas").fadeOut(500);
          } 
      }
  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    //Demo.init();

    // Init DataTables
    

    $('#datatable3').dataTable({
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 5,
      "aLengthMenu": [
        [5, 10, 25, 50, -1],
        [5, 10, 25, 50, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }
    });

    var alphabet = $('<div class="dt-abc-filter"/>').append('<span class="abc-label">Search: </span> ');
    var columnData = table6.column(0).data();
    var bins = bin(columnData);

    $('<span class="active"/>')
      .data('letter', '')
      .data('match-count', columnData.length)
      .html('None')
      .appendTo(alphabet);

    for (var i = 0; i < 26; i++) {
      var letter = String.fromCharCode(65 + i);

      $('<span/>')
        .data('letter', letter)
        .data('match-count', bins[letter] || 0)
        .addClass(!bins[letter] ? 'empty' : '')
        .html(letter)
        .appendTo(alphabet);
    }

    var info = $('<div class="alphabetInfo"></div>')
      .appendTo(alphabet);

    var _alphabetSearch = '';

    $.fn.dataTable.ext.search.push(function(settings, searchData) {
      if (!_alphabetSearch) {
        return true;
      }
      if (searchData[0].charAt(0) === _alphabetSearch) {
        return true;
      }
      return false;
    });

    function bin(data) {
      var letter, bins = {};
      for (var i = 0, ien = data.length; i < ien; i++) {
        letter = data[i].charAt(0).toUpperCase();

        if (bins[letter]) {
          bins[letter]++;
        } else {
          bins[letter] = 1;
        }
      }
      return bins;
    }

    // ROW GROUPING
    var table7 = $('#datatable7').DataTable({
      "columnDefs": [{
        "visible": false,
        "targets": 2
      }],
      "order": [
        [2, 'asc']
      ],
      "sDom": 't<"dt-panelfooter clearfix"ip>',
      "displayLength": 25,
      "drawCallback": function(settings) {
        var api = this.api();
        var rows = api.rows({
          page: 'current'
        }).nodes();
        var last = null;

        api.column(2, {
          page: 'current'
        }).data().each(function(group, i) {
          if (last !== group) {
            $(rows).eq(i).before(
              '<tr class="row-label ' + group.replace(/ /g, '').toLowerCase() + '"><td colspan="5">' + group + '</td></tr>'
            );
            last = group;
          }
        });
      }
    });

    // Order by the grouping
    $('#datatable7 tbody').on('click', 'tr.row-label', function() {
      var currentOrder = table7.order()[0];
      if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
        table7.order([2, 'desc']).draw();
      } else {
        table7.order([2, 'asc']).draw();
      }
    });

    $('#datatable8').DataTable({
      "sDom": 'Rt<"dt-panelfooter clearfix"ip>',
    });


    // COLLAPSIBLE ROWS
    function format ( d ) {
      // `d` is the original data object for the row
      return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
          '<td class="fw600 pr10">Full name:</td>'+
          '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="fw600 pr10">Extension:</td>'+
          '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
          '<td class="fw600 pr10">Extra info:</td>'+
          '<td>And any further details here (images etc)...</td>'+
        '</tr>'+
      '</table>';
    }
     
    var table = $('#datatable9').DataTable({
      "sDom": 'Rt<"dt-panelfooter clearfix"ip>',
      "ajax": "vendor/plugins/datatables/examples/data_sources/objects.txt",
      "columns": [
        {
          "className":      'details-control',
          "orderable":      false,
          "data":           null,
          "defaultContent": ''
        },
        { "data": "name" },
        { "data": "position" },
        { "data": "office" },
        { "data": "salary" }
      ],
      "order": [[1, 'asc']]
    });
     
    // Add event listener for opening and closing details
    $('#datatable9 tbody').on('click', 'td.details-control', function () {
      var tr = $(this).closest('tr');
      var row = table.row( tr );

      if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
      }
      else {
        // Open this row
        row.child( format(row.data()) ).show();
        tr.addClass('shown');
      }
    });


    // MISC DATATABLE HELPER FUNCTIONS

    // Add Placeholder text to datatables filter bar
    $('.dataTables_filter input').attr("placeholder", "Enter Terms...");

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>