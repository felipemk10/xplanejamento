<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

$id_pro = (int)$_GET['id_pro'];
$id_ah 	= (int)$_GET['id_ah'];

// tipo habite-se
if ( $_GET['th'] == 'p' ) {
	$nomeHabitese = 'HABITE-SE PARCIAL';
} else {
	$nomeHabitese = 'HABITE-SE';
}

// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

if ( $id_pro > 0 ) {
  //  Listando informações sobre o processo.
  $consulta = $configuracoes->consulta("SELECT 
    processos_analise.id_ana,
    processos_analise.id_pro,
    processos_analise.tipo,
    processos_analise.situacaoprojeto,
    processos_analise.obsgerais,
    processos_analise.descricao_img1,
    processos_analise.descricao_img2,
    processos_analise.descricao_img3,
    processos_analise.descricao_img4,
    processos_analise.descricao_img5
                           

    FROM 
    processos.processos_analise

    WHERE processos.processos_analise.id_pro = $id_pro and tipo = 'fi'");
    $linha = $consulta->fetch();

    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();

      $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
        id_pro,
        -- 1 = Alvará, 2 = Habite-se
        tipo,
        numero,   
        id_cg,
        ano,
        id_ah2,
        id_que,
        substituir,
        datahora

        FROM 

        processos.processos_alvarahabitese 

        WHERE  processos_alvarahabitese.id_pro = $id_pro and processos_alvarahabitese.id_ah = $id_ah");
      
      
      $linha_alvarahabitese = $consulta_alvarahabitese->fetch();

      if ( $tipoprocesso == 4 ) { 
	      //  Listando informações sobre responsável técnico.
	      $con_listagem_alvara = $configuracoes->consulta("SELECT 
	        processos_condominio.id_con,
	        processos_condominio.finalidadeobra,
	        processos_condominio.areaterreno,
	        processos_condominio.situacaoterreno,
	        processos_condominio.desmembramento

	        FROM 
	        processos.processos_condominio

	        WHERE processos.processos_condominio.id_pro = $id_pro");

	        $listagem_alvara = $con_listagem_alvara->fetch();

	        $id_alv = $listagem_alvara['id_con'];
	        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial

	        /* Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
	          pat = a construir
	          sbc = área sub solo a construir
	          are = área Existente
	          sbe = área sub solo existente
	        */
      } else {
      	//  Listando informações sobre responsável técnico.
	      $con_listagem_alvara = $configuracoes->consulta("SELECT 
	        processos_alvara.id_alv,
	        processos_alvara.finalidadeobra,
	        processos_alvara.areaterreno,
	        processos_alvara.situacaoterreno,
	        processos_alvara.desmembramento,
	        processos_alvara.taxapermeabilidade

	        FROM 
	        processos.processos_alvara

	        WHERE processos.processos_alvara.id_pro = $id_pro");

	        $listagem_alvara = $con_listagem_alvara->fetch();

	        $id_alv = $listagem_alvara['id_alv'];
	        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial

	        /* Calcula a Taxa de Ocupação e Ind. Utilização através dos registros do tipo informado abaixo:
	          pat = a construir
	          sbc = área sub solo a construir
	          are = área Existente
	          sbe = área sub solo existente
	        */
      }

        if ( $tipoprocesso != 4 ) { 
          $con_listagem_pavimento = $configuracoes->consulta("SELECT 
            processos_pavimentacao.id_pav,
            processos_pavimentacao.area,
            processos_pavimentacao.tipo

            FROM 
            processos.processos_pavimentacao

            WHERE processos.processos_pavimentacao.id_alv = $id_alv 
            and (processos.processos_pavimentacao.tipo = 'pat' 
            or processos.processos_pavimentacao.tipo = 'sbc'
            or processos.processos_pavimentacao.tipo = 'are' 
            or processos.processos_pavimentacao.tipo = 'sbe') 
            ORDER BY processos.processos_pavimentacao.id_pav ASC");

          $total_contruir = 0;
          $total_pat = 0;
          $total_sbc = 0;
          $total_are = 0;
          $total_sbe = 0;

          $total_patare_terreo = 0;
          $i = 0;
          foreach ( $con_listagem_pavimento as $listagem_pavimento ) { 
              
              $total_contruir = $total_contruir + $listagem_pavimento['area'];

              if ( $listagem_pavimento['tipo']  == 'pat' ) {
                $total_pat = $total_pat+$listagem_pavimento['area'];
                if ( $i == 0 ) {
                  $total_patare_terreo = $listagem_pavimento['area'];
                  $i++;
                }
              }
              if (  $listagem_pavimento['tipo']  == 'sbc' ) {
                $total_sbc = $total_sbc+$listagem_pavimento['area'];
              }
              if (  $listagem_pavimento['tipo']  == 'are' ) {
                $total_are = $total_are+$listagem_pavimento['area'];
                 if ( $i == 1 ) {
                  $total_patare_terreo = $total_patare_terreo + $listagem_pavimento['area'];
                  $i++;
                }
              }
              if (  $listagem_pavimento['tipo']  == 'sbe' ) {
                $total_sbe = $total_sbe+$listagem_pavimento['area'];
              }
          }

          // Como a variável é usada em outras funções abaixo, criei uma nova para não perder o valor afim de colocar o mesmo no subtotal no rodapé do relatório.
          $total_pat2 = $total_pat;
        }
}

include("biblioteca/mpdf60/mpdf.php");

$html = "
<body style='text-align:center;font-family:Helvetica;color:#393939;'>
	<table width='800' align='center' border='0' style='font-size:15px;border-style:dashed;'>
		<tr>
			<td colspan='4' align='center'>
				<h2 align='center' style='padding-top:20px;padding-bottom:20px;font-size:30px;'>";

					$html .= $nomeHabitese;

$html .= "</h2> <br /><br /><br /><br /><br /><br />
			</td>
		</tr>
			<tr style='font-size:18px;'>
				<td align='left' width='50' style='padding-top:5px;padding-bottom:5px;font-size:12px;'>EMISSÃO:</td>
				<td align='left' style='padding-top:5px;padding-bottom:5px;'><strong>";


				$html .= $formatacoes->formatar_data('/',$linha_alvarahabitese['datahora']);

$html .=		"</strong></td>
				<td align='right' style='padding-top:5px;padding-bottom:5px;font-size:12px;'>NÚMERO:</td>
				<td width='170' align='right' style='font-size:20px;font-style:italic;'><strong>";

		$html .= $linha_alvarahabitese['numero'].' / '.$linha_alvarahabitese['ano'];


$html .= "			</strong></td>
			</tr>";	

if ( (int)$linha_alvarahabitese['substituir'] > 0 ) {

	$substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$linha_alvarahabitese['substituir']." and tipo = ".$linha_alvarahabitese['tipo']." and id_pro != 0 ORDER BY id_ah DESC")->fetch();//Irving modif. 18/09/2020
	$html .= "<tr>
				<td colspan='4' align='center' style='background:#EEEED1;'><strong>Este documento substitui o habite-se existente de número:</strong></td>
			 </tr>
			 <tr>
				<td align='center' colspan='4' style='background:#EEEED1;'><strong>";

				if ( $linha2['tipoprocesso'] == 2 ) { 
					$html .= 'R.'; 
				} 
				$html .= $linha_alvarahabitese['substituir'].' / '.$substituto['ano'];


	$html .= "			</strong></td>
			</tr>";
}

$html .= "		
		<tr align='center'>
			<td width='150' colspan='3' style='padding-top:5px;padding-bottom:5px;border-style:dashed;border-color:#EEEED1;'><strong>DADOS DO PROPRIETÁRIO</strong></td>
		</tr>";

//  Listando informações sobre os proprietários.
$con_listagem_proprietario = $configuracoes->consulta("SELECT 
cg.nome,
cg.cpfcnpj

FROM 
processos.processos_proprietario

INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

foreach ( $con_listagem_proprietario as $listagem_proprietario ) {

$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td align='right' width='20'><b>NOME:</b></td>
						<td width='4'></td>
						<td align='left' width='400'>";
						$html .= strtoupper($listagem_proprietario['nome']);
						$html .= "</td>
						<td align='4' width='20'><b>";
						if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
				        	$html .= 'CPF';
				        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
				          	$html .= 'CNPJ';
				        }

						$html .= "</b></td>
						<td width='4'></td>
						<td align='left'>"; 
						if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
				          $html .= $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'###.###.###-##');
				        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
				          $html .= $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'##.###.###/####-##');
				        }
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$html .= "		
		<tr align='center'>
			<td width='150' colspan='3' style='padding-top:5px;padding-bottom:5px;border-style:dashed;border-color:#EEEED1;'><strong>DADOS DO AUTOR/RESP. TÉCNICO</strong></td>
		</tr>";

$con_listagem_autor = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");
foreach ( $con_listagem_autor as $listagem_autor ) { 
	$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td align='right' width='200'><b>AUTOR:</b></td>
						<td width='4'></td>
						<td align='left' width='300'>";
						$html .= strtoupper($listagem_autor['nome']);
						$html .= "</td>
						<td align='4' width='20'><b>";
						if ( $listagem_autor['tipoprofissional'] == 'e' ) { 
							$html .= 'CREA'; 
						} else if ( $listagem_autor['tipoprofissional'] == 'a' ) { 
							$html .= 'CAU'; 
						}

						$html .= "</b></td>
						<td width='4'></td>
						<td align='left'>"; 
						$html .= $listagem_autor['creacau'];
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$con_listagem_resptecnico = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");
foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) {
	$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td align='right' width='200'><b>RESP. TÉCNICO:</b></td>
						<td width='4'></td>
						<td align='left' width='300'>";
						$html .= strtoupper($listagem_resptecnico['nome']);
						$html .= "</td>
						<td align='right' width='20'><b>";
						if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { 
							$html .= 'CREA'; 
						} else if ( $listagem_resptecnico['tipoprofissional'] == 'a' ) { 
							$html .= 'CAU'; 
						}

						$html .= "</b></td>
						<td width='4'></td>
						<td align='left'>"; 
						$html .= $listagem_resptecnico['creacau'];
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$html .= "<tr align='center'>
			<td width='150' colspan='3' style='padding-top:5px;padding-bottom:5px;border-style:dashed;border-color:#EEEED1;'><strong>DADOS DO PROJETO</strong></td>
		</tr>";

//  Listando informações sobre autor.
$con_listagem_autor = $configuracoes->consulta("SELECT 
  cg.nome,
  cg.creacau,
  -- e = engenheiro, a = arquiteto
  processos_profissional.tipoprofissional

  FROM 
  processos.processos_profissional

  INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

  WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

foreach ( $con_listagem_autor as $listagem_autor ) {

$html .= "<tr>
			<td colspan='4'>
				<table style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td width='20'><b>AUTORIA:</b></td>
						<td width='2'></td>
						<td>";
						$html .= strtoupper($listagem_autor['nome']);
						$html .= "</td>
						<td align='right' width='50'><b>";
						if ( $listagem_autor['tipoprofissional'] == 'e' ) { 
							$html .= 'CREA'; 
						} else if ( $listagem_autor['tipoprofissional'] == 'a' ) { 
							$html .= 'CAU'; 
						}
						$html .= "</b></td>
						<td width='150'>";
						$html .= $listagem_autor['creacau'];
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td width='20'><b>ENDEREÇO:</b></td>
						<td width='2'></td>
						<td align='left'>";
						$html .= $linha2['endereco'];
						$html .= "</td>
						<td align='right' width='50' colspan='2'><b>NÚMERO:</b></td>
						<td width='150'>";
						$html .= $linha2['numero'];
						$html .= "</td>
					</tr>
					<tr>
						<td width='20' align='right'><b>QUADRA:</b></td>
						<td></td>
						<td>";
						$html .= $linha2['quadra'];
						$html .= "</td>
						<td align='right' width='50' colspan='2'><b>LOTE:</b></td>
						<td width='150'>";
						$html .= $linha2['lote'];
						$html .= "</td>
					</tr>
					<tr>
						<td width='20' align='right'><b>BAIRRO:</b></td>
						<td></td>
						<td>"; 
						$html .= $linha2['bairro'];
						$html .= "</td>
						<td align='right' width='50'><b></b></td>
						<td align='right' width='50'><b>CIDADE:</b></td>
						<td width='300'>LUÍS EDUARDO MAGALHÃES - BA</td>
					</tr>";


					if ( $tipoprocesso >= 1 and $tipoprocesso <= 3 or $tipoprocesso == 6 ) { 
			        	include_once('includes/impressao/habitese_alvara_r.php');
			        } else if ( $tipoprocesso == 4 and $_GET['condominio_pavimentos'] != 't' ) { 
			        	include_once('includes/impressao/habitese_condominio_r.php');
			        } else if ( $tipoprocesso == 4 and $_GET['condominio_pavimentos'] == 't' ) { 
			        	include_once('includes/impressao/habitese_condominio_pavimentos.php');
			        }						

				$html .= "</table>
	 			</td>
	 		</tr>
		</table>
	</body>";



 $mpdf = new mPDF(); 
 $mpdf->SetDisplayMode('fullpage');
 $mpdf->SetHTMLFooter($rodape);
 $mpdf->AddPage('', // L - landscape, P - portrait 
    '', '', '', '',
    15, // margin_left
    15, // margin right
    55, // margin top
    0, // margin bottom
    10, // margin header
    30  // margin footer
  ); 

 //$css = file_get_contents("css/estilo.css");
 //$mpdf->WriteHTML($css,1);

 //$mpdf->SetHTMLHeader($header);
 $mpdf->WriteHTML($html);
 $mpdf->Output();

 //exit;