<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
$formatacoes = new formatacoes;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$func_alvarahabitese = new func_alvarahabitese;
//  Função para includes reconhecer a página e modificar sua exibição.
$aprovacaologs = true;
$id_pro = (int)$_GET['id_pro'];
// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

//  Condição para condomínios com pavimentos
if ( $_GET['condominio_pavimentos'] == 't' ) {
  $pavimento = 't';
}

//  Função para includes reconhecer a página e modificar sua exibição.
$emissaoalvarahabitese = true;

if ( $_POST['form'] == 'alvarahabitese' ) {
  
  if ( isset($_POST['checksub']) and (int)$_POST['numerosubstituicao'] > 0 and (int)$_POST['anosubstituicao'] > 0 ) {
    $substituir = $_POST['numerosubstituicao'];
    $tipo2 = 1;

    if ( $configuracoes->consulta("SELECT numero FROM processos.processos_alvarahabitese WHERE numero = ".(int)$_POST['numerosubstituicao']." and ano = ".(int)$_POST['anosubstituicao']." and tipo = ".(int)$_POST['tipo']." and tipo2 = 2 ORDER BY id_ah DESC")->rowCount() == 0 ) {
      $func_alvarahabitese->manipulacoes(
        0,
        //  1 = Alvará, 2 = Habite-se, 3 = Alvará Regularização
        (int)$_POST['tipo'],
        (int)$_POST['numerosubstituicao'],   
        $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'), 
        (int)$_POST['anosubstituicao'],
        0,
        0,
        0,
        2,
        $_POST['dataah'],
        0,
        'cadastro');
    }
  }

  if ( isset($_POST['checkbotaogerarmanual']) and (int)$_POST['numerogerarmanual'] > 0 and (int)$_POST['anogerarmanual'] > 0 ) {
    $numero = (int)$_POST['numerogerarmanual'];
    $ano    = (int)$_POST['anogerarmanual'];
    $tipo2 = 2;
  } else {
    $numero = 0;
    $ano    = date('Y');
    $tipo2 = 1;
  }

  $func_alvarahabitese->manipulacoes(
    $id_pro,
    //  1 = Alvará, 2 = Habite-se, 3 = Alvará Regularização
    (int)$_POST['tipo'],
    $numero,   
    $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'), 
    $ano,
    (int)$_POST['id_ah'],
    (int)$_POST['id_que'],
    (int)$substituir,
    $tipo2,
    $_POST['dataah'],
    0,
    'cadastro');

  ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Procedimento efetuado com sucesso!');
              window.open('detalhes.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&tipoprocesso=<?php echo (int)$_GET['tipoprocesso']; ?>&condominio_pavimentos=<?php echo $pavimento; ?>','_self');
          }, 500);
        });
    </script>
    
    <?php
} else if ( $_POST['form'] == 'deleteah' ) {
  $func_alvarahabitese->manipulacoes(
        0,
        0,
        0,   
        0, 
        0,
        0,
        0,
        0,
        0,
        0,
        //  usado para remover registro.
        (int)$_POST['delete_id_ah'],
        'excluir');
   ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Procedimento efetuado com sucesso!');
              window.open('detalhes.php?id_pro=<?php echo (int)$_POST['id_pro']; ?>&tipoprocesso=<?php echo (int)$_POST['tipoprocesso']; ?>&condominio_pavimentos=<?php echo $pavimento; ?>','_self');
          }, 500);
        });
    </script>
    
    <?php
}