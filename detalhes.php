<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
$formatacoes = new formatacoes;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$func_alvarahabitese = new func_alvarahabitese;
//  Função para includes reconhecer a página e modificar sua exibição.
$aprovacaologs = true;
$id_pro = (int)$_GET['id_pro'];
// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

//  Função para includes reconhecer a página e modificar sua exibição.
$emissaoalvarahabitese = true;

if ( $id_pro > 0 ) {
    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();
}
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a>Alvará / Habite-se</a>
            </li>

          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <div class="panel-body bg-light">

              <?php 
                  if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 2 or $linha2['tipoprocesso'] == 3 or $linha2['tipoprocesso'] == 6 ) {
                    include_once('includes/info_alvara.php');
                  } else if ( $linha2['tipoprocesso'] == 4 and $_GET['condominio_pavimentos'] != 't' ) {
                    include_once('includes/info_condominio.php');
                  } else if ( $linha2['tipoprocesso'] == 4 and $_GET['condominio_pavimentos'] == 't' ) {
                    include_once('includes/info_condominio_pavimentos.php');
                  } else if ( $linha2['tipoprocesso'] == 5 ) {
                    include_once('includes/info_redimensionamento.php');
                  } else if ( $linha2['tipoprocesso'] == 7 ) {//Irving
                    include_once('includes/info_loteamento.php');
                  }

              ?>
              <?php ?>

              <?php if ( $tipoprocesso != 5 ) { 

              if ( $linha2['tipoprocesso'] != 4 ) {
                $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
                  id_ah,
                  id_pro,
                  --  1 = Alvará, 2 = Habite-se, 3 Alvará Regularização
                  tipo,
                  numero,   
                  id_cg,
                  ano,
                  substituir

                  FROM 

                  processos.processos_alvarahabitese 

                  WHERE processos_alvarahabitese.id_pro = $id_pro");
                  
                  if ( $consulta_alvarahabitese->rowCount() > 0 ) { ?>
                <table class="table">
                  <thead>
                    <tr class="primary">
                      <th style="color:white;">ID/ANO</th>
                      <th colspan="2"></th> 
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $alvara = false;
                  $habitese = false;
                  foreach ( $consulta_alvarahabitese as $listagem_alvarahabitese ) { 
                    //  CONSULTANDO DO REGISTRO SUBSTITUIDO
                    $substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$listagem_alvarahabitese['substituir']." and tipo = ".$listagem_alvarahabitese['tipo']." and id_pro != 0 ORDER BY id_ah DESC")->fetch();//Irving modif. 18/09/2020
                    ?>
                    <tr>
                      <td><?php if ( $listagem_alvarahabitese['tipo'] == 3 ) { echo 'R.'; } echo $listagem_alvarahabitese['numero'].'/'.$listagem_alvarahabitese['ano']; ?></td>
                      <td><a 
                      <?php
                        if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) { ?>
                          href="alvara_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&obs=" target="_blank">
                        <?php } else if ( $listagem_alvarahabitese['tipo'] == 2 ) { ?>
                          href="habitese_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&th=t&obs=" target="_blank">
                          <?php } ?>
                        <?php 

                        if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) {
                          $alvara = true;
                          $id_ah = $listagem_alvarahabitese['id_ah'];
                          echo 'Visualizar Alvará'; if ( (int)$listagem_alvarahabitese['substituir'] > 0 ) { echo ' - Substituindo o alvará: '; if ( $listagem_alvarahabitese['tipo'] == 3 ) { echo 'R.'; } echo $listagem_alvarahabitese['substituir'].'/'.$substituto['ano']; }
                        } else if ( $listagem_alvarahabitese['tipo'] == 2 ) {
                          $habitese = true;
                          echo 'Visualizar Habite-se'; if ( (int)$listagem_alvarahabitese['substituir'] > 0 ) { echo ' - Substituindo o habite-se: '.$listagem_alvarahabitese['substituir'].'/'.$substituto['ano']; }
                        }
                      ?>
                      </a>
                      <?php if ( $listagem_alvarahabitese['tipo'] == 2 ) { ?> | 
                      <a href="habitese_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>&th=p&obs=" target="_blank"><b>Parcial</b></a>
                      <?php } ?>
                      </td>
                      <td>
                         <form method="post" id="admin-form" action="back_geraralvara.php">
                          <input type="hidden" name="form" value="deleteah">
                          <input type="hidden" name="delete_id_ah" value="<?php echo (int)$listagem_alvarahabitese['id_ah']; ?>">
                          <input type="hidden" name="id_pro" value="<?php echo (int)$_GET['id_pro']; ?>">
                          <input type="hidden" name="tipoprocesso" value="<?php echo (int)$_GET['tipoprocesso']; ?>">
                          <button type="submit" class="btn-info">Excluir</button>
                        </form>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <?php } ?>
                <table class="table">
                  <thead>
                    <tr class="primary">
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ( $alvara == false ) { ?>
                    <tr>
                      <td>
                        <form method="post" id="admin-form" action="back_geraralvara.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&tipoprocesso=<?php echo (int)$_GET['tipoprocesso']; ?>">
                          <input type="hidden" name="form" value="alvarahabitese">
                          <input type="hidden" name="tipo" value="<?php 
                            if ( $tipoprocesso == 2 ) { 
                              echo 3;
                            } else {
                              echo 1;
                            }
                          ?>">
                          <div class="panel-footer text-left">
                            <table>
                              <tr>
                                <td><button type="submit" class="button btn-success">Gerar Alvará</button></td>
                                <td>
                                  <div class="col-md-100" style="margin-left: 10px;">
                                    <label class="option option-primary">
                                      <input type="checkbox" name="checksub" id="checksub" onclick="jschecksub();" value="true">
                                      <span class="checkbox"></span>Subs. Alvará
                                    </label>
                                  </div>
                                  <div class="col-md-6 boxsubstituicao" style="display: none;">
                                    <table width="300">
                                      <tr>
                                        <td align="left">
                                          <div class="col-md-8">
                                            <input type="text" class="gui-input col-md-6" id="numerosubstituicao" name="numerosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                          </div>
                                        </td>
                                        <td align="left">
                                          <div class="col-md-8">
                                            <input type="text" class="gui-input col-md-6" id="anosubstituicao" name="anosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                </td> 
                                <td>
                                  <div class="col-md-100" style="margin-left: 10px;">
                                    <label class="option option-primary">
                                      <input type="checkbox" name="checkbotaogerarmanual" id="checkbotaogerarmanual" onclick="jscheckgerarmanual2();" value="true">
                                      <span class="checkbox"></span>Número manual
                                    </label>
                                  </div>
                                </td>
                                <td>
                                  <table width="300">
                                    <tr>
                                      <td align="left">
                                        <div class="col-md-8 boxgerarmanual" style="display: none;">
                                          <input type="text" class="gui-input col-md-8" name="numerogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                        </div>
                                      </td>
                                      <td align="left">
                                        <div class="col-md-8 boxgerarmanual" style="display: none;">
                                          <input type="text" class="gui-input col-md-8" name="anogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td> 
                                <td width="100">
                                  Data emissão
                                  <input type="text" class="gui-input col-md-3" id="dataah" name="dataah" maxlength="10" placeholder="Emissão">
                                </td> 
                              </tr>
                            </table>
                          </div>
                        </form>
                      </td>
                    </tr>
                    <?php } if ( $alvara == true and $habitese == false ) { ?>
                    <tr>
                      <td>
                        <form method="post" id="admin-form" action="back_geraralvara.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&tipoprocesso=<?php echo (int)$_GET['tipoprocesso']; ?>">
                          <input type="hidden" name="form" value="alvarahabitese">
                          <input type="hidden" name="id_ah" value="<?php echo $id_ah; ?>">
                          <input type="hidden" name="tipo" value="2">
                          <div class="panel-footer text-left">
                            <table>
                              <tr>
                                <td><button type="submit" class="button btn-success">Gerar Habite-se</button></td>
                                <td>
                                  <div class="col-md-100" style="margin-left: 10px;">
                                    <label class="option option-primary">
                                      <input type="checkbox" name="checksub" id="checksub" onclick="jschecksub();" value="true">
                                      <span class="checkbox"></span>Subs. Habite-se
                                    </label>
                                  </div>
                                  <div class="col-md-6 boxsubstituicao" style="display: none;">
                                    <table width="300">
                                      <tr>
                                        <td align="left">
                                          <div class="col-md-8">
                                            <input type="text" class="gui-input col-md-6" id="numerosubstituicao" name="numerosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                          </div>
                                        </td>
                                        <td align="left">
                                          <div class="col-md-8">
                                            <input type="text" class="gui-input col-md-6" id="anosubstituicao" name="anosubstituicao" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                </td> 
                                <td>
                                  <div class="col-md-100" style="margin-left: 10px;">
                                    <label class="option option-primary">
                                      <input type="checkbox" name="checkbotaogerarmanual" id="checkbotaogerarmanual" onclick="jscheckgerarmanual2();" value="true">
                                      <span class="checkbox"></span>Número manual
                                    </label>
                                  </div>
                                </td>
                                <td>
                                  <table width="300">
                                    <tr>
                                      <td align="left">
                                        <div class="col-md-8 boxgerarmanual" style="display: none;">
                                          <input type="text" class="gui-input col-md-8" name="numerogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Número">
                                        </div>
                                      </td>
                                      <td align="left">
                                        <div class="col-md-8 boxgerarmanual" style="display: none;">
                                          <input type="text" class="gui-input col-md-8" name="anogerarmanual" onkeydown="somente_numero(this);" onkeypress="somente_numero(this);" onkeyup="somente_numero(this);" maxlength="10" placeholder="Ano">
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="100">
                                  Data emissão
                                  <input type="text" class="gui-input col-md-3" id="dataah" name="dataah" maxlength="10" placeholder="Emissão">
                                </td> 
                              </tr>
                            </table>
                          </div>
                        </form>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              <?php } } ?>
            </div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
      </section>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/jquery.maskedinput.min.js"></script>
  <script src="js/mascaras.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    $("#dataah").mask("99/99/9999");

      function jscheckgerarmanual2(){
        if ( $('input[id=checkbotaogerarmanual]').is(':checked')) {
          $(".boxgerarmanual").fadeIn(500);
          
          //$(".boxsubstituicao").css("display","none");
          //$("#checksub").attr('checked', false);
        }else{
          $(".boxgerarmanual").css("display","none");
        } 
      }
      function jscheckgerarmanual($valor){ 
        if ( $('input[id=checkbotaogerarmanual'+$valor+']').is(':checked')) { 
          $(".boxgerarmanual"+$valor).fadeIn(500);
          
          //$(".boxsubstituicao"+$valor).css("display","none");
          //$("#checksub"+$valor).attr('checked', false);
        }else{
          $(".boxgerarmanual"+$valor).css("display","none");
        } 
      }

      function jschecksub(){
        if ( $('input[id=checksub]').is(':checked')) { 
          $(".boxsubstituicao").fadeIn(500);

          //$(".boxgerarmanual").css("display","none");
          //$("#checkbotaogerarmanual").attr('checked', false);
        }else{
          $(".boxsubstituicao").css("display","none");
        } 
      }

      function jschecksub2($valor){
        if ( $('input[id=checksub'+$valor+']').is(':checked')) { 
          $(".boxsubstituicao"+$valor).fadeIn(500);

          //$(".boxgerarmanual"+$valor).css("display","none");
          //$("#checkbotaogerarmanual"+$valor).attr('checked', false);
        }else{
          $(".boxsubstituicao"+$valor).css("display","none");
        } 
      }
      function ap30(){ 
        if ( $('input[name=checkocultdetalhes]').is(':checked')) {
            $("#camposocultosmaisdetana").fadeIn(500);
          }else{
            $("#camposocultosmaisdetana").css("display","none");
          } 
      }
      function aplogs($acionador,$objeto){ 
        if ( $('input[name='+$acionador+']').is(':checked')) {
          $("#"+$objeto).fadeIn(500);
        }else{
          $("#"+$objeto).css("display","none");
        } 
      }
      function apfotos(){ 
      if ( $('input[name=checkocultofotos]').is(':checked')) {
        $("#boxfotos").fadeIn(500);
      }else{
        $("#boxfotos").css("display","none");
      } 
    }
  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {


    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>