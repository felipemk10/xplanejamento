<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$funcoes = new funcoes;
$validacoes = new validacoes;
$func_log_analise = new func_log_analise;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));


$id_pro = (int)$_GET['id_pro'];
// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];
if ( $id_pro > 0 ) {

  //  Listando informações sobre o processo.
  $consulta = $configuracoes->consulta("SELECT 
    processos.id_pro,
    processos.endereco,
    processos.quadra,
    processos.lote,
    processos.numero,
    processos.bairro,
    processos.cidade,
    processos.estado,
    processos.tipoprocesso,
    processos.datahora, 
    processos.situacaoprojeto 

    FROM 

    processos.processos 

    WHERE processos.id_pro = $id_pro and processos.ativo = true");
    $linha2 = $consulta->fetch();
}

if ( $_POST['formulario'] == 'ok' ) {
  $situacaoprojeto  = $_POST['situacaoprojeto'];
  $requerente       = $_POST['requerente'];
  $obsgerais        = $_POST['obsgerais'];
  $obsgerais2       = 'Requerente: '.$requerente.' | Obs: '.$_POST['obsgerais'];

   $func_log_analise->manipulacoes(
      0,
      $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'),
      $id_pro,
      'ch',
      $situacaoprojeto,
      $obsgerais2,
      '',   
      '',   
      '',   
      '',   
      '',
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,
      'cadastro'
    );    

   //  Pega nome e e-mail do cliente e usuário.
    $tusuario = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos

      INNER JOIN geral.cg ON cg.id_cg = processos.id_cg
      WHERE processos.id_pro = $id_pro");
    $tusuario = $tusuario->fetch();
 
    //--Procedimento envio de e-mail (usando Mailjet)--

    if ( $tipoprocesso == 1 ) {
      $descr_tipoprocesso = 'Alvará de Construção';
    } else if ( $tipoprocesso == 2 ) {
      $descr_tipoprocesso = 'Alvará de Regularização de Obras';
    } else if ( $tipoprocesso == 3 ) {
      $descr_tipoprocesso = 'Alvará de Acréscimo de Área';
    } else if ( $tipoprocesso == 4 ) {
      $descr_tipoprocesso = 'Condomínio Edilício';
    } else if ( $tipoprocesso == 5 ) {
      $descr_tipoprocesso = 'Redimensionamento';
    }

    /*
      1 - A ser analisado = white, 
      2 - Em análise = #ddd, 
      3 - Pendência de documentos ou correção = #c4d79b, 
      4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
      5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
      6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
      7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
      8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
      9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
      10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
      11 - Aprovado = #eada6d, 
      12 - Dispensar Vistoria
    */

    if ( $situacaoprojeto == 13 ) {
      $descr_situacaoprojeto = 'Documentos retirados pelo requerente'.$requerente.', às '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 14 ) {
      $descr_situacaoprojeto = 'Documentos entregues pelo requerente'.$requerente.', às '.date('d/m/Y H:i');
    }

    require 'vendor/autoload.php';

    $resources = new \Mailjet\Resources();

    $api1 = 'f74843b63a568c5d7116cff5d971eb45';
    $api2 = '06fed94a47fa0688ef15fd36453ec365';

    $mj = new \Mailjet\Client($api1, $api2);
    $body = [
        'FromEmail' => "nao-responda@valleteclab.com",
        'FromName' => "Prefeitura de Luís Eduardo Magalhães - XPlanejamento",
        'Subject' => 'Checagem de Processo [ '.$descr_tipoprocesso.' ]',
        'Html-part' => '[ E-mail resposta, por favor não responda esse e-mail ] <br />
          Prezado(a) Senhor(a),<br />
          <br /><br />
          Protocolo: '.$id_pro.'<br /> 
          
          <strong>Situação do projeto:</strong><br />
          '.$descr_situacaoprojeto.'
          <br />
          <strong>Observações</strong><br />
          '.$obsgerais.'
          <br />
          <br />
          www.luiseduardomagalhaes.ba.gov.br/planejamento <br />
          <br /><br />
          Agradecemos o contato!',
        'Recipients' => [
            [
                'Email' => "".$tusuario['email'].""
            ]
        ]
    ];
    $response = $mj->post($resources::$Email, ['body' => $body]);
    $response->success() && var_dump($response->getData());

    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Procedimento efetuado com sucesso!');
              window.open('listar_processos.php?tipoprocesso=<?php echo $tipoprocesso; ?>','_self');
          }, 1000);
        });
    </script>
    <?php
}

?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia" />
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a>Retirada/Entrada</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">
      
                <form method="post" id="admin-form">
                <input type="hidden" name="formulario" value="ok">
                  <div class="panel-body bg-light">


            <div class="section-divider col-md-12"><span>Registro de retirada e entrada de documentos:</span></div>
            
                    
                    <div class="section row">
                      <div class="col-md-12">
                        <strong>Situação do projeto:</strong>
                      </div>
                    </div>
                    <div class="option-group field"> 
                      <div class="section row">
                          <div class="col-md-3"> 
                              <label class="option option-primary">
                              <input name="situacaoprojeto" type="radio" value="13">
                              <span class="radio"></span>Retirada</label>
                          </div>
                      </div>
                      <div class="section row">
                          <div class="col-md-5">                       
                              <label class="option option-primary">
                              <input name="situacaoprojeto" type="radio" value="14">
                              <span class="radio"></span>Entrada</label>
                          </div>
                      </div>
                    </div>
                    <br />
                    <div class="section row">
                      <div class="col-md-8">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Requerente</label> 
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <label for="requerente" class="field"><input name="requerente" id="requerente" class="gui-input" type="text"></label>
                          </div>
                        </div>   
                      </div> 
                   </div>
                    <br />
                    <div class="section" id="spy3">
                      <label for="comment" class="field prepend-icon">
                        <textarea class="gui-textarea" id="obsgerais" name="obsgerais"></textarea>
                        <label for="comment" class="field-icon">
                          <i class="fa fa-comments"></i>
                        </label>
                      </label>
                    </div>

                    <div class="panel-footer text-left">
                      <button type="submit" class="button btn-success">Registrar</button>
                    </div>
                    </form>
            </div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <script type="text/javascript">
    function ap30(){ 
        if ( $('input[name=checkocultdetalhes]').is(':checked')) {
            $("#camposocultosmaisdetana").fadeIn(500);
          }else{
            $("#camposocultosmaisdetana").css("display","none");
          } 
      }
  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        requerente: {
          required: true
        },
        obsgerais: {
          required: true
        },
        situacaoprojeto: {
          required: true
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        requerente: {
          required: 'Digite o Requerente (Mínino de caracteres 5)'
        },
        obsgerais: {
          required: 'Coloque uma breve observação'
        },
        situacaoprojeto: {
          required: 'Defina a situação do projeto'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>