<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require '../vendor/autoload.php';
session_start();

$autenticacao       = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios           = new \Biblioteca\usuarios;

$manipuladores      = new Biblioteca\manipuladores;

$processos      = new \Biblioteca\processos;

$func_usuario           = new \Biblioteca\func_usuario;
$func_alvara            = new \Biblioteca\func_alvara;
$func_redimensionamento = new \Biblioteca\func_redimensionamento;
$func_profissional      = new \Biblioteca\func_profissional;
$func_proprietario      = new \Biblioteca\func_proprietario;
$func_processo          = new \Biblioteca\func_processo;
$func_pavimentacao      = new \Biblioteca\func_pavimentacao;

$processos->setTipoProcesso($_GET['tipoprocesso']);
$processos->setSituacaoProjeto((int)$_GET['situacaoprojeto']);

$tipoprocesso     = $processos->getTipoProcesso();
$situacaoprojeto  = $processos->getSituacaoProjeto();

if ( $_POST['form'] == 'cadastro' ) {

  $endereco = $manipuladores->anti_injection($_POST["endereco"]);
  $bairro   = $manipuladores->anti_injection($_POST["bairro"]);
  $quadra   = $manipuladores->anti_injection($_POST["quadra"]);
  $lote     = $manipuladores->anti_injection($_POST["lote"]);
  $numero   = (int)$_POST["numero"];
  $estado   = $manipuladores->anti_injection($_POST["estado"]);
  $cidade   = (int)$_POST["cidade"];
  
  $nome_proprietario    = $manipuladores->anti_injection($_POST["nome_proprietario"]);
  $email_proprietario   = $manipuladores->anti_injection($_POST["email_proprietario"]);
  $cpfcnpj_proprietario = $manipuladores->retira_simbolos($_POST["cpfcnpj_proprietario"]);
  
  $proprietarios          = $_POST['nomeProprietarioAdd'];
  $email_proprietarios    = $_POST["emailProprietarioAdd"];
  $cpfcnpj_proprietarios  = $_POST["cpfcnpjProprietarioAdd"];

  $autoria                = $manipuladores->anti_injection($_POST["autoria"]);
  $autoria_creacau        = $manipuladores->anti_injection($_POST["autoria_creacau"]);
  $autoria_profissional   = $manipuladores->anti_injection($_POST["autoria_profissional"]);
  
  $resptecnico              = $manipuladores->anti_injection($_POST["resptecnico"]);
  $resptecnico_creacau      = $manipuladores->anti_injection($_POST["resptecnico_creacau"]);
  $resptecnico_profissional = $manipuladores->anti_injection($_POST["resptecnico_profissional"]);
  
  $areatotalterreno   = $manipuladores->numerodouble('ajuste',$_POST["areatotalterreno"]);
  $sa_qtdlotes        = (int)$_POST["sa_qtdlotes"];
  $sp_qtdlotes        = (int)$_POST["sp_qtdlotes"];
  $tipo               = (int)$_POST["tipo"];
  $descricaolotes     = $manipuladores->anti_injection($_POST["descricaolotes"]);
  $requerente         = $manipuladores->anti_injection($_POST["requerente"]);
  $requerentetelefone = str_replace( ' ', '', $manipuladores->retira_simbolos($_POST["requerentetelefone"]));
  
  if ( empty($nome_proprietario) ) {
    echo 'Digite o nome do Proprietário';
  } else if ( empty($email_proprietario) ) {
    echo 'Digite o e-mail do Proprietário';
  } else if ( empty($cpfcnpj_proprietario) ) {
    echo 'Digite o CPF/CNPJ do Proprietário';
  } else {
    
    if ( isset($_POST['addproprietario']) ) {
      foreach( $proprietarios as $key => $n ) {
        if ( empty($proprietarios[$key]) ) {
          $result_temp = false;
          $msg_temp = 'Digite o nome do Proprietário';
        } else if ( empty($email_proprietarios[$key]) ) {
          $result_temp = false;
          $msg_temp = 'Digite o e-mail do Proprietário';
        } else if ( empty($cpfcnpj_proprietarios[$key]) ) {
          $result_temp = false;
          $msg_temp ='Digite o CPF/CNPJ do Proprietário';
        } else {
          $result_temp = true;
        }
      }
    } else {
      $result_temp = true;
    }
    if ( $result_temp == false ) {
      echo $msg_temp;
    } else {
      if ( empty($autoria) ) {
        echo 'Digite o nome do autor';
      } else if ( empty($autoria_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( $autoria_profissional != 'e' and $autoria_profissional != 'a' ) {
        echo 'Informe se o autor é Engenheiro ou Arquiteto';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico) ) {
        echo 'Digite o nome do autor';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( isset($_POST['resp_open']) and $resptecnico_profissional != 'e' and $resptecnico_profissional != 'a' ) {
        echo 'Informe se o Responsável Técnico é Engenheiro ou Arquiteto';
      } else if ( empty($endereco) ) {
        echo 'Preencha o endereço';
      } else if ( $numero == 0 and empty($quadra) and empty($lote) ) {
        echo 'Preencha quadra/lote ou o número do local';
      } else if ( $numero == 0 and empty($quadra) ) {
        echo 'Preencha a quadra';
      } else if ( $numero == 0 and empty($lote) ) {
        echo 'Preencha a lote';
      } else if ( empty($bairro) ) {
        echo 'Preencha o bairro';
      } else if ( empty($estado) ) {
        echo 'Escolha o estado';
      } else if ( empty($cidade) ) {
        echo 'Escolha a cidade';
      } else if ( empty($areatotalterreno) ) {
        echo 'Digite a área Total do Terreno';
      } else if ( empty($sa_qtdlotes) ) {
        echo 'Digite uma Quantidade de Lotes - Situação Atual';
      } else if ( empty($sp_qtdlotes) ) {
        echo 'Digite uma Quantidade de Lotes - Situação Pretendida';
      } else if ( empty($descricaolotes) ) {
        echo 'Digite a Descrição do(s) Lote(s)';
      } else if ( empty($requerente) ) {
        echo 'Digite o Requerente (Mínino de caracteres 5)';
      } else if ( empty($requerentetelefone) ) {
        echo 'Digite um telefone para o Requerente';
      } else {

        if ( $result_temp == true ) {
            //  Cadastra processo
            $id_pro = $func_processo->manipulacoes(0,$tipoprocesso,1,$endereco,$bairro,$quadra,$lote,$numero,$cidade,$estado,'cadastro');
            //  Cadastro proprietário  
            $id_proprietario = $func_usuario->manipulacoes(0, $nome_proprietario, $email_proprietario, '', '', 2919553, 'ba', '', $cpfcnpj_proprietario, '', 2, 'cadastro');
            $func_proprietario->manipulacoes($id_pro, $id_proprietario, 'cadastro');
            unset($id_proprietario);

            //  Cadastrando proprietários
            if ( isset($_POST['addproprietario']) ) {
              foreach( $proprietarios as $key => $n ) {
                  $id_proprietario = $func_usuario->manipulacoes(0, $proprietarios[$key], $email_proprietarios[$key], '', '', 2919553, 'ba', '', $cpfcnpj_proprietarios[$key], '', 2, 'cadastro'); 
                  $func_proprietario->manipulacoes($id_pro, $id_proprietario, 'cadastro');                  
              }
            }

            // Cadastrando autor
            $id_autoria = $func_usuario->manipulacoes(0, $autoria, '', '', '', 2919553, 'ba', '', '', $autoria_creacau, 2, 'cadastro');
            $func_profissional->manipulacoes($id_pro, $id_autoria, $autoria_profissional, 'a', 'cadastro');
            

            //  Cadastrando Resp. Técnico
            $id_resptecnico = $func_usuario->manipulacoes(0, $resptecnico, '', '', '', 2919553, 'ba', '', '', $resptecnico_creacau, 2, 'cadastro');
            $func_profissional->manipulacoes($id_pro, $id_resptecnico, $resptecnico_profissional,'r', 'cadastro'); 
            
            // Cadastrando Redimensionamento
            $func_redimensionamento->manipulacoes(0, $id_pro, $areatotalterreno, $sa_qtdlotes, $sp_qtdlotes, $descricaolotes, $requerente, $requerentetelefone, $tipo, 'cadastro');

            ?>
            <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
            <script type="text/javascript">
              $(function () {
                /* Irving: Adicionado caixa com texto informativo ao cadastrar alvará */
                if($('#formResultAdd').text()!="") $('#formResult').text("Atenção: Melhoria no sistema permite que o cliente possa alterar seus dados de solicitações cadastradas até a checagem de documentos.");
                $('#formResultX').hide();//Irving
                /* ------------------------------------------------------------------ */
                setTimeout(function(){
                    alert('Procedimento efetuado com sucesso!');
                    $('#formResultX').show();//Irving
                    $('#formResultAdd').hide();
                    window.open('listar_processos.php?tipoprocesso=5','_self');
                  }, 1000);
                });
            </script>
            <?php            
        }
      }
    }
  }
}