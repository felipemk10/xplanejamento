<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require '../vendor/autoload.php';
session_start();

use Biblioteca\db;

$autenticacao       = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios           = new \Biblioteca\usuarios;

$manipuladores      = new Biblioteca\manipuladores;

$processos          = new \Biblioteca\processos;

$func_usuario       = new \Biblioteca\func_usuario;
$func_loteamento    = new \Biblioteca\func_loteamento;
$func_profissional  = new \Biblioteca\func_profissional;
$func_proprietario  = new \Biblioteca\func_proprietario;
$func_processo      = new \Biblioteca\func_processo;
//$func_pavimentacao  = new \Biblioteca\func_pavimentacao;

$processos->setTipoProcesso($_GET['tipoprocesso']);
//$processos->setSituacaoProjeto((int)$_GET['situacaoprojeto']);

$tipoprocesso     = $processos->getTipoProcesso();
$situacaoprojeto  = $processos->getSituacaoProjeto();


if ( $_POST['form'] == 'alteracao' ) {
  $id_pro         = (int)$_POST["id_pro"];
  $id_lot         = (int)$_POST["id_lot"];
  $id_autoria     = (int)$_POST["id_autoria"];
  $id_resptecnico = (int)$_POST["id_resptecnico"];

  $endereco = $manipuladores->anti_injection($_POST["endereco"]);
  if($_POST["campobairro"]==true) $bairro = $manipuladores->anti_injection($_POST["selectbairro"]); else $bairro = $manipuladores->anti_injection($_POST["inputbairro"]);
  $quadra   = $manipuladores->anti_injection($_POST["quadra"]);
  $lote     = $manipuladores->anti_injection($_POST["lote"]);
  $numero   = (int)$_POST["numero"];
  $estado   = $manipuladores->anti_injection($_POST["estado"]);
  $cidade   = (int)$_POST["cidade"];
  
  $id_proprietario      = $_POST['IDProprietario'];
  $nome_proprietario    = $manipuladores->anti_injection($_POST["nomeProprietario"]);
  $email_proprietario   = $manipuladores->anti_injection($_POST["emailProprietario"]);
  $cpfcnpj_proprietario = $manipuladores->retira_simbolos($_POST["cpfcnpjProprietario"]);
  
  if(isset($_POST['IDProprietarioAdd'])) $id_proprietarios       = $_POST['IDProprietarioAdd'];
  if(isset($_POST['nomeProprietarioAdd'])) $proprietarios        = $_POST['nomeProprietarioAdd'];
  if(isset($_POST['emailProprietarioAdd'])) $email_proprietarios = $_POST["emailProprietarioAdd"];
  if(isset($_POST['cpfcnpjProprietarioAdd'])) $cpfcnpj_proprietarios = $_POST["cpfcnpjProprietarioAdd"];

  $autoria              = $manipuladores->anti_injection($_POST["autoria"]);
  $autoria_creacau      = $manipuladores->anti_injection($_POST["autoria_creacau"]);
  $autoria_profissional = $manipuladores->anti_injection($_POST["autoria_profissional"]);
  
  $resptecnico              = $manipuladores->anti_injection($_POST["resptecnico"]);
  $resptecnico_creacau      = $manipuladores->anti_injection($_POST["resptecnico_creacau"]);
  if(isset($_POST["resptecnico_profissional"])) $resptecnico_profissional = $manipuladores->anti_injection($_POST["resptecnico_profissional"]);
    
  $loteamentoproposto = $manipuladores->anti_injection($_POST["loteamentoproposto"]);
  $matricula          = $manipuladores->anti_injection($_POST["matricula"]);
  $areatotal          = (double)$manipuladores->numerodouble('ajuste',$_POST["areatotal"]);
  $areaviaspublicas   = (double)$manipuladores->numerodouble('ajuste',$_POST['areaviaspublicas']);
  $areaverde          = (double)$manipuladores->numerodouble('ajuste',$_POST['areaverde']);
  $areaequipamentopublico = (double)$manipuladores->numerodouble('ajuste',$_POST['areaequipamentopublico']);
  $arealotes          = (double)$manipuladores->numerodouble('ajuste',$_POST['arealotes']);
  $quantidade_lotes   = (int)$_POST['quantidade_lotes'];
  if(isset($_POST["lot_ativo"])) $lot_ativo = 't'; else $lot_ativo = 'f';
  

  if ( empty($nome_proprietario) ) {
    echo 'Digite o nome do Proprietário';
  } else if ( empty($email_proprietario) ) {
    echo 'Digite o e-mail do Proprietário';
  } else if ( empty($cpfcnpj_proprietario) ) {
    echo 'Digite o CPF/CNPJ do Proprietário';
  } else {
    if ( isset($_POST['addproprietario']) ) { 
      foreach( $proprietarios as $key => $n ) {
        if (empty($proprietarios[$key]) and (!empty($email_proprietarios[$key]) or !empty($cpfcnpj_proprietarios[$key]))) {//Irving
          $result_temp = false;
          $msg_temp = 'Digite o nome de um dos Proprietários';
          break;
        } else if (empty($email_proprietarios[$key]) and (!empty($proprietarios[$key]) or !empty($cpfcnpj_proprietarios[$key]))) {//Irving
          $result_temp = false;
          $msg_temp = 'Digite o e-mail de um dos Proprietários';
          break;
        } else if (empty($cpfcnpj_proprietarios[$key]) and (!empty($proprietarios[$key]) or !empty($email_proprietarios[$key]))) {//Irving
          $result_temp = false;
          $msg_temp ='Digite o CPF/CNPJ de um dos Proprietários';
          break;
        } else {
          $result_temp = true;
        }
      }
    } else {
      $result_temp = true;
    }
    
    if ( $result_temp == false ) {
      echo $msg_temp;
    } else {
      if ( empty($autoria) ) {
        echo 'Digite o nome do autor';
      } else if ( empty($autoria_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( $autoria_profissional != 'e' and $autoria_profissional != 'a' ) {
        echo 'Informe se o autor é Engenheiro ou Arquiteto';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico) ) {
        echo 'Digite o nome do Resp. Técnico';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico_creacau) ) {
        echo 'Digite o CREA/CAU do Resp. Técnico';
      } else if ( isset($_POST['resp_open']) and $resptecnico_profissional != 'e' and $resptecnico_profissional != 'a' ) {
        echo 'Informe se o Responsável Técnico é Engenheiro ou Arquiteto';
      } else if ( empty($endereco) ) {
        echo 'Preencha o endereço';
      } else if ( $numero == 0 and empty($quadra) and empty($lote) ) {
        echo 'Preencha quadra/lote ou o número do local';
      } else if ( $numero == 0 and empty($quadra) ) {
        echo 'Preencha a quadra';
      } else if ( $numero == 0 and empty($lote) ) {
        echo 'Preencha a lote';
      } else if ( empty($bairro) ) {
        echo 'Preencha o bairro';
      } else if ( empty($estado) ) {
        echo 'Escolha o estado';
      } else if ( empty($cidade) ) {
        echo 'Escolha a cidade';
      } else if ( empty($loteamentoproposto) ) {
        echo 'Preencha o nome do loteamento proposto';
      } else if ( empty($matricula) ) {
        echo 'Preencha a matrícula do loteamento proposto';
      } else if ( empty($areatotal) ) {
        echo 'Preencha a área total do loteamento';
      } else if ( empty($areaviaspublicas) ) {
        echo 'Preencha a área de vias públicas do loteamento';
      } else if ( empty($areaverde) ) {
        echo 'Preencha a área verde do loteamento';
      } else if ( empty($areaequipamentopublico) ) {
        echo 'Preencha a área de equipamentos públicos do loteamento';
      } else if ( empty($arealotes) ) {
        echo 'Preencha a área de lotes do loteamento';
      } else if ( empty($quantidade_lotes) ) {
        echo 'Preencha a quantidade de lotes do loteamento';
      } else {

        $result_temp = true;
        if ( $result_temp == true ) {
            
            //  Atualiza processo
            $func_processo->manipulacoes($id_pro,$tipoprocesso,1,$endereco,$bairro,$quadra,$lote,$numero,$cidade,$estado,'alteracao');
            //  Atualizando proprietário principal
            $func_usuario->manipulacoes($id_proprietario, $nome_proprietario, $email_proprietario, '', '', 2919553, 'ba', '', $cpfcnpj_proprietario, '', 2, 'alteracao');
            
            //  Cadastrando/Aualizando proprietários
            if ( isset($_POST['addproprietario']) ) {
              foreach( $proprietarios as $key => $n ) { 
                  if ( $id_proprietarios[$key] > 0 ) {
                    if(!empty($proprietarios[$key])){//Irving - não incluir proprietários sem nenhum campo preenchido
                      $func_usuario->manipulacoes($id_proprietarios[$key], $proprietarios[$key], $email_proprietarios[$key], '', '', 2919553, 'ba', '', $cpfcnpj_proprietarios[$key], '', 2, 'alteracao');
                    } else {// Exclui o proprietário secundário.
                      $sql = db::prepare("DELETE FROM processos.processos_proprietario WHERE id_pro = $id_pro and id_cg = $id_proprietarios[$key]");
                      $sql->execute();
                    } 
                  } else {
                    if(!empty($proprietarios[$key])){//Irving - não incluir proprietários sem nenhum campo preenchido
                      $id_proprietario_temp = $func_usuario->manipulacoes(0, $proprietarios[$key], $email_proprietarios[$key], '', '', 2919553, 'ba', '', $cpfcnpj_proprietarios[$key], '', 2, 'cadastro'); 
                      $func_proprietario->manipulacoes($id_pro, $id_proprietario_temp, 'cadastro');
                    }
                  }              
              }
            } else {
                // Exclui todos os proprietários secundários.
                $sql = db::prepare("DELETE FROM processos.processos_proprietario WHERE id_pro = $id_pro and id_cg != $id_proprietario");
      
                $sql->execute();
            }

            // Atulizando autor
            $func_usuario->manipulacoes($id_autoria, $autoria, '', '', '', 2919553, 'ba', '', '', $autoria_creacau, 2, 'alteracao');
            $func_profissional->manipulacoes($id_pro, $id_autoria, $autoria_profissional, 'a', 'alteracao');
            

            //Atualizando Resp. Técnico
            if ( isset($_POST['resp_open']) ) {
              if ( $id_resptecnico > 0 ) {
                  $func_usuario->manipulacoes($id_resptecnico, $resptecnico, '', '', '', 2919553, 'ba', '', '', $resptecnico_creacau, 2, 'alteracao');
                  $func_profissional->manipulacoes($id_pro, $id_resptecnico, $resptecnico_profissional,'r', 'alteracao');
                } else {
                  $id_resptecnico = $func_usuario->manipulacoes(0, $resptecnico, '', '', '', 2919553, 'ba', '', '', $resptecnico_creacau, 2, 'cadastro');
                  $func_profissional->manipulacoes($id_pro, $id_resptecnico, $resptecnico_profissional,'r', 'cadastro');
                }
            } else {
                // Exclui todos os proprietários secundários.
                $sql = db::prepare("DELETE FROM processos.processos_profissional WHERE id_pro = $id_pro and tipo = 'r'");
      
                $sql->execute();
            }
             
            //Atualizando Loteamento
            $consulta = db::prepare("SELECT nome FROM processos.processos_loteamento WHERE id_pro = ".$id_pro." LIMIT 1;");
            $consulta->execute();

            if ($anterior = $consulta->fetch()) 
            {
              // Irving - Alt json file
              $sFile = "../loteamentos.json";

              if (file_exists($sFile)) { 
                $json_str = file_get_contents($sFile);
              } else {
                echo 'Não achou o arquivo de loteamentos.';
                exit;
              }

              // faz o parsing da string, criando o array "loteamentos"
              $jsonObj = json_decode($json_str);

              foreach ($jsonObj->loteamentos as $key => $value) {
                if(strtoupper($anterior->nome) != strtoupper($loteamentoproposto) and strtoupper($value->nome) == strtoupper($loteamentoproposto)) 
                {
                  echo 'Não é permitida a duplicidade de nomes de loteamentos.';
                  exit;
                }
                else if (strtoupper($value->nome) == strtoupper($anterior->nome)) {
                  $jsonObj->loteamentos[$key]->nome = strtoupper($loteamentoproposto);
                  if($lot_ativo=='t') $jsonObj->loteamentos[$key]->ativo = 'S';
                  else $jsonObj->loteamentos[$key]->ativo = 'N';
                  //Irving - Ordenação dos loteamentos puxados de loteamentos.json
                  $lot_nome = array_column($jsonObj->loteamentos, 'nome');
                  array_multisort($lot_nome, SORT_ASC, $jsonObj->loteamentos);
                }
              }

              // encode array to json and save to file
              file_put_contents($sFile, json_encode($jsonObj));
              //atualiza loteamento no BD
              $func_loteamento->manipulacoes($id_lot, $id_pro, $manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode'), strtoupper($loteamentoproposto), $matricula, $areatotal, $areaviaspublicas, $areaverde, $areaequipamentopublico, $arealotes, $quantidade_lotes, $lot_ativo, 'alteracao');
            }

?>
            
            <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
            <script type="text/javascript">
              $(function () {
                $('#formResultX').hide();//Irving
                if($('#formResultAdd').text()!="") $('#formResultAdd').hide();//Irving
                setTimeout(function(){
                    alert('Procedimento efetuado com sucesso!');
                    $('#formResultX').show();//Irving
                    $('#formResultAdd').hide();
                    window.open('listar_processos.php?tipoprocesso=<?php echo $tipoprocesso; ?>','_self');
                  }, 1000);
                });
            </script>
            <?php
        }
      }
    }
  }
}