<?php  
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require '../vendor/autoload.php';
session_start();

$autenticacao       = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios           = new \Biblioteca\usuarios;

$manipuladores      = new Biblioteca\manipuladores;

$processos          = new \Biblioteca\processos;

$func_usuario       = new \Biblioteca\func_usuario;
$func_loteamento    = new \Biblioteca\func_loteamento;
$func_profissional  = new \Biblioteca\func_profissional;
$func_proprietario  = new \Biblioteca\func_proprietario; 
$func_processo      = new \Biblioteca\func_processo;
//$func_pavimentacao  = new \Biblioteca\func_pavimentacao;

$processos->setTipoProcesso($_GET['tipoprocesso']);
if(isset($_GET['situacaoprojeto'])) $processos->setSituacaoProjeto((int)$_GET['situacaoprojeto']);//Irving - if isset

$tipoprocesso     = $processos->getTipoProcesso();
$situacaoprojeto  = $processos->getSituacaoProjeto();

if ( $_POST['form'] == 'cadastro' ) {

  $endereco = $manipuladores->anti_injection($_POST["endereco"]);
  if($_POST["campobairro"]==true) $bairro = $manipuladores->anti_injection($_POST["selectbairro"]); else $bairro = $manipuladores->anti_injection($_POST["inputbairro"]); 
  $quadra   = $manipuladores->anti_injection($_POST["quadra"]);
  $lote     = $manipuladores->anti_injection($_POST["lote"]);
  $numero   = $manipuladores->anti_injection($_POST["numero"]);
  $estado   = $manipuladores->anti_injection($_POST["estado"]);
  $cidade   = (int)$_POST["cidade"];
  
  $nome_proprietario    = $manipuladores->anti_injection($_POST["nome_proprietario"]);
  $email_proprietario   = $manipuladores->anti_injection($_POST["email_proprietario"]);
  $cpfcnpj_proprietario = $manipuladores->retira_simbolos($_POST["cpfcnpj_proprietario"]);
  
  if(isset($_POST['addproprietario'])) $proprietarios          = $_POST['nomeProprietarioAdd'];//Irving - if isset
  if(isset($_POST['addproprietario'])) $email_proprietarios    = $_POST["emailProprietarioAdd"];//Irving - if isset
  if(isset($_POST['addproprietario'])) $cpfcnpj_proprietarios  = $_POST["cpfcnpjProprietarioAdd"];//Irving - if isset

  $autoria              = $manipuladores->anti_injection($_POST["autoria"]);
  $autoria_creacau      = $manipuladores->anti_injection($_POST["autoria_creacau"]);
  $autoria_profissional = $manipuladores->anti_injection($_POST["autoria_profissional"]);
  
  $resptecnico              = $manipuladores->anti_injection($_POST["resptecnico"]);
  $resptecnico_creacau      = $manipuladores->anti_injection($_POST["resptecnico_creacau"]);
  if(isset($_POST['resp_open'])) $resptecnico_profissional = $manipuladores->anti_injection($_POST["resptecnico_profissional"]);//Irving - if isset
    
  $areatotal            = (double)$manipuladores->numerodouble('ajuste',$_POST["areatotal"]);
  
  $loteamentoproposto     = $manipuladores->anti_injection($_POST["loteamentoproposto"]);
  $areaviaspublicas       = $manipuladores->numerodouble('ajuste',$_POST["areaviaspublicas"]);
  $areaverde              = $manipuladores->numerodouble('ajuste',$_POST["areaverde"]);
  $areaequipamentopublico = $manipuladores->numerodouble('ajuste',$_POST["areaequipamentopublico"]);
  $arealotes              = $manipuladores->numerodouble('ajuste',$_POST["arealotes"]);

  $quantidade_lotes       = intval($_POST["quantidade_lotes"]);
  $matricula              = $manipuladores->anti_injection($_POST["matricula"]);

  if ( empty($nome_proprietario) ) {
    echo 'Digite o nome do Proprietário';
  } else if ( empty($email_proprietario) ) {
    echo 'Digite o e-mail do Proprietário';
  } else if ( empty($cpfcnpj_proprietario) ) {
    echo 'Digite o CPF/CNPJ do Proprietário';
  } else {
    
    if ( isset($_POST['addproprietario']) ) {
      foreach( $proprietarios as $key => $n ) { 
        if (empty($proprietarios[$key]) and (!empty($email_proprietarios[$key]) or !empty($cpfcnpj_proprietarios[$key]))) {//Irving
          $result_temp = false;
          $msg_temp = ' Digite o nome de um dos Proprietários';
          break;
        } else if (empty($email_proprietarios[$key]) and (!empty($proprietarios[$key]) or !empty($cpfcnpj_proprietarios[$key]))) {//Irving
          $result_temp = false;
          $msg_temp = 'Digite o e-mail de um dos Proprietários';
          break;
        } else if (empty($cpfcnpj_proprietarios[$key]) and (!empty($proprietarios[$key]) or !empty($email_proprietarios[$key]))) {//Irving
          $result_temp = false;
          $msg_temp ='Digite o CPF/CNPJ de um dos Proprietários';
          break;
        } else {
          $result_temp = true;
        }
      }
    } else {
      $result_temp = true;
    }

    if ( $result_temp == false ) {
      echo $msg_temp;
    } else {
      if ( empty($autoria) ) {
        echo 'Digite o nome do autor';
      } else if ( empty($autoria_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( $autoria_profissional != 'e' and $autoria_profissional != 'a' ) {
        echo 'Informe se o autor é Engenheiro ou Arquiteto';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico) ) {
        echo 'Digite o nome do autor';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( isset($_POST['resp_open']) and $resptecnico_profissional != 'e' and $resptecnico_profissional != 'a' ) {
        echo 'Informe se o Responsável Técnico é Engenheiro ou Arquiteto';
      } else if ( empty($endereco) ) {
        echo 'Preencha o endereço';
      } else if ( empty($estado) ) {
        echo 'Escolha o estado';
      } else if ( empty($cidade) ) {
        echo 'Escolha a cidade';
      } else if ( empty($areatotal) ) {
        echo 'Preencha a área total do loteamento';
      } else if ( empty($areaviaspublicas) ) {
        echo 'Preencha a área de vias públicas do loteamento';
      } else if ( empty($areaverde) ) {
        echo 'Preencha a área verde do loteamento';
      } else if ( empty($areaequipamentopublico) ) {
        echo 'Preencha a área de equipamentos públicos do loteamento';
      } else if ( empty($arealotes) ) {
        echo 'Preencha a área de lotes do loteamento';
      } else if ( empty($loteamentoproposto) ) {
        echo 'Preencha o nome do loteamento proposto';
      } else if ( empty($matricula) ) {
        echo 'Preencha o número de matrícula do loteamento proposto';
      } else if ( empty($quantidade_lotes) ) {
        echo 'Preencha a quantidade de lotes do loteamento proposto';
      } else {

        // Irving - Alt json file
        $sFile = "../loteamentos.json";

        if (file_exists($sFile)) { 
          $json_str = file_get_contents($sFile);
        } else {
          echo 'Não achou o arquivo de loteamentos.';
          exit;
        }

        // faz o parsing da string, criando o array "loteamentos"
        $jsonObj = json_decode($json_str);

        foreach ($jsonObj->loteamentos as $key => $value) {//Verifica se há duplicidade
          if(strtoupper($value->nome) == strtoupper($loteamentoproposto)) 
          {
            echo 'Não é permitida a duplicidade de nomes de loteamentos.';
            exit;
          }
        }

        if ( $result_temp == true ) {
            
            //  Cadastra processo
            $id_pro = $func_processo->manipulacoes(0,$tipoprocesso,1,$endereco,$bairro,$quadra,$lote,$numero,$cidade,$estado,'cadastro');

            //  Cadastra proprietário  
            $id_proprietario = $func_usuario->manipulacoes(0,$nome_proprietario,$email_proprietario,'','',2919553,'ba','',$cpfcnpj_proprietario,'',2,'cadastro');
            $func_proprietario->manipulacoes($id_pro, $id_proprietario, 'cadastro');
            unset($id_proprietario);
           
            //  Cadastrando proprietários
            if ( isset($_POST['addproprietario']) ) {
              foreach( $proprietarios as $key => $n ) {
                if(!empty($proprietarios[$key])) {
                  $id_proprietario = $func_usuario->manipulacoes(0, $proprietarios[$key], $email_proprietarios[$key], '', '', 2919553, 'ba', '', $cpfcnpj_proprietarios[$key], '', 2, 'cadastro');
               
                  $func_proprietario->manipulacoes($id_pro, $id_proprietario, 'cadastro');
                }                  
              }
            }

            // Cadastrando autor
            $id_autoria = $func_usuario->manipulacoes(0, $autoria, '', '', '', 2919553, 'ba', '', '', $autoria_creacau, 2, 'cadastro');
            $func_profissional->manipulacoes($id_pro, $id_autoria, $autoria_profissional, 'a', 'cadastro');
            

            //  Cadastrando Resp. Técnico
            $id_resptecnico = $func_usuario->manipulacoes(0, $resptecnico, '', '', '', 2919553, 'ba', '', '', $resptecnico_creacau, 2, 'cadastro');
            if(isset($_GET['resptecnico_profissional'])) $func_profissional->manipulacoes($id_pro, $id_resptecnico, $resptecnico_profissional,'r', 'cadastro');//Irving - if isset    

            // Cadastrando Loteamento
            $id_lot = $func_loteamento->manipulacoes(0, $id_pro, $manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode'), strtoupper($loteamentoproposto), $matricula, $areatotal, $areaviaspublicas, $areaverde, $areaequipamentopublico, $arealotes, $quantidade_lotes, false, 'cadastro');

            // Irving - Add to json file
            $sFile = "../loteamentos.json";

            if (file_exists($sFile)) { 
              $json_str = file_get_contents($sFile);
            } else {
              echo 'Não achou o arquivo de loteamentos.';
              exit;
            }

            // faz o parsing da string, criando o array "loteamentos"
            $jsonObj = json_decode($json_str);
            $jsonObj->loteamentos[] = array('nome'=>strtoupper($loteamentoproposto), 'ativo'=>'N');
            //Irving - Ordenação dos loteamentos puxados de loteamentos.json
            $lot_nome = array_column($jsonObj->loteamentos, 'nome');
            array_multisort($lot_nome, SORT_ASC, $jsonObj->loteamentos);

            file_put_contents('../loteamentos.json', json_encode($jsonObj));
            
?>
            <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
            <script type="text/javascript">
              $(function () {
                /* Irving: Adicionado caixa com texto informativo ao cadastrar alvará */
                if($('#formResultAdd').text()!="") $('#formResult').text("Atenção: Melhoria no sistema permite que o cliente possa alterar seus dados de solicitações cadastradas até a checagem de documentos.");
                $('#formResultX').hide();//Irving
                /* ------------------------------------------------------------------ */
                setTimeout(function(){
                    alert('Procedimento efetuado com sucesso!');
                    $('#formResultX').show();//Irving
                    $('#formResultAdd').hide();
                    window.open('listar_processos.php?tipoprocesso=<?php echo $tipoprocesso; ?>','_self');
                  }, 1000);
                });
            </script>
<?php 
        }
      }
    }
  }
}
?>