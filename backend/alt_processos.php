<?php

if ( (int)$_POST['id_pro'] > 0 ) {
	//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
	require '../vendor/autoload.php';
	session_start();

	//use Biblioteca\db;

	$autenticacao       = new \Biblioteca\autenticacao;
	$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
	$usuarios           = new \Biblioteca\usuarios;

	$manipuladores      = new Biblioteca\manipuladores;

	$processos          = new \Biblioteca\processos;


	$processos->altTipoProcesso((int)$_POST['id_pro'],(int)$_POST['tipoprocesso']);

	?>
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript">
	  $(function () {
	  	$('#formResultX').hide();//Irving
        if($('#formResultAdd').text()!="") $('#formResultAdd').hide();//Irving
	    setTimeout(function(){
	    	$('#formResultX').show();//Irving
            $('#formResultAdd').hide();
	        alert('Procedimento efetuado com sucesso!');
	        window.open('listar_processos.php?tipoprocesso=<?php echo (int)$_POST['tipoprocesso']; ?>','_self');
	      }, 1000);
	    });
	</script>
<?php  
}