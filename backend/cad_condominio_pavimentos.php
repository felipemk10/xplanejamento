<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require '../vendor/autoload.php';
session_start();

$autenticacao       = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios           = new \Biblioteca\usuarios;

$manipuladores      = new Biblioteca\manipuladores;

$processos          = new \Biblioteca\processos;

$func_usuario       = new \Biblioteca\func_usuario;
$func_alvara        = new \Biblioteca\func_alvara;
$func_profissional  = new \Biblioteca\func_profissional;
$func_proprietario  = new \Biblioteca\func_proprietario;
$func_processo      = new \Biblioteca\func_processo;
$func_pavimentacao  = new \Biblioteca\func_pavimentacao;
$func_condominio    = new \Biblioteca\func_condominio;
$func_que           = new \Biblioteca\func_que;
$func_que_pavimentos= new \Biblioteca\func_que_pavimentos;

$processos->setTipoProcesso($_GET['tipoprocesso']);
$processos->setSituacaoProjeto((int)$_GET['situacaoprojeto']);

$tipoprocesso     = $processos->getTipoProcesso();
$situacaoprojeto  = $processos->getSituacaoProjeto();

if ( $_POST['form'] == 'cadastro' ) {

  $endereco   = $manipuladores->anti_injection($_POST["endereco"]);
  $bairro     = $manipuladores->anti_injection($_POST["bairro"]);
  $quadra     = $manipuladores->anti_injection($_POST["quadra"]);
  $lote       = $manipuladores->anti_injection($_POST["lote"]);
  $numero     = (int)$_POST["numero"];
  $estado     = $manipuladores->anti_injection($_POST["estado"]);
  $cidade     = (int)$_POST["cidade"];
  
  $nome_proprietario    = $manipuladores->anti_injection($_POST["nome_proprietario"]);
  $email_proprietario   = $manipuladores->anti_injection($_POST["email_proprietario"]);
  $cpfcnpj_proprietario = $manipuladores->retira_simbolos($_POST["cpfcnpj_proprietario"]);
  
  $proprietarios          = $_POST['nomeProprietarioAdd'];
  $email_proprietarios    = $_POST["emailProprietarioAdd"];
  $cpfcnpj_proprietarios  = $_POST["cpfcnpjProprietarioAdd"];

  $autoria              = $manipuladores->anti_injection($_POST["autoria"]);
  $autoria_creacau      = $manipuladores->anti_injection($_POST["autoria_creacau"]);
  $autoria_profissional = $manipuladores->anti_injection($_POST["autoria_profissional"]);
  
  $resptecnico              = $manipuladores->anti_injection($_POST["resptecnico"]);
  $resptecnico_creacau      = $manipuladores->anti_injection($_POST["resptecnico_creacau"]);
  $resptecnico_profissional = $manipuladores->anti_injection($_POST["resptecnico_profissional"]);
    
  $finalidadeobra   = $manipuladores->anti_injection($_POST["finalidadeobra"]);
  $areaterreno      = $manipuladores->numerodouble('ajuste',$_POST["areaterreno"]);
  $situacaoterreno  = $manipuladores->anti_injection($_POST["situacaoterreno"]);

  if ( $_POST["desmembramento"] != true ) {
    $desmembramento           = f;
    $desmembramento_entregue  = f;
  } else {
    $desmembramento           = true;
    $desmembramento_entregue  = $manipuladores->anti_injection($_POST["desmembramento_entregue"]);
  }
  
  $area_construir           = $manipuladores->anti_injection($_POST["area_construir"]);
  $area_sub_construir       = $manipuladores->anti_injection($_POST["area_sub_construir"]);
  $area_existente           = $manipuladores->anti_injection($_POST["area_existente"]);
  $area_sub_solo_existente  = $manipuladores->anti_injection($_POST["area_sub_solo_existente"]);  

  $area_pavimentacao_impermeavel  = $manipuladores->anti_injection($_POST["area_pavimentacao_impermeavel"]);
  $tipo_pav                       = $manipuladores->anti_injection($_POST["tipo_pav"]);
  
  if ( empty($nome_proprietario) ) {
    echo 'Digite o nome do Proprietário';
  } else if ( empty($email_proprietario) ) {
    echo 'Digite o e-mail do Proprietário';
  } else if ( empty($cpfcnpj_proprietario) ) {
    echo 'Digite o CPF/CNPJ do Proprietário';
  } else {
    
    if ( isset($_POST['addproprietario']) ) {
      foreach( $proprietarios as $key => $n ) {
        if ( empty($proprietarios[$key]) ) {
          $result_temp = false;
          $msg_temp = 'Digite o nome do Proprietário';
        } else if ( empty($email_proprietarios[$key]) ) {
          $result_temp = false;
          $msg_temp = 'Digite o e-mail do Proprietário';
        } else if ( empty($cpfcnpj_proprietarios[$key]) ) {
          $result_temp = false;
          $msg_temp ='Digite o CPF/CNPJ do Proprietário';
        } else {
          $result_temp = true;
        }
      }
    } else {
      $result_temp = true;
    }
    if ( $result_temp == false ) {
      echo $msg_temp;
    } else {
      if ( empty($autoria) ) {
        echo 'Digite o nome do autor';
      } else if ( empty($autoria_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( $autoria_profissional != 'e' and $autoria_profissional != 'a' ) {
        echo 'Informe se o autor é Engenheiro ou Arquiteto';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico) ) {
        echo 'Digite o nome do autor';
      } else if ( isset($_POST['resp_open']) and empty($resptecnico_creacau) ) {
        echo 'Digite o CREA/CAU do autor';
      } else if ( isset($_POST['resp_open']) and $resptecnico_profissional != 'e' and $resptecnico_profissional != 'a' ) {
        echo 'Informe se o Responsável Técnico é Engenheiro ou Arquiteto';
      } else if ( empty($endereco) ) {
        echo 'Preencha o endereço';
      } else if ( $numero == 0 and empty($quadra) and empty($lote) ) {
        echo 'Preencha quadra/lote ou o número do local';
      } else if ( $numero == 0 and empty($quadra) ) {
        echo 'Preencha a quadra';
      } else if ( $numero == 0 and empty($lote) ) {
        echo 'Preencha a lote';
      } else if ( empty($bairro) ) {
        echo 'Preencha o bairro';
      } else if ( empty($estado) ) {
        echo 'Escolha o estado';
      } else if ( empty($cidade) ) {
        echo 'Escolha a cidade';
      } else if ( $finalidadeobra != 'rc' and $finalidadeobra != 'cm' ) {
        echo 'Finalidade de Obra inválida';
      } else if ( $situacaoterreno != 'm' and $situacaoterreno != 'e' ) {
        echo 'Situação do Terreno inválida';
      } else if ( isset($desmembramento) and empty($desmembramento_entregue) ) {
        echo 'Informe a opção de Entregue';
      } else if ( empty($areaterreno) ) {
        echo 'Preencha a área do terreno';
      } else {

        $result_temp = true;
       
        if ( $result_temp == true ) {
            //  Cadastra processo
            $id_pro = $func_processo->manipulacoes(0,$tipoprocesso,1,$endereco,$bairro,$quadra,$lote,$numero,$cidade,$estado,'cadastro');
            //  Cadastro proprietário  
            $id_proprietario = $func_usuario->manipulacoes(0, $nome_proprietario, $email_proprietario, '', '', 2919553, 'ba', '', $cpfcnpj_proprietario, '', 2, 'cadastro');
            $func_proprietario->manipulacoes($id_pro, $id_proprietario, 'cadastro');
            unset($id_proprietario);
            
            //  Cadastrando proprietários
            if ( isset($_POST['addproprietario']) ) {
              foreach( $proprietarios as $key => $n ) {
                  $id_proprietario = $func_usuario->manipulacoes(0, $proprietarios[$key], $email_proprietarios[$key], '', '', 2919553, 'ba', '', $cpfcnpj_proprietarios[$key], '', 2, 'cadastro'); 
                  $func_proprietario->manipulacoes($id_pro, $id_proprietario, 'cadastro');                  
              }
            }

            // Cadastrando autor
            $id_autoria = $func_usuario->manipulacoes(0, $autoria, '', '', '', 2919553, 'ba', '', '', $autoria_creacau, 2, 'cadastro');
            $func_profissional->manipulacoes($id_pro, $id_autoria, $autoria_profissional, 'a', 'cadastro');
            

            //  Cadastrando Resp. Técnico
            $id_resptecnico = $func_usuario->manipulacoes(0, $resptecnico, '', '', '', 2919553, 'ba', '', '', $resptecnico_creacau,2, 'cadastro');
            $func_profissional->manipulacoes($id_pro, $id_resptecnico, $resptecnico_profissional,'r', 'cadastro'); 

            // Cadastrando Condomínio
            $id_con = $func_condominio->manipulacoes(0, $id_pro, $manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode'), $finalidadeobra, $areaterreno, $situacaoterreno, $desmembramento, $desmembramento_entregue, 'cadastro');
            
            if ( count($_POST['num_casa']) > 0 ) {
              foreach( $_POST["num_casa"] as $key => $n ) {

                if ( $_POST['tipo_processo_unidade'][$key] != null ) { 
                  $id_que = $func_que->manipulacoes(0, $id_con, $manipuladores->numerodouble('ajuste',$_POST['area_construir'][$key]), $manipuladores->numerodouble('ajuste',$_POST['area_existente'][$key]), $manipuladores->numerodouble('ajuste',$_POST["area_uso_exclusivo"][$key]), $manipuladores->numerodouble('ajuste',$_POST["area_comum_proporcional"][$key]), $_POST['tipo_processo_unidade'][$key], 'cadastro');

                  if ( count($_POST['numero_pav_condominio'.($key+1)]) > 0 ) {
                    $idcasa = $key+1;
                    //echo $idcasa.'22';
                    foreach( $_POST["numero_pav_condominio".($key+1)] as $key => $n ) {
                      $func_que_pavimentos->manipulacoes(0,$id_que, $id_con, $manipuladores->numerodouble('ajuste',$_POST['area_construir'.($idcasa)][$key]), $manipuladores->numerodouble('ajuste',$_POST['area_existente'.($idcasa)][$key]), $manipuladores->numerodouble('ajuste',$_POST["area_uso_exclusivo".($idcasa)][$key]), $manipuladores->numerodouble('ajuste',$_POST["area_comum_proporcional".($idcasa)][$key]), 'cadastro');
                    }
                  }
                }
                          
              }
            }
            ?>
            <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
            <script type="text/javascript">
              $(function () {
                /* Irving: Adicionado caixa com texto informativo ao cadastrar alvará */
                if($('#formResultAdd').text()!="") $('#formResult').text("Atenção: Melhoria no sistema permite que o cliente possa alterar seus dados de solicitações cadastradas até a checagem de documentos.");
                $('#formResultX').hide();//Irving
                /* ------------------------------------------------------------------ */
                setTimeout(function(){
                    alert('Procedimento efetuado com sucesso!');
                    $('#formResultX').show();//Irving
                    $('#formResultAdd').hide();
                    window.open('listar_processos.php?tipoprocesso=4','_self');
                  }, 1000);
                });
            </script>
            <?php
        }
      }
    }
  }
}