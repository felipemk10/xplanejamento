<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require '../vendor/autoload.php';
session_start();

$autenticacao   	= new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios       	= new \Biblioteca\usuarios;

$manipuladores  	= new Biblioteca\manipuladores;

$processos      	= new \Biblioteca\processos;
$func_usuario   	= new \Biblioteca\func_usuario;
$func_usuario_sis   = new \Biblioteca\func_usuario_sis;

if ( $_POST['formulario'] == 'ok' ) {
  
  $nome = $manipuladores->anti_injection($_POST["nome"]);
  $email = $manipuladores->anti_injection($_POST["email"]);
  $endereco = $manipuladores->anti_injection($_POST["endereco"]);
  $bairro = $manipuladores->anti_injection($_POST["bairro"]);
  $cidade = (int)$_POST["cidade"];
  $estado = $manipuladores->anti_injection($_POST["estado"]);
  $telefone = str_replace( ' ', '', $manipuladores->retira_simbolos($_POST["telefone"]));
  $cpfcnpj = $manipuladores->retira_simbolos($_POST["cpfcnpj"]);

  $senha = $manipuladores->anti_injection($_POST["senha"]);  
  $tipousuario = $manipuladores->anti_injection($_POST["tipousuario"]);  
  $departamento = $manipuladores->anti_injection($_POST["departamento"]); 
  $cargofuncao = $manipuladores->anti_injection($_POST["cargofuncao"]);

  $func_usuario->manipulacoes(
    $manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode'), 
    $nome,   
    $email,  
    $endereco,   
    $bairro,   
    $cidade,
    $estado,   
    $telefone,   
    $cpfcnpj,  
    '',
     1,
    'alteracao');

  $func_usuario_sis->manipulacoes(
    $manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode'), 
    $senha,  
    $tipousuario,  
    $departamento,   
    $cargofuncao,
    '',
    'alteracao');

  	echo 'Ok!';
    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
        	$('#formResultAdd').hide();
            alert('Procedimento efetuado com sucesso!');
          }, 1000);
        });
    </script>
    <?php
}