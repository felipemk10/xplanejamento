function pg_modal(pg, largura) {
    $.ajax({
        type: "POST",
        url: pg,
        cache: true,
        beforeSend: function () {
            $('#carregamento').show();
            //$('#carregamento').modal({
              //backdrop: 'static',
              //keyboard: false,
            //});
        },
        success: function (a) {
            $('#carregamento').hide();
            //$('#carregamento').modal('hide');
            
            if ( largura > 0 ) {
                $('#myModal').css({'height':'100%', 'width':largura+'px', 'margin-top':'-70px', 'margin-left':'-'+(largura/2)+'px'});
            }
            $('#myModal').html(a);
            $('#myModal').modal({
              backdrop: 'static',
              keyboard: false,
            });
        }
    });
}
//Envio de formulário
function formularios_dinamicos(a, b, c, d, desabilitar, texto_modal) {
    /* 
        * a = Formulário
        * b = url a ser enviado os valores do formulário
        * c = Objeto de carregamento a ser exibido
        * d = Objeto para receber o resultado do formulario
    */
    var h = $(a).serialize();
    // Caso haja algum erro que impessa de enviar os dados do formulário, é exibido uma mensagem de erro
    var i = 'Ops... houve um erro!';
    $.ajax({
        type: "POST",
        url: b,
        data: h,
        mimeType: "multipart/form-data",
            
        beforeSend: function () {
            $(d).html(a).hide();
            if ( c != '' ) {
                $(c).show();
            } else {
                //$('#myModal .modal-header #myModalLabel').html(texto_modal);
                //$('#myModal').modal({
                //  backdrop: 'static',
                //  keyboard: false
                //});
            }
            if ( desabilitar = 's' ) {
                $("input").attr("disabled", "disabled");
                $("button").attr("disabled", "disabled");
            }
        },
        success: function (a) {
            if ( c != '' ) {
                $(c).hide();
            } else {
                //$('#myModal .modal-header #myModalLabel').html('');
                //$('#myModal').modal('hide');
            }
            if ( d != '' ) {
                $(d+'Add').show();
                $(d).html(a).show();
            }
            if ( desabilitar = 's' ) {
                $("input").removeAttr("disabled", "disabled");
                $("button").removeAttr("disabled", "disabled");
            }
        },
        error: function (a) {
            //$('#e_carregamento_conteudo').html(i).show();
        }
    })
}

    function apalterarsenha(){
            if ($('input[name=alterarsenha]').is(':checked')){
                $("#boxalterarsenha").fadeIn(500);
            }else{
                $("#boxalterarsenha").fadeOut(500);
            }   
    }

    function ap9(){
            if ($('input[name=desmembramento]').is(':checked')){
                            $(".enblobacampos2").fadeIn(500);
                    }else{
                            $(".enblobacampos2").fadeOut(500);
                    }	
    }

    function ap10(){
            if ($('input[id=pessoasim]').is(':checked')){
                $("#cpfcnpj_proprietario").mask("999.999.999-99");
            }	
    }

    function ap11(){
            if ($('input[id=pessoanao]').is(':checked')){
                $("#cpfcnpj_proprietario").mask("99.999.999/9999-99");
            }	
    }

    function ap100(){
            if ($('input[id=pessoasim]').is(':checked')){
                $("#cpfcnpjProprietario").mask("999.999.999-99");
            }   
    }

    function ap110(){
            if ($('input[id=pessoanao]').is(':checked')){
                $("#cpfcnpjProprietario").mask("99.999.999/9999-99");
            }   
    }

    function ap12(){
            if ($("#engenheiro_responsavel_tecnico").attr("checked")){
                            $("#enblobacampos15").css("display","none");
                            $("#enblobacampos14").fadeIn(500);
                    }else{
                            $("#enblobacampos15").css("display","none");
                    }	
    }

    function ap13(){
            if ($("#arquiteto_responsavel_tecnico").attr("checked")){
                            $("#enblobacampos14").css("display","none");
                            $("#enblobacampos15").fadeIn(500);
                    }else{
                            $("#enblobacampos14").css("display","none");
                    }	
    }

    function ap14(){
            if ($("#desmembramento").attr("checked")){
                            $(".enblobacampos2").fadeIn(500);
                    }else{
                            $(".enblobacampos2").fadeOut(500);
                    }	
    }

    // ===== FUNCAO PARA OCULTAR DESOCULTAR CAMPOS DE TIPOS DE PROFISSIONAL (AUTORIA)

    function ap17(){
            if ($("#engenheiro_autoria").attr("checked")){
                            $("#enblobacampos151").css("display","none");
                            $("#enblobacampos141").fadeIn(500);
                    }else{
                            $("#enblobacampos151").css("display","none");
                    }	
    }

    function ap18(){
            if ($("#arquiteto_autoria").attr("checked")){
                            $("#enblobacampos141").css("display","none");
                            $("#enblobacampos151").fadeIn(500);
                    }else{
                            $("#enblobacampos141").css("display","none");
                    }	
    }

    // FUNCAO PARA DESOCULTAR RESP TECNICO
    function ap19(){
            if ($('input[name=resp_open]').is(':checked')){
                            $("#camporesptec").fadeIn(500);
                    }else{
                            $("#camporesptec").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR CAMPO PARA ADICIONAR MAIS PROPRIETARIOS
    function ap20(){ 
            if ($('input[name=addproprietario]').is(':checked')){
                            $("#campomaisproprietario").fadeIn(500);
                    }else{
                            $("#campomaisproprietario").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR QUADRO E LOTE
    function ap200(){
            if ($("#qd_lt_open").attr("checked")){
                            $("#enblobacampos200").fadeIn(500);
                    }else{
                            $("#enblobacampos200").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR NUMERO DA RESIDENCIA
    function ap201(){
            if ($("#numero_open").attr("checked")){
                            $("#enblobacampos201").fadeIn(500);
                    }else{
                            $("#enblobacampos201").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR AREA EXISTENTE
    // ALTERACAO
    function ap2022(){
            if ($('input[name=area_existente_check]').is(':checked')){
                            $("#blocoareaexistente").fadeIn(500);
                    }else{
                            $("#blocoareaexistente").fadeOut(500);
                    }	
    }


    // FUNCAO PARA DESOCULTAR AREA SUB-SOLOEXISTENTE
    // ALTERACAO
    function ap2022subexistente(){
            if ($('input[name=area_subsoloexistente_check]').is(':checked')){
                            $("#blocoareasubexistente").fadeIn(500);
                    }else{
                            $("#blocoareasubexistente").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR AREA EXISTENTE
    // CADASTRO
    function ap202(){
            if ($('input[name=area_existente_check]').is(':checked')){
                            $("#englobaaddpavexistente").fadeIn(500);
                    }else{
                            $("#englobaaddpavexistente").fadeOut(500);
                    }	
    }


    // FUNCAO PARA DESOCULTAR AREA SUBSOLOEXISTENTE
    // CADASTRO
    function ap20222(){
            if ($('input[name=area_subsoloexistente_check]').is(':checked') ) {
                            $("#englobaaddpavsubsoloexistente").fadeIn(500);
                    }else{
                            $("#englobaaddpavsubsoloexistente").fadeOut(500);
                    }	
    }


      // FUNCAO PARA DESOCULTAR AREA SUBSOLOCONSTRUIR
    // CADASTRO
    function ap20222construir(){
            if ($("#area_subsoloconstruir_check").attr("checked")){
                            $("#blocoareasubconstruir").fadeIn(500);
                    }else{
                            $("#blocoareasubconstruir").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR AREA SUBSOLOCONSTRUIR
    // CADASTRO
    function ap20222construircad(){
            if ($('input[name=area_subsoloconstruir_check]').is(':checked')){
                            $("#englobaaddpavsubsoloconstruir").fadeIn(500);
                    }else{
                            $("#englobaaddpavsubsoloconstruir").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR BOX DE INCREMENTACAO DE PAVIMENTOS AREA EXISTENTE
    // ALTERACAO
    function ap203(){
            if ($("#area_existente_check_cad_pav").attr("checked")){
                            $("#cadaddmaispavareaexistente").fadeIn(500);
                    }else{
                            $("#cadaddmaispavareaexistente").fadeOut(500);
                    }	
    }


    // FUNCAO PARA DESOCULTAR BOX DE INCREMENTACAO DE PAVIMENTOS AREA SUB-SOLOEXISTENTE
    // ALTERACAO
    function ap203subexistente(){
            if ($("#area_subsoloexistente_check_cad_pav").attr("checked")){
                            $("#cadaddmaispavareasubsoloexistente").fadeIn(500);
                    }else{
                            $("#cadaddmaispavareasubsoloexistente").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR BOX DE INCREMENTACAO DE PAVIMENTOS AREA SUB-SOLOCONSTRUIR
    // ALTERACAO
    function ap203subconstruir(){
            if ($("#area_subsoloconstruir_check_cad_pav").attr("checked")){
                            $("#cadaddmaispavareasubsoloconstruir").fadeIn(500);
                    }else{
                            $("#cadaddmaispavareasubsoloconstruir").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR BOX DE INCREMENTACAO DE PAVIMENTOS AREA A CONTRUIR
    // ALTERACAO
    function ap204(){
            if ($("#area_construir_check_cad_pav").attr("checked")){
                            $("#cadaddmaispavaconstruir").fadeIn(500);
                    }else{
                            $("#cadaddmaispavaconstruir").fadeOut(500);
                    }	
    }

    // FUNCAO PARA DESOCULTAR BOX DE INCREMENTACAO DE UNIDADES
    // ALTERACAO
    function ap205(){
            if ($("#unidades_check_cad_pav").attr("checked")){
                            $("#cadaddmaispavunidades").fadeIn(500);
                    }else{
                            $("#cadaddmaispavunidades").fadeOut(500);
                    }	
    }

    // IRVING - FUNCAO PARA LISTAR OU NÃO BAIRROS NO CAMPO DE SELECT/INPUT
    function listabairros(mudanca){
        if($("#campobairro").is(":checked")) tipo = 'lista'; 
        else tipo = 'digita';
        
        if($("#cod_cidades").val()!=2919553) {
            if(tipo=='lista' && mudanca=='muda_checkbox') alert('Somente há listagem de bairros existentes para o município de Luís Eduardo Magalhães - BA');
            tipo = 'digita'; 
            $("#campobairro").prop("checked", false);
        }
        if(mudanca=='muda_cidade' && $("#cod_cidades").val()==2919553) {
            tipo = 'lista';
            $("#campobairro").prop("checked", true);
        }

        if(tipo=='lista' && $("#bairro").attr("name")=="inputbairro") {
            $("#bairro").attr("id", "inputbairro");
            $("#inputbairro").fadeOut(500);
            $("#selectbairro").fadeIn(500);
            $("#selectbairro").attr("id", "bairro");
        }
        if(tipo=='digita' && $("#bairro").attr("name")=="selectbairro") {
            $("#bairro").attr("id", "selectbairro");
            $("#selectbairro").fadeOut(500);
            $("#inputbairro").fadeIn(500);
            $("#inputbairro").attr("id", "bairro");
        }
    }

    //  ADICIONA UNIDADES QUANDO NAO CONTER NENHUMA UNIDADE PREVIAMENTE CADASTRADA
    fieldsUnidadesZero = 0;
    var valorUnidadesZero = 1;
            function addInputUnidadesZero(){
                    if (fieldsUnidadesZero != 80 || valorUnidadesZero <= 80){
                             document.getElementById('unidades_zero').innerHTML += "<div class=\"enblobacamposcasas\"><div class=\"titulotextocad9_cad\">Casa "+valorUnidadesZero+"</div>\n\
                                <div class=\"campos_cadunidades\">\n\
                                    <input type=\"text\" name=\"area_construir[]\" id=\"area_construir\" class=\"camposdadosunidades_cad\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"/>\n\
                                <div class=\"titulotextocad323\">m<sup>2</sup></div>\n\
                                </div>\n\
                                <div class=\"campos_cadunidades\">\n\
                                    <input type=\"text\" name=\"area_existente[]\" id=\"area_existente\" class=\"camposdadosunidades_cad\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"/>\n\
                                <div class=\"titulotextocad323\">m<sup>2</sup></div>\n\
                                </div>\n\
                                <div class=\"campos_cadunidades\">\n\
                                    <input type=\"text\" name=\"area_uso_exclusivo[]\" id=\"area_uso_exclusivo\" class=\"camposdadosunidades_cad\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"/>\n\
                                    <div class=\"titulotextocad323\">m<sup>2</sup></div>\n\
                                </div>\n\
                                \n\
                                <div class=\"campos_cadunidades\">\n\
                                    <input type=\"text\" name=\"area_comum_proporcional[]\" id=\"area_comum_proporcional\" class=\"camposdadosunidades_cad\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"/>\n\
                                    <div class=\"titulotextocad323\">m<sup>2</sup></div>\n\
                                </div>\n\
                                <div class=\"campos_cadunidades\">\n\
                                      <select name='tipo_processo[]' id='tipo_processo' class=\"campos2222_select\">\n\
                                            <option value='0'>--Selecione--</option>\n\
                                            <option value='1'>Constru&ccedil;&atilde;o</option>\n\
                                            <option value='2'>Regulariza&ccedil;&atilde;o</option>\n\
                                            <option value='3'>Acr&eacute;scimo de &Aacute;rea</option>\n\
                                      </select>\n\
                                </div>\n\
                            </div>";

                            document.getElementById('unidades_zero').innerHTML += "<input type=\"hidden\" value=\""+valorUnidadesZero+"\" name=\"num_casa[]\" />";
                            valorUnidadesZero ++
                            fieldsUnidadesZero += 1;


    }else{
            document.getElementById('unidades_zero').innerHTML += "<br />Apenas 150 unidades são permitidas!";
            document.getElementById('unidades_zero').foto.disabled=true;
            }
    }

    // FIM

    // ADICIONADA PAVIMENTOS DE AREA EXISTENTE QUANDO NAO CONTER NENHUMA AREA EXISTENTE CADASTRADA
    fieldsAreaExistenteZero = 0;
    var valorAreaExistenteZero = 2;
    function addInputAreaExistenteZero(){
            if (fieldsAreaExistenteZero != 80 || valorAreaExistenteZero <= 80){
                    document.getElementById('area_existente_zero').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valorAreaExistenteZero+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_area_existente_cad[]\" id=\"pavimentos_area_existente_cad\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                    document.getElementById('area_existente_zero').innerHTML += "<input type=\"hidden\" value=\""+valorAreaExistenteZero+"\" name=\"numero_pavimento_area_existente_cad[]\" />";
                    valorAreaExistenteZero ++
                    fieldsAreaExistenteZero += 1;
    }else{
    document.getElementById('area_existente_zero').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
    document.getElementById('area_existente_zero').foto.disabled=true;
    }
    }
    // FIM




    //........................... ADICIONADA PAVIMENTOS DE AREA EXISTENTE QUANDO NAO CONTER NENHUMA AREA SUBSOLOEXISTENTE CADASTRADA.......................
    fieldsAreaSubsoloExistenteZero = 0;
    var valorAreaSubsoloExistenteZero = 2;
    function addInputAreaSubsoloExistenteZero(){
            if (fieldsAreaSubsoloExistenteZero != 80 || valorAreaSubsoloExistenteZero <= 80){
                    document.getElementById('area_subsoloexistente_zero').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valorAreaSubsoloExistenteZero+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_area_subsoloexistente_cad[]\" id=\"pavimentos_area_subsoloexistente_cad\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                    document.getElementById('area_subsoloexistente_zero').innerHTML += "<input type=\"hidden\" value=\""+valorAreaSubsoloExistenteZero+"\" name=\"numero_pavimento_area_subsoloexistente_cad[]\" />";
                    valorAreaSubsoloExistenteZero ++
                    fieldsAreaSubsoloExistenteZero += 1;
    }else{
    document.getElementById('area_subsoloexistente_zero').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
    document.getElementById('area_subsoloexistente_zero').foto.disabled=true;
    }
    }
    // ...................................................................FIM.............................................................

    //........................... ADICIONADA PAVIMENTOS DE AREA CONSTRUIR QUANDO NAO CONTER NENHUMA AREA SUBSOLOCONSTRUIR CADASTRADA.......................
    fieldsAreaSubsoloConstruirZero = 0;
    var valorAreaSubsoloConstruirZero = 2;
    function addInputAreaSubsoloConstruirZero(){
            if (fieldsAreaSubsoloConstruirZero != 80 || valorAreaSubsoloConstruirZero <= 80){
                    document.getElementById('area_subsoloconstruir_zero').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valorAreaSubsoloConstruirZero+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_area_subsoloconstruir_cad[]\" id=\"pavimentos_area_subsoloconstruir_cad\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                    document.getElementById('area_subsoloconstruir_zero').innerHTML += "<input type=\"hidden\" value=\""+valorAreaSubsoloConstruirZero+"\" name=\"numero_pavimento_area_subsoloconstruir_cad[]\" />";
                    valorAreaSubsoloConstruirZero ++
                    fieldsAreaSubsoloConstruirZero += 1;
    }else{
    document.getElementById('area_subsoloconstruir_zero').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
    document.getElementById('area_subsoloconstruir_zero').foto.disabled=true;
    }
    }
    // ...................................................................FIM.............................................................



    // .......................................ADICIONADA PAVIMENTOS DE AREA A CONSTRUIR QUANDO NAO CONTER NENHUMA AREA A CONSTRUIR CADASTRADA..............................
    fieldsAreaConstruirZero = 0;
    var valorAreaConstruirZero = 2;
    function addInputAreaConstruirZero(){
            if (fieldsAreaConstruirZero != 80 || valorAreaConstruirZero <= 80){
                    document.getElementById('area_construir_zero').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valorAreaConstruirZero+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_cad[]\" id=\"pavimentos_cad\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                    document.getElementById('area_construir_zero').innerHTML += "<input type=\"hidden\" value=\""+valorAreaConstruirZero+"\" name=\"numero_pavimento_cad[]\" />";
                    valorAreaConstruirZero ++
                    fieldsAreaConstruirZero += 1;
    }else{
    document.getElementById('area_construir_zero').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
    document.getElementById('area_construir_zero').foto.disabled=true;
    }
    }
    //.............................................................................FIM............................................................................


    //  FUNCAO PARA ADCIONAR INPUTS DE UNIDADES
    fields3 = 0;
    var valor3 = 1;
            function addInput_unidades(){
                    if (fields3 != 150 || valor3 <= 150){
                             document.getElementById('text_unidades').innerHTML += "<table class=\"table table-hover\"><tbody><tr>" +
                             "<td width='100'>Casa "+valor3+"</td>" +
                              "<td><input name=\"area_construir[]\" id=\"area_construir[]\" class=\"gui-input\" type=\"text\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"></td>" +
                              "<td><input name=\"area_existente[]\" id=\"area_existente[]\" class=\"gui-input\" type=\"text\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"></td>" +
                              "<td><input name=\"area_uso_exclusivo[]\" id=\"area_uso_exclusivo[]\" class=\"gui-input\" type=\"text\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"></td>" +
                              "<td><input name=\"area_comum_proporcional[]\" id=\"area_comum_proporcional[]\" class=\"gui-input\" type=\"text\" onkeydown=\"Mascara(this,Area);\" onkeypress=\"Mascara(this,Area);\" onkeyup=\"Mascara(this,Area);\" maxlength=\"15\"></td>" +
                              "<td><label class=\"select\">" +
                                "<select id=\"tipo_processo_unidade[]\" name=\"tipo_processo_unidade[]\">" +
                                  "<option value=\"\">Selecione</option>" +
                                  "<option value=\"c\">Construção</option>" +
                                  "<option value=\"r\">Regularização</option>" +
                                  "<option value=\"a\">Ácrescimo de área</option>" +
                                "</select>" +
                                "<i class=\"arrow\"></i>" +
                              "</label>" +
                            "</td></tr></tbody></table>";

                            document.getElementById('text_unidades').innerHTML += "<input type=\"hidden\" value=\""+valor3+"\" name=\"num_casa[]\" />";
                            valor3 ++
                            fields3 += 1;


    }else{
            document.getElementById('text_unidades').innerHTML += "<br />Apenas 150 unidades são permitidas!";
            document.getElementById('text_unidades').foto.disabled=true;
            }
    }
    // FIM

    


    //  FUNCAO PARA ADCIONAR INPUTS DE UNIDADES CONDOMINIO COM PAVIMENTO
    fields5 = 0;
    var valor5 = 1;
    function addInput_unidades_condominio(){
        if (fields5 != 150 || valor5 <= 150){
            document.getElementById('text_unidades').innerHTML += "<table class=\"table table-hover\"><tbody><tr>" +
            "<td><b>Casa "+valor5+"</b></td>" +
            "<td><label class=\"select\">" +
            "<select id=\"tipo_processo_unidade[]\" name=\"tipo_processo_unidade[]\">" +
              "<option value=\"\">Selecione</option>" +
              "<option value=\"c\">Construção</option>" +
              "<option value=\"r\">Regularização</option>" +
              "<option value=\"a\">Ácrescimo de área</option>" +
            "</select>" +
            "<i class=\"arrow\"></i>" +
            "</label>" +

            "<td>" +
                "<button type='button' class='button btn-primary' onclick='addInput_pav_condominio("+valor5+");'>Adicionar</button>" +
            "</td>" +

            "</td></tr>" +

            "<tr><td colspan='3'>" +
                "<div id='pav_condominio"+valor5+"'>" +
                //"<input type=\"text\" value=\"0\" name=\"numero_pav_condominio"+numerocasa+"[]\" id=\"numero_pav_condominio"+numerocasa+"[]\" />" +
                "</div>" +  
            "</td></tr>" +
            "</tbody></table>";

            document.getElementById('text_unidades').innerHTML += "<input type=\"hidden\" value=\""+valor5+"\" name=\"num_casa[]\" />";
            document.getElementById('pav_condominio'+valor5).innerHTML += "<input type=\"hidden\" value=\"0\" name=\"numero_pav_condominio"+valor5+"[]\" id=\"numero_pav_condominio"+valor5+"[]\" />";
            valor5 ++
            fields5 += 1;

        } else {
            document.getElementById('text_unidades').innerHTML += "<br />Apenas 150 unidades são permitidas!";
            document.getElementById('text_unidades').foto.disabled=true;
        }
    }
    // FIM

    //...........................FUNCAO PARA ADCIONAR PAVIMENTOS  CONDOMINIO COM PAVIMENTO.............
    function addInput_pav_condominio(numerocasa){
        valor = document.querySelectorAll('.pav_condominio'+numerocasa).length + 1;

        if ( valor < 80 ){
            document.getElementById('pav_condominio'+numerocasa).innerHTML += 
                    "<table class='pav_condominio"+numerocasa+"'><tr>" +
                        "<td align='center' width='200'>Pav "+valor+"</td>" +
                        "<td >" +
                            "<div class='row'>" +
                                "<div class='col-md-12'>" +
                                    "<label>Área a Construir(m²):</label>" + 
                                 "</div>" +
                            "</div>" +
                            "<div class='row'>" +
                              "<div class='col-md-10'>" +
                                "<input name='area_construir"+numerocasa+"[]' id='area_construir"+numerocasa+"[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15'>" +
                              "</div>" +
                            "</div>" +  
                        "</td>" +
                        "<td>" +
                            "<div class='row'>" +
                                "<div class='col-md-12'>" +
                                    "<label>Área Existente(m²):</label>" + 
                                 "</div>" +
                            "</div>" +
                            "<div class='row'>" +
                              "<div class='col-md-10'>" +
                                "<input name='area_existente"+numerocasa+"[]' id='area_existente"+numerocasa+"[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15'>" +
                              "</div>" +
                            "</div>" +  
                        "</td>" +
                        "<td>" +
                            "<div class='row'>" +
                                "<div class='col-md-12'>" +
                                    "<label>Área de Uso Exclusico(m²):</label>" + 
                                 "</div>" +
                            "</div>" +
                            "<div class='row'>" +
                              "<div class='col-md-10'>" +
                                "<input name='area_uso_exclusivo"+numerocasa+"[]' id='area_uso_exclusivo"+numerocasa+"[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15'>" +
                              "</div>" +
                            "</div>" +  
                        "</td>" +
                        "<td>" +
                            "<div class='row'>" +
                                "<div class='col-md-12'>" +
                                    "<label>Área Comum Proporcional(m²):</label>" + 
                                 "</div>" +
                            "</div>" +
                            "<div class='row'>" +
                              "<div class='col-md-10'>" +
                                "<input name='area_comum_proporcional"+numerocasa+"[]' id='area_comum_proporcional"+numerocasa+"[]' class='gui-input' type='text' onkeydown='Mascara(this,Area);' onkeypress='Mascara(this,Area);' onkeyup='Mascara(this,Area);' maxlength='15'>" +
                              "</div>" +
                            "</div>" +  
                        "</td>" +
                        "</tr></table>";
            //document.getElementById('text').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valor+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos[]\" id=\"pavimentos[]\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" class=\"camposdados_pav\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
            document.getElementById("numero_pav_condominio"+numerocasa+"[]").remove();
            document.getElementById('pav_condominio'+numerocasa).innerHTML += "<input type=\"hidden\" value=\""+valor+"\" name=\"numero_pav_condominio"+numerocasa+"[]\" id=\"numero_pav_condominio"+numerocasa+"[]\" />";
            valor++
        } else {
            document.getElementById('pav_condominio'+numerocasa).innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
            document.getElementById('form').foto.disabled=true;
        }
    }

            //  FUNCAO PARA ADCIONAR INPUTS DE AREA EXISTENTES
    fields2 = 0;
    var valor2 = 2;
            function addInput_areaexistente(){
                    if (fields2 != 80 || valor2 <= 80){
                            document.getElementById('text_areaexistente').innerHTML += "<div class=\"section row\"><div class=\"col-md-8\"><div class=\"row\"><div class=\"col-md-12\"><label>"+valor2+"&ordm; Pavimento:</label></div></div><div class=\"row\"><div class=\"col-md-3\"><input name=\"pavimentos_area_existente[]\" id=\"pavimentos_area_existente[]\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" class=\"gui-input\" type=\"text\"></div></div></div></div>";
                            //document.getElementById('text_areaexistente').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valor2+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_area_existente[]\" id=\"pavimentos_area_existente\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                            document.getElementById('text_areaexistente').innerHTML += "<input type=\"hidden\" value=\""+valor2+"\" name=\"numero_pavimento_area_existente[]\" />";
                            valor2 ++
                            fields2 += 1;
    }else{
            document.getElementById('text_areaexistente').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
            document.getElementById('text_areaexistente').foto.disabled=true;
            }
    }
    // FIM

    //...........................FUNCAO PARA ADCIONAR INPUTS DE AREA DO SUB-SOLOEXISTENTE.............

        fieldssubexistente = 0;
        var valorsubexistente = 2;
                function addInput_areasubsoloexistente(){
                        if (fieldssubexistente != 80 || valorsubexistente <= 80){
                                document.getElementById('text_areasubsoloexistente').innerHTML += "<div class=\"section row\"><div class=\"col-md-8\"><div class=\"row\"><div class=\"col-md-12\"><label>"+valorsubexistente+"&ordm; Pavimento:</label></div></div><div class=\"row\"><div class=\"col-md-3\"><input name=\"area_sub_solo_existente[]\" id=\"area_sub_solo_existente[]\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" class=\"gui-input\" type=\"text\"></div></div></div></div>";
                            
                                //document.getElementById('text_areasubsoloexistente').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valorsubexistente+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_area_subsoloexistente[]\" id=\"pavimentos_area_subsoloexistente\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                                document.getElementById('text_areasubsoloexistente').innerHTML += "<input type=\"hidden\" value=\""+valorsubexistente+"\" name=\"numero_pavimento_area_subsoloexistente[]\" />";
                                valorsubexistente ++
                                fieldssubexistente += 1;
        }else{
                document.getElementById('text_areasubsoloexistente').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
                document.getElementById('text_areasubsoloexistente').foto.disabled=true;
                }
        }

    // ...................................................FIM ................................



     //...........................FUNCAO PARA ADCIONAR INPUTS DE AREA DO SUB-SOLOCONSTRUIR.............

        fieldssubconstruir = 0;
        var valorsubconstruir = 2;
                function addInput_areasubsoloconstruir(){
                        if (fieldssubconstruir != 80 || valorsubconstruir <= 80){
                                document.getElementById('text_areasubsoloconstruir').innerHTML += "<div class=\"section row\"><div class=\"col-md-8\"><div class=\"row\"><div class=\"col-md-12\"><label>"+valorsubconstruir+"&ordm; Pavimento:</label></div></div><div class=\"row\"><div class=\"col-md-3\"><input name=\"pavimentos_area_subsoloconstruir[]\" id=\"pavimentos_area_subsoloconstruir[]\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" class=\"gui-input\" type=\"text\"></div></div></div></div>";
                        
                                //document.getElementById('text_areasubsoloconstruir').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valorsubconstruir+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos_area_subsoloconstruir[]\" id=\"pavimentos_area_subsoloconstruir\" class=\"camposdados_pav\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                                document.getElementById('text_areasubsoloconstruir').innerHTML += "<input type=\"hidden\" value=\""+valorsubconstruir+"\" name=\"numero_pavimento_area_subsoloconstruir[]\" />";
                                valorsubconstruir ++
                                fieldssubconstruir += 1;
        }else{
                document.getElementById('text_areasubsoloconstruir').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
                document.getElementById('text_areasubsoloconstruir').foto.disabled=true;
                }
        }

    // ...................................................FIM SUBSOLOCONSTRUIR................................








    //  FUNCAO PARA ADCIONAR INPUTS
    fields = 0;
    var valor = 2;
            function addInput(){
                    if (fields != 80 || valor <= 80){
                        document.getElementById('text').innerHTML += "<div class=\"section row\"><div class=\"col-md-8\"><div class=\"row\"><div class=\"col-md-12\"><label>"+valor+"&ordm; Pavimento:</label></div></div><div class=\"row\"><div class=\"col-md-3\"><input name=\"pavimentos[]\" id=\"pavimentos[]\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" maxlength=\"15\" class=\"gui-input\" type=\"text\"></div></div></div></div>";
                        //document.getElementById('text').innerHTML += "<div class=\"enblobacampos\"><div class=\"titulotextocad\">"+valor+"&ordm; Pavimento</div><div class=\"campos\"><input type=\"text\" name=\"pavimentos[]\" id=\"pavimentos[]\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" class=\"camposdados_pav\" maxlength=\"15\" /><div class=\"titulotextocad3\">m<sup>2</sup></div></div></div>";
                        document.getElementById('text').innerHTML += "<input type=\"hidden\" value=\""+valor+"\" name=\"numero_pavimento[]\" />";
                            valor++
                            fields += 1;
    }else{
            document.getElementById('text').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
            document.getElementById('form').foto.disabled=true;
            }
    }

    // FIM        	
    

    // FUNCAO PARA AJUSTES DE INPUTS DE AREAS --- Irving
    function Areas(input)
    {
      //Área total do loteamento
      var total = Number(document.getElementById('areatotal').value.replace(",","."));
      //Área somente de lotes do loteamento
      var lotes = Number(document.getElementById('arealotes').value.replace(",","."));
      //Área de Vias Públicas
      var arvip = Number(document.getElementById('areaviaspublicas').value.replace(",","."));
      //Área verde
      var arver = Number(document.getElementById('areaverde').value.replace(",","."));
      //Área de Equipamentos Públicos
      var areqp = Number(document.getElementById('areaequipamentopublico').value.replace(",","."));
      //Área de Transferência Municipal
      var ATM   = Number(document.getElementById('ATM').value.replace(",","."));
      var ATManterior = 0;

      if(input.id == 'areatotal') {//mexeu no total
        ATManterior = ATM; //para cálculo de proporcionalidade de alterações
        //Cálculo da ATM caso seja aumento proporcional ao total anterior ou 50%---50% caso não
        if(ATM + lotes > 0) ATM = Math.round(ATM * total / (ATM + lotes) *100)/100; 
        else ATM = Math.round(total/2 *100)/100; 
        lotes = total - ATM; //Cálculo da área de lotes

        if(ATManterior > 0 ){ //alteração proporcional a um valor anterior de ATM 
          areqp = Math.round(areqp * ATM / ATManterior *100)/100;
          arver = Math.round(arver * ATM / ATManterior *100)/100;
        }
        else {//alteração não proporcional porém equilibrada entre 3 valores de áreas
          arver = Math.round(ATM/3 *100)/100;
          areqp = Math.round(ATM/3 *100)/100;
        }
        arvip = ATM - arver - areqp;
      }

      if(input.id == 'arealotes') {//mexeu na área de lotes
        ATManterior = ATM; //para cálculo de proporcionalidade de alterações
        ATM = total - lotes; //Cálculo da ATM
        if(ATM < total * 0.35) {//ATM abaixo de 35%
          ATM = Math.round(lotes * 0.35/0.65 *100)/100; //mínimo de 35% do total com arredondamento
          if(ATM < (lotes + ATM) * 0.35) ATM = ATM + 0.01; //garantia de arredondamento aceitável
          total = lotes + ATM; //ajuste do total por alteração necessária de ATM
        }
        
        if(ATManterior > 0) { //alteração proporcional a um valor anterior de ATM
          arvip = arvip * ATM / ATManterior;
          arver = arver * ATM / ATManterior;
        }
        else { //alteração não proporcional porém equilibrada entre 3 áreas com limites mínimos
          arvip = ATM/3;
          arver = ATM/3;
        }
        if(arvip < total * 0.15) arvip = total * 0.15; //garantia de no mínimo 15% do total
        arvip = Math.round(arvip *100)/100; //arredondamento
        if(arvip < total * 0.15) arvip = arvip + 0.01; //garantia de arredondamento aceitável

        if(arver < total * 0.10) arver = total * 0.10; //garantia de no mínimo 10% do total
        arver = Math.round(arver *100)/100; //arredondamento
        if(arver < total * 0.10) arver = arver + 0.01; //garantia de arredondamento aceitável

        areqp = ATM - arvip - arver;
      }
      
      if(input.id == 'ATM') {//mexeu na área de Tranferência Municipal
        ATM = total - lotes; //volta ao valor que era antes ... cálculo de ATM automático
      }

      if(input.id == 'areaviaspublicas') {//mexeu na área de Vias Públicas
        if(arvip < total*0.15) arvip = total*0.15; //ajuste de vias públicas abaixo de 15% do total
        if((ATM-arvip) < total * 0.15) arvip = ATM - total * 0.15; //área verde ou de equipamentos abaixo do limite
        arvip = Math.round(arvip *100)/100; //arredondamento
        if(arvip < total * 0.10) arvip = arvip + 0.01; //garantia de arredondamento aceitável

        arver = Math.round((ATM - arvip) * arver/(arver + areqp) *100)/100; //cálculo proporcional restante e arredondamento
        if(arver < total * 0.10) arver = total * 0.10; //garantia de arver no mínimo 10% do total
        if(ATM-arvip-arver < total * 0.05){ //garantia de areqp no mínimo 5% do total
          arver = Math.round(ATM - arvip - total * 0.05 *100)/100; //cálculo earredondamento
          if(arver < total * 0.10) arver = arver + 0.01; //garantia de arredondamento
          if(ATM-arvip-arver < total * 0.05) arver = arver - 0.01;//garantia de arredondamento aceitável
        } 

        areqp = ATM - arvip - arver;
      }

      if(input.id == 'areaverde') {//mexeu na área verde
        if(arver < total*0.10) arver = total*0.10; //ajuste de áreas verdes abaixo de 10% do total
        if((ATM-arver) < total * 0.20) arver = ATM - total * 0.20; //área de vias ou equipamentos abaixo do limite
        arver = Math.round(arver *100)/100; //arredondamento
        if(arver < total * 0.10) arver = arver + 0.01; //garantia de arredondamento aceitável

        if(ATM-arver-arvip < total * 0.05){ //garantia de areqp no mínimo 5% do total
          areqp = Math.round(total * 0.05 *100)/100; //cálculo e arredondamento
          if(areqp < total * 0.05) areqp = areqp + 0.01; //garantia de arredondamento aceitável
          arvip = ATM - arver - areqp;
        } else areqp = ATM - arver - arvip;
      }

      if(input.id == 'areaequipamentopublico'){//mexeu na área de equipamentos públicos
        if(areqp < total*0.05) areqp = total*0.05; //ajuste de equipamentos abaixo de 5% do total
        if((ATM-areqp) < total * 0.25) areqp = ATM - total * 0.25; //área de vias ou verde abaixo do limite
        areqp = Math.round(areqp *100)/100; //arredondamento
        if(areqp < total * 0.05) areqp = areqp + 0.01; //garantia de arredondamento aceitável

        if((ATM-areqp-arvip) < total * 0.10){//garantia de mínimo 10% para área verde
          arver = total * 0.10; //ajuste sem área verde abaixo do limite
          arver = Math.round(arver *100)/100; //arredondamento
          if(arver < total * 0.10) arver = arver + 0.01; //garantia de arredondamento aceitável
          arvip = ATM - arver - areqp; //cálculo de área de vias públicas
        }
        else arver = ATM - arvip - areqp; //calcula área verde preservando área de vias púlicas
      }

      document.getElementById('areatotal').value = total.toFixed(2).replace(".",",");
      document.getElementById('arealotes').value = lotes.toFixed(2).replace(".",",");
      document.getElementById('areaviaspublicas').value = arvip.toFixed(2).replace(".",",");
      document.getElementById('areaverde').value = arver.toFixed(2).replace(".",",");
      document.getElementById('areaequipamentopublico').value = areqp.toFixed(2).replace(".",",");
      document.getElementById('ATM').value = (ATM).toFixed(2).replace(".",",");
      //alert('total = ' + total + '\nlotes = ' + lotes + '\narvip = ' + arvip + '\narver = ' + arver + '\nareqp = ' + areqp + '\nATM = ' + ATM + '\nATManterior = ' + ATManterior);
    }
    // FIM

    //  FUNCAO PARA ADCIONAR INPUTS DE AREA EXISTENTES
    fields4 = 0;
    var valor4 = 1;
            function addInputProprietario(){ 
                    if (fields4 != 20 || valor4 <= 20){
                            document.getElementById('textProprietario').innerHTML +=

                                    "<div class=\"section row\">" +
                                      "<div class=\"col-md-6\">" +
                                        "<label for=\"nomeProprietarioAdd[]\" class=\"field prepend-icon putproprietariosadd\">" +
                                          "<input type=\"text\" name=\"nomeProprietarioAdd[]\" id=\"nomeProprietarioAdd\" class=\"gui-input\" placeholder=\"Nome\">" +
                                          "<input type=\"hidden\" name=\"idAlvaraAddProprietario[]\" name=\"idAlvaraAddProprietario[]\" value=\""+valor4+"\" />" +
                                          "<label for=\"firstname\" class=\"field-icon\">" +
                                            "<i class=\"fa fa-user\"></i>" +
                                          "</label>" +
                                        "</label>" +
                                      "</div>" +
                                    "</div>" +

                                    "<div class=\"section row\">" +
                                      "<div class=\"col-md-6\">" +
                                        "<label for=\"emailProprietarioAdd[]\" class=\"field prepend-icon putproprietariosadd\">" +
                                          "<input type=\"text\" name=\"emailProprietarioAdd[]\" id=\"emailProprietarioAdd\" class=\"gui-input\" placeholder=\"E-mail\">" +
                                          "<input type=\"hidden\" name=\"emailProprietarioAdd[]\" value=\""+valor4+"\" />" +
                                          "<label for=\"emailProprietarioAdd[]\" class=\"field-icon\">" +
                                            "<i class=\"fa fa-envelope\"></i>" +
                                          "</label>" +
                                        "</label>" +
                                      "</div>" +
                                    "</div>" +
                                    
                                    "<div class=\"section row\">" +
                                      "<div class=\"col-md-6\">" +
                                        "<label for=\"cpfcnpjProprietarioAdd[]\" class=\"field prepend-icon putproprietariosadd\">" +
                                          "<input type=\"text\" name=\"cpfcnpjProprietarioAdd[]\" id=\"cpfcnpjProprietarioAdd\" class=\"gui-input\" placeholder=\"CPF/CNPJ\">" +
                                          "<input type=\"hidden\" name=\"cpfcnpjProprietarioAdd[][]\" value=\""+valor4+"\" />" +
                                          "<label for=\"cpfcnpjProprietarioAdd[]\" class=\"field-icon\">" +
                                            "<i class=\"fa fa-check\"></i>" +
                                          "</label>" +
                                        "</label>" +
                                      "</div>" +
                                    "</div> <hr />";
                            valor4 ++
                            fields4 += 1;
    }else{
            document.getElementById('textProprietario').innerHTML += "<br />Apenas 20 Propriet&aacute;rios são permitidos!";
            document.getElementById('textProprietario').foto.disabled=true;
            }
    }
    // FIM


    //  FUNCAO PARA ADCIONAR INPUTS DE LOTEAMENTOS -- IRVING
    fields4 = 0;
    var valor4 = 1;
            function addInputProprietarioNovo(){ 
                    if (fields4 != 20 || valor4 <= 20){
                            document.getElementById('textProprietario').innerHTML +=

                                    "<div class=\"section row\">" +
                                      "<div class=\"col-md-6\">" +
                                        "<label for=\"nomeProprietarioAdd[]\" class=\"field prepend-icon putproprietariosadd\">" +
                                          "<input type=\"text\" name=\"nomeProprietarioAdd[]\" id=\"nomeProprietarioAdd\" class=\"gui-input\" placeholder=\"Nome\">" +
                                          "<input type=\"hidden\" name=\"idLotAddProprietario[]\" name=\"idAddProprietario[]\" value=\""+valor4+"\" />" +
                                          "<label for=\"firstname\" class=\"field-icon\">" +
                                            "<i class=\"fa fa-user\"></i>" +
                                          "</label>" +
                                        "</label>" +
                                      "</div>" +
                                    "</div>" +

                                    "<div class=\"section row\">" +
                                      "<div class=\"col-md-6\">" +
                                        "<label for=\"emailProprietarioAdd[]\" class=\"field prepend-icon putproprietariosadd\">" +
                                          "<input type=\"email\" name=\"emailProprietarioAdd[]\" id=\"emailProprietarioAdd\" class=\"gui-input\" placeholder=\"E-mail\">" +
                                          "<input type=\"hidden\" name=\"emailLotProprietarioAdd[]\" value=\""+valor4+"\" />" +
                                          "<label for=\"emailProprietarioAdd[]\" class=\"field-icon\">" +
                                            "<i class=\"fa fa-envelope\"></i>" +
                                          "</label>" +
                                        "</label>" +
                                      "</div>" +
                                    "</div>" +
                                    
                                    "<div class=\"section row\">" +
                                      "<div class=\"col-md-6\">" +
                                        "<label for=\"cpfcnpjProprietarioAdd[]\" class=\"field prepend-icon putproprietariosadd\">" +
                                          "<input type=\"input\" name=\"cpfcnpjProprietarioAdd[]\" id=\"cpfcnpjProprietarioAdd\" class=\"gui-input\" placeholder=\"CPF/CNPJ\">" +
                                          "<input type=\"hidden\" name=\"cpfcnpjLotProprietarioAdd[][]\" value=\""+valor4+"\" />" +
                                          "<label for=\"cpfcnpjProprietarioAdd[]\" class=\"field-icon\">" +
                                            "<i class=\"fa fa-check\"></i>" +
                                          "</label>" +
                                        "</label>" +
                                      "</div>" +
                                    "</div> <hr />";
                            valor4 ++
                            fields4 += 1;
    }else{
            document.getElementById('textProprietario').innerHTML += "<br />Apenas 20 Propriet&aacute;rios são permitidos!";
            document.getElementById('textProprietario').foto.disabled=true;
            }
    }
    // FIM


    // MASCARA PARA MOEDA 
    function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){
        var sep = 0;
        var key = '';
        var i = j = 0;
        var len = len2 = 0;
        var strCheck = '0123456789';
        var aux = aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;
        if (whichCode == 13) return true;
        key = String.fromCharCode(whichCode); // Valor para o c�digo da Chave
        if (strCheck.indexOf(key) == -1) return false; // Chave inv�lida
        len = objTextBox.value.length;
        for(i = 0; i < len; i++)
            if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;
        aux = '';
        for(; i < len; i++)
            if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);
        aux += key;
        len = aux.length;
        if (len == 0) objTextBox.value = '';
        if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;
        if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;
        if (len > 2) {
            aux2 = '';
            for (j = 0, i = len - 3; i >= 0; i--) {
                if (j == 3) {
                    aux2 += SeparadorMilesimo;
                    j = 0;
                }
                aux2 += aux.charAt(i);
                j++;
            }
            objTextBox.value = '';
            len2 = aux2.length;
            for (i = len2 - 1; i >= 0; i--)
            objTextBox.value += aux2.charAt(i);
            objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);
        }
        return false;
    }

    /*
     * FUNCAO PARA DESOCULTAR E OCULTAR BOX COM SELECT PARA ALTERAÇÃO DO PROCESSO
     */
    function fadeBoxAlterProcess () {
        if ($("#boxAltProcess").attr("checked")){
            $(".altboxprocess").fadeIn(500);
        }else{
            $(".altboxprocess").fadeOut(500);
        }	
    }

    function FadeLegenda(){
        if ($("#legendafade").attr("checked")){
            $(".quadrocimalegenda2").fadeIn(500);
        }else{
            $(".quadrocimalegenda2").fadeOut(500);
        }	
    }

    function FadeLegenda3(){
        if ($("#legendafade3").attr("checked")){
            $(".quadrocimalegenda3").fadeIn(500);
        }else{
            $(".quadrocimalegenda3").fadeOut(500);
        }	
    }





    // FUNCAO PARA DESOCULTAR AREA SUBSOLOEXISTENTE
    // CADASTRO
    function impermeavel(){
            if ($('input[name=area_impermeavel_check]').is(':checked')){
                            $("#englobaaddpavPermeavel").fadeIn(500);
                    }else{
                            $("#englobaaddpavPermeavel").fadeOut(500);
                    }	
    }             

    //  FUNCAO PARA ADCIONAR INPUTS DA AREA IMPERMEAVEL -- PAGINA DE CADASTRO
    fieldsImpermeavel = 0;
    var valorimpermeavel = 2;
            function addInput_areaImpermeavel(){
                if (fieldsImpermeavel != 80 || valorimpermeavel <= 80){
                         document.getElementById('text_areaImpermeavel').innerHTML += 
                           "<div class=\"section row\">" +      
                              "<div class=\"col-md-3\">" +
                                    "<label>Tipo de Área:</label>" +
                                      "<label class=\"select\">" +
                                        "<select id=\"tipo_impermeavel\" name=\"tipo_impermeavel[]\">" +
                                          "<option value=\"\">Selecione o tipo</option>" +
                                          "<option value=\"apa\">Piscina</option>" +
                                          "<option value=\"adk\">Deck</option>" +
                                        "</select>" +
                                        "<i class=\"arrow\"></i>" +
                                      "</label>" +
                                "</div>" +
                              "</div>" +
                               "<div class=\"section row\">" +
                                "<div class=\"col-md-8\">" +
                                "<div class=\"row\">" +
                                  "<div class=\"col-md-12\">" +
                                    "<label>Área em (m²)</label>" + 
                                  "</div>" +
                                "</div>" +
                                "<div class=\"row\">" +
                                  "<div class=\"col-md-3\">" +
                                    "<input name=\"pavimentos_area_impermeavel[]\" id=\"pavimentos_area_impermeavel\" class=\"gui-input\" type=\"text\"  onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\  maxlength=\"15\">" +
                                  "</div>" +
                                "</div>" +   
                              "</div>";
                         /*document.getElementById('text_areaImpermeavel').innerHTML += 
                           "<div class=\"englobaaddpavPermeavel\">" +
                                "<div class=\"enblobacampos\">" +
                                            "<div class=\"titulotextocad\">" +
                                                    "Tipo de &aacute;rea:" +
                                            "</div>" +

                                            "<div class=\"campos2\">" +
                                                 "<select name=\"tipo_impermeavel[]\" id=\"tipo_impermeavel\" class=\"selectcustom\">"+
                                                        "<option value=\"0\">Escolha um valor</option>" +
                                                        "<option value=\"1\">Piscina</option>" +
                                                        "<option value=\"2\">Deck</option>" +
                                                "</select>" +
                                            "</div>" +

                               "</div>" +

                               "<div class=\"enblobacampos\">" +
                                        "<div class=\"titulotextocad\">" +
                                                "&Aacute;rea:" +
                                        "</div>" +

                                        "<div class=\"campos\">" +
                                                "<input type=\"text\" name=\"pavimentos_area_impermeavel[]\" id=\"pavimentos_area_impermeavel\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" class=\"camposdados_pav\"  maxlength=\"15\" />" +
                                        "</div>" +

                                        "<div class=\"textoerro\">" +															
                                                "<span id=\"area_impermeavel_info\">( * ) Apenas n&uacute;meros</span>" +
                                        "</div>" +

                               "</div>"+
                           "</div>"
                        ;*/

                        document.getElementById('text_areaImpermeavel').innerHTML += "<input type=\"hidden\" value=\""+valorimpermeavel+"\" name=\"numero_area_impermeavel[]\" />";
                        valorimpermeavel ++
                        fieldsImpermeavel += 1;

                }else{
                        document.getElementById('text_areaImpermeavel').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
                        document.getElementById('text_areaImpermeavel').foto.disabled=true;
                }
    }
    // FIM	        

   
    
    
    //  FUNCAO PARA ADCIONAR INPUTS DA AREA IMPERMEAVEL -- PAGINA DE ALTERACAO
    fieldsImpermeavelZero = 0;
    var valorimpermeavelZero = 2;
            function addInput_areaImpermeavelZero(){
                if (fieldsImpermeavelZero != 80 || valorimpermeavelZero <= 80){
                         document.getElementById('area_impermeavel_zero').innerHTML += 
                           "<div class=\"englobaaddpavPermeavel\">" +
                                "<div class=\"enblobacampos\">" +
                                            "<div class=\"titulotextocad\">" +
                                                    "Tipo de &aacute;rea:" +
                                            "</div>" +

                                            "<div class=\"campos2\">" +
                                                 "<select name=\"tipo_impermeavel_cad[]\" id=\"tipo_impermeavel\" class=\"selectcustom\">"+
                                                        "<option value=\"0\">Escolha um valor</option>" +
                                                        "<option value=\"1\">Piscina</option>" +
                                                        "<option value=\"2\">Deck</option>" +
                                                "</select>" +
                                            "</div>" +

                               "</div>" +

                               "<div class=\"enblobacampos\">" +
                                        "<div class=\"titulotextocad\">" +
                                                "&Aacute;rea:" +
                                        "</div>" +

                                        "<div class=\"campos\">" +
                                                "<input type=\"text\" name=\"pavimentos_area_impermeavel_cad[]\" id=\"pavimentos_area_impermeavel_cad\" onKeyDown=\"Mascara(this,Area);\" onKeyPress=\"Mascara(this,Area);\" onKeyUp=\"Mascara(this,Area);\" class=\"camposdados_pav\"  maxlength=\"15\" />" +
                                        "</div>" +

                                        "<div class=\"textoerro\">" +															
                                                "<span id=\"area_impermeavel_info\">( * ) Apenas n&uacute;meros</span>" +
                                        "</div>" +

                               "</div>"+
                           "</div>"
                        ;

                        document.getElementById('area_impermeavel_zero').innerHTML += "<input type=\"hidden\" value=\""+valorimpermeavelZero+"\" name=\"numero_area_impermeavel_cad[]\" />";
                        valorimpermeavelZero ++
                        fieldsImpermeavelZero += 1;

                }else{
                        document.getElementById('area_impermeavel_zero').innerHTML += "<br />Apenas 80 Pavimentos são permitidos!";
                        document.getElementById('area_impermeavel_zero').foto.disabled=true;
                }
    }
    // FIM
    
    
    // FUNCAO PARA DESOCULTAR AREA SUB-SOLOEXISTENTE
    // ALTERACAO
    function ap2022Impermeavel(){
            if ($("#area_impermeavel_check").attr("checked")){
                            $("#blocoareaimpermeavel").fadeIn(500);
                    }else{
                            $("#blocoareaimpermeavel").fadeOut(500);
                    }	
    }
    
    // FUNCAO PARA DESOCULTAR BOX DE INCREMENTACAO DE PAVIMENTOS AREA SUB-SOLOEXISTENTE
    // ALTERACAO
    function ap203impermeavel(){
            if ($("#area_impermeavel_check_cad_pav").attr("checked")){
                            $("#cadaddmaispavareimpermeavel").fadeIn(500);
                    }else{
                            $("#cadaddmaispavareimpermeavel").fadeOut(500);
                    }	
    }
    
    