<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));

$id_pro = (int)$_GET['id_pro'];

// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

//  Função para includes reconhecer a página e modificar sua exibição.
$emissaoalvarahabitese  = false;

if ( $id_pro > 0 ) {
    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();
}


?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>
<body class="admin-validation-page sb-l-o sb-r-c onload-check sb-l-m sb-l-disable-animation" data-spy="scroll" data-target="#nav-spy" data-offset="200" style="min-height: 94px;">


  <!-- Start: Main -->
  <div id="main">

    

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

     <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout">

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="height: 294px;">
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border panel-system">

                <form method="post" action="/" id="admin-form" novalidate="novalidate">

                  <div class="panel-body bg-light alinha-conteudo">
                    <!-- .section-divider -->
                    <div class="section row">
                      <div class="col-md-3">
                        <img src="img/logo_prefeitura_home.png">
                      </div>
                      <div class="col-md-7">Sistema de Planejamento, Orçamentário e Gestão</div>
                        
                      </div>
                    <div class="section row  menu">
                      <?php include_once('includes/menuindex.php'); ?>
                    </div>
                    <div class="section row " style="border: 1px solid margin-left">
                      <div class="col-md-12">
                        <div class="row">

                          <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <div class="panel-body bg-light">

              <?php 
                  if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 2 or $linha2['tipoprocesso'] == 3 or $linha2['tipoprocesso'] == 6 ) {
                    include_once('includes/info_alvara.php');
                  } else if ( $linha2['tipoprocesso'] == 4 ) {
                    include_once('includes/info_condominio.php');
                  } else if ( $linha2['tipoprocesso'] == 5 ) {
                    include_once('includes/info_redimensionamento.php');
                  }

             ?>
              <?php ?>

              <?php
              if ( $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode') > 0 ) { 
              if ( $linha2['tipoprocesso'] != 4 ) {
                $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
                  id_ah,
                  id_pro,
                  --  1 = Alvará, 2 = Habite-se, 3 Alvará Regularização
                  tipo,
                  numero,   
                  id_cg,
                  ano

                  FROM 

                  processos.processos_alvarahabitese 

                  WHERE processos_alvarahabitese.id_pro = $id_pro ");
                  
                  if ( $consulta_alvarahabitese->rowCount() > 0 ) {  

                    
                  ?>
                <table class="table">
                  <thead>
                    <tr class="primary">
                      <th style="color:white;">ID/ANO</th>
                      <th></th> 
                    </tr>
                  </thead>
                  <tbody>
                  <?php 
                  $alvara = false;
                  $habitese = false;
                  foreach ( $consulta_alvarahabitese as $listagem_alvarahabitese ) { 

                    ?>
                    <tr>
                      <td><?php if ( $listagem_alvarahabitese['tipo'] == 3 ) { echo 'R.'; } echo $listagem_alvarahabitese['numero'].'/'.$listagem_alvarahabitese['ano']; ?></td>
                      <td><a 
                      <?php
                        if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) { ?>
                          href="alvara_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>" target="_blank">
                        <?php } else if ( $listagem_alvarahabitese['tipo'] == 2 ) { ?>
                          href="habitese_impressao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>&id_ah=<?php echo $listagem_alvarahabitese['id_ah']; ?>" target="_blank">
                          <?php } ?>
                        <?php 

                        if ( $listagem_alvarahabitese['tipo'] == 1 or $listagem_alvarahabitese['tipo'] == 3 ) {
                          $alvara = true;
                          $id_ah = $listagem_alvarahabitese['id_ah'];
                          echo 'Visualizar Alvará';
                        } else if ( $listagem_alvarahabitese['tipo'] == 2 ) {
                          $habitese = true;
                          echo 'Visualizar Habite-se';
                        }
                      ?>
                      </a></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <?php } ?>
                <table class="table">
                  <thead>
                    <tr class="primary">
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              <?php } } ?>

              <hr />


                    <?php 
                      //  Listando informações sobre responsável técnico.
                      $con_listagem_analise = $configuracoes->consulta("SELECT 
                        processos_analise.id_ana,
                        processos_analise.id_pro,
                        processos_analise.tipo,
                        processos_analise.situacaoprojeto,
                        processos_analise.obsgerais,
                        processos_analise.valor1,
                        processos_analise.valor2,
                        processos_analise.valor3,
                        processos_analise.valor4,
                        processos_analise.valor5,
                        processos_analise.valor6,
                        processos_analise.valor7,
                        processos_analise.valor8
                                               

                        FROM 
                        processos.processos_analise

                        WHERE processos.processos_analise.id_pro = $id_pro and tipo = 'ch'");

                        $listagem_analise = $con_listagem_analise->fetch();

                        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
                    ?>
                    <input type="hidden" name="id_ana" value="<?php echo $listagem_analise['id_ana']; ?>">
                    <div class="section row">
                      <div class="col-md-6">Projeto de Edificações (Arquitetônico) - 1 (uma)cópia para aprovação;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option field option-primary">
                                <input disabled name="valor1" type="radio" value="t"<?php if ( $listagem_analise['valor1'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option field option-primary">
                                <input disabled name="valor1" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor1'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div> 
                    </div>
                     <div class="section row">
                      <div class="col-md-6">Memorial Descritivo da Obra - 1 (uma) cópia para aprovação;;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor2" type="radio" value="t"<?php if ( $listagem_analise['valor2'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor2" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor2'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-12">
                        <em>*Após a análise do projeto e consequente aprovação do mesmo, o requerente deverá providenciar 3 cópias do projeto e memorial;</em>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Título de propriedade ou documento que comprove a justa posse (escritura ou contrato de compra e venda);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor3" type="radio" value="t"<?php if ( $listagem_analise['valor3'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor3" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor3'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div> 
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Certidão negativa do proprietário (imóvel);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor4" type="radio" value="t"<?php if ( $listagem_analise['valor4'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor4" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor4'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Cópia de documentos pessoais do proprietário (RG e CPF);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor5" type="radio" value="t"<?php if ( $listagem_analise['valor5'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor5" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor5'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div> 
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Cópia da(s) ART de autoria do projeto e responsabilidade técnica;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor6" type="radio" value="t"<?php if ( $listagem_analise['valor6'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor6" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor6'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Memorial Descritivo da Obra - 1 (uma) cópia para aprovação;;</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor7" type="radio" value="t"<?php if ( $listagem_analise['valor7'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor7" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor7'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div> 
                        </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Certidão negativa do profissional autor do projeto e responsável técnico (contribuinte);</div>
                        <div class="option-group field">
                          <div class="col-md-2">                       
                            <label class="option option-primary">
                                <input disabled name="valor8" type="radio" value="t"<?php if ( $listagem_analise['valor8'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                          </div>
                          <div class="col-md-2">
                            <label class="option option-primary">
                                <input disabled name="valor8" type="radio" value="false"<?php if ( $con_listagem_analise->rowCount() > 0 and !$listagem_analise['valor8'] ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                          </div>
                        </div>
                    </div>
                    <hr />
                    <div class="section row">
                      <div class="col-md-12">
                        <h3>Situação do projeto: <strong>

                       	<?php
                        	/*
                              1 - A ser analisado = white, 
                              2 - Em análise = #ddd, 
                              3 - Pendência de documentos ou correção = #c4d79b, 
                              4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
                              5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
                              6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
                              7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
                              8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
                              9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
                              10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
                              11 - Aprovado = #eada6d, 
                              12 - Dispensar Vistoria
                            */
                            // Cor das legendas
                            if ( $linha2['situacaoprojeto'] == 1 ) {
                            	echo "A ser analisado";
                            } else if ( $linha2['situacaoprojeto'] == 2 ) {
                            	echo "Em análise";
                            } else if ( $linha2['situacaoprojeto'] == 3 ) {
                            	echo "Pendência de documentos ou correção";
                            } else if ( $linha2['situacaoprojeto'] == 4 ) {
                            	echo "Processo não permitido ou reprovado na análise/vistoria/análise";
                            } else if ( $linha2['situacaoprojeto'] == 5 ) {
                            	echo "Processo encaminhado à procuradoria - Dúvida na vistoria";
                            } else if ( $linha2['situacaoprojeto'] == 6 ) {
                              	echo "Processo encaminhado ao departamento imobiliário - Dúvida na vistoria";
                            } else if ( $linha2['situacaoprojeto'] == 7 ) {
                              	echo "Aprovado - Pendente de pagamento de taxa para elaboração do decreto";
                            } else if ( $linha2['situacaoprojeto'] == 8 ) {
                            	echo "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
                            } else if ( $linha2['situacaoprojeto'] == 9 ) {
                            	echo "Aprovado - Decreto assinado - para entregar";
                            } else if ( $linha2['situacaoprojeto'] == 10 ) {
                            	echo "Aprovado - Processo finalizado e decreto entregue";
                            } else if ( $linha2['situacaoprojeto'] == 11 ) {
                            	echo "Aprovado";
                            }
                        ?></strong></h3>
                      </div>
                    </div>
            </div>
          </div><?php include('includes/rodapeindex.php'); ?>
          <!-- end: .tab-content -->
              </div>
              
              </div><!-- conteudo-->
            </div><!-- end .section row section -->
          </div>

        </div>
        <!-- end: .admin-form -->

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    function apFotos(){ 
        if ( $('input[id=statusdispensa]').is(':checked')) {
            $("#imagens").css("display","none");
          }else{
            $("#imagens").fadeIn(500);
          } 
      }
      function ap30(){ 
        if ( $('input[name=checkocultdetalhes]').is(':checked')) {
            $("#camposocultosmaisdetana").fadeIn(500);
          }else{
            $("#camposocultosmaisdetana").css("display","none");
          } 
      }
      function aplogs(){ 
        if ( $('input[name=checkocultologs]').is(':checked')) {
          $("#boxlogs").fadeIn(500);
        }else{
          $("#boxlogs").css("display","none");
        } 
      }
  </script>
  
  <script type="text/javascript">
  jQuery(document).ready(function() {


    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>