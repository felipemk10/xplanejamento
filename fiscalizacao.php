<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$funcoes = new funcoes;
$validacoes = new validacoes;
$func_analise = new func_analise;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$id_pro = (int)$_GET['id_pro'];
// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

if ( $id_pro > 0 ) {
  //  Listando informações sobre o processo.
  $consulta = $configuracoes->consulta("SELECT 
    processos_analise.id_ana,
    processos_analise.id_pro,
    processos_analise.tipo,
    processos_analise.situacaoprojeto,
    processos_analise.obsgerais,
    processos_analise.descricao_img1,
    processos_analise.descricao_img2,
    processos_analise.descricao_img3,
    processos_analise.descricao_img4,
    processos_analise.descricao_img5
                           

    FROM 
    processos.processos_analise

    WHERE processos.processos_analise.id_pro = $id_pro and tipo = 'fi'");
    $linha = $consulta->fetch();

    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();


      if ( $linha2['tipoprocesso'] == 5 ) {
        //  Listando informações sobre responsável técnico.
        $con_listagem_redimensionamento = $configuracoes->consulta("SELECT 
        processos_redimensionamento.id_red,
        processos_redimensionamento.tipo,
        processos_redimensionamento.areatotalterreno,
        processos_redimensionamento.sa_qtdlotes,  
        processos_redimensionamento.sp_qtdlotes,
        processos_redimensionamento.descricaolotes,
        processos_redimensionamento.requerente,
        processos_redimensionamento.requerentetelefone

        FROM 
        processos.processos_redimensionamento

        WHERE processos.processos_redimensionamento.id_pro = $id_pro");

        $listagem_redimensionamento = $con_listagem_redimensionamento->fetch();

        $id_red = $listagem_redimensionamento['id_red'];
        // Legenda finalidadedeobra:  re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial
      }
}

if ( $_POST['formulario'] == 'ok' ) {
  
  $id_ana = (int)$_POST['id_ana'];
  $situacaoprojeto = $_POST['situacaoprojeto'];
  $obsgerais = $_POST['obsgerais'];

  // Pasta onde o arquivo vai ser salvo
  $_UP['pasta'] = 'vistoria/';
  // Tamanho máximo do arquivo (em Bytes)
  $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
  // Array com as extensões permitidas
  $_UP['extensoes'] = array('jpeg', 'jpg', 'png', 'gif');
  // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
  $_UP['renomeia'] = true;


  // Array com os tipos de erros de upload do PHP
  $_UP['erros'][0] = 'Não houve erro';
  $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
  $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
  $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
  $_UP['erros'][4] = 'Não foi feito o upload do arquivo';

  // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
  if ( $situacaoprojeto != 15 and $id_ana > 0 and !empty($_FILES['imagem1']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem1']['name']) ) {
    if ($_FILES['arquivo']['error'] != 0) {
      die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['imagem1']['error']]);
      exit; // Para a execução do script
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem2']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem2']['name']) ) {
    if ($_FILES['arquivo']['error'] != 0) {
      die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['imagem2']['error']]);
      exit; // Para a execução do script
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem3']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem3']['name']) ) {
    if ($_FILES['arquivo']['error'] != 0) {
      die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['imagem3']['error']]);
      exit; // Para a execução do script
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem4']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem4']['name']) ) {
    if ($_FILES['arquivo']['error'] != 0) {
      die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['imagem4']['error']]);
      exit; // Para a execução do script
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem5']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem5']['name'])) {
    if ($_FILES['arquivo']['error'] != 0) {
      die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['imagem5']['error']]);
      exit; // Para a execução do script
    }
  }

  // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar

  // Faz a verificação da extensão do arquivo
  if ( $situacaoprojeto != 15 and $id_ana > 0 and !empty($_FILES['imagem1']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and $situacaoprojeto != 12 and !empty($_FILES['imagem1']['name']) ) {
    $extensao = strtolower(end(explode('.', $_FILES['imagem1']['name'])));
    if (array_search($extensao, $_UP['extensoes']) === false) {
      echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem2']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem2']['name']) ) {
    $extensao = strtolower(end(explode('.', $_FILES['imagem2']['name'])));
    if (array_search($extensao, $_UP['extensoes']) === false) {
      echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem3']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem3']['name']) ) {
    $extensao = strtolower(end(explode('.', $_FILES['imagem3']['name'])));
    if (array_search($extensao, $_UP['extensoes']) === false) {
      echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem4']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem4']['name']) ) {
    $extensao = strtolower(end(explode('.', $_FILES['imagem4']['name'])));
    if (array_search($extensao, $_UP['extensoes']) === false) {
      echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem5']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem5']['name']) ) {
    $extensao = strtolower(end(explode('.', $_FILES['imagem5']['name'])));
    if (array_search($extensao, $_UP['extensoes']) === false) {
      echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
      exit;
    }
  }
  // Faz a verificação do tamanho do arquivo
  if ( !empty($_FILES['imagem1']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and $situacaoprojeto != 12 and !empty($_FILES['imagem1']['name'])) {
    if ($_UP['tamanho'] < $_FILES['imagem1']['size']) {
      echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem2']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem2']['name']) ) {
    if ($_UP['tamanho'] < $_FILES['imagem2']['size']) {
      echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem3']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem3']['name']) ) {
    if ($_UP['tamanho'] < $_FILES['imagem3']['size']) {
      echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem4']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem4']['name']) ) {
    if ($_UP['tamanho'] < $_FILES['imagem4']['size']) {
      echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
      exit;
    }
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem5']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem5']['name']) ) {
    if ($_UP['tamanho'] < $_FILES['imagem5']['size']) {
      echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
      exit;
    }
  }

  // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta

  // Primeiro verifica se deve trocar o nome do arquivo
  if ($_UP['renomeia'] == true) {
    // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
    $imagem1 = $id_pro.'_1.jpg';
    $imagem2 = $id_pro.'_2.jpg';
    $imagem3 = $id_pro.'_3.jpg';
    $imagem4 = $id_pro.'_4.jpg';
    $imagem5 = $id_pro.'_5.jpg';
  }

  // Caso seja escolhido o tipo Dispensar Vistoria, excluir fotos.
  if ( $situacaoprojeto == 12 ) {
    unlink($_UP['pasta'] . $imagem1);
    unlink($_UP['pasta'] . $imagem2);
    unlink($_UP['pasta'] . $imagem3);
    unlink($_UP['pasta'] . $imagem4);
    unlink($_UP['pasta'] . $imagem5);
  }
  // Depois verifica se é possível mover o arquivo para a pasta escolhida
  if ( !empty($_FILES['imagem1']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem1']['name']) ) {
    move_uploaded_file($_FILES['imagem1']['tmp_name'], $_UP['pasta'] . $imagem1);
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem2']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem2']['name'])) {
    move_uploaded_file($_FILES['imagem2']['tmp_name'], $_UP['pasta'] . $imagem2);
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem3']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem3']['name'])) {
    move_uploaded_file($_FILES['imagem3']['tmp_name'], $_UP['pasta'] . $imagem3);
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem4']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem4']['name'])) {
    move_uploaded_file($_FILES['imagem4']['tmp_name'], $_UP['pasta'] . $imagem4);
  }
  if ( $id_ana > 0 and !empty($_FILES['imagem5']['name']) or $id_ana == 0 and $situacaoprojeto != 12 and !empty($_FILES['imagem5']['name'])) {
    move_uploaded_file($_FILES['imagem5']['tmp_name'], $_UP['pasta'] . $imagem5);
  }

  
  if ( $situacaoprojeto != 15 ) {
    $descricao_img1 = $_POST['descricao_img1'];
    $descricao_img2 = $_POST['descricao_img2'];
    $descricao_img3 = $_POST['descricao_img3'];
    $descricao_img4 = $_POST['descricao_img4'];
    $descricao_img5 = $_POST['descricao_img5'];
  }
  if ( $id_ana > 0 ) {
    $formulario = 'alteracao';
  } else {
    $formulario = 'cadastro';
  } 
  

  $id_ana2 = $func_analise->manipulacoes(
      $id_ana,
      $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'),
      $id_pro,
      'fi',
      $situacaoprojeto,
      $obsgerais,
      $descricao_img1,   
      $descricao_img2,   
      $descricao_img3,   
      $descricao_img4,   
      $descricao_img5,
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,   
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,  
      f,
      $formulario
    );

    if ( $id_ana == 0 ) {
      $id_ana = $id_ana2;
    }

    //  Pega nome e e-mail do cliente e usuário.
    $tusuario = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos

      INNER JOIN geral.cg ON cg.id_cg = processos.id_cg
      WHERE processos.id_pro = $id_pro");
    $tusuario = $tusuario->fetch();

    $tautor = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos_analise

      INNER JOIN geral.cg ON cg.id_cg = processos_analise.id_cg
      WHERE processos_analise.id_ana = $id_ana");
    $tautor = $tautor->fetch();

    //--Procedimento envio de e-mail (usando Mailjet)--


    if ( $tipoprocesso == 1 ) {
      $descr_tipoprocesso = 'Alvará de Construção';
    } else if ( $tipoprocesso == 2 ) {
      $descr_tipoprocesso = 'Alvará de Regularização de Obras';
    } else if ( $tipoprocesso == 3 ) {
      $descr_tipoprocesso = 'Alvará de Acréscimo de Área';
    } else if ( $tipoprocesso == 4 ) {
      $descr_tipoprocesso = 'Condomínio Edilício';
    } else if ( $tipoprocesso == 5 ) {
      if ( $listagem_redimensionamento['tipo'] == 1 ) {
        $descr_tipoprocesso = 'Redimensionamento - Remembramento';
      } else if ( $listagem_redimensionamento['tipo'] == 2 ) {
        $descr_tipoprocesso = 'Redimensionamento - Desmembramento';
      }
    } else if ( $tipoprocesso == 15 ) {
      $descr_tipoprocesso = 'Processo não encaminhado para análise: Inconsistência na vistoria';
    }


    /*
      1 - A ser analisado = white, 
      2 - Em análise = #ddd, 
      3 - Pendência de documentos ou correção = #c4d79b, 
      4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
      5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
      6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
      7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
      8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
      9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
      10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
      11 - Aprovado = #eada6d, 
      12 - Dispensar Vistoria
    */

    if ( $situacaoprojeto == 1 ) {
      $descr_situacaoprojeto = 'A ser analisado '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 2 ) {
      $descr_situacaoprojeto = 'Documentos checados e encaminhados para análise - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 3 ) {
      $descr_situacaoprojeto = 'Insuficiente - Pendência de Documentos ou correção - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 4 ) {
      $descr_situacaoprojeto = 'Reprovado - Processo não permitido ou reprovado na análise/vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 5 ) {
      $descr_situacaoprojeto = 'Processo encaminhado à procuradoria - Dúvida na vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 6 ) {
      $descr_situacaoprojeto = 'Processo encaminhado ao departamento imobiliário - Dúvida na vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 7 ) {
       $descr_situacaoprojeto = 'Aprovado - Pendente de pagamento de taxa para elaboração do decreto - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 8 ) {
      $descr_situacaoprojeto = 'Aprovado - Fazer decreto e pegar assinatura - com taxa paga - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 9 ) {
      $descr_situacaoprojeto = 'Aprovado - Decreto assinado - para entregar - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 10 ) {
      $descr_situacaoprojeto = 'Aprovado - Processo finalizado e decreto entregue'.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 11 ) {
      $descr_situacaoprojeto = 'Aprovado'.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 12 ) {
      $descr_situacaoprojeto = 'Documentos encaminhados para análise - Vistoria dispensada - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 15 ) {
      $descr_situacaoprojeto = 'Motivos prováveis: Projeto em desacordo com a obra; fiscal não teve acesso para vistoria. - '.date('d/m/Y H:i');
    }

    require 'vendor/autoload.php';

    $resources = new \Mailjet\Resources();

    $api1 = 'f74843b63a568c5d7116cff5d971eb45';
    $api2 = '06fed94a47fa0688ef15fd36453ec365';

    $mj = new \Mailjet\Client($api1, $api2);
    $body = [
        'FromEmail' => "nao-responda@valleteclab.com",
        'FromName' => "Prefeitura de Luís Eduardo Magalhães - XPlanejamento",
        'Subject' => 'Fiscalização de Processo [ '.$descr_tipoprocesso.' ]',
        'Html-part' => '[ E-mail resposta, por favor não responda esse e-mail ] <br />
          Prezado(a) Senhor(a),<br />
          <br /><br />
          Status de processo de <strong>'.$descr_tipoprocesso.'</strong>, Protocolo: '.$id_pro.'<br /> 
          <strong>Situação do projeto:</strong><br />
          '.$descr_situacaoprojeto.'
          <br />
          <strong>Observações</strong><br />
          '.$obsgerais.'
          <br />
          <br />
          www.luiseduardomagalhaes.ba.gov.br/planejamento <br />
          <br /><br />
          Agradecemos o contato!',
        'Recipients' => [
            [
                'Email' => "".$tusuario['email'].""
            ]
        ]
    ];
    $response = $mj->post($resources::$Email, ['body' => $body]);
    $response->success() && var_dump($response->getData());

    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Procedimento efetuado com sucesso!');
              window.open('listar_processos.php?tipoprocesso=<?php echo $tipoprocesso; ?>','_self');
          }, 1000);
        });
    </script>
    <?php
}

$_SESSION["empresa_upload"] = 4;
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a>Fiscalização</a>
            </li>

          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <div class="panel-body bg-light">

                <?php 
                  if ( $linha2['tipoprocesso'] == 1 or $linha2['tipoprocesso'] == 2 or $linha2['tipoprocesso'] == 3 or $linha2['tipoprocesso'] == 6 ) {
                    include_once('includes/info_alvara.php');
                  } else if ( $linha2['tipoprocesso'] == 4 ) {
                    include_once('includes/info_condominio.php');
                  } else if ( $linha2['tipoprocesso'] == 5 ) {
                    include_once('includes/info_redimensionamento.php');
                  }
                ?>
                <br />
                <br />
                <iframe src="jqueryUpload.php" frameborder="0" width="600" height="500"></iframe>
                <?php //include('jqueryUpload.php'); ?>
                <form method="post" id="admin-form" enctype="multipart/form-data">
                  <input type="hidden" name="formulario" value="ok">
                  <input type="hidden" name="id_ana" value="<?php echo $linha['id_ana']; ?>">
                  <div class="panel-body bg-light">

                    <div class="row">
                      <div class="col-md-6">
                        <div class="section">
                          <div id="imagens" style="<?php if ( $linha['situacaoprojeto'] == 12 or $linha['situacaoprojeto'] == 15 ) { ?>display: none; <?php } ?>">
                            <div class="section-divider mt20 mb40">
                            <span> Imagens do Local </span>
                          </div>
                            <?php if (file_exists('vistoria/'.$id_pro.'_1.jpg')) { ?><a onclick="pg_modal('modals/removerFoto.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&id_foto=1','');"><strong>remover foto</strong></a><?php } ?>
                            <label class="field prepend-icon append-button file">
                              <span class="button">Imagem 1</span>
                              <input type="file" class="gui-file" name="imagem1" id="imagem1" onChange="document.getElementById('uploader1').value = this.value;">
                              <input type="text" class="gui-input" name="uploader1" id="uploader1" placeholder="Escolha uma imagem" value="<?php if (file_exists('vistoria/'.$id_pro.'_1.jpg')) { ?>vistoria/<?php echo $id_pro; ?>_1.jpg<?php } ?>">
                              <label class="field-icon">
                                <i class="fa fa-upload"></i>
                              </label>
                            </label>
                            <?php if (file_exists('vistoria/'.$id_pro.'_1.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_1.jpg" target="_blank">Visualizar Imagem 1</a>
                            <?php } ?>
                            <div class="section" id="spy3">
                              <label for="descricao_img1" class="field prepend-icon">
                                <textarea class="gui-textarea" id="descricao_img1" name="descricao_img1"><?php echo $linha['descricao_img1']; ?></textarea>
                                <label for="descricao_img1" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                            </div>
                            
                            <?php if (file_exists('vistoria/'.$id_pro.'_2.jpg')) { ?><a onclick="pg_modal('modals/removerFoto.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&id_foto=2','');"><strong>remover foto</strong></a><?php } ?>
                            <label class="field prepend-icon append-button file">
                              <span class="button">Imagem 2</span>
                              <input type="file" class="gui-file" name="imagem2" id="imagem2" onChange="document.getElementById('uploader2').value = this.value;">
                              <input type="text" class="gui-input" id="uploader2" placeholder="Escolha uma imagem"  value="<?php if (file_exists('vistoria/'.$id_pro.'_2.jpg')) { ?>vistoria/<?php echo $id_pro; ?>_2.jpg<?php } ?>">
                              <label class="field-icon">
                                <i class="fa fa-upload"></i>
                              </label>
                            </label>
                            <?php if (file_exists('vistoria/'.$id_pro.'_2.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_2.jpg" target="_blank">Visualizar Imagem 2</a>
                            <?php } ?>
                            <div class="section" id="spy3">
                              <label for="descricao_img2" class="field prepend-icon">
                                <textarea class="gui-textarea" id="descricao_img2" name="descricao_img2"><?php echo $linha['descricao_img2']; ?></textarea>
                                <label for="descricao_img2" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                            </div>

                            <?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?><a onclick="pg_modal('modals/removerFoto.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&id_foto=3','');"><strong>remover foto</strong></a><?php } ?>
                            <label class="field prepend-icon append-button file">
                              <span class="button">Imagem 3</span>
                              <input type="file" class="gui-file" name="imagem3" id="imagem3" onChange="document.getElementById('uploader3').value = this.value;" value="<?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?>vistoria/<?php echo $id_pro; ?>_1.jpg<?php } ?>">
                              <input type="text" class="gui-input" id="uploader3" placeholder="Escolha uma imagem"  value="<?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?>>vistoria/<?php echo $id_pro; ?>_3.jpg<?php } ?>">
                              <label class="field-icon">
                                <i class="fa fa-upload"></i>
                              </label>
                            </label>
                            <?php if (file_exists('vistoria/'.$id_pro.'_3.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_3.jpg" target="_blank">Visualizar Imagem 3</a>
                            <?php } ?>
                            <div class="section" id="spy3">
                              <label for="descricao_img3" class="field prepend-icon">
                                <textarea class="gui-textarea" id="descricao_img3" name="descricao_img3"><?php echo $linha['descricao_img3']; ?></textarea>
                                <label for="descricao_img3" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                            </div>

                            <?php if (file_exists('vistoria/'.$id_pro.'_4.jpg')) { ?><a onclick="pg_modal('modals/removerFoto.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&id_foto=4','');"><strong>remover foto</strong></a><?php } ?>
                            <label class="field prepend-icon append-button file">
                              <span class="button">Imagem 4</span>
                              <input type="file" class="gui-file" name="imagem4" id="imagem4" onChange="document.getElementById('uploader4').value = this.value;">
                              <input type="text" class="gui-input" id="uploader4" placeholder="Escolha uma imagem"  value="<?php if (file_exists('vistoria/'.$id_pro.'_4.jpg')) { ?>vistoria/<?php echo $id_pro; ?>_4.jpg<?php } ?>">
                              <label class="field-icon">
                                <i class="fa fa-upload"></i>
                              </label>
                            </label>

                            <?php if (file_exists('vistoria/'.$id_pro.'_4.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_4.jpg" target="_blank">Visualizar Imagem 4</a>
                            <?php } ?>

                            <div class="section" id="spy3">
                              <label for="descricao_img4" class="field prepend-icon">
                                <textarea class="gui-textarea" id="descricao_img4" name="descricao_img4"><?php echo $linha['descricao_img4']; ?></textarea>
                                <label for="descricao_img4" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                            </div>


                            <?php if (file_exists('vistoria/'.$id_pro.'_5.jpg')) { ?><a onclick="pg_modal('modals/removerFoto.php?id_pro=<?php echo (int)$_GET['id_pro']; ?>&id_foto=5','');"><strong>remover foto</strong></a><?php } ?>
                            <label class="field prepend-icon append-button file">
                              <span class="button">Imagem 5</span>
                              <input type="file" class="gui-file" name="imagem5" id="imagem5" onChange="document.getElementById('uploader5').value = this.value;">
                              <input type="text" class="gui-input" id="uploader5" placeholder="Escolha uma imagem"  value="<?php if (file_exists('vistoria/'.$id_pro.'_5.jpg')) { ?>vistoria/<?php echo $id_pro; ?>_5.jpg<?php } ?>">
                              <label class="field-icon">
                                <i class="fa fa-upload"></i>
                              </label>
                            </label>

                            <?php if (file_exists('vistoria/'.$id_pro.'_5.jpg')) { ?>
                            <a href="vistoria/<?php echo $id_pro; ?>_5.jpg" target="_blank">Visualizar Imagem 5</a>
                            <?php } ?>

                            <div class="section" id="spy3">
                              <label for="descricao_img5" class="field prepend-icon">
                                <textarea class="gui-textarea" id="descricao_img5" name="descricao_img5"><?php echo $linha['descricao_img5']; ?></textarea>
                                <label for="descricao_img5" class="field-icon">
                                  <i class="fa fa-comments"></i>
                                </label>
                              </label>
                            </div>

                          </div>
                          <div class="section-divider mt20 mb40">
                            <span> Observações Gerais </span>
                          </div>
                          
                          <div class="section" id="spy3">
                            <label for="comment" class="field prepend-icon">
                              <textarea class="gui-textarea" id="obsgerais" name="obsgerais"><?php echo $linha['obsgerais']; ?></textarea>
                              <label for="comment" class="field-icon">
                                <i class="fa fa-comments"></i>
                              </label>
                            </label>
                          </div>


                          <div class="option-group field"> 
                            <div class="section row">
                                <div class="col-md-8">                       
                                    <label class="option option-primary">
                                    <input name="situacaoprojeto" type="radio" value="2" onclick="apFotos();" <?php if ( $linha['situacaoprojeto'] == 2 ) { echo "checked='checked'"; } ?>>
                                    <span class="radio"></span>Encaminhar para análise</label>
                                </div>
                            </div>
                            <div class="section row">
                                <div class="col-md-8"> 
                                    <label class="option option-primary">
                                    <input name="situacaoprojeto" id="statusdispensa" type="radio" value="12" onclick="apFotos();" <?php if ( $linha['situacaoprojeto'] == 12 ) { echo "checked='checked'"; } ?>>
                                    <span class="radio"></span>Dispensar Vistoria</label>
                                </div>
                            </div>
                            <div class="section row">
                                <div class="col-md-11"> 
                                    <label class="option option-primary">
                                    <input name="situacaoprojeto" id="statusdispensa" onclick="apFotos();" type="radio" value="15" <?php if ( $linha['situacaoprojeto'] == 15 ) { echo "checked='checked'"; } ?>>
                                    <span class="radio"></span><strong>Inconsistência na vistoria</strong><br />  - Processo não encaminhado para análise;<br />
                                      - Projeto em desacordo com a obra;<br />
                                      - Fiscal não teve acesso para vistoria.
                                  </label>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


                      <div class="panel-footer text-left">
                        <button type="submit" class="button btn-success">Atualizar</button>
                      </div>
            </div>
            <div id="sidebar-right-tab2" class="tab-pane"></div>
            <div id="sidebar-right-tab3" class="tab-pane"></div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    function apFotos(){ 
        if ( $('input[id=statusdispensa]').is(':checked')) {
            $("#imagens").css("display","none");
          }else{
            $("#imagens").fadeIn(500);
          } 
      }
      function ap30(){ 
        if ( $('input[name=checkocultdetalhes]').is(':checked')) {
            $("#camposocultosmaisdetana").fadeIn(500);
          }else{
            $("#camposocultosmaisdetana").css("display","none");
          } 
      }
  </script>
  
  <script type="text/javascript">
  jQuery(document).ready(function() {


    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        <?php if ( $linha['id_ana'] > 0 ) { ?>
          uploader1: {
            required: function(element) { 
              if ($('input[name=statusdispensa]').is(':checked')){
                required: true
              }
            }
          },
        <?php } else { ?>
          imagem1: {
            required: function(element) { 
              if ($('input[name=statusdispensa]').is(':checked')){
                required: true
                extension: "jpg|png|gif|jpeg"
              }
            }
          },         
        <?php } ?>
         
        descricao_img1: {
          required: true
        },
        obsgerais: {
          required: true
        },
        status: {
          required: true
        }

      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        <?php if ( $linha['id_ana'] > 0 ) { ?>
          uploader1: {
            required: 'Envie a imagem 1'
          },
        <?php } else { ?>
          imagem1: {
            required: 'Envie a imagem 1'
          },
        <?php } ?>
        descricao_img1: {
          required: 'Coloque as descrições sobre cada foto'
        },
        obsgerais: {
          required: 'Coloque um breve observação geral'
        },
        status: {
          required: 'Informe a situação da vistoria'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>