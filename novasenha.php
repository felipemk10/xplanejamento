<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao            = new conexao;
$configuracoes      = new configuracoes;
$validacoes         = new validacoes;
$formatacoes        = new formatacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
//$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));
//---



$link = $configuracoes->consulta("SELECT usuarios.id_cg, usuarios.senha FROM geral.recuperasenha 
  INNER JOIN geral.usuarios ON usuarios.id_cg = recuperasenha.id_cg
  
  WHERE recuperasenha.id = ".(int)$formatacoes->criptografia($_GET['hash'],'base64','decode')." and recuperasenha.linkativo = true");

if ( $link->rowCount() == 0 ) {
  echo '<h1>Link inválido.</h1>';exit();
}


if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $link = $link->fetch();

   
    $senha        = $validacoes->anti_injection($_POST["senha"]);
    $repitasenha  = $validacoes->anti_injection($_POST["repitasenha"]);

    $sql = $conexao->prepare("UPDATE geral.usuarios SET 
        senha = '".md5($senha)."'
      WHERE 
        id_cg = ".$link['id_cg']."");
    $sql->execute();

    $sql = $conexao->prepare("UPDATE geral.recuperasenha SET 
        linkativo = false
      WHERE 
        id_cg = ".$link['id_cg']."");
    $sql->execute();

    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Senha alterada com sucesso! Você será redirecionado para fazer seu login!');
              window.open('login.php','_self');
          }, 1000);
        });
    </script>
    <?php

}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body class="external-page external-alt sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content" class="animated fadeIn">

        <div class="admin-form theme-info mw500" style="margin-top: 10%;" id="login">
          <div class="row mb15 table-layout">

            <div class="col-xs-6 pln">
              <a href="dashboard.html" title="Return to Dashboard">
                <img src="img/vale.jpeg" title="XPlanejamento Logo" class="img-responsive w250">
              </a>
            </div>
          </div>

          <div class="panel">
            <form method="post" action="" id="resetar">
              <!-- end .form-body section -->
              <div class="panel-footer p25 pv15">
                <div class="section mn">
                  <div class="smart-widget sm-right smr-80">
                    <div class="section row">
                      <label for="senha" class="field">
                        <input type="password" name="senha" id="senha" class="gui-input" placeholder="Digite sua nova senha">
                      </label>
                    </div>
                    <div class="section row">
                      <label for="repitasenha" class="field">
                        <input type="password" name="repitasenha" id="repitasenha" class="gui-input" placeholder="Digite sua nova senha novamente">
                      </label>
                    </div>
                  </div>
                  <div class="section row">
                    <button type="submit" class="button btn-success">Alterar</button>
                  </div>
                  <?php if ( !empty($msg_formulario) ) { ?>
                  <div class="panel-footer clearfix" style="text-align:center;">
                    <a href="#" class="btn btn-danger mb10 mr5 notification" data-note-style="danger"><?php echo $msg_formulario; ?></a>
                  </div>
                  <?php } ?>
                  <!-- end .smart-widget section -->

                </div>
                <!-- end section -->
        
              </div>
              <!-- end .form-footer section -->

            </form>

          </div>

        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>
  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    //xDemo.init();

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#resetar").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        senhaatual: {
            required: true
        },
        senha: {
            required: true
        },
        repitasenha: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#senha'
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        senhaatual: {
          required: 'Informe sua senha atual'
        },
        senha: {
          required: 'Informe sua senha'
        },
        repitasenha: {
          required: 'Insira a mesma senha informada acima',
          equalTo: 'Senhas não estão iguais'
        }
      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>
