<?php
class cidades {
	//	Se a apisuporte funciona na cidade solicitada.
	function validar_cidade_com_apisuporte($id_cidade)  {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		$id_cidade = (INT)$id_cidade;
		$this->cidade = $this->classe_conexao->query("SELECT id, uf, nome FROM opt_cidades WHERE id = '$id_cidade' and funcionamento = 's'")->fetchAll();
		
		if ( count($this->cidade) == 1 ) {
			return true;
		} else {
			return false;
		}
	}
	//	Lista as cidades com a apisuporte em funcionamento.
	function cidades_com_apisuporte($ddd) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		if ( $ddd > 0 ) {
			return $this->classe_conexao->query("SELECT id, uf, nome, sigla, ddd FROM opt_cidades WHERE ddd = '$ddd' and funcionamento = 's'");
		} else {
			return $this->classe_conexao->query("SELECT id, uf, nome, sigla, ddd FROM opt_cidades WHERE funcionamento = 's'")->fetchAll();
		}
	}
	//	Dados das cidades com apisuporte.
	function dados_cidade_com_apisuporte($id_cidade) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$id_cidade = (INT)$id_cidade;
		$this->classe_conexao = new conexao;
		$this->cidade = $this->classe_conexao->query("SELECT id, uf, nome, sigla, ddd FROM opt_cidades WHERE id = '$id_cidade' and funcionamento = 's'");
		return $this->cidade->fetch();
	}
}