<?php
class empresas_validacoes {
	//	Mensagens de erro padrões no site.
	function mensagens_padrao($numero_msg) {
		switch ( $numero_msg ){
			//	Usuário
			case 1: $this->msg = "Data de nascimento inválida."; break;
			case 2: $this->msg = 'Nível de privacidade inválido.'; break;
			case 3: $this->msg = 'Nos diga qual cidade você mora.'; break;
			case 4: $this->msg = 'Selecione seu gênero. Ex: Masculino ou Feminino.'; break;
			case 5: $this->msg = 'Preencha sua data de nascimento.'; break;
			case 6: $this->msg = 'Selecione uma cidade válida.'; break;
			case 7: $this->msg = 'Escreva sobre você.'; break;
			case 8: $this->msg = 'Ex: Masculino ou Feminino'; break;
			case 9: $this->msg = 'Nos diga onde você mora.'; break;
			case 10: $this->msg = 'Escreva uma breve descrição.'; break;
			case 11: $this->msg = 'Digite seu nome'; break;
			case 12: $this->msg = 'Digite seu sobrenome'; break;
			case 13: $this->msg = 'Seu nome possui poucos caracteres.'; break;
			case 14: $this->msg = 'Seu nome possui muitos caracteres.'; break;
			case 15: $this->msg = 'Seu sobrenome possui muitos caracteres.'; break;
			case 16: $this->msg = 'Seu sobrenome possui muitos caracteres.'; break;
			case 17: $this->msg = 'Sua descrição possui muitos caracteres.'; break;
			case 18: $this->msg = 'Você não pode mais digitar.'; break;
			//	Empresa
			case 19: $this->msg = 'Relate sobre a empresa.'; break;
			case 20: $this->msg = 'Em qual data a empresa foi fundada?'; break;
			case 21: $this->msg = "É necessário informar o mês."; break;
			case 22: $this->msg = "A data informada está incorreta."; break;
			//	Fone
			case 23: $this->msg = "Insira o fone"; break;
			case 24: $this->msg = "Digite apenas números"; break;
			case 25: $this->msg = "Número de caracteres inválido"; break;
			case 26: $this->msg = "Escolha uma operadora válida para o fone"; break;
			case 27: $this->msg = "Digite um número válido de sua localidade"; break;
			//	E-mail
			case 28: $this->msg = "Insira o e-mail"; break;
			case 29: $this->msg = "Digite um e-mail válido"; break;
			//---
			case 30: $this->msg = "Você o limite de caracteres"; break;
			//	Site
			case 31: $this->msg = "Digite uma url válida"; break;
			//	Empresa
			case 32: $this->msg = 'Digite o nome fantasia'; break;
			case 33: $this->msg = 'Digite a razão social'; break;
			case 34: $this->msg = 'Seu nome fantasia possui poucos caracteres.'; break;
			case 35: $this->msg = 'Seu nome fantasia possui muitos caracteres.'; break;
			case 36: $this->msg = 'A razão social possui muitos caracteres.'; break;
			case 37: $this->msg = 'A razão social possui muitos caracteres.'; break;
			//	Consulta_empresa
			case 38: $this->msg = 'Digite o nome fantasia ou razão social.'; break;
			case 39: $this->msg = 'O nome possui poucos caracteres.'; break;
			case 40: $this->msg = 'O nome possui muitos caracteres.'; break;
			//	Adicionar colaborador.
			case 41: $this->msg = 'Digite o nome do colaborador.'; break;
			case 42: $this->msg = 'Escolha um colaborador válido.'; break;
			case 43: $this->msg = 'Este colaborador já está conectado nesta empresa.'; break;
			case 44: $this->msg = 'Escolha o tipo para o colaborador.'; break;
			case 45: $this->msg = 'Escolha um tipo de usuário válido.'; break;
			//	Dados empresa.
			case 46: $this->msg = 'Digite o seu CPF.'; break;
			case 47: $this->msg = 'Digite o seu CPF corretamente.'; break;
			case 48: $this->msg = 'Este CPF está sendo utilizado por outro usuário.'; break;
			case 49: $this->msg = 'Escolha um tipo de usuário válido.'; break;
			case 50: $this->msg = 'Este CPF não confere com os registros. SAP 3628-1120.'; break;
			case 51: $this->msg = 'Este CNPJ está sendo utilizado por outra empresa.'; break;
			case 52: $this->msg = 'Este CNPJ não confere com os registros. SAP 3628-1120.'; break;
			case 53: $this->msg = 'Digite uma url para a página de sua empresa.'; break;
			case 54: $this->msg = 'Digite a url corretamente, sem acentos, símbolos e espaços.'; break;
			case 55: $this->msg = 'Digite no mínimo 5 letras para url.'; break;
			case 56: $this->msg = 'Digite menos de 30 caracteres para url.'; break;
			case 57: $this->msg = 'Está url está sendo utilizada por outra empresa.'; break;
			case 58: $this->msg = 'Digite o endereço onde está sua empresa.'; break;
			case 59: $this->msg = 'Digite mais caracteres no campo de endereço.'; break;
			case 60: $this->msg = 'Digite menos caracteres no campo de endereço.'; break;
			case 61: $this->msg = 'Nos diga qual o tipo de logradouro.'; break;
			case 62: $this->msg = 'Escolha corretamente o tipo de logradouro.'; break;
			case 63: $this->msg = 'Digite o bairro'; break;
			case 64: $this->msg = 'Digite mais caracteres no campo de bairro.'; break;
			case 65: $this->msg = 'Digite menos caracteres no campo de bairro.'; break;
			case 66: $this->msg = 'Digite o número de endereço'; break;
			case 67: $this->msg = 'Digite apenas números.'; break;
			case 68: $this->msg = 'Digite menos caracteres no campo de número.'; break;
			case 69: $this->msg = 'Digite o número de cep.'; break;
			case 70: $this->msg = 'Digite apenas números no cep.'; break;
			case 71: $this->msg = 'Digite um cep válido.'; break;
			case 72: $this->msg = 'Digite mais caracteres no campo de número.'; break;
			case 73: $this->msg = 'Preencha ao menos um complemento.'; break;
			case 74: $this->msg = 'Escolha a cidade.'; break;
			case 75: $this->msg = 'Escolha a operadora corretamente para o número.'; break;
			//	Validar fone.
			case 76: $this->msg = 'Digite o código de verificação enviado válido!'; break;
			//	Palavras chaves
			case 77: $this->msg = 'Digite as palavras chave.'; break;
			case 78: $this->msg = 'Suas palavras chave estão pequenas.'; break;
			//	Contrato de adesão.
			case 79: $this->msg = 'Para continuar, você precisa concorda com o contrato de adesão.'; break;
			//	Adicionar empresa.
			case 80: $this->msg = 'Você já está conectado nesta empresa.'; break;
			//	URL
			case 81: $this->msg = 'Está url não está disponível, digite outra.'; break;
		}
		return $this->msg;
	}

	function validar_campo($id_empresa, $campo, $valor, $cidade) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		// Mensagens pré-configuradas
		$this->branco         =   "Preencha o campo";
		$this->letras         =   "Digite letras além de números";
		$this->numeros        =   "Digite apenas números!";
		$this->info_igual     =   "Existe existe uma empresa com este nome!";
		$this->mais_carac     =   "Digite mais caracteres";
		$this->menos_carac    =   "Digite menos caracteres";
		$this->prefixos       =   "Prefixo inválido!";
		$this->num_invalido   =   "Digitos não conferem!";
		$this->sac            =   "Número SAC não confere!";
		$this->cnpj           =   "CNPJ inválido!";
		$this->cpf            =   "CPF inválido!";
		$this->email          =   "E-mail inválido!";
		$this->erro_site      =   "Site inválido!";
		$this->erro_cnpj      =   "O numero do CNPJ está inválido!";
		$this->erro_cpf       =   "O numero do CPF está inválido!";
		$this->erro_rg        =   "O numero do RG está inválido!";
		
		$this->classe_validacoes = new validacoes;
		
		if ( $campo == 'fantasia' ) {
			$this->classe_conexao = new conexao;
			
			$this->fantasia = $this->classe_conexao->query("SELECT fantasia FROM empresas WHERE fantasia = '$valor' and id != '$id_empresa' and id_cidade = '$cidade' and ativo = 's'");
            
            if ( $valor == NULL ) {
            	return $this->branco.' fantasia!';
            } elseif ( strlen($valor) < 3 ) {
                return $this->mais_carac.' no campo FANTASIA!';
            } elseif ( strlen($valor) > 50 ) {
                return $this->menos_carac.' no campo FANTASIA!';
            } elseif ( is_numeric($valor) ) {
                return $this->letras.' no campo fantasia!';
            } else if ( $this->fantasia->rowCount() == 1 ) {
            	return $this->info_igual;
            } else {
                return true;
            }
        }
        
        if ( $campo == 'razao' ) {
			if ( $valor != NULL ) {
				$this->classe_conexao = new conexao;
				
				$this->razao = $this->classe_conexao->query("SELECT razao FROM empresas WHERE razao = '$valor' and id != '$id_empresa' and id_cidade = '$cidade' and ativo = 's'");
				
	            if ( strlen($valor) < 3 ) {
	                return $this->mais_carac.' no campo RAZÃO!';
	            } elseif ( strlen($valor) > 50 ) {
	                return $this->menos_carac.' no campo RAZÃO!';
	            } elseif ( is_numeric($valor) ) {
	                return $this->letras.' no campo razao!';
	            } else if ( $this->razao->rowCount() == 1 ) {
	            	return $this->info_igual;
	            } else {
	                return true;
	            }
            } else {
                return true;
            } 
        }
        
        if ( $campo == 'cnpj' ) {
        	if ( $valor != NULL ) {
        		$this->classe_conexao = new conexao;
        		
        		$this->sql = $this->classe_conexao->query("SELECT id FROM empresas WHERE cnpj = '$valor' and id != '$id_empresa' and ativo = 's'")->rowCount();
        		
				if ( strlen($valor) < 14 ) {
					return $this->mais_carac.' no campo CNPJ!';
				} elseif ( strlen($valor) > 14 ) {
					return $this->menos_carac.' no campo CNPJ!';
				} elseif ( !$this->classe_validacoes->cnpj($valor) ) {
					return $this->erro_cnpj;
				} else if ( $this->sql == 1 ) {
					return 'Este CNPJ já está sendo utilizado.';
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
		if ( $campo == "email" ) {
			$valor = $this->classe_validacoes->anti_injection(trim($valor));
			$valor = strtolower($valor);
			if ( $valor != NULL ) {
				if ( strlen($valor) < 8 ) {
					return $this->mais_carac.' no campo E-MAIL!';
				} else if ( strlen($valor) > 50 ) {
					return $this->menos_carac.' no campo E-MAIL!';
				} else if ( !$this->classe_validacoes->email($valor) ) {
					return $this->email;
				} else {
					return true;
				}
			} else {
				return true;
			}
		}
		if ( $campo == "palavras_chave" ) {
			$valor = strtolower($valor);
			if ( $valor == NULL ) {
            	return $this->branco.' PALAVRAS-CHAVE!';
            } else if ( is_numeric($valor) ) {
				return $this->letras.' no campo PALAVRAS-CHAVE!';
			} else {
				return true;
			}
		}
	}
	
	function fones_iguais($id_fone1,$id_fone2,$id_fone3) {
		if ( !empty($id_fone1) and !empty($id_fone2) and $id_fone1 == $id_fone2 or 
		!empty($id_fone2) and !empty($id_fone3) and $id_fone2 == $id_fone3 or 
		!empty($id_fone1) and !empty($id_fone3) and $id_fone1 == $id_fone3 ) {
			return 'Existem fones iguais';
		} else {
			return true;
		}
	}
	
	function fone($cidade,$ddd, $fone, $descricao, $obrigatorio) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classes.
		$this->classe_fone = new fone;
		$this->classe_formatacoes = new formatacoes;
		//---
		
		$fone = str_replace(' ', '', trim($this->classe_formatacoes->retira_simbolos($fone)));
		
		if ( $this->classe_fone->Tipo_Fone($fone) == 'cel' or $this->classe_fone->Tipo_Fone($fone) == 'tel' ) {
			if ( $ddd > 0 and strlen($fone) == 8 ) {
				$fone = '55'.$ddd.$fone;

			} else if ( $ddd == 0 and strlen($fone) == 8 ) {
				return 'Não se esqueça de colocar o DDD.';
			} else if ( $ddd == 0 and strlen($fone) == 10 ) {
				$fone = '55'.$fone;
			}

			if ( strlen($fone) == 10 and substr($fone,0,2) == $ddd ) {
				$fone = '55'.$fone;
			} else if ( strlen($fone) == 12 and substr($fone,0,4) == '55'.$ddd ) {
				$fone = $fone;
			}
			/*	Observações
			Caso cidade for igual a zero não ocorrerá validação de localidade, 
			somente para celular.
			*/
			if ( $cidade > 0 and substr($fone,2,2) != $ddd ) {
				return 'Digite um número válido de sua localidade.'; 
			} 
		}
		
		if ( !empty($fone) ) {
			if ( $this->classe_fone->Tipo_Fone($fone) == 'tel' and $this->classe_fone->Validar_Prefixo_Cidade($fone,$cidade) == true or 
				$this->classe_fone->Tipo_Fone($fone) == 'cel' and $this->classe_fone->Validar_Prefixo_Cidade($fone,$cidade) == true ) {
				return $fone;
			} else if ( $this->classe_fone->Tipo_Fone($fone) == 'tel' and $this->classe_fone->Validar_Prefixo_Cidade($fone,$cidade) == false or 
				$this->classe_fone->Tipo_Fone($fone) == 'cel' and $this->classe_fone->Validar_Prefixo_Cidade($fone,$cidade) == false ) {
				return 'Digite um número válido de sua localidade';
			} else if ( $this->classe_fone->Tipo_Fone($fone) == '0800' or $this->classe_fone->Tipo_Fone($fone) == '4003' or $this->classe_fone->Tipo_Fone($fone) == 'especiais' ) {
				return $fone;
			} else {
				return 'Digite corretamente '.$descricao.'.';
			}
		} else {
			if ( $obrigatorio == 'sim' ) {
				return ucfirst($descricao).' é obrigatório.';
			} else {
				return true;
			}	
		}
	}
}