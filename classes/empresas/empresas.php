<?php
class empresas {
	//	Verifica se a empresa possui uma página ativa.
	function possui_pagina($id_empresa) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		$this->numero_registro = $this->con_empresa = $this->classe_conexao->query("SELECT id FROM fin_negociacoes
		WHERE id_empresa = '$id_empresa' and ativo = 's' ORDER BY data_registro DESC LIMIT 1")->rowCount();
		
		if ( $this->numero_registro > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	function imprimir_logradouro($id_logradouro) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		return $this->classe_conexao->query("SELECT opt_logradouros.id, opt_enderecos.tipo_end, opt_enderecos.nome AS endereco, opt_bairros.nome AS bairro, opt_logradouros.cep FROM opt_logradouros 
		INNER JOIN opt_enderecos ON opt_enderecos.id = opt_logradouros.id_end
		INNER JOIN opt_bairros ON opt_bairros.id = opt_logradouros.id_bairro
		
		WHERE opt_logradouros.id = '$id_logradouro'")->fetch();
	}
	
	function imprimir_email($id_email) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		$this->con_email = $this->classe_conexao->query("SELECT email FROM emails WHERE id = '$id_email' and ativo = 's'")->fetch();
		return $this->con_email = $this->con_email['email'];
	}
	
	function imprimir_fone($id_fone) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		$this->classe_fone = new fone;
		
		$this->con_fone = $this->classe_conexao->query("SELECT id_operadora, fone FROM fones WHERE id = '$id_fone' and ativo = 's'")->fetch();
		if ( $this->con_fone['fone'] > 0 ) {
			return $this->classe_fone->Formatar_Fone_Dinamica2($this->con_fone['fone']);
		}
	}
	
	function consulta($consulta) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		return $this->classe_conexao->query($consulta);
	}
	
	function update($consulta) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		$this->sql = $this->classe_conexao->prepare($consulta);
		return $this->sql->execute();
	}
	
	function con_empresa($id_empresa) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		
		$this->sql = $this->classe_conexao->query("SELECT id, fantasia, razao, cnpj, ie, id_fone1, id_fone2, id_fone3, id_email1, id_email2, id_email3, palavras_chave, id_cidade, id_logradouro, numero, tipo_compl1, compl1, tipo_compl2, compl2, tipo_compl3, compl3, site, url, ativo FROM empresas WHERE id = '$id_empresa'");
		return $this->sql;
	}
	
	function info_empresa_pagina($id_empresa) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		
		return $this->classe_conexao->query("SELECT 
		empresas.id, 
		empresas.fantasia, 
		empresas.razao,
		empresas.id_fone1, 
		empresas.id_fone2,
		empresas.id_fone3,
		empresas.id_email1,
		empresas.id_email2,
		empresas.id_email3,
		empresas.site,
		empresas.id_cidade,
		empresas.palavras_chave,
		empresas.url,
		opt_cidades.nome AS cidade, 
		opt_cidades.uf,
		opt_estado.nome AS estado,
		opt_enderecos.tipo_end,
		opt_enderecos.nome AS endereco,
		opt_bairros.nome AS bairro,
		opt_logradouros.cep
		
		FROM empresas 
			INNER JOIN opt_cidades ON opt_cidades.id = empresas.id_cidade
			INNER JOIN opt_estado ON opt_estado.uf = opt_cidades.uf
			INNER JOIN opt_logradouros ON opt_logradouros.id = empresas.id_logradouro
			INNER JOIN opt_enderecos ON opt_enderecos.id = opt_logradouros.id_end
			INNER JOIN opt_bairros ON opt_bairros.id = opt_logradouros.id_bairro
		WHERE 
		
		empresas.id = '$id_empresa' and 
		empresas.ativo = 's'");
	}
}