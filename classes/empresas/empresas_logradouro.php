<?php
class empresas_logradouro {
	function inserir_logradouro($correios_logradouro,$cidade,$endereco,$bairro,$logradouro,$tipo_logradouro,$tipo_logradouro2,$cep) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresa = new empresas;
		$this->classe_configuracoes = new configuracoes;
		//	---
		
		if ( $correios_logradouro > 0 ) {
			$this->opt_correios_logradouros = $this->classe_empresas->consulta("SELECT opt_correios_logradouros.tipo FROM opt_correios_logradouros WHERE opt_correios_logradouros.id = '$correios_logradouro'")->fetch();
			$tipo_logradouro = $this->opt_correios_logradouros['tipo'];
		}
		if ( !$this->validar_tipo_logradouro($tipo_logradouro) ) {
			$tipo_logradouro = $this->tipo_end_alteracoes('impressao',$tipo_logradouro2);
		}
		
		$endereco = trim($this->classe_formatacoes->converter_termo($endereco,1));
		$bairro = trim($this->classe_formatacoes->termo_abreviado($bairro));
		
		$this->opt_enderecos = $this->classe_conexao->query("SELECT id FROM opt_enderecos WHERE id_cidade = '$cidade' and tipo_end = '$tipo_logradouro' and nome = '$endereco'");
		if ( $this->opt_enderecos->rowCount() == 1 ) {
			$this->linha_endereco = $this->opt_enderecos->fetch();
			$this->id_endereco = $this->linha_endereco['id'];
		} else {
			//	Inserindo registro sobre o envio.
			$this->sql = $this->classe_conexao->prepare("INSERT INTO opt_enderecos (id_cidade, tipo_end, nome) VALUES (?,?,?)");	
			$this->sql->bindValue(1, $cidade);
			$this->sql->bindValue(2, $tipo_logradouro);
			$this->sql->bindValue(3, $endereco);
			$this->sql->execute();
			$this->id_endereco = $this->classe_conexao->lastInsertId();
		}
		
		$this->opt_bairros = $this->classe_conexao->query("SELECT id FROM opt_bairros WHERE id_cidade = '$cidade' and nome = '$bairro'");
		if ( $this->opt_bairros->rowCount() == 1 ) {
			$this->linha_bairro	=	$this->opt_bairros->fetch();
			$this->id_bairro =	$this->linha_bairro['id'];
		} else {
			//	Inserindo registro sobre o envio.
			$this->sql = $this->classe_conexao->prepare("INSERT INTO opt_bairros (id_cidade, nome) VALUES (?,?)");	
			$this->sql->bindValue(1, $cidade);
			$this->sql->bindValue(2, $bairro);
			$this->sql->execute();
			$this->id_bairro = $this->classe_conexao->lastInsertId();
		}
		
		$this->opt_logradouros = $this->classe_conexao->query("SELECT id FROM opt_logradouros WHERE id_cidade = '$cidade' and id_bairro = '".$this->id_bairro."' and id_end = '".$this->id_endereco."' and cep = '$cep'");
		
		if ( $this->opt_logradouros->rowCount() > 0 ) {
			$this->opt_logradouros = $this->opt_logradouros->fetch();
			return $this->opt_logradouros['id'];
		} else {
			//	Inserindo registro sobre o envio.
			$this->sql = $this->classe_conexao->prepare("INSERT INTO opt_logradouros (id_cidade, id_bairro, id_end, cep) VALUES (?,?,?,?)");
			$this->sql->bindValue(1, $cidade);
			$this->sql->bindValue(2, $this->id_bairro);
			$this->sql->bindValue(3, $this->id_endereco);
			$this->sql->bindValue(4, $cep);
			$this->sql->execute();
			return $this->classe_conexao->lastInsertId();
		}
	}
	
	function formatacao_script_logradouro($tipo, $modo) {
		if ( $modo == 'compl' ) {
			switch ( $tipo ){
				case 1: $tipo = "andar"; break;
				case 2: $tipo = "bloco"; break;
				case 3: $tipo = "apartamento"; break;
				case 4: $tipo = "loja"; break;
				case 5: $tipo = "casa"; break;
				case 6: $tipo = "terreo"; break;
				case 7: $tipo = "sala"; break;
				case 8: $tipo = "edificio"; break;
				case 9: $tipo = "condominio"; break;
				case 10: $tipo = "quadra"; break;
				case 11: $tipo = "fundos"; break;
				case 12: $tipo = "lote"; break;
				case 13: $tipo = "anexo"; break;
			}
			return $tipo;
		}
	}
	
	function validar_complementos($numero,$tipo_compl1,$compl1,$tipo_compl2,$compl2,$tipo_compl3,$compl3) {
		if ( $tipo_compl1 == 0 and !empty($compl1) or $tipo_compl1 > 0 and $tipo_compl1 <= 13 and empty($compl1) ) {
			$this->sem_compl1		=	'n';
		} else {
			if ( (INT)$tipo_compl1 == 0 and empty($compl1) ) {
				$valcompl = $valcompl+1;
			}
			$this->sem_compl1		=	's';
		}
		if ( $tipo_compl2 == 0 and !empty($compl2) or $tipo_compl2 > 0 and $tipo_compl2 <= 13 and empty($compl2) ) {
			$this->sem_compl2		=	'n';
		} else {
			if ( (INT)$tipo_compl2 == 0 and empty($compl2) ) {
				$valcompl = $valcompl+1;
			}
			$this->sem_compl2		=	's';
		}
		if ( $tipo_compl3 == 0 and !empty($compl3) or $tipo_compl3 > 0 and $tipo_compl3 <= 13 and empty($compl3) ) {
			$this->sem_compl3		=	'n';
		} else {
			if ( (INT)$tipo_compl3 == 0 and empty($compl3) ) {
				$valcompl = $valcompl+1;
			}
			$this->sem_compl3		=	's';
		}
		
		if ( $this->sem_compl1 == 's' and $this->sem_compl2 == 's' and $this->sem_compl3 == 's' ) {
			if ( empty($numero) and $valcompl == 3 ) {
				return 'Preencha o número ou um complemento.';
			} else {
				return true;
			}
		} else {
			return 'Preencha o complemento corretamente.';
		}
	}
	
	//	Remover palavras "rua" e "avenida".
	function tipo_end_alteracoes($tipo,$resultado) {
		$this->classe_formatacoes = new formatacoes;
	
		if ( $tipo == 'impressao' ) {
			$resultado = $this->classe_formatacoes->converter_termo($resultado,0);
			$resultado = str_replace( 'avenida', 'av', $resultado);
			$resultado = str_replace( 'quadra', 'qd', $resultado);
			$resultado = str_replace( 'estrada', 'est', $resultado);
			$resultado = str_replace( 'travessa', 'tv', $resultado);
			$resultado = str_replace( 'alameda', 'al', $resultado);
			$resultado = str_replace( 'praca', "pc", $resultado);
			$resultado = str_replace( 'rua', 'r', $resultado);
			$resultado = str_replace( 'condominio', 'condo', $resultado);
			$resultado = str_replace( 'viela', 'vl', $resultado);
			$resultado = str_replace( 'rodovia', 'rod', $resultado);
		} else if ( $tipo == 'remocao' ) {
			$resultado = $this->classe_formatacoes->converter_termo($resultado,0);
			$resultado = str_replace( 'avenida', '', $resultado);
			$resultado = str_replace( 'quadra', '', $resultado);
			$resultado = str_replace( 'estrada', '', $resultado);
			$resultado = str_replace( 'travessa', '', $resultado);
			$resultado = str_replace( 'alameda', '', $resultado);
			$resultado = str_replace( 'praca', '', $resultado);
			$resultado = str_replace( 'rua', '', $resultado);
			$resultado = str_replace( 'condominio', '', $resultado);
			$resultado = str_replace( 'viela', '', $resultado);
			$resultado = str_replace( 'rodovia', '', $resultado);
			
			$this->palavras	= explode(' ', $resultado);
			$this->t_palavras =	count($this->palavras);
			for($this->i = 0; $this->i < $this->t_palavras; $this->i++){
				if ( !$this->validar_tipo_logradouro($this->palavras[$this->i]) ) {
					$this->novo = $this->novo.' '.$this->palavras[$this->i];
				}
			}
			$resultado = trim($this->novo);
		}
		return $resultado;
	}

	function validar_tipo_logradouro($tipo_logradouro) {
		if ( $tipo_logradouro == 'av' ) {
			return true;
		} else if ( $tipo_logradouro == 'qd' ) {
			return true;
		} else if ( $tipo_logradouro == 'est' ) {
			return true;
		} else if ( $tipo_logradouro == 'tv' ) {
			return true;
		} else if ( $tipo_logradouro == 'al' ) {
			return true;
		} else if ( $tipo_logradouro == "pc" ) {
			return true;
		} else if ( $tipo_logradouro == 'r' ) {
			return true;
		} else if ( $tipo_logradouro == 'condo' ) {
			return true;
		} else if ( $tipo_logradouro == 'vl' ) {
			return true;
		} else if ( $tipo_logradouro == 'rod' ) {
			return true;
		} else {
			return false;
		}
	}
	
	function validar_logradouro($correios_logradouro,$endereco,$bairro,$logradouro,$tipo_logradouro,$tipo_logradouro2,$numero,$cep) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classes ---
		$this->classe_validacoes = new validacoes;
		$this->classe_empresas = new empresas;
		$this->classe_empresas_validacoes = new empresas_validacoes;
		//	---
		if ( $correios_logradouro > 0 ) {
			$this->opt_correios_logradouros = $this->classe_empresas->consulta("SELECT opt_correios_logradouros.lote_ini, opt_correios_logradouros.lote_fin, opt_correios_logradouros.cep FROM opt_correios_logradouros WHERE opt_correios_logradouros.id = '$correios_logradouro'");
		} else if ( $logradouro == 0 and $correios_logradouro == 0 ) {
			$teste = $this->classe_empresas->consulta("SELECT opt_correios_logradouros.lado, opt_correios_logradouros.lote_ini, opt_correios_logradouros.lote_fin, opt_correios_logradouros.cep FROM opt_correios_logradouros WHERE opt_correios_logradouros.logradouro = '$endereco' and opt_correios_logradouros.cep != '$cep'");
			foreach ( $teste as $linha_teste ) {
				if ( $numero % 2 == 0 ) {
					$lado_numero = 'Par';
				} else {
					$lado_numero = 'Impar';
				}
				if ( !empty($linha_teste['lado']) and $linha_teste['lado'] != 'Ambos' ) {
					if ( $linha_teste['lado'] == $lado_numero ) {
						if ( $numero > 0 and $linha_teste['lote_ini'] > 0 and $numero < $linha_teste['lote_ini'] or (INT)$numero > 0 and $linha_teste['lote_ini'] > 0 and $numero > $linha_teste['lote_fin'] ) {
							$end_status = 'error';
						} else {
							$end_status = 'ok';
						}
					}
				} else {
					if ( $numero > 0 and $linha_teste['lote_ini'] > 0 and $numero < $linha_teste['lote_ini'] or (INT)$numero > 0 and $linha_teste['lote_ini'] > 0 and $numero > $linha_teste['lote_fin'] ) {
						$end_status = 'error';
					} else {
						$end_status = 'ok';
					}
				}
				//	Apagando variável.
				unset($lado_numero);
			}
		}
		if ( !$this->validar_tipo_logradouro($tipo_logradouro) ) {
			$tipo_logradouro = $this->tipo_end_alteracoes('impressao',$tipo_logradouro2);
		}
		
		//	Verificar se o usuário colocou o tipo de endereço no campo endereço.
		$this->separar_palavras = explode(' ', $endereco);

		if ( empty($endereco) ) {
			return 'Digíte o endereço onde está localizada esta empresa.';
		} else if ( strlen($endereco) < 1 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(59);
		} else if ( strlen($endereco) > 100 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(60);
		} else if ( $logradouro == 0 and $correios_logradouro == 0 and empty($tipo_logradouro) ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(61);
		} else if ( $logradouro == 0 and $correios_logradouro == 0 and !$this->validar_tipo_logradouro($tipo_logradouro) ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(62);
		} else if ( $this->validar_tipo_logradouro($this->tipo_end_alteracoes('impressao',$this->separar_palavras[0])) ) {
			return 'Não precisa digitar o tipo de endereço, basta selecionar.';
		} else if ( empty($bairro) ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(63);
		} else if ( strlen($bairro) < 1 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(64);
		} else if ( strlen($bairro) > 100 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(65);
		} else if ( strtolower($numero) == 's/n' ) {
			return 'Caso não haja número de endereço, deixe o campo em branco.';
		} else if ( strlen($numero) > 5 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(68);
		} else if ( empty($cep) ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(69);
		} else if ( !is_numeric($cep) ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(70);
		} else if ( !$this->classe_validacoes->cep($cep) ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(71);
		} else if ( strlen($cep) < 8 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(71);
		} else if ( strlen($cep) > 8 ) {
			return $this->classe_empresas_validacoes->mensagens_padrao(71);
		} else if ( $logradouro > 0 and $correios_logradouro > 0 ) {
			return 'Digite o logradouro corretamente.'; 
		} else if ( $end_status == 'error' ) {
			return 'O número de endereço não corresponde a este CEP.';
		} else if ( $logradouro == 0 and $correios_logradouro > 0 and $this->opt_correios_logradouros->rowCount() == 1 ) {
			$this->linha_opt_correios_logradouros = $this->opt_correios_logradouros->fetch();
			if ( (INT)$numero > 0 and $this->linha_opt_correios_logradouros['lote_ini'] > 0 and $numero < $this->linha_opt_correios_logradouros['lote_ini'] or (INT)$numero > 0 and $this->linha_opt_correios_logradouros['lote_ini'] > 0 and $numero > $this->linha_opt_correios_logradouros['lote_fin'] ) {
				return 'Este logradouro deve esta na faixa de números entre '.$this->linha_opt_correios_logradouros['lote_ini'].' até '.$this->linha_opt_correios_logradouros['lote_fin'].'. Digite um número corretamente!';
			} else if ( $cep > 0 and $cep != $this->linha_opt_correios_logradouros['cep'] ) {
				return 'Este CEP não corresponde ao endereço.';
			} else {
				return true;
			}
		} else {
			return true;
		}	
	}
}