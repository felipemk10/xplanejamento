<?php
class empresas_alteracoes {
	function update($sql) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		$this->sql = $this->classe_conexao->prepare($sql);
		return $this->sql->execute();
	}

	//	Ativa ou desativa cadastro da empresa.
	function status_empresa($id_usuario, $id_empresa, $modo) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		if ( $modo == 'ativar' ) {
		
			//	--- Classe ---
			$this->classe_conexao = new conexao;
			$this->classe_configuracoes = new configuracoes;
			//	---
			
			$this->sql = $this->classe_conexao->prepare("UPDATE empresas SET data_tivacao = '".$this->classe_configuracoes->imprimir_data()."',  usuario_tivacao = '$id_usuario', ativo = 's' WHERE id = '$id_empresa'");
			$this->sql->execute();
			
			$this->campo_alteracao($id_usuario, $id_empresa, 'ativo', 'n', 's');
		} else if ( $modo == 'desativar' ) {
			//	--- Classe ---
			$this->classe_conexao = new conexao;
			//	---
			
			$this->sql = $this->classe_conexao->prepare("UPDATE empresas SET data_tivacao = '".$this->classe_configuracoes->imprimir_data()."',  usuario_tivacao = '$id_usuario', ativo = 'n' WHERE id = '$id_empresa'");
			$this->sql->execute();
			
			$this->campo_alteracao($id_usuario, $id_empresa, 'ativo', 's', 'n');
		}
		return true;
	}
	
	//	Registrar a alteração do campo da empresa.
	function campo_alteracao($id_usuario, $id_empresa, $campo, $antigo_valor, $novo_valor) {
		if ( $antigo_valor != $novo_valor ) {
			//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
			require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
			
			//	--- Classe ---
			$this->classe_conexao = new conexao;
			$this->classe_configuracoes = new configuracoes;
			//	---
			
			$this->sql = $this->classe_conexao->prepare("INSERT INTO empresas_campos_alteracoes (id_usuario, id_empresa, data_hora, campo, antes, depois) VALUES (?,?,?,?,?,?)");	
			$this->sql->bindValue(1, $id_usuario);
			$this->sql->bindValue(2, $id_empresa);
			$this->sql->bindValue(3, $this->classe_configuracoes->imprimir_data());
			$this->sql->bindValue(4, $campo);
			$this->sql->bindValue(5, $antigo_valor);
			$this->sql->bindValue(6, $novo_valor);
			$this->sql->execute();
		}
	}
}