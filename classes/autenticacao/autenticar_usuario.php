<?php
class autenticar_usuario {
	function autenticar($id_usuario,$tipo) {
		//	Iniciando classes.
		$this->classe_usuarios = new usuarios;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_configuracoes = new configuracoes;
		//---
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		
		$this->temposessao = 10800; // Obs: Media em segundos.

		if (  $tipo == 'bloquear' and $id_usuario > 0 ) {
			?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>','_self');</script><?php
		} else if ( $tipo == 'permitir' and (INT)$id_usuario == 0 or 
		$tipo == 'permitir' and !$this->classe_usuarios->usuario_empresas($id_usuario) or 
		$tipo == 'permitir' and !empty($_SESSION["sessiontime"]) and $_SESSION["sessiontime"] < (time() - $this->temposessao) ) {
			//	Apagando todas as sessões.
			session_unset();
			//---
			
			?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>','_self');</script><?php
		} else if ( $tipo == 'permitir' and (INT)$_SESSION['id_usuario'] > 0 ) {
			$_SESSION["sessiontime"] = time();
		}
	}
}