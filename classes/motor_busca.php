<?php
/*
	Instrução sobre a classe motor busca.
	
	SOBRE: Dentro da classe possui duas funções, sendo que a função "motor" é 
	que se encarrega de realizar praticamente toda a ação.
	
	CAMPOS: É requerido o "TERMO", "CIDADE" em que a apisuporte esteja prestan-
	do seus serviços e o "SOBRE" que define se a consulta é sobre empresas ou usuári-
	ios.
	
	RESPOSTA DA CLASSE DO TIPO USUÁRIOS:
	Ex: $linha[0] == Quantidade de resultados | $linha[1] == Resultados.
	
	RESPOSTA DA CLASSE DO TIPO EMPRESAS:
	Ex: $linha[0] == Quantidade de resultados | $linha[1] == Resultados. | $linha[2] == ID da palavra(termo buscado) 
	
	Veja abaixo outras respotas:
	PC == Poucos caracteres.
	CI == Cidade inválida.
	TI == Tipo inválido.
	SR == Sem resultados.
*/
class motor_busca {
	static function ordem_relevancia($a, $b) {
		return $b['relevancia'] / $a['relevancia'];
	}
	
	function motor($q, $cidade, $sobre, $data_hora, $id_inicial, $id_final, $modelo) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		$this->validacoes = new validacoes;
		$this->formatacoes = new formatacoes;
		//	Convertendo $id_inicial para INT.
		$id_inicial = (INT)$id_inicial;
		
		$this->q = $this->validacoes->anti_injection($q);
		
		$this->q_ = $this->formatacoes->retira_artigos($this->formatacoes->retira_acentos($this->formatacoes->retira_simbolos(strip_tags($this->q))));
		
		if ( $this->validacoes->detectar_artigo('sem_romanos',$this->q_) ) {
			$this->q_ = null;
		} else {
			$this->q_ = $this->formatacoes->singular_plural($this->q_);
		}
		if ( strlen($this->q_) > 1 ) {
			
			$this->result = $this->classe_conexao->query("SELECT id FROM opt_cidades WHERE id = '".(INT)$cidade."' and funcionamento = 's'")->fetchAll();
			if ( (INT)$this->result[0] == 1 ) {
				if ( $sobre == 'empresas' ) {
					//	Formatação para inclusão no SQL.
					if ( $id_inicial > 0 ) {
						$id_inicial = 'LIMIT '.$id_inicial.',11';
					} else {
						$id_inicial = '';
					}
					
					//	Buscando ligação de bairro com o termo.
					$this->con_bairro = $this->classe_conexao->query("SELECT opt_bairros.id,
					( ((MATCH(opt_bairros.nome) AGAINST ('".$this->q_."*' IN BOOLEAN MODE))) ) AS relevancia
					FROM opt_bairros
					 
					WHERE ( MATCH(opt_bairros.nome) AGAINST ('".$this->q_."' IN BOOLEAN MODE) ) AND opt_bairros.id_cidade = '$cidade'
					LIMIT 1")->fetch();
					
					if ( $modelo == 'colaborador' ) {
						if ( is_numeric(str_replace( ' ', '', $this->q_)) and strlen($this->q_) >= 8 ) {
							$this->con_empresa = $this->classe_conexao->query("SELECT 
							empresas.id, 
							empresas.fantasia,
							empresas.url,
							empresas.numero,
							empresas.tipo_compl1,
							empresas.tipo_compl2,
							empresas.tipo_compl3,
							empresas.compl1,
							empresas.compl2,
							empresas.compl3,
							empresas.data_cadastro,
							empresas.data_edicao,
							empresas.ativo,
							opt_cidades.nome AS cidade, 
							opt_cidades.uf,
							opt_enderecos.tipo_end,
							opt_enderecos.nome AS endereco,
							opt_logradouros.cep,
							opt_bairros.nome AS bairro
							
							FROM empresas
							 
							INNER JOIN opt_logradouros ON opt_logradouros.id = empresas.id_logradouro
							INNER JOIN opt_cidades ON opt_cidades.id = empresas.id_cidade
							INNER JOIN opt_bairros ON opt_bairros.id = opt_logradouros.id_bairro
							INNER JOIN opt_enderecos ON opt_enderecos.id = opt_logradouros.id_end 
							
							INNER JOIN fones ON fones.fone LIKE '%".str_replace( ' ', '', $this->q_)."%'
						    
						    WHERE
						    
						    empresas.id_fone1 = fones.id AND empresas.id_cidade = '$cidade' ||
						    empresas.id_fone2 = fones.id AND empresas.id_cidade = '$cidade' ||
						    empresas.id_fone3 = fones.id AND empresas.id_cidade = '$cidade'");
						} else if ( substr($this->q,0,2) == 'id' ) {
							$this->q_ = str_replace( 'id ', '',$this->q_);
							$this->con_empresa = $this->classe_conexao->query("SELECT 
							empresas.id, 
							empresas.fantasia,
							empresas.url,
							empresas.numero,
							empresas.tipo_compl1,
							empresas.tipo_compl2,
							empresas.tipo_compl3,
							empresas.compl1,
							empresas.compl2,
							empresas.compl3,
							empresas.data_cadastro,
							empresas.data_edicao,
							empresas.ativo,
							opt_cidades.nome AS cidade, 
							opt_cidades.uf,
							opt_enderecos.tipo_end,
							opt_enderecos.nome AS endereco,
							opt_logradouros.cep,
							opt_bairros.nome AS bairro
							
							FROM empresas
							 
							INNER JOIN opt_logradouros ON opt_logradouros.id = empresas.id_logradouro
							INNER JOIN opt_cidades ON opt_cidades.id = empresas.id_cidade
							INNER JOIN opt_bairros ON opt_bairros.id = opt_logradouros.id_bairro
							INNER JOIN opt_enderecos ON opt_enderecos.id = opt_logradouros.id_end 
							 
							WHERE empresas.id = '".$this->q_."' AND empresas.id_cidade = '$cidade'");
						} else {
							$this->con_empresa = $this->classe_conexao->query("SELECT 
							empresas.id, 
							empresas.fantasia,
							empresas.url,
							empresas.numero,
							empresas.tipo_compl1,
							empresas.tipo_compl2,
							empresas.tipo_compl3,
							empresas.compl1,
							empresas.compl2,
							empresas.compl3,
							empresas.data_cadastro,
							empresas.data_edicao,
							empresas.ativo,
							opt_cidades.nome AS cidade, 
							opt_cidades.uf,
							opt_enderecos.tipo_end,
							opt_enderecos.nome AS endereco,
							opt_logradouros.cep,
							opt_bairros.nome AS bairro,
							( ( ( (4.0 * (MATCH(empresas.fantasia) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(1.5 * (MATCH(empresas.razao) AGAINST ('".$this->q_."' IN BOOLEAN MODE)))
							+(1.7 * (MATCH(empresas.palavras_chave) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.5 * (MATCH(empresas.url) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.4 * (MATCH(empresas.numero) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(empresas.tipo_compl1) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(empresas.compl1) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(empresas.tipo_compl2) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(empresas.compl2) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(empresas.tipo_compl3) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(empresas.compl3) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.2 * (MATCH(opt_enderecos.nome) AGAINST ('".$this->q_."*' IN BOOLEAN MODE)))
							+(0.5 * (MATCH(opt_logradouros.cep) AGAINST ('".$this->q_."*' IN BOOLEAN MODE))) ) / 10 ) ) AS relevancia
							
							FROM empresas
							 
							INNER JOIN opt_logradouros ON opt_logradouros.id = empresas.id_logradouro
							INNER JOIN opt_cidades ON opt_cidades.id = empresas.id_cidade
							INNER JOIN opt_bairros ON opt_bairros.id = opt_logradouros.id_bairro
							INNER JOIN opt_enderecos ON opt_enderecos.id = opt_logradouros.id_end 
							 
							WHERE ( MATCH(empresas.fantasia,empresas.razao,empresas.palavras_chave,empresas.url,opt_enderecos.nome,opt_logradouros.cep) AGAINST ('".$this->q_."' IN BOOLEAN MODE) ) AND empresas.id_cidade = '$cidade'
							HAVING relevancia > 0 ORDER BY relevancia DESC $id_inicial");
						}
					}
					
					//	Cria o array para ser reordenado com o page rank.
					$this->lista = array();
					while($this->rs = $this->con_empresa->fetch(PDO::FETCH_ASSOC)) {
						if ( $this->empresas_resultados <= 300 ) {
							if ( $this->empresas_resultados < 300 ) {
								$this->empresas_resultados = (INT)$this->empresas_resultados + 1;
							}
							//	Limitando o número de resultados exibidos.
							if ( (INT)$id_final > 0 ) {
								if ( $this->n_empresas < $id_final ) {
									$this->lista[] = $this->rs;
									$this->n_empresas = $this->n_empresas + 1;
								}
							} else {
								$this->lista[] = $this->rs;
							}
						}
					}
					
					if ( $this->empresas_resultados > 0 ) {
						return array((INT)$this->empresas_resultados,$this->lista,$this->id_palavra,$this->n_empresas);
					} else {
						return array('SR');
					}
				} else {
					return array('TI');
				}
			} else {
				return array('CI');
			}
		} else {
			return array('PC');
		}
	}
}