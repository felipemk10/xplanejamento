<?php
class sms_processar_grupo {
	function processar() {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		//	Iniciando classes.
		$this->banco = new conexao;
		$this->classe_empresas = new empresas;
		$this->classe_configuracoes = new configuracoes;
		
		//	Tempo de execusão de script zerado, assim não haverá o erro maximum 30 seconds
		$this->classe_configuracoes->tempo_execucao(0);
		
		$this->consulta_grupo = $this->classe_empresas->consulta("SELECT id, id_grupo, id_empresa, id_usuario, mensagem, servico FROM sms_contatos_grupos_envios WHERE situacao = 'f' or situacao = 'a' and data_agendamento = '".date('Y-m-d H:i')."' LIMIT 10");
		
		if ( $this->consulta_grupo->rowCount() > 0 ) {
			foreach ( $this->consulta_grupo as $linha_grupo ) {
				$grupo_contatos = $this->classe_empresas->consulta("SELECT fones.fone FROM sms_contatos
					INNER JOIN fones ON fones.id = sms_contatos.id_fone and fones.ativo = 's'
					WHERE sms_contatos.id_empresa = '".$linha_grupo['id_empresa']."' and sms_contatos.id_grupo = '".$linha_grupo['id_grupo']."' and sms_contatos.ativo = 's'");
					
				foreach ( $grupo_contatos as $linha_grupo_contatos ) {
					//	Inserindo registro sobre o envio.
					$this->sql = $this->banco->prepare("INSERT INTO sms_logs (id_empresa, id_usuario, destinatario, mensagem, id_grupo_envio, servico, data_registro, situacao) VALUES (?,?,?,?,?,?,?,?)");
					$this->sql->bindValue(1, $linha_grupo['id_empresa']);
					$this->sql->bindValue(2, $linha_grupo['id_usuario']);
					$this->sql->bindValue(3, $linha_grupo_contatos['fone']);
					$this->sql->bindValue(4, $linha_grupo['mensagem']);
					$this->sql->bindValue(5, $linha_grupo['id']);
					$this->sql->bindValue(6, $linha_grupo['servico']);
					$this->sql->bindValue(7, $this->classe_configuracoes->imprimir_data());
					$this->sql->bindValue(8, 'f');
					//	Executa a instrução de consulta.
					$this->sql->execute();
				}
				// Atualizando situação do envio de grupo.
				$this->classe_empresas->consulta("UPDATE sms_contatos_grupos_envios SET situacao = 'p' WHERE id = '".$linha_grupo['id']."'");
			}
		} else {
			$this->consulta_grupo = $this->classe_empresas->consulta("SELECT id, id_grupo, id_empresa, id_usuario, mensagem, servico FROM sms_contatos_grupos_envios WHERE situacao = 'p' LIMIT 10");
		
			if ( $this->consulta_grupo->rowCount() > 0 ) {
				foreach ( $this->consulta_grupo as $linha_grupo ) {
					if ( $this->classe_empresas->consulta("SELECT id FROM sms_logs WHERE id_grupo_envio = '".$linha_grupo['id']."' and situacao = 'f'")->rowCount() == 0 ) {
						// Atualizando situação do envio de grupo.
					$this->classe_empresas->consulta("UPDATE sms_contatos_grupos_envios SET situacao = 'e' WHERE id = '".$linha_grupo['id']."'");
					}
				}
			}
		}
		
	}
}

if ( $_GET['processar'] == 'sim' and $_SERVER['SERVER_ADDR'] == '172.31.19.44' ) {
	$classe = new sms_processar_grupo;
	$classe->processar();
}