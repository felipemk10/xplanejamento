<?php
class sms_formularios {
	function adicionar_contato($id_usuario,$id_empresa,$nome,$fone,$grupo) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_validacoes = new validacoes;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresas = new empresas;
		$this->classe_fones = new fone;
		$this->classe_empresas_validacoes = new empresas_validacoes;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_usuarios = new usuarios;
		//---
		
		//	Rodando anti-injection.
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		$id_empresa = (INT)$this->classe_formatacoes->criptografia($id_empresa,'base64','decode');
		$nome = $this->classe_formatacoes->converter_termo(trim($this->classe_validacoes->anti_injection($nome)),1);
		$fone = trim($this->classe_validacoes->anti_injection($fone));
		$grupo = trim($this->classe_validacoes->anti_injection($grupo));
		//---
		
		if ( $nome == 'NOME' ) {
			$nome = null;
		}
		
		if ( $id_empresa == 0 ) {
			echo 'Empresa inválida.';
		} else if ( $id_usuario == 0 ) {
			echo 'Usuário inválida.';
		} else if ( !$this->classe_usuarios->usuario_colaborador($id_usuario,$id_empresa) ) {
			echo 'Você não está conectado a esta empresa.';
		} else if ( empty($nome) ) {
			echo 'Digite o nome do contato.';
		} else if ( strlen($nome) < 3 ) {
			echo 'O nome está muito pequeno';
		} else if ( strlen($nome) > 50 ) {
			echo 'O nome está muito grande';
		} else if ( is_numeric($nome) ) {
			echo 'Seu nome não pode ter somente números';
		} else if ( !is_numeric($this->classe_empresas_validacoes->fone(0,0, $fone, 'o fone do contato', 'sim')) ) {
			echo $this->classe_empresas_validacoes->fone(0,0, $fone, 'o fone do contato', 'sim');
		} else {
			$fone = $this->classe_empresas_validacoes->fone(0,0, $fone, 'o fone do contato', 'sim');

			if ( $this->classe_fones->Tipo_Fone($fone) != 'cel' ) {
				echo 'É permitido apenas número de celular.';
			} else if ( $grupo > 0 and $this->classe_empresas->consulta("SELECT id FROM sms_contatos_grupos WHERE id = '$grupo' and id_empresa = '$id_empresa' and ativo = 's'")->rowCount() == 0 ) {
				echo 'Este grupo não existe.';
			} else {
				if ( $this->classe_empresas->consulta("SELECT fones.id FROM fones 
				INNER JOIN sms_contatos ON sms_contatos.id_empresa = '$id_empresa' and sms_contatos.id_fone = fones.id and sms_contatos.ativo = 's'
				WHERE fones.fone = '$fone' and fones.ativo = 's'")->rowCount() > 0 ) {
					echo 'Já existe um contato com este número.';
				} else {
					$this->sql = $this->classe_conexao->prepare("INSERT INTO sms_contatos (id_empresa, id_grupo, data_registro, nome, id_fone, ativo) VALUES (?,?,?,?,?,?)");	
					$this->sql->bindValue(1, $id_empresa);
					$this->sql->bindValue(2, $grupo);
					$this->sql->bindValue(3, $this->classe_configuracoes->imprimir_data());
					$this->sql->bindValue(4, $this->classe_formatacoes->converter_termo($nome,1));
					$this->sql->bindValue(5, $this->classe_fones->inserir_fone($fone));
					$this->sql->bindValue(6, 's');
					$this->sql->execute();
					
					?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>paginas/sms/estaticas/contatos.php?contatos_msg=Adicionado contato com sucesso!','_self');</script><?php
				}
			}
		}
	}

	function editar_contato($id_usuario,$id_empresa,$id_contato,$nome,$fone,$grupo) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_validacoes = new validacoes;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresas = new empresas;
		$this->classe_fones = new fone;
		$this->classe_empresas_validacoes = new empresas_validacoes;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_usuarios = new usuarios;
		//---
		
		//	Rodando anti-injection.
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		$id_empresa = (INT)$this->classe_formatacoes->criptografia($id_empresa,'base64','decode');
		$id_contato = (INT)$this->classe_formatacoes->criptografia($id_contato,'base64','decode');
		$nome = $this->classe_formatacoes->converter_termo(trim($this->classe_validacoes->anti_injection($nome)),1);
		$fone = trim($this->classe_validacoes->anti_injection($fone));
		$grupo = (INT)$grupo;
		//---
		
		
		if ( $nome == 'NOME' ) {
			$nome = null;
		}
		
		if ( $id_empresa == 0 ) {
			echo 'Empresa inválida.';
		} else if ( $id_usuario == 0 ) {
			echo 'Usuário inválida.';
		} else if ( !$this->classe_usuarios->usuario_colaborador($id_usuario,$id_empresa) ) {
			echo 'Você não está conectado a esta empresa.';
		} else if ( empty($nome) ) {
			echo 'Digite o nome do contato.';
		} else if ( strlen($nome) < 3 ) {
			echo 'O nome está muito pequeno';
		} else if ( strlen($nome) > 50 ) {
			echo 'O nome está muito grande';
		} else if ( is_numeric($nome) ) {
			echo 'Seu nome não pode ter somente números';
		} else if ( !is_numeric($this->classe_empresas_validacoes->fone(0,0, $fone, 'o fone do contato', 'sim')) ) {
			echo $this->classe_empresas_validacoes->fone(0,0, $fone, 'o fone do contato', 'sim');
		} else {
			$fone = $this->classe_empresas_validacoes->fone(0,0, $fone, 'o fone do contato', 'sim');

			if ( $this->classe_fones->Tipo_Fone($fone) != 'cel' ) {
				echo 'É permitido apenas número de celular.';
			} else if ( $grupo > 0 and $this->classe_empresas->consulta("SELECT id FROM sms_contatos_grupos WHERE id = '$grupo' and id_empresa = '$id_empresa' and ativo = 's'")->rowCount() == 0 ) {
				echo 'Este grupo não existe.';
			} else {
				if ( $this->classe_empresas->consulta("SELECT fones.id FROM fones 
				INNER JOIN sms_contatos ON sms_contatos.id != $id_contato and sms_contatos.id_empresa = '$id_empresa' and sms_contatos.id_fone = fones.id and sms_contatos.ativo = 's'
				WHERE fones.fone = '$fone' and fones.ativo = 's'")->rowCount() > 0 ) {
					echo 'Já existe um contato com este número.';
				} else {
					$this->classe_empresas->consulta("UPDATE sms_contatos SET 
						id_grupo = '".$grupo."',
						nome = '".$this->classe_formatacoes->converter_termo($nome,1)."', 
						id_fone = '".$this->classe_fones->inserir_fone($fone)."' WHERE id = '".$id_contato."' and id_empresa = '$id_empresa' and ativo = 's'");

					
					?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>paginas/sms/estaticas/contatos.php?contatos_msg=Editado contato com sucesso!','_self');</script><?php
				}
			}
		}
	}

	function remover_contato($id_usuario,$id_empresa,$id_contato) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresas = new empresas;
		$this->classe_configuracoes = new configuracoes;
		//---

		//	Rodando anti-injection.
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		$id_empresa = (INT)$this->classe_formatacoes->criptografia($id_empresa,'base64','decode');
		$id_contato = (INT)$this->classe_formatacoes->criptografia($id_contato,'base64','decode');
		//---
		
		if ( $id_usuario > 0 and $id_empresa > 0 and $id_contato > 0 ) {
			if ( $this->classe_empresas->consulta("SELECT id FROM sms_contatos WHERE id = $id_contato and id_empresa = '$id_empresa' and ativo = 's'")->rowCount() == 1 ) {
				$this->classe_empresas->consulta("UPDATE sms_contatos SET 
					ativo = 'n' WHERE id = '".$id_contato."' and id_empresa = '$id_empresa' and ativo = 's'");

				header("Location: ".$this->classe_configuracoes->url_acesso()."paginas/sms/estaticas/contatos.php?contatos_msg=Removido contato com sucesso!");
			} else {
				header("Location: ".$this->classe_configuracoes->url_acesso()."paginas/sms/estaticas/contatos.php");
			}
		} else {
			header("Location: ".$this->classe_configuracoes->url_acesso()."paginas/sms/estaticas/contatos.php");
		}			
		
	}

	function enviar_sms_unitario($id_usuario,$id_empresa,$id_contato,$fone,$mensagem, $agendar, $data_agendamento) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_validacoes = new validacoes;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresas = new empresas;
		$this->classe_fones = new fone;
		$this->classe_empresas_validacoes = new empresas_validacoes;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_usuarios = new usuarios;
		$this->classe_sms_formatacoes = new sms_formatacoes;
		//---
		
		//	Rodando anti-injection.
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		$id_empresa = (INT)$this->classe_formatacoes->criptografia($id_empresa,'base64','decode');
		$id_contato = (INT)$this->classe_formatacoes->criptografia($id_contato,'base64','decode');
		//	Adicionando o valor de segundos a data de agendamento.
		if ( $agendar == 'sim' ) {
			$data_agendamento = $data_agendamento.':00';
			$ano = substr($data_agendamento,6,4);
			$mes = substr($data_agendamento,3,2);
			$dia = substr($data_agendamento,0,2);
			
			$data_agendamento = $ano.'-'.$mes.'-'.$dia.substr($data_agendamento,10,9);
			
			//	Apagando variáveis.
			unset($ano,$mes,$dia);
		}
		//---
		$mensagem = trim($this->classe_sms_formatacoes->retira_acentos($this->classe_sms_formatacoes->retira_simbolos($mensagem)));
		//---

		if ( $id_contato > 0 ) {
			$this->contato_informacoes = $this->classe_empresas->consulta("SELECT fones.fone FROM sms_contatos 
			INNER JOIN fones on fones.id = sms_contatos.id_fone and fones.ativo = 's'
			WHERE sms_contatos.id = '$id_contato' and sms_contatos.ativo = 's'")->fetch();
			$fone = $this->contato_informacoes['fone'];
		}
		
		// Conferências
		if ( $this->classe_empresas->consulta("SELECT id FROM sms_saldo WHERE id_empresa = '$id_empresa' and saldo > 0")->rowCount() == 0 ) {
			return "Saldo insuficiente para realizar este envio :(";
		} else if ( !is_numeric($this->classe_empresas_validacoes->fone(0,0, $fone, 'o celular', 'sim')) ) {
			echo $this->classe_empresas_validacoes->fone(0,0, $fone, 'o celular', 'sim');
		} else if ( $agendar == 'sim' and !$this->classe_validacoes->isValidDateTime($data_agendamento) ) {
			return "Digite a data do agendamento corretamente.";
		} else if ( $agendar == 'sim' and $data_agendamento < $this->classe_configuracoes->imprimir_data() ) {
			return "Escolha uma data igual ou maior do que a data atual";
		} elseif ( strlen($mensagem) == 0 ) {
			return "Não deixe sua mensagem em branco, escreva algo ;)";
		} elseif ( strlen($mensagem) > 140 ) {
			return "É possível digitar somente 140 caracteres :)";
		} else {
			//	Coloque o 55 no número.
			$fone = $this->classe_empresas_validacoes->fone(0,0, $fone, 'o celular', 'sim');

			if ( $this->classe_empresas->consulta("SELECT * FROM sms_logs WHERE id_empresa = $id_empresa and destinatario = '$fone' and data_registro >= '".substr($this->classe_configuracoes->imprimir_data(),0,10)."' and situacao != 'n'")->rowCount() >= 5 ) {
				return "Este fone já recebeu muitos SMS hoje de sua empresa,<br /> digite outro :]";
			} else {
				$sms_api = new sms_api;
			    if ( !$sms_api->envio_sms($fone, $mensagem, $id_empresa, $id_usuario, 0, 'ws', $data_agendamento) ) {
			    	return 'Não foi possível enviar o sms, tente mais tarde :s';
			    } else {
			    	?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>paginas/sms/estaticas/envio_sms_unitario.php?contatos_msg=SMS enviados com sucesso ;)','_self');</script><?php
			    }
			    //	Apagando variável.
			    unset($sms_api);
			}
		}
	}
	function enviar_sms_grupo($id_usuario,$id_empresa,$id_grupo,$mensagem, $agendar, $data_agendamento) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_validacoes = new validacoes;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresas = new empresas;
		$this->classe_fones = new fone;
		$this->classe_empresas_validacoes = new empresas_validacoes;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_usuarios = new usuarios;
		$this->classe_sms_formatacoes = new sms_formatacoes;
		//---
		
		//	Rodando anti-injection.
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		$id_empresa = (INT)$this->classe_formatacoes->criptografia($id_empresa,'base64','decode');
		$id_grupo = $id_grupo;
		$mensagem = trim($this->classe_sms_formatacoes->retira_acentos($this->classe_sms_formatacoes->retira_simbolos($mensagem)));
		//	Adicionando o valor de segundos a data de agendamento.
		if ( $agendar == 'sim' ) {
			$data_agendamento = $data_agendamento.':00';
			$ano = substr($data_agendamento,6,4);
			$mes = substr($data_agendamento,3,2);
			$dia = substr($data_agendamento,0,2);
			
			$data_agendamento = $ano.'-'.$mes.'-'.$dia.substr($data_agendamento,10,9);
			
			//	Apagando variáveis.
			unset($ano,$mes,$dia);
		}
		//---

		$this->sms_saldo = $this->classe_empresas->consulta("SELECT saldo FROM sms_saldo WHERE id_empresa = '$id_empresa'")->fetch();
		$this->contatos_grupo = $this->classe_empresas->consulta("SELECT * FROM sms_contatos WHERE id_grupo = '$id_grupo' and ativo = 's'")->rowCount();

		// Conferências
		if ( $this->sms_saldo['saldo'] < $this->contatos_grupo ) {
			return 'Saldo não é suficiente para realizar este envio, <br /> o grupo selecionado contém mais contatos do que o saldo atual.';
		} else if ( $this->contatos_grupo == 0 ) {
			return 'Não existem contatos neste grupo.';
		} else if ( $this->classe_empresas->consulta("SELECT id FROM sms_saldo WHERE id_empresa = '$id_empresa' and saldo > 0")->rowCount() == 0 ) {
			return "Saldo insuficiente para realizar este envio :(";
		} else if ( $agendar == 'sim' and !$this->classe_validacoes->isValidDateTime($data_agendamento) ) {
			return "Digite a data do agendamento corretamente.";
		} else if ( $agendar == 'sim' and $data_agendamento < $this->classe_configuracoes->imprimir_data() ) {
			return "Escolha uma data igual ou maior do que a data atual";
		} elseif ( strlen($mensagem) == 0 ) {
			return "Não deixe sua mensagem em branco, escreva algo ;)";
		} elseif ( strlen($mensagem) > 140 ) {
			return "É possível digitar somente 140 caracteres :)";
		} else {
			if ( $this->classe_empresas->consulta("SELECT id FROM sms_contatos_grupos_envios WHERE id_empresa = $id_empresa and id_grupo = '$id_grupo' and data_registro >= '".substr($this->classe_configuracoes->imprimir_data(),0,10)."' and situacao = 'e'")->rowCount() >= 5 ) {
				return "Este grupo já recebeu muitos SMS hoje de sua empresa,<br /> digite outro :]";
			} else {
				$sms_api = new sms_api;
			    if ( !$sms_api->envio_sms(0, $mensagem, $id_empresa, $id_usuario, $id_grupo, 'ws', $data_agendamento) ) {
			    	return 'Não foi possível enviar o sms, tente mais tarde :s';
			    } else {
			    	?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>paginas/sms/estaticas/envio_sms_grupo.php?contatos_msg=SMS enviados com sucesso ;)','_self');</script><?php
			    }
			    //	Apagando variável.
			    unset($api_sms);
			}
		}
	}

	function adicionar_contato_grupo($id_usuario,$id_empresa,$nome) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	--- Classe ---
		$this->classe_conexao = new conexao;
		$this->classe_validacoes = new validacoes;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_empresas = new empresas;
		$this->classe_empresas_validacoes = new empresas_validacoes;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_usuarios = new usuarios;
		//---
		
		//	Rodando anti-injection.
		$id_usuario = (INT)$this->classe_formatacoes->criptografia($id_usuario,'base64','decode');
		$id_empresa = (INT)$this->classe_formatacoes->criptografia($id_empresa,'base64','decode');
		$nome = $this->classe_formatacoes->converter_termo(trim($this->classe_validacoes->anti_injection($nome)),1);
		//---
		
		
		if ( $nome == 'NOME DO GRUPO' ) {
			$nome = null;
		}

		if ( $id_empresa == 0 ) {
			echo 'Empresa inválida.';
		} else if ( $id_usuario == 0 ) {
			echo 'Usuário inválida.';
		} else if ( !$this->classe_usuarios->usuario_colaborador($id_usuario,$id_empresa) ) {
			echo 'Você não está conectado a esta empresa.';
		} else if ( empty($nome) ) {
			echo 'Digite o nome do grupo.';
		} else if ( strlen($nome) < 3 ) {
			echo 'O nome está muito pequeno';
		} else if ( strlen($nome) > 20 ) {
			echo 'O nome está muito grande';
		} else if ( is_numeric($nome) ) {
			echo 'O nome do grupo não pode ter somente números';
		} else {
			if ( $this->classe_empresas->consulta("SELECT id FROM sms_contatos_grupos 
			WHERE id_empresa = '$id_empresa' and nome = '$nome' and ativo = 's'")->rowCount() > 0 ) {
				echo 'Já existe um grupo com este nome.';
			} else {
				$this->sql = $this->classe_conexao->prepare("INSERT INTO sms_contatos_grupos (id_empresa, nome, ativo) VALUES (?,?,?)");	
				$this->sql->bindValue(1, $id_empresa);
				$this->sql->bindValue(2, $nome);
				$this->sql->bindValue(3, 's');
				$this->sql->execute();
				
				?><script language='javascript' type='text/javascript'>window.open('<?php echo $this->classe_configuracoes->url_acesso(); ?>paginas/sms/estaticas/contatos.php?contatos_grupo_msg=Grupo adicionado com sucesso!','_self');</script><?php
			}
		}
	}
}