<?php
class sms_envio {
	function chamar_sms_api($saldo_nescessario, $destinatarios, $mensagem) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		//	Iniciando classes.
		$this->banco = new conexao;
		$this->classe_empresas = new empresas;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_sms_api = new sms_api;
		
		//	Tempo de execusão de script zerado, assim não haverá o erro maximum 30 seconds
		$this->classe_configuracoes->tempo_execucao(0);
		
		$this->servidor_informacoes = $this->banco->query("SELECT id, tipo FROM sms_servidores
		WHERE sms_servidores.saldo > '$saldo_nescessario' and sms_servidores.ativo = 's' LIMIT 1");
		
		if ( $this->servidor_informacoes->rowCount() == 1 ) {
			$this->servidor_informacoes = $this->servidor_informacoes->fetch();
			
			//	Identifica se a api requisitada é de um servidor externo ou interno.
			if ( $this->servidor_informacoes['tipo'] == 'e' ) {
				if ( $this->classe_sms_api->api_http_externa($this->servidor_informacoes['id'],$destinatarios,$mensagem) ) {
					//	Debita saldo.
					$this->classe_empresas->consulta("UPDATE sms_servidores SET saldo = saldo - 1 WHERE id = '".$this->servidor_informacoes['id']."2'");
						
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	}

	function enviar() {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		//	Iniciando classes.
		$this->banco = new conexao;
		$this->classe_empresas = new empresas;
		$this->classe_configuracoes = new configuracoes;
		$this->classe_sms_validacoes = new sms_validacoes;
		
		//	Tempo de execusão de script zerado, assim não haverá o erro maximum 30 seconds
		$this->classe_configuracoes->tempo_execucao(0);
		
		//	Apagando e zerando variáveis antes do foreach.
		//unset($fones);
		
		//$ii = -1;
		
		$destinatarios = $this->classe_empresas->consulta("SELECT id, id_empresa, destinatario, mensagem FROM sms_logs WHERE 
		situacao = 'f' or situacao = 'a' and data_agendamento = '".date('Y-m-d H:i')."' LIMIT 0,100");
		
		$total_destinatarios = $destinatarios->rowCount();
		
		foreach ( $destinatarios as $linha_destinatarios ) {
			
			//$i++;
			//$ii++;
			
			/*
			//	Organiza os destinatários neste primeiro if.
			if ( $ii == 1 ) {
				
				if ( $this->chamar_smsapi($total_destinatarios,substr($n_fones,1,strlen($n_fones)),$linha_destinatarios['mensagem']) == true ) {
					//	Debitar saldo.
					$this->classe_empresas->consulta("UPDATE sms_logs SET situacao = 'e' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f';
						UPDATE sms_saldo SET saldo = saldo - 1 WHERE id_empresa = '".$linha_destinatarios['id_empresa']."';");
				} else {
					$this->classe_empresas->consulta("UPDATE sms_logs SET situacao = 'n' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f'");
				}
				$ii = 0;
				unset($n_fones);
			}*/
			
			//	Valida o limite de SMS por dia para cada destinatário, saldo disponível da empresa, etc...
			if ( $this->classe_sms_validacoes->validar_envio($linha_destinatarios['id_empresa'],$linha_destinatarios['destinatario']) ) {
				
				// Este if deve ser desativado assim que a função de envio agrupado for ativada.
				if ( $this->chamar_sms_api($total_destinatarios,$linha_destinatarios['destinatario'],$linha_destinatarios['mensagem']) == true ) {
					//	Debitar saldo.
					$this->classe_empresas->consulta("
						UPDATE sms_logs SET data_envio = '".$this->classe_configuracoes->imprimir_data()."', situacao = 'e' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f' or id = '".$linha_destinatarios['id']."' and situacao = 'a';
						UPDATE sms_saldo SET saldo = saldo - 1 WHERE id_empresa = '".$linha_destinatarios['id_empresa']."';");
				} else {
					$this->classe_empresas->consulta("UPDATE sms_logs SET situacao = 'n' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f' or id = '".$linha_destinatarios['id']."' and situacao = 'a'");
				}
			
				//$n_fones .= ','.$linha_destinatarios['destinatario'];
			} else {
				$this->classe_empresas->consulta("UPDATE sms_logs SET situacao = 'n' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f' or id = '".$linha_destinatarios['id']."' and situacao = 'a'");
			}
			
			/*
			//	Caso exista destinatários que sobraram do if acima é acionado neste abaixo.
			if ( $i == $total_destinatarios ) {
				if ( $this->chamar_smsapi($total_destinatarios,substr($n_fones,1,strlen($n_fones)),$linha_destinatarios['mensagem']) == true ) {
					//	Debitar saldo.
					$this->classe_empresas->consulta("UPDATE sms_logs SET situacao = 'e' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f';
						UPDATE sms_saldo SET saldo = saldo - 1 WHERE id_empresa = '".$linha_destinatarios['id_empresa']."';");
				} else {
					$this->classe_empresas->consulta("UPDATE sms_logs SET situacao = 'n' WHERE id = '".$linha_destinatarios['id']."' and situacao = 'f'");
				}
			}*/
			
		}
	}
}

if ( $_GET['processar'] == 'sim' and $_SERVER['SERVER_ADDR'] == '172.31.19.44' ) {
	$classe = new sms_envio;
	$classe->enviar();
}