<?php

class sms_formatacoes {
	function retira_simbolos($string) {
	    $string = trim($string);
	    
	    $string = str_replace( '´', '', $string );
	    $string = str_replace( '`', '', $string );
	    $string = str_replace( '~', '', $string );
	    $string = str_replace( '^', '', $string );
	    $string = str_replace( '¨', '', $string );


	    $string = str_replace( '¨', '', $string );
	    $string = str_replace( '¹', '', $string );
	    $string = str_replace( '²', '', $string );
	    $string = str_replace( '³', '', $string );
	    $string = str_replace( '£', '', $string );
	    $string = str_replace( '¢', '', $string );
	    $string = str_replace( '¬', '', $string );
	    $string = str_replace( '§', '', $string );
	    $string = str_replace( 'ª', '', $string );
	    $string = str_replace( 'º', '', $string );
	    return $string;
	}

	//---

	function retira_acentos($str, $enc = "UTF-8"){
		$acentos = array(
		'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
		'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
		'C' => '/&Ccedil;/',
		'c' => '/&ccedil;/',
		'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
		'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
		'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
		'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
		'N' => '/&Ntilde;/',
		'n' => '/&ntilde;/',
		'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
		'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
		'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
		'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
		'Y' => '/&Yacute;/',
		'y' => '/&yacute;|&yuml;/',
		'a.' => '/&ordf;/',
		'o.' => '/&ordm;/');

		return preg_replace($acentos, array_keys($acentos), htmlentities($str,ENT_NOQUOTES, $enc));
	}
}