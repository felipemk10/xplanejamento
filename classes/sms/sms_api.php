<?php
class sms_api {
	//	Função para enviar a solicitação de SMS para a plataforma apisuporte SMS.
	function api_http_interna($host, $port, $username, $password, $phoneNoRecip, $msgText, $tipo_sms) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');

		//	Conecta ao banco de dados.
		$this->banco = new conexao;
		//	Configurações
		$this->classe_configuracoes = new configuracoes;
		
		$phoneNoRecip = '0'.substr($phoneNoRecip,2,10);
	
		$phone_sms = $this->banco->query("SELECT mensagem FROM sms_logs WHERE data_registro >= '".substr($this->classe_configuracoes->imprimir_data(),0,10)."' and destinatario = '$phoneNoRecip' and situacao = 'e' ORDER BY data_registro DESC")->fetch();
	    
	    if ( $phone_sms['msg'] != $msgText ) {
			/* Parameters:
			    $host - IP address or host name of the NowSMS server
			    $port - "Port number for the web interface" of the NowSMS Server
			    $username - "SMS Users" account on the NowSMS server
			    $password - Password defined for the "SMS Users" account on the NowSMS Server
			    $phoneNoRecip - One or more phone numbers (comma delimited) to receive the text message
			    $msgText - Text of the message
			    Additional NowSMS URL parameters can follow $msgText as additional parameters (e.g., "SMSCRoute=routename", "Sender=1234")
			*/
			
			
			$this->fp = fsockopen($host, $port, $errno, $errstr);
		    if (!$this->fp) {
		        echo "errno: $errno \n";
		        echo "errstr: $errstr\n";
		        return $result;
		    }
			
		    $this->getString = "GET /?Phone=" . rawurlencode($phoneNoRecip) . "&Text=" . rawurlencode($msgText);
		    /* Parse extra function parameters for more URL parameters */
		    $this->numArgs = 7;
		    while ($this->numArgs < func_num_args()) {
		       if ( substr(func_get_arg($this->numArgs),0,6) == "Sender" ) {
		       		$this->argString	=	'Sender='.substr(func_get_arg($this->numArgs),7,12);
				    $this->remetente	=	substr(func_get_arg($this->numArgs),7,12);
				} else {
		       		$this->argString = substr(func_get_arg($this->numArgs),7,12);
		       }
		       $this->getString = $this->getString . "&" . substr($this->argString, 0, strpos($this->argString,'=')) . "=" . rawurlencode(substr($this->argString, strpos($this->argString,'=')+1));
		       $this->numArgs += 1;
		    }
		    $this->getString = $this->getString . " HTTP/1.0\n";
		    
		    fwrite($this->fp, $this->getString);
		    if ($username != "") {
		       $this->auth = $username . ":" . $password;
		       $this->auth = base64_encode($this->auth);
		       fwrite($this->fp, "Authorization: Basic " . $this->auth . "\n");
		    }
		    fwrite($this->fp, "\n");
		  
		    $this->res = "";
		 
		    while(!feof($this->fp)) {
		        $this->res .= fread($this->fp,1);
		    }
		    fclose($this->fp);
	    
	   
		    //	Verificando se a mensagem foi enviada com sucesso.
			if ( substr($this->res,13,2) == 'OK' ) {
				//	Inserindo registro sobre o envio.
				$this->sql = $this->banco->prepare("INSERT INTO sms_logs (remetente, destinatario, mensagem, tipo, situacao, data_registro) VALUES (?,?,?,?,?,?)");
				$this->sql->bindValue(1, $this->remetente);
				$this->sql->bindValue(2, $phoneNoRecip);
				$this->sql->bindValue(3, $msgText);
				$this->sql->bindValue(4, $tipo_sms);
				$this->sql->bindValue(5, 'e');
				$this->sql->bindValue(6, $this->classe_configuracoes->imprimir_data());
				//	Executa a instrução de consulta.
				$this->sql->execute();
				
				//	Debitar saldo.
				$this->sql = $this->banco->prepare("UPDATE sms_chips SET saldo = saldo - 1 WHERE numero = '$this->remetente'");
				$this->sql->execute();
				
			} else {
				//	Inserindo registro sobre o envio.
				$this->sql = $this->banco->prepare("INSERT INTO sms_logs (remetente, destinatario, mensagem, tipo, situacao, data_registro) VALUES (?,?,?,?,?,?)");
				$this->sql->bindValue(1, $this->remetente);
				$this->sql->bindValue(2, $phoneNoRecip);
				$this->sql->bindValue(3, $msgText);
				$this->sql->bindValue(4, $tipo_sms);
				$this->sql->bindValue(5, 'n');
				$this->sql->bindValue(6, $this->classe_configuracoes->imprimir_data());
				//	Executa a instrução de consulta.
				$this->sql->execute();
				
			}
		    return '55'.substr($this->remetente,1,10);
	    }
	}
	//	Função para enviar a solicitação de SMS para a plataforma apisuporte SMS.
	function api_http_externa($id_servidor,$fone,$msg) {
		if ( $id_servidor == 1 ) {
			$credencial = "8E4682C3821C949C2018582CA7D207AFAE2A7E0F";
			$principal = "apisuporte"; 
			$auxuser = "uemerson@outlook.com";
			$mobile = $fone;
			$sendproj = "N"; 
			$msg= mb_convert_encoding($msg, "UTF-8");
			$msg = URLEncode($msg);
			$response 
			=fopen("http://www.mpgateway.com/v_2_00/smspush/enviasms.aspx?CREDENCIAL=".$credencial.
			"&PRINCIPAL_USER=".$principal."&AUX_USER=".$auxuser."&MOBILE=".$mobile."&SEND_PROJECT=".$sendproj."&MESSAGE=".$msg,"r");
			$status_code= fgets($response,4);
			if ( $status_code == 000 ) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	// Encaminhar as mensagens.
	function envio_sms($fone, $mensagem, $id_empresa, $id_usuario, $id_grupo, $servico, $data_agendamento) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		//	Iniciando classes.
		$this->banco = new conexao;
		$this->classe_empresas = new empresas;
		$this->classe_fones = new fone;
		$this->classe_configuracoes = new configuracoes;
		//---
		
		if ( $fone == 0 ) {
			$this->saldo_necessario = $this->classe_empresas->consulta("SELECT * FROM sms_contatos WHERE id_empresa = '$id_empresa' and id_grupo = '$id_grupo' and ativo = 's'")->rowCount();
		} else {
			$this->saldo_necessario = 0;
		}
		$this->servidor_informacoes = $this->banco->query("SELECT id, tipo FROM sms_servidores
			WHERE sms_servidores.saldo > '".$this->saldo_necessario."' and sms_servidores.ativo = 's' LIMIT 1");

		if ( $this->servidor_informacoes->rowCount() == 1 ) {
			$this->servidor_informacoes = $this->servidor_informacoes->fetch();

			if ( $this->servidor_informacoes['tipo'] == 'e' ) {
				if ( $fone == 0 ) {
					//	Inserindo registro sobre envio.
					
					//	Caso a data de agendamento esteja preenchida, realizamos o agendamento.
					if ( !empty($data_agendamento) ) {
						$this->sql = $this->banco->prepare("INSERT INTO sms_contatos_grupos_envios (id_servidor, id_empresa, id_usuario, id_grupo, mensagem, servico, situacao, data_registro, data_agendamento) VALUES (?,?,?,?,?,?,?,?,?)");
						$this->sql->bindValue(1, $this->servidor_informacoes['id']);
						$this->sql->bindValue(2, $id_empresa);
						$this->sql->bindValue(3, $id_usuario);
						$this->sql->bindValue(4, $id_grupo);
						$this->sql->bindValue(5, $mensagem);
						$this->sql->bindValue(6, $servico);
						$this->sql->bindValue(7, 'a');
						$this->sql->bindValue(8, $this->classe_configuracoes->imprimir_data());
						$this->sql->bindValue(9, $data_agendamento);
						//	Executa a instrução de consulta.
						$this->sql->execute();
					} else {
						$this->sql = $this->banco->prepare("INSERT INTO sms_contatos_grupos_envios (id_servidor, id_empresa, id_usuario, id_grupo, mensagem, servico, situacao, data_registro) VALUES (?,?,?,?,?,?,?,?)");
						$this->sql->bindValue(1, $this->servidor_informacoes['id']);
						$this->sql->bindValue(2, $id_empresa);
						$this->sql->bindValue(3, $id_usuario);
						$this->sql->bindValue(4, $id_grupo);
						$this->sql->bindValue(5, $mensagem);
						$this->sql->bindValue(6, $servico);
						$this->sql->bindValue(7, 'f');
						$this->sql->bindValue(8, $this->classe_configuracoes->imprimir_data());
						//	Executa a instrução de consulta.
						$this->sql->execute();
					}
					return true;
				} else {
					//	Inserindo registro sobre o envio.
					
					//	Caso a data de agendamento esteja preenchida, realizamos o agendamento.
					if ( !empty($data_agendamento) ) {
						$this->sql = $this->banco->prepare("INSERT INTO sms_logs 
						(id_servidor, id_empresa, id_usuario, destinatario, mensagem, servico, data_registro, data_agendamento, situacao) 
						VALUES (?,?,?,?,?,?,?,?,?)");
						
						$this->sql->bindValue(1, $this->servidor_informacoes['id']);
						$this->sql->bindValue(2, $id_empresa);
						$this->sql->bindValue(3, $id_usuario);
						$this->sql->bindValue(4, $fone);
						$this->sql->bindValue(5, $mensagem);
						$this->sql->bindValue(6, $servico);
						$this->sql->bindValue(7, $this->classe_configuracoes->imprimir_data());
						$this->sql->bindValue(8, $data_agendamento);
						$this->sql->bindValue(9, 'a');
						//	Executa a instrução de consulta.
						$this->sql->execute();
						
						return true;
					} else {
						$this->sql = $this->banco->prepare("INSERT INTO sms_logs 
						(id_servidor, id_empresa, id_usuario, destinatario, mensagem, servico, data_registro, data_envio, situacao) 
						VALUES (?,?,?,?,?,?,?,?,?)");
						
						$this->sql->bindValue(1, $this->servidor_informacoes['id']);
						$this->sql->bindValue(2, $id_empresa);
						$this->sql->bindValue(3, $id_usuario);
						$this->sql->bindValue(4, $fone);
						$this->sql->bindValue(5, $mensagem);
						$this->sql->bindValue(6, $servico);
						$this->sql->bindValue(7, $this->classe_configuracoes->imprimir_data());
						$this->sql->bindValue(8, $this->classe_configuracoes->imprimir_data());
						
						if ( $this->api_http_externa($this->servidor_informacoes['id'],$fone,$mensagem) == true and $this->classe_empresas->consulta("SELECT id FROM sms_logs WHERE id_empresa = $id_empresa and destinatario = '$fone' and data_registro >= '".substr($this->classe_configuracoes->imprimir_data(),0,10)."' and situacao != 'n'")->rowCount() < 5 ) {
							$this->sql->bindValue(9, 'e');
							//	Executa a instrução de consulta.
							$this->sql->execute();
							//	Debitar saldo.
							$this->sql2 = $this->banco->prepare("
								UPDATE sms_saldo SET saldo = saldo - 1 WHERE id_empresa = '$id_empresa';
								UPDATE sms_servidores SET saldo = saldo - 1 WHERE id = '".$this->servidor_informacoes['id']."';");
							$this->sql2->execute();
							return true;
						} else {
							$this->sql->bindValue(9, 'n');
							//	Executa a instrução de consulta.
							$this->sql->execute();
							return false;
						}
					}
				}
			} else {
				$this->servidor_informacoes = $this->banco->query("SELECT 
				sms_chips.numero, 
				sms_chips.saldo, 
				
				sms_servidores.host, 
				sms_servidores.porta, 
				sms_servidores.usuario, 
				sms_servidores.senha FROM sms_servidores
			INNER JOIN sms_chips ON sms_chips.id_servidor = sms_servidores.id and sms_chips.id_operadora = '$id_operadora' and sms_chips.ativo = 's'
			
			WHERE sms_servidores.tipo = 'i' and sms_servidores.ativo = 's' LIMIT 1")->fetch();
			
			//return $this->sms("".$this->servidor_informacoes['host']."", $this->servidor_informacoes['porta'], "".$this->servidor_informacoes['usuario']."", "".$this->servidor_informacoes['senha']."", $fone,$mensagem,$tipo,"Sender=".$this->servidor_informacoes['numero']."");

			}
		} else {
			return false;
		}
	}
}