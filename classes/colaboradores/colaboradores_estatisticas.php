<?php
class colaboradores_estatisticas {
	function estatisticas_cidade($id_cidade, $modo) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		
		if ( $modo == 'cadastro' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE data_cadastro LIKE '%".date('Y-m-d')."%' && id_cidade = '$id_cidade'")->rowCount();
		} else if ( $modo == 'edicao' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE data_edicao LIKE '%".date('Y-m-d')."%' && id_cidade = '$id_cidade'")->rowCount();
		} else if ( $modo == 'total' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE id_cidade = '$id_cidade' and ativo = 's'")->rowCount();
		} else if ( $modo == 'pagina' ) {
			return $this->classe_conexao->query("SELECT fin_negociacoes.id FROM fin_negociacoes 
			INNER JOIN empresas ON empresas.id = fin_negociacoes.empresa and empresas.id_cidade = '$id_cidade'
			WHERE fin_negociacoes.ativo = 's' GROUP BY empresas.id ")->rowCount();
		}
	}
	function estatisticas_colaborador($id_usuario, $modo) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		
		if ( $modo == 'cadastro-dia' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE data_cadastro LIKE '%".date('Y-m-d')."%' && usuario_cadastro = '$id_usuario'")->rowCount();
		} else if ( $modo == 'edicao-dia' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE data_cadastro != '%".date('Y-m-d')."%' and data_edicao LIKE '%".date('Y-m-d')."%' && usuario_edicao = '$id_usuario'")->rowCount();
		} else if ( $modo == 'cadastro-mes' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE data_cadastro LIKE '%".date('Y-m')."%' && usuario_cadastro = '$id_usuario'")->rowCount();
		} else if ( $modo == 'edicao-mes' ) {
			return $this->classe_conexao->query("SELECT id FROM empresas WHERE data_cadastro != '".date('Y-m')."' and data_edicao LIKE '%".date('Y-m')."%' && usuario_edicao = '$id_usuario'")->rowCount();
		}
	}
	
}