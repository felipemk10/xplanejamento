<?php
class colaboradores_financeiro {
	function numero_faturas_aberto($id_empresa) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		return $this->sql = $this->classe_conexao->query("SELECT id FROM fin_faturas WHERE id_empresa = '$id_empresa' and situacao = 'f' and tipo = 'fb'")->rowCount();
	}
		
	function abater_fatura($id_fatura) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classe.
		$this->classe_conexao = new conexao;
		$this->classe_configuracoes = new configuracoes;
		//---
		
		$this->sql = $this->classe_conexao->prepare("UPDATE fin_faturas SET data_abatimento = '".substr($this->classe_configuracoes->imprimir_data(),0,10)."', situacao = 'd' WHERE id = '$id_fatura';
		UPDATE fin_boletos_emitidos SET situacao = 'd' WHERE id_fatura = '$id_fatura'");
		$this->sql->execute();
	}

	function con_boleto($id_boleto) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		//	Executa a instrução de consulta
		return $this->sql = $this->classe_conexao->query("SELECT id, valor FROM fin_boletos_emitidos WHERE id_fatura = '$id_boleto'");
	}
	
	function con_lancamentos($id_fatura) {
		if ( $id_fatura > 0 ) {
			require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
			$this->classe_conexao = new conexao;
			//	Executa a instrução de consulta
			return $this->classe_conexao->query("SELECT id, data_emissao, descricao, quantidade, total FROM fin_fat_lanc WHERE id_fatura = '$id_fatura'");
		}
	}
	
	function con_lancamentos_total($id_fatura) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		//	Executa a instrução de consulta
		return $this->classe_conexao->query("SELECT total FROM fin_fat_lanc WHERE id_fatura = '$id_fatura'");
	}
	
	function con_fatura($id_empresa, $d_mes) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		//	Executa a instrução de consulta
		return $this->sql = $this->classe_conexao->query("SELECT data_emissao, data_fechamento, data_pagamento, data_abatimento, data_vencimento, valor, situacao, tipo FROM fin_faturas WHERE fin_faturas.id_empresa = '$id_empresa' and fin_faturas.mes = '$d_mes'");
	}


	function con_empresa_fatura($id_empresa,$d_mes) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		//	Executa a instrução de consulta
		return $this->sql = $this->classe_conexao->query("SELECT empresas.id AS id, fin_faturas.id AS id_fatura, 
		fin_faturas.mes AS mes, 
		fin_faturas.data_vencimento AS data_vencimento, 
		fin_faturas.data_pagamento, 
		fin_faturas.data_abatimento, 
		fin_faturas.valor_pagamento, 
		fin_faturas.valor AS valor, 
		fin_faturas.situacao AS situacao, 
		fin_faturas.tipo
		FROM 
		
		empresas, fin_faturas WHERE
		fin_faturas.id_empresa = '$id_empresa' && fin_faturas.mes LIKE '$d_mes'");
	}

	function faturas_vencidas($id_empresa) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classes.
		$this->classe_conexao = new conexao;
		$this->classe_configuracoes = new configuracoes;
		//---
		
		//	Executa a instrução de consulta
		return $this->sql = $this->classe_conexao->query("SELECT mes, valor FROM fin_faturas WHERE
        id_empresa = '$id_empresa' and situacao = 'f' and tipo = 'fb' and
        data_vencimento < '".substr($this->classe_configuracoes->imprimir_data(),0,10)."'
        GROUP BY mes ORDER BY mes DESC");
	}

	//	Exibir mês em string.
	function string_mes($data) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classe.
		$this->classe_configuracoes = new configuracoes;
		//---
		
		if ( strlen($data) >= 1 and strlen($data) <=2 ) {
			switch ( $data ){
				case 1: $sting_data = "Janeiro"; break;
				case 2: $sting_data = "Fevereiro"; break;
				case 3: $sting_data = "Março"; break;
				case 4: $sting_data = "Abril"; break;
				case 5: $sting_data = "Maio"; break;
				case 6: $sting_data = "Junho"; break;
				case 7: $sting_data = "Julho"; break;
				case 8: $sting_data = "Agosto"; break;
				case 9: $sting_data = "Setembro"; break;
				case 10: $sting_data = "Outubro"; break;
				case 11: $sting_data = "Novembro"; break;
				case 12: $sting_data = "Dezembro"; break;
			}
		} else if ( strlen($data) == 7 ) {
			switch ( substr($data,5,2) ){
				case 1: $sting_data = "Janeiro"; break;
				case 2: $sting_data = "Fevereiro"; break;
				case 3: $sting_data = "Março"; break;
				case 4: $sting_data = "Abril"; break;
				case 5: $sting_data = "Maio"; break;
				case 6: $sting_data = "Junho"; break;
				case 7: $sting_data = "Julho"; break;
				case 8: $sting_data = "Agosto"; break;
				case 9: $sting_data = "Setembro"; break;
				case 10: $sting_data = "Outubro"; break;
				case 11: $sting_data = "Novembro"; break;
				case 12: $sting_data = "Dezembro"; break;
			}
			if ( isset($data) and substr($data,0,4) != substr($this->classe_configuracoes->imprimir_data(),0,4) ) {
				$sting_data = $sting_data.'-'.substr($data,0,4);
			} else {
				$sting_data = $sting_data;
			}
		}
		return $sting_data;
	}

	function lista_meses($id_empresa) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		//	Executa a instrução de consulta
		return $this->sql = $this->classe_conexao->query("SELECT mes FROM fin_faturas WHERE id_empresa = '$id_empresa' GROUP BY mes ORDER BY mes DESC ");
	}
}