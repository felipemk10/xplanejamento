<?php
class colaboradores_historico_comercial {
	function inserir_historico($tipo,$id_empresa,$vendedor,$modo,$contato,$descricao) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		if ( $tipo == 'cml' and $id_empresa > 0 and $modo != NULL and $modo != "-" and $contato != NULL and $descricao != NULL or 
		$tipo == 'cba' and $id_empresa > 0 and $modo != NULL and $modo != "-" and $contato != NULL and $descricao != NULL ) {
			
			//	INICIANDO CLASSES.
			$this->classe_conexao = new conexao;
			$this->classe_validacoes = new validacoes;
			$this->classe_formatacoes = new formatacoes;
			$this->classe_usuario_logado = new usuarios;
			$this->classe_configuracoes = new configuracoes;
			//---
			
			if ( $tipo == 'cml' and $vendedor > 0 or 
			$tipo == 'cba' and $vendedor > 0 ) {
				$this->linha_usuario = $this->classe_usuario_logado->usuario($vendedor);
				$vendedor   =   $this->linha_usuario["nome"];
			} else if ( $tipo == 'cba' and $vendedor == 0 ) {
				$vendedor = 'SISTEMA FINANCEIRO AUTOMATIZADO';
			}
			echo $vendedor;
			
			$contato    =   $this->classe_formatacoes->converter_termo($this->classe_validacoes->anti_injection($contato),1);
			$descricao  =   strtoupper($descricao);	
			
			$this->sql = $this->classe_conexao->prepare("INSERT INTO comercial (tipo,id_empresa, data, id_vendedor, contato, modo, descricao)
			VALUES (?,?,?,?,?,?,?)");
			
			$this->sql->bindValue(1, $tipo);
			$this->sql->bindValue(2, $id_empresa);
			$this->sql->bindValue(3, $this->classe_configuracoes->imprimir_data());
			$this->sql->bindValue(4, $vendedor);
			$this->sql->bindValue(5, $contato);
			$this->sql->bindValue(6, $modo);
			$this->sql->bindValue(7, $descricao);
			$this->sql->execute();
			return true;
		} else {
			echo 'Existe campos não preenchidos corretamente.';
		}
	}

	function listar_historicos($id_empresa) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		//	Executa a instrução de consulta
		return $this->classe_conexao->query("SELECT data, tipo, modo, contato, id_vendedor, descricao FROM comercial WHERE id_empresa = '$id_empresa' ORDER BY data DESC");
	}

	function n_colaboradores_historicos() {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classes.
		$this->classe_conexao = new conexao;
		$this->classe_configuracoes = new configuracoes;
		//---
		
		//	Executa a instrução de consulta
		return $this->sql = $this->classe_conexao->query("SELECT * FROM (SELECT COUNT(id_vendedor) AS contador,id_vendedor FROM comercial WHERE data LIKE '%".substr($this->classe_configuracoes->imprimir_data(),0,10)."%' GROUP BY id_vendedor ORDER BY COUNT(id_vendedor) DESC) comercial");
		
	}
}