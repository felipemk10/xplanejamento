<?php
class email {
	//	Confirmar de o e-mail está verificado.
	function Verificacao($email) {
		$this->classe_conexao = new conexao;
		//	Retorna a quantidade de e-mail.
		return $this->classe_conexao->query("SELECT id FROM emails WHERE id = '$email' and verificado = 's'")->rowCount();
	}
	
	//	Inserir um novo e-mail.
	function inserir_email($email) {
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		
		if ( empty($email) ) {
			return 0;
		} else {
			$this->con_email = $this->classe_conexao->query("SELECT id FROM emails WHERE email = '$email'");
			if ( $this->con_email->rowCount() == 1 ) {
				$this->linha_email = $this->con_email->fetch();
				
				$this->sql = $this->classe_conexao->prepare("UPDATE emails SET ativo = 's' WHERE id = '".$this->linha_email['id']."' and ativo = 'n'");
				$this->sql->execute();
				return $this->linha_email['id'];
			} else if ($this->con_email->rowCount() == 0 ) {
				$this->sql = $this->classe_conexao->prepare("INSERT INTO emails (email, data_cadastro, data_tivacao) VALUES (?,?,?)");	
				$this->sql->bindValue(1, $email);
				$this->sql->bindValue(2, date('Y-m-d H:i:s'));
				$this->sql->bindValue(3, date('Y-m-d H:i:s'));
				$this->sql->execute();
				
				return $this->classe_conexao->lastInsertId();
				
				$this->sql = $this->classe_conexao->prepare("UPDATE emails SET ativo = 's' WHERE id = '".$this->classe_conexao->lastInsertId()."' and ativo = 'n'");
				$this->sql->execute();
				
			} 
		}
	}
}