<?php
class n_m_empresas {
	function qtd_porcentagem($id_cidade) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		// Iniciando classe.
		$this->classe_empresas = new empresas;
		$this->classe_configuracoes = new configuracoes;
		//---
		$this->opt_cidades = $this->classe_empresas->consulta("SELECT n_m_empresas FROM opt_cidades WHERE id = '$id_cidade'")->fetch();
		$this->empresas = $this->classe_empresas->consulta("SELECT data_cadastro, data_edicao FROM empresas WHERE id_cidade = '$id_cidade' and ativo = 's'");
		
		foreach ( $this->empresas as $linha ) {
			if ( $linha['data_edicao'] != '0000-00-00 00:00:00' and substr($linha['data_edicao'],0,7) > date("Y-m", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-12 month")) or 
			$linha['data_edicao'] == '0000-00-00 00:00:00' and substr($linha['data_cadastro'],0,7) > date("Y-m", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-12 month")) ) {
				$this->i++;
			}
		}
		if ( $this->i > $this->opt_cidades['n_m_empresas'] ) {
			return 100;
		} else {
			return ($this->i * 100 ) / $this->opt_cidades['n_m_empresas'];
		}
	}
	function empresas_desatualizadas($id_cidade) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		//	Iniciando classe.
		$this->classe_empresas = new empresas;
		$this->classe_configuracoes = new configuracoes;
		//---
		$this->empresas = $this->classe_empresas->consulta("SELECT data_cadastro, data_edicao FROM empresas WHERE id_cidade = '$id_cidade' and ativo = 's'");
		
		$this->i = 0;
		foreach ( $this->empresas as $linha ) {
			if ( $linha['data_edicao'] != '0000-00-00 00:00:00' and substr($linha['data_edicao'],0,7) < date("Y-m", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-12 month")) or 
			$linha['data_edicao'] == '0000-00-00 00:00:00' and substr($linha['data_cadastro'],0,7) < date("Y-m", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-12 month")) ) {
				$this->i++;
			}
		}
		$this->empresas = $this->classe_empresas->consulta("SELECT COUNT(id) AS total_empresas FROM empresas WHERE id_cidade = '$id_cidade' and ativo = 's'")->fetch();
		return $this->porcentagem = (($this->i * 100) / $this->empresas['total_empresas']);
	}
	
	function porcentagem($id_cidade) {
		$this->porcentagem_empresas = ceil($this->qtd_porcentagem($id_cidade));
		//$this->empresas_desatualizadas = ceil($this->empresas_desatualizadas($id_cidade));
		
		return $this->porcentagem_empresas;
	}
}