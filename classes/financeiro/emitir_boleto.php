<?php
class emitir_boleto {
	function banco_do_brasil($id_boleto, $tipo_retorno, $certificacao) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classe.
		$classe_empresas = new empresas;
		$classe_formatacoes = new formatacoes;
		$classe_configuracoes = new configuracoes;
		//---
		
		$linha_boleto = $classe_empresas->consulta("SELECT 
		fin_boletos_emitidos.id,
		fin_boletos_emitidos.id_empresa,
		fin_boletos_emitidos.valor,
		fin_boletos_emitidos.data_emissao,
		fin_boletos_emitidos.data_vencimento,
		fin_boletos_emitidos.id_fatura,
		fin_boletos_emitidos.tipo,
		fin_boletos_emitidos.situacao,
		
		empresas.fantasia, 
		empresas.id_fone1, 
		empresas.id_fone2, 
		empresas.id_fone3, 
		empresas.razao, 
		empresas.id_logradouro, 
		empresas.numero,
		
		fin_agencias.numero AS n_agencia,
		
		fin_contas.numero AS n_conta,
		fin_contas.convenio,
		fin_contas.contrato,
		fin_contas.carteira,
		fin_contas.variacao_carteira,
		fin_contas.identificacao,
		fin_contas.cpf_cnpj,
		fin_contas.endereco,
		fin_contas.cidade_uf,
		fin_contas.cedente,
		fin_contas.ativo
		
		FROM fin_boletos_emitidos 
		
		INNER JOIN empresas ON empresas.id = fin_boletos_emitidos.id_empresa
		
		INNER JOIN fin_agencias ON fin_agencias.id = fin_boletos_emitidos.id_agencia
		INNER JOIN fin_contas ON fin_contas.id = fin_boletos_emitidos.id_conta
		
		
		WHERE fin_boletos_emitidos.id = '$id_boleto'")->fetch();
		
		if ( $linha_boleto['id_fatura'] == 0 or $linha_boleto['situacao'] == 'd' ) {
			exit();
		}
		if ( $certificacao != 'legal' and $classe_empresas->consulta("SELECT fin_faturas.mes, fin_faturas.data_vencimento, fin_boletos_emitidos.id AS id_boleto, fin_boletos_emitidos.valor FROM fin_faturas
			INNER JOIN fin_boletos_emitidos ON fin_boletos_emitidos.id_fatura = fin_faturas.id
			WHERE fin_faturas.id = '".$linha_boleto['id_fatura']."' and fin_faturas.id_empresa = '".$linha_boleto['id_empresa']."' and fin_faturas.data_vencimento = '".substr($classe_configuracoes->imprimir_data(),0,7)."-10' and fin_faturas.tipo = 'fb' and fin_faturas.situacao = 'f' ORDER BY fin_faturas.id DESC LIMIT 1")->rowCount() == true ) {
		echo "Prezado <b>Cliente</b>,<br /><br />
	
		Foi enviado uma grande quantidade de e-mails pelo nosso sistema, por favor desconsidere as <br/>faturas enviadas em 04 de Fevereiro. 
		<br /><br />
		Pedimos desculpas pelo ocorrido.
		<br /><br />
		Salientamos que o acontecimento não está em conformidade com os padrões de <b>excelência</b> que <br />exigimos entre os nossos <b>colaboradores</b>.
		<br /><br />
		Ademais, cumpre informar que foram tomadas todas as <b>providências</b> no sentido de <b>garantir</b> que <br />tal ocasião jamais se repita.
		<br /><br />
		Assim sendo, reiterando nossas desculpas, nos despedimos sob protestos de elevada estima e <br />consideração.
		<br /><br />
		Atenciosamente,
		<br /><br />
		Equipe apisuporte<br />
		0800 075 1120";
		exit();
		}
		
		$n_agencia = $linha_boleto['n_agencia'];
		$n_conta = $linha_boleto['n_conta'];
		$n_convenio = $linha_boleto['convenio'];
		$n_contrato = $linha_boleto['contrato'];
		$n_carteira = $linha_boleto['carteira'];
		$n_variacao_carteira = $linha_boleto['variacao_carteira'];
		
		$identificacao = $linha_boleto['identificacao'];
		$cpf_cnpj = $linha_boleto['cpf_cnpj'];
		$conta_endereco = $linha_boleto['endereco'];
		$conta_cidade_uf = $linha_boleto['cidade_uf'];
		$conta_cedente = $linha_boleto['cedente'];
		

		if ( $emissao_bol == "inf" ) { $tipo = "Mensalidade - Plano de Informacao"; } elseif ( $emissao_bol == "pub" ) { $tipo = "Referente Publicidade";  } elseif ( $emissao_bol == "asn" ) { $tipo = "Referente Assinantura";  }
	
		if ( $linha_boleto['razao'] == NULL ) { $sacado = $classe_formatacoes->converter_termo($linha_boleto['fantasia'],1); } else { $sacado = $classe_formatacoes->converter_termo($linha_boleto['razao'],1); }
	
		//---
		$consulta_logradouro = $classe_empresas->consulta("SELECT
	
		opt_enderecos.nome AS nome_endereco,
		opt_enderecos.tipo_end AS tipo_end,
		opt_bairros.nome AS nome_bairro,
		opt_logradouros.cep AS cep,
		opt_logradouros.id_cidade AS cidade
	
		FROM opt_logradouros
	
		INNER JOIN opt_enderecos ON opt_logradouros.id_end = opt_enderecos.id
		INNER JOIN opt_bairros ON opt_logradouros.id_bairro = opt_bairros.id
	
		WHERE opt_logradouros.id = '".$linha_boleto['id_logradouro']."'")->fetch();
		$end = strtoupper($consulta_logradouro["tipo_end"]." ".$consulta_logradouro["nome_endereco"]." / ".$consulta_logradouro["nome_bairro"].", ".$linha["numero"]);
		$cep = substr($consulta_logradouro['cep'],0,5)."-".substr($consulta_logradouro['cep'],5,3);
		
		$linha_cidade = $classe_empresas->consulta("SELECT nome, uf FROM opt_cidades WHERE id = ".$consulta_logradouro['cidade']."")->fetch();
		$cidade = $linha_cidade['nome']."/".$linha_cidade['uf'];
		
		// DADOS DO BOLETO PARA O SEU CLIENTE
		$dias_de_prazo_para_pagamento = 5;
		$taxa_boleto = 0;

		$data_venc = "";  // Prazo de X dias OU informe data: "13/04/2006";
		
		$dadosboleto["nosso_numero"] = $linha_boleto['id'];
		$dadosboleto["numero_documento"] = $linha_boleto['id'];	// Num do pedido ou do documento
		$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
		$dadosboleto["data_documento"] = $linha_boleto['data_emissao']; // Data de emissÃ£o do Boleto
		$dadosboleto["data_processamento"] = substr($classe_configuracoes->imprimir_data(),0,10); // Data de processamento do boleto (opcional)
		// Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
		$dadosboleto["valor_boleto"] = number_format($linha_boleto['valor']+$taxa_boleto, 2, ',', '.'); 	// Valor do Boleto - REGRA: Com vÃ­rgula e sempre com duas casas depois da virgula
	
		// DADOS DO SEU CLIENTE
		$dadosboleto["sacado"] = $sacado;
		$dadosboleto["endereco1"] = $end;
		$dadosboleto["endereco2"] = $cidade." -  CEP: ".$cep;
		
		// INFORMACOES PARA O CLIENTE
		$dadosboleto["demonstrativo1"] = $tipo;
		$dadosboleto["demonstrativo2"] = "Api Suporte - apisuporte.com";
		$dadosboleto["demonstrativo3"] = "77 9921-1120";
		
		// INSTRUÇÕES PARA O CAIXA
		$dadosboleto["instrucoes1"] = "";
		$dadosboleto["instrucoes2"] = "<br>- APOS O VENCIMENTO: MULTA DE 2% | JUROS DE 0,033% AO DIA.";
		$dadosboleto["instrucoes3"] = "";
		$dadosboleto["instrucoes4"] = "";
		
		// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
		$dadosboleto["quantidade"] = "";
		$dadosboleto["valor_unitario"] = "";
		$dadosboleto["aceite"] = "N";		
		$dadosboleto["especie"] = "R$";
		$dadosboleto["especie_doc"] = "DM";
		
		
		// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
		
		
		// DADOS DA SUA CONTA - BANCO DO BRASIL
		$dadosboleto["agencia"] = $n_agencia; // Num da agencia, sem digito
		$dadosboleto["conta"] = $n_conta; 	// Num da conta, sem digito
		
		// DADOS PERSONALIZADOS - BANCO DO BRASIL
		$dadosboleto["convenio"] = $n_convenio;  // Num do convênio - REGRA: 6 ou 7 ou 8 dígitos
		$dadosboleto["contrato"] = $n_contrato; // Num do seu contrato
		$dadosboleto["carteira"] = $n_carteira;
		$dadosboleto["variacao_carteira"] = $n_variacao_carteira;  // Variação da Carteira, com traço (opcional)
		
		// TIPO DO BOLETO
		$dadosboleto["formatacao_convenio"] = 7; // REGRA: 8 p/ Convênio c/ 8 dígitos, 7 p/ Convênio c/ 7 dígitos, ou 6 se Convênio c/ 6 dígitos
		$dadosboleto["formatacao_nosso_numero"] = 2; // REGRA: Usado apenas p/ Convênio c/ 6 dígitos: informe 1 se for NossoNúmero de até 5 dígitos ou 2 para opção de até 17 dígitos
		
		/*
		#################################################
		DESENVOLVIDO PARA CARTEIRA 18
		
		- Carteira 18 com Convenio de 8 digitos
		  Nosso número: pode ser até 9 dígitos
		
		- Carteira 18 com Convenio de 7 digitos
		  Nosso número: pode ser até 10 dígitos
		
		- Carteira 18 com Convenio de 6 digitos
		  Nosso número:
		  de 1 a 99999 para opção de até 5 dígitos
		  de 1 a 99999999999999999 para opção de até 17 dígitos
		
		#################################################
		*/
		
		
		// SEUS DADOS
		$dadosboleto["identificacao"] = $identificacao;
		$dadosboleto["cpf_cnpj"] = $cpf_cnpj;
		$dadosboleto["endereco"] = $conta_endereco;
		$dadosboleto["cidade_uf"] = $conta_cidade_uf;
		$dadosboleto["cedente"] = $conta_cedente;
		
		// NÃO ALTERAR!
		if ( $tipo_retorno == 'boleto' ) {
			include_once("".$_SERVER['DOCUMENT_ROOT']."/paginas/boletos/include/funcoes_bb.php");
			include_once("".$_SERVER['DOCUMENT_ROOT']."/paginas/boletos/include/layout_bb.php");
		} else if ( $tipo_retorno == 'cod_barras' ) {
			include_once("".$_SERVER['DOCUMENT_ROOT']."/paginas/boletos/include/funcoes_bb.php");
			return $dadosboleto["linha_digitavel"];
		}
	}
}