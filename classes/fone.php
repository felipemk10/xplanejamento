<?php
class fone {
    function Detectar_Operadora($fone) {
    	if ( $this->Tipo_Fone($fone) == 'ccel' ) {
			/*
		    77 - NEXTEL
		    23 - TELEMIG
		    12 - CTBC
		    14 - BRASIL TELECOM
		    20 - VIVO
		    21 - CLARO
		    31 - TNL (Oi)
		    24 - AMAZONIA
		    37 - UNICEL
		    41 - TIM
		    43 - SERCOMERCIO
		    98 - Fixo
		    99 - Número não encontrado
		    999 - Chave invalida
		    995 - IP excedeu 6 consultas/hora nas últimas 24 horas
		    990 - IP black listed
		    */
			$this->url = file_get_contents("http://consultanumero1.telein.com.br/sistema/consulta_numero.php?numero=".$fone."&chave=in457fo854l43em2&socsp");
			
			switch ($this->url) {
				case 77:
					return $this->operadora = 7;
					break;
				case 23:
					return $this->operadora = 8;
					break;
				case 12:
					return $this->operadora = 9;
					break;
				case 14:
					return $this->operadora = 10;
					break;
				case 20:
					return $this->operadora = 3;
					break;
				case 21:
					return $this->operadora = 5;
					break;
				case 31:
					return $this->operadora = 2;
					break;
				case 24:
					return $this->operadora = 11;
					break;
				case 37:
					return $this->operadora = 12;
					break;
				case 41:
					return $this->operadora = 4;
					break;
				case 43:
					return $this->operadora = 13;
					break;
				default:
					return $this->operadora = false;
					break;
			}
		} else {
			return $this->operadora = 0;
		}
	}
	
	function Tipo_Fone($fone) {
		$telefones_emergencia = array(
		'102'=>'Serviço de Informação de Código de Assinante',
		'142'=>'Centro de Atendimento para a Intermediação da Comunicação para Portadores de Necessidades Especiais',
		'100'=>'Secretaria dos Direitos Humanos',
		'128'=>'Serviços de Emergência no âmbito do Mercosul',
		'180'=>'Delegacias Especializadas de Atendimento à Mulher',
		'181'=>'Disque Denúncia',
		'190'=>'Polícia Militar',
		'191'=>'Polícia Rodoviária Federal',
		'192'=>'Serviço Público de Remoção de Doentes (ambulância)',
		'193'=>'Corpo de Bombeiros',
		'194'=>'Polícia Federal',
		'197'=>'Polícia Civil',
		'198'=>'Polícia Rodoviária Estadual',
		'199'=>'Defesa Civil',
		'103'=>'XY Serviços Ofertados por prestadoras de Serviço Telefônico Fixo Comutado (STFC)
	Disque 103 seguido do código da prestadora desejada.
	Para falar com a Telefonica, por favor disque 103 15 ou consulte o serviço de informações 102',
		'105'=>'X Serviços Ofertados por prestadoras de Serviço Móvel de Interesse Coletivo
	Disque 105 seguido do código da prestadora desejada ou consulte o serviço de informações 102.',
		'106'=>'Serviços Ofertados pelas prestadoras dos Serviços de Comunicação Eletrônica de Massa',
		'115'=>'Serviços da prestadora de Água e Esgoto',
		'116'=>'Serviços da prestadora de Energia Elétrica',
		'118'=>'Serviços de Transporte Público',
		'127'=>'Ministério Público',
		'132'=>'Assistência a Dependentes de Agentes Químicos',
		'135'=>'Ministério da Previdência Social - INSS',
		'138'=>'Governo Federal',
		'141'=>'Centro de Valorização da Vida (CVV)',
		'144'=>'Agência Nacional de Energia Elétrica (Aneel)',
		'148'=>'Justiça Eleitoral',
		'150'=>'Vigilância Sanitária',
		'151'=>'Procon',
		'152'=>'Ibama',
		'153'=>'Guarda Municipal',
		'154'=>'Detran',
		'155'=>'Serviço Estadual',
		'156'=>'Serviço Municipal',
		'157'=>'Informações sobre oferta de empregos (Sine)',
		'158'=>'Delegacias Regionais do Trabalho',
		'159'=>'Atendimento aos Serviços Ofertados por Órgãos do Poder Judiciário',
		'160'=>'Disque Saúde',
		'161'=>'Atendimento a Denúncia por Órgãos da Administração Pública',
		'130'=>'Hora Certa',
		'134'=>'Despertador Automático',
		'1052'=>'Atendimento Claro',
		'1057'=>'Central de Relacionamento – Acredito ser somente para o Oi Fixo, se alguém tiver certeza deixe uma mensagem aqui.',
		'10331'=>'Suporte Oi Fixo, Oi Banda Larga (Velox) e outros serviços');
		
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		// Inicidando sessão.	
		$this->classe_conexao = new conexao;
		//---
		
		if ( substr($fone,0,4) == '4003' and strlen($fone) == 8 ) {
			return '4003';
		} else if ( substr($fone,0,4) == '0800' and strlen($fone) == 10 ) {
			return '0800';
		} else if ( substr($fone,0,4) == '0800' and strlen($fone) == 11 ) {
			return '0800';
		} else if ( !empty($telefones_emergencia[$fone]) ) {
			return 'especiais';
		} else if ( substr($fone,0,1) == 8 and strlen($fone) == 8 or 
		substr($fone,0,1) == 9 and strlen($fone) == 8 or 
		substr($fone,2,1) == 8 and strlen($fone) == 10 or 
		substr($fone,2,1) == 9 and strlen($fone) == 10 ) {
			return 'cel';
		} else if ( substr($fone,0,2) == 55 and substr($fone,4,1) == 8 and strlen($fone) == 12 or 
			substr($fone,0,2) == 55 and substr($fone,4,1) == 9 and strlen($fone) == 12 ) {
			return 'cel';
		} else if ( $this->classe_conexao->query("SELECT cidade FROM opt_prefixos WHERE prefixo = '".substr($fone,0,4)."'")->rowCount() > 0 and strlen($fone) == 8 ) {
			return 'tel';
		} else if ( $this->classe_conexao->query("SELECT cidade FROM opt_prefixos WHERE prefixo = '".substr($fone,2,4)."'")->rowCount() > 0 and strlen($fone) == 10 ) {
			return 'tel';
		} else if ( $this->classe_conexao->query("SELECT cidade FROM opt_prefixos WHERE prefixo = '".substr($fone,4,4)."'")->rowCount() > 0 and strlen($fone) == 12 ) {
			return 'tel';
		} else {
			return false;
		}
	}
	
	//	Ao validar o fone caso esteja tudo certo o sistema retorna true.
	function Validar_Fone($fone) {
		if ( $this->Tipo_Fone($fone) != null ) { 
			if ( empty($fone) ) {
				return 'Insira o fone';
			} else if ( !is_numeric($fone) ) {
				return 'Coloque apenas números.';
			} else if ( substr($fone,0,2) != 55 and $this->Tipo_Fone($fone) == 'tel' or 
			substr($fone,0,2) != 55 and $this->Tipo_Fone($fone) == 'cel') {
				return 'Lembre-se de colocar o DDD.';
			} else if ( strlen($fone) < 12 and $this->Tipo_Fone($fone) == 'tel' or 
			strlen($fone) < 12 and $this->Tipo_Fone($fone) == 'cel' ) {
				return 'Digite o número de telefone corretamente. Lembre-se de colocar o DDD.';
			} else {
				return 'ok';
			}
		} else {
			return 'Digite o número de telefone corretamente.';
		}
	}
	
	//	Verificar se o prefíxo condiz com a cidade.
    function Validar_Prefixo_Cidade($fone, $cidade) {
    	if ( $this->Tipo_Fone($fone) == 'tel' ) {
			//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
			require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
			
			//	Iniciando classes.
			$this->classe_conexao = new conexao;
			//---

			//	Executa a instrução de consulta
			$this->sql = $this->classe_conexao->query("SELECT cidade FROM opt_prefixos WHERE prefixo = '".substr($fone,0,4)."'")->fetch();
			if ( (INT)$this->sql[0] == $cidade ) {
				return true;
			} else {
				//	Executa a instrução de consulta
				$this->sql2 = $this->classe_conexao->query("SELECT cidade FROM opt_prefixos WHERE prefixo = '".substr($fone,4,4)."'")->fetch();
				if ( (INT)$this->sql2[0] == $cidade ) {
					return true;
				} else {
					return false;
				}
			}
		} else {
			return true;
		}
    }
	
	//	Informar qual cidade pertence tal prefíxo.
	function Cidade_Prefixo($fone) {
		if ( $this->Tipo_Fone($fone) == 'tel' ) {
			//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
			
			$this->classe_conexao = new conexao;
		
			//	Executa a instrução de consulta
			$this->result = $this->classe_conexao->query("SELECT cidade FROM opt_prefixos WHERE prefixo = '".substr($fone,4,4)."'")->fetch();
			
			if ( (INT)$this->result[0] > 0 ) {
				return $this->result[0];
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	//	Formatação usada para executar script.
	function Formatar_Fone_Script($fone) {
		if ( !empty($fone) ) {
			if ( substr($fone,0,2) != 55 and $this->Tipo_Fone('55'.$fone) == 'tel' or 
			substr($fone,0,2) != 55 and $this->Tipo_Fone('55'.$fone) == 'cel' ) {
				$this->fone = '55'.$fone;
			} else {
				$this->fone = $fone;
			}
			
			$this->fone	= str_replace( ' ', '', $this->fone);
			$this->fone	= str_replace( '-', '', $this->fone);
			
			return $this->fone;
		}
	}
	
	//	Formatação usada para executar script.
	function Formatar_Fone_Dinamica($fone) {
		if ( $this->Tipo_Fone($this->Formatar_Fone_Script($fone)) == 'tel' or 
		$this->Tipo_Fone($this->Formatar_Fone_Script($fone)) == 'cel' ) {
			$this->fone = substr($this->Formatar_Fone_Script($fone),2,10);
		} else {
			$this->fone = $fone;
		}
		
		return $this->fone;
	}
	
	//	Formatação usada para executar script.
	function Formatar_Fone_Dinamica2($fone) {
		if ( $this->Tipo_Fone($this->Formatar_Fone_Script($fone)) == 'tel' or 
		$this->Tipo_Fone($this->Formatar_Fone_Script($fone)) == 'cel' ) {
			$this->fone = substr($this->Formatar_Fone_Script($fone),4,8);
		} else {
			$this->fone = $fone;
		}
		
		return $this->fone;
	}
	
	//	Formata o fone para exibir no site.
	function Formatar_Fone_Site($fone) {
		if ($fone) {
			if ( $this->Tipo_Fone($fone) == 'especiais' ) {
				return $fone;
			} else if ( $this->Tipo_Fone($fone) == '4003' ) {
				return substr($fone,0,4)." ".substr($fone,4,4);
			} else if ( $this->Tipo_Fone($fone) == '0800' and strlen($fone) == 10 ) {
				return substr($fone,0,4)." ".substr($fone,4,2)." ".substr($fone,6,4);
			} else if ( $this->Tipo_Fone($fone) == '0800' and strlen($fone) == 11 ) {
				return substr($fone,0,4)." ".substr($fone,4,3)." ".substr($fone,7,4);
			} else if ( $this->Tipo_Fone($fone) == 'tel' ) {
				return substr($fone,2,2)." ".substr($fone,4,4)."-".substr($fone,8,4); 
			} elseif ( $this->Tipo_Fone($fone) == 'cel' ) {
				return substr($fone,2,2)." ".substr($fone,4,4)."-".substr($fone,8,4);
			}
		} 
	}
	
	//	Formata o fone para exibir na página das empresas.
	function Formatar_Fone_Empresa($fone) {
		if ($fone) {
			if ( $this->Tipo_Fone($fone) == 'especiais' ) {
				return $fone;
			} else if ( $this->Tipo_Fone($fone) == '4003' ) {
				return substr($fone,0,4)." ".substr($fone,4,4);
			} else if ( $this->Tipo_Fone($fone) == '0800' and strlen($fone) == 10 ) {
				return substr($fone,0,4)." ".substr($fone,4,2)." ".substr($fone,6,4);
			} else if ( $this->Tipo_Fone($fone) == '0800' and strlen($fone) == 11 ) {
				return substr($fone,0,4)." ".substr($fone,4,3)." ".substr($fone,7,4);
			} else if ( $this->Tipo_Fone($fone) == 'tel' ) {
				return substr($fone,4,4)."-".substr($fone,8,4); 
			} elseif ( $this->Tipo_Fone($fone) == 'cel' ) {
				return substr($fone,4,4)."-".substr($fone,8,4);
			}
		} 
	}
	
	function Verificacao($fone) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		$this->con_fone = $this->classe_conexao->query("SELECT id FROM fones WHERE id = '$fone' and verificado = 's'")->fetchAll();
		return (INT)count($this->con_fone);
	}
	
	function inserir_fone($fone) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		//	Iniciando classes.
		$this->classe_conexao = new conexao;
		$this->classe_fone = new fone;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_configuracoes = new configuracoes;
		//---
		
		$fone = $this->classe_formatacoes->retira_simbolos($fone);
		
		if ( (INT)$fone == 0 ) {
			return 0;
		} else {
			$this->con_fone = $this->classe_conexao->query("SELECT id FROM fones WHERE fone = '$fone'");
			if ( $this->con_fone->rowCount() == 1 ) {
				$this->linha_fone =	$this->con_fone->fetch();
				
				$this->sql = $this->classe_conexao->prepare("UPDATE fones SET ativo = 's' WHERE id = '".$this->linha_fone['id']."' and ativo = 'n'");
				$this->sql->execute();
				
				return $this->linha_fone['id'];
			} else if ($this->con_fone->rowCount() == 0 ) {
				$this->sql = $this->classe_conexao->prepare("INSERT INTO fones (id_operadora, fone, data_cadastro, data_tivacao) VALUES (?,?,?,?)");	
				$this->sql->bindValue(1, $this->classe_fone->Detectar_Operadora($fone));
				$this->sql->bindValue(2, $fone);
				$this->sql->bindValue(3, $this->classe_configuracoes->imprimir_data());
				$this->sql->bindValue(4, $this->classe_configuracoes->imprimir_data());
				$this->sql->execute();
				
				return $this->classe_conexao->lastInsertId();
				
				$this->sql = $this->classe_conexao->prepare("UPDATE fones SET ativo = 's' WHERE id = '".$this->classe_conexao->lastInsertId()."' and ativo = 'n'");
				$this->sql->execute();
				
			}
		}
	}
	
}