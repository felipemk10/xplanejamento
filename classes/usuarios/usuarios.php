<?php
class usuarios {
	function atualizar_tour($id_usuario,$valor_tour) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		$this->sql = $this->classe_conexao->prepare("UPDATE usuarios SET tour = '$valor_tour' WHERE id = '$id_usuario'");
		$this->sql->execute();
	}

	function consulta($consulta) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		return $this->classe_conexao->query($consulta);
	}
	
	function update($consulta) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		$this->classe_conexao = new conexao;
		$this->sql = $this->classe_conexao->prepare($consulta);
		return $this->sql->execute();
	}
	
	function usuario($id_usuario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		$this->con_usuario = $this->classe_conexao->query("SELECT 
		usuarios.id, 
		usuarios.nome, 
		usuarios.sobrenome,
		usuarios.id_fone, 
		usuarios.id_email, 
		usuarios.id_cidade, 
		usuarios.senha_padrao
		
		FROM usuarios
		
		WHERE usuarios.id = '$id_usuario' and usuarios.ativo = 's'");
		return $this->con_usuario->fetch();
	}
	
	//	Se há alguma ligação com alguma empresa e se for responsável pela mesma.
	function usuario_colaborador($id_usuario, $id_empresa) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		
		$id_empresa = (INT)$id_empresa;
		$id_usuario = (INT)$id_usuario;
		
		if ( $this->classe_conexao->query("SELECT id FROM usuarios_empresas WHERE id_empresa = '$id_empresa' and id_usuario = '$id_usuario' and tipo = 'adm'")->rowCount() > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	//	Se há alguma ligação com alguma empresa e se for responsável pela mesma.
	function usuario_empresas($id_usuario) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
		
		$this->classe_conexao = new conexao;
		
		return $this->classe_conexao->query("SELECT empresas.id, empresas.fantasia, empresas.url, usuarios_empresas.tipo 
		FROM usuarios_empresas 
		INNER JOIN empresas ON empresas.id = usuarios_empresas.id_empresa
		
		WHERE usuarios_empresas.id_usuario = '$id_usuario' and usuarios_empresas.tipo != 'add'");
	}
}