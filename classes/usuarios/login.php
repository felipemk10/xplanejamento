<?php
class login {
	function logar($email,$senha) {
		//	A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
		require_once(''.$_SERVER['DOCUMENT_ROOT'].'/paginas/scripts/autoload.php');
	
		//	Iniciando classses.
		$this->classe_conexao = new conexao;
		$this->classe_formatacoes = new formatacoes;
		$this->classe_validacoes = new validacoes;
		$this->classe_configuracoes = new configuracoes;
		
		//	Rodando anti-injecgtion nas variáveis.
		$email = $this->classe_validacoes->anti_injection($email);
		$senha = $this->classe_validacoes->anti_injection($senha);
		//---
		
		if ( empty($email) ) {
			return 'Digite seu e-mail.';
		} else if ( !$this->classe_validacoes->email($email) ) {
			return 'Digite seu e-mail corretamente.';
		} else if ( empty($senha) ) {
			return 'Digite sua senha.';
		} else {
			//	Criptografando a variável.
			$senha = md5($senha);
			
			if ( (INT)$this->classe_formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode') == 0 and !empty($email) and !empty($senha) ) {
				
				// consultando se o usuário enviou a senha correta
			    $this->sql_usuario = $this->classe_conexao->query("SELECT usuarios.id FROM usuarios
		        INNER JOIN emails ON emails.email = '".$email."' and emails.ativo = 's'
		        WHERE
	    		usuarios.id_email = emails.id and usuarios.senha = '".$senha."' and usuarios.ativo = 's'");

			    //  Verifica se encontra o usuário administrador e grava as sessões correspondentes.
			    if ( $this->sql_usuario->rowCount() == 1 ) {	
			    	$this->linha_usu = $this->sql_usuario->fetch();
			        
			        $_SESSION['id_usuario'] = $this->classe_formatacoes->criptografia($this->linha_usu['id'],'base64','encode');
			        $_SESSION["sessiontime"] = time();
		            
		            // Guardando hash da senha.
		            $this->sql = $this->classe_conexao->prepare("UPDATE usuarios SET status = 'on', acessos = acessos+1, ultimo_acesso = '".$this->classe_configuracoes->imprimir_data()."', ultima_acao = '".$this->classe_configuracoes->imprimir_data()."' WHERE id = '".$this->linha_usu['id']."'");
					$this->sql->execute();
					
					//	Iniciando classe.
					$this->classe_usuarios = new usuarios;
					//---
					
					if ( $this->classe_usuarios->usuario_colaborador($this->linha_usu['id'],1) ) {
						//	Registrando ID da empresa sobre uma sessão.
						$_SESSION['id_empresa'] = $this->classe_formatacoes->criptografia(1,'base64','encode');;
						
			        	//	Redirecionando para o sistema de colaboradores.
			        	header("Location: ".$this->classe_configuracoes->url_acesso()."colaboradores/estaticas/buscar.php");
			        } else if ( $this->classe_usuarios->usuario_empresas($this->linha_usu['id'])->rowCount() == 1 ) {
				        $this->linha_usuario_empresa = $this->classe_usuarios->usuario_empresas($this->linha_usu['id'])->fetch();
				        //	Registrando ID da empresa sobre uma sessão.
				        $_SESSION['id_empresa'] = $this->classe_formatacoes->criptografia($this->linha_usuario_empresa['id'],'base64','encode');
				        //	Redirecionando para o sistema Web SMS.
			        	header("Location: ".$this->classe_configuracoes->url_acesso()."paginas/sms/estaticas/");
			        }
			    }
			} else {
				return 'Poxa, e-mail e/ou senha está incorreto.<br />Tente desta vez diferente!';
			}
		}
	}
}