<?php

// * @author Andre Lourenco Pedroso - alp.pedroso@gmail.com
// *
// * @date 15 de Janeiro de 2007
// *
 
/**
 *
 * - Manipulacao de Data ou Hora.
 *
 * 		Operacoes: soma DIA, MES ,ANO, HORA, MINUTOS, SEGUNDOS.
 *		Formatos :
 *			Data: 15/01/2007
 *			Hora: 10:35:00
 * 		Para subtrair, basta passar um valor negativo:
 * 		Ex:
 * 			$obj->somaDia(-10);
 *
 * - Calcula diferenca entre duas datas.
 *
 * 		Operacoes: difDataHora.
 *		Formatos :
 *			Data: 15/01/2007 10:35:00
 * 		E necessario passar duas datas como parametro e o tipo de retorno desejado:
 * 		Ex:
 * 			$obj->difDataHora($dataMenor,$dataMaior,"m");
 *
 */
class FuncDataHora
{
	private $data;
	private $hora;
 
	function FuncDataHora($data="",$hora="")
	{
		if($hora=="")
		{
			$hora = date("H:i:s");
		}
		if($data=="")
		{
			$data = date("Y-m-d");
		}
		else if ($this->validaData($data,"d"))
		{
			die ("Padrao de data ($data) invalido! - Padrao = 15/01/2007");
		}
		$this->data = explode("/",$data);
		$this->hora = explode(":",$hora);
	}
	private function validaData($data,$op)
	{
		switch($op)
		{
			case "d": // Padrao: 15/01/2007
				$er = "/(([0][1-9]|[1-2][0-9]|[3][0-1])-([0][1-9]|[1][0-2])-([0-9]{4}))/";
				if(preg_match($er,$data))
				{
					return 0;
				}
				else
				{
					return 1;
				}
				break;
			case "dh": // Padrao 15/01/2007 10:30:00
				$er = "/(([0][1-9]|[1-2][0-9]|[3][0-1])-([0][1-9]|[1][0-2])-([0-9]{4})*)/";
				if(preg_match($er,$data))
				{
					return 0;
				}
				else
				{
					return 1;
				}
				break;
		}
	}
	// DATA
	public function somaDia($dias=1)
	{
		$this->FuncDataHora(strftime("%Y-%m-%d", mktime(0, 0, 0, $this->data[1], $this->data[0]+$dias, $this->data[2])),"");
		return $this->data;
	}
	public function somaMes($meses=1)
	{
		$this->FuncDataHora(strftime("%Y-%m-%d", mktime(0, 0, 0, $this->data[1]+$meses, $this->data[0], $this->data[2])),"");
		return $this->data;
	}
	public function somaAno($anos=1)
	{
		$this->FuncDataHora(strftime("%Y-%m-%d", mktime (0, 0, 0, $this->data[1], $this->data[0], $this->data[2]+$anos)),"");
		return $this->data;
	}
	public function getData()
	{
		return $this->data[0]."-".$this->data[1]."-".$this->data[2];
	}
	// HORA
	public function somaSegundo($segundos=1)
	{
		$this->FuncDataHora("",strftime("%H:%M:%S",mktime($this->hora[0],$this->hora[1],$this->hora[2]+$segundos, 0, 0, 0)));
		return $this->hora;
	}
	public function somaMinuto($minutos=1)
	{
		$this->FuncDataHora("",strftime("%H:%M:%S",mktime($this->hora[0],$this->hora[1]+$minutos,$this->hora[2], 0, 0, 0)));
		return $this->hora;
	}
	public function somaHora($horas=1)
	{
		$this->FuncDataHora("",strftime("%H:%M:%S",mktime($this->hora[0]+$horas,$this->hora[1],$this->hora[2], 0, 0, 0)));
		return $this->hora;
	}
	public function getHora()
	{
		return $this->hora[0].":".$this->hora[1].":".$this->hora[2];
	}
 
 
	/**
	 *
	 * Retorna diferença entre as datas em Dias, Horas ou Minutos
	 * Function difDataHora(data menor, [data maior],[dias horas minutos segundos])
	 *
	 * Formato 04/05/2006 12:00:00
	 *
	 * Chame a funcao com o valor NULL como 'data maior' para 'data maior' = data atual.
	 *
	 * Formatacao do retorno [dias horas minutos segundos]:
	 *
	 * "s": Segundos
	 * "m": Minutos
	 * "H": Horas
	 * "h": Horas arredondada
	 * "D": Dias
	 * "d": Dias arredontados
	 *
	 * Original: Gambiarra.com.br Bozo@gambiarra.com.br
	 *
	 * Modificado: Andre Lourenco Pedroso alp.pedroso@gmail.com
	 * Data 15/01/2007 10:00
	 */
	public function difDataHora($datamenor,$datamaior="",$tipo="")
	{	
		if($this->validaData($datamenor,"dh"))
		{
			die ("data errada - $datamenor");
		}
		if($datamaior==""){
			echo $datamaior = date("Y-m-d H:i:s");
		}
		if($tipo==""){
			$tipo = "h";
		}
		list ($anomenor, $mesmenor,$diamenor, $horamenor, $minutomenor, $segundomenor) = preg_split("/[-: ]/",$datamenor);
		list ($anomaior, $mesmaior, $diamaior, $horamaior, $minutomaior, $segundomaior) = preg_split("/[-: ]/",$datamaior);
		return mktime($horamaior,$minutomaior,$segundomaior,$mesmaior,$diamaior, $anomaior).' = '.mktime($horamenor,$minutomenor,$segundomenor,$mesmenor,$diamenor, $anomenor);
		$segundos =	mktime($horamaior,$minutomaior,$segundomaior,$mesmaior,$diamaior, $anomaior)-mktime($horamenor,$minutomenor,$segundomenor,$mesmenor,$diamenor, $anomenor);
 		switch($tipo){
			case "s": // Segundo
				$diferenca = $segundos;
				break;
			case "M": // Minuto
				$diferenca = $segundos/60;
			case "m": // Minuto Arrendondado
				$diferenca = round($segundos/60);
				break;
			case "H": // Hora
				$diferenca = $segundos/3600;
				break;
			case "h": // Hora Arredondada
				$diferenca = round($segundos/3600);
				break;
			case "D": // Dia
				$diferenca = $segundos/86400;
				break;
			case "d": // Dia Arredondado
				$diferenca = round($segundos/86400);
				break;
		}
		return $diferenca;
	}
}

//	-----------

class formatacoes {
	function termo_abreviado($termo) {
		$this->classe_formatacoes = new formatacoes;
		$termo = $this->classe_formatacoes->converter_termo($termo,0);
		$termo = str_replace('industrial', 'ind', $termo);
		$termo = str_replace('loteamento', 'lot', $termo);
		$termo = str_replace('sao', 's', $termo);
		$termo = str_replace('distrito', 'dt', $termo);
		$termo = str_replace('santo', 'sto', $termo);
		$termo = str_replace('santa', 'sta', $termo);
		$termo = str_replace('residencial', 'res', $termo);
		$termo = str_replace('jardim', 'jd', $termo);
		$termo = str_replace('parque', 'prq', $termo);
		$termo = str_replace('setor', 'st', $termo);
		return $this->classe_formatacoes->converter_termo($termo,1);
	}
	

	function string_mes($data) {
		if ( strlen($data) >= 1 and strlen($data) <=2 ) {
			switch ( $data ){
				case 1: $sting_data = "Janeiro"; break;
				case 2: $sting_data = "Fevereiro"; break;
				case 3: $sting_data = "Março"; break;
				case 4: $sting_data = "Abril"; break;
				case 5: $sting_data = "Maio"; break;
				case 6: $sting_data = "Junho"; break;
				case 7: $sting_data = "Julho"; break;
				case 8: $sting_data = "Agosto"; break;
				case 9: $sting_data = "Setembro"; break;
				case 10: $sting_data = "Outubro"; break;
				case 11: $sting_data = "Novembro"; break;
				case 12: $sting_data = "Dezembro"; break;
			}
		} else if ( strlen($data) == 7 ) {
			switch ( substr($data,5,2) ){
				case 1: $sting_data = "Janeiro"; break;
				case 2: $sting_data = "Fevereiro"; break;
				case 3: $sting_data = "Março"; break;
				case 4: $sting_data = "Abril"; break;
				case 5: $sting_data = "Maio"; break;
				case 6: $sting_data = "Junho"; break;
				case 7: $sting_data = "Julho"; break;
				case 8: $sting_data = "Agosto"; break;
				case 9: $sting_data = "Setembro"; break;
				case 10: $sting_data = "Outubro"; break;
				case 11: $sting_data = "Novembro"; break;
				case 12: $sting_data = "Dezembro"; break;
			}
			if ( isset($data) and substr($data,0,4) != date('Y') ) {
				$sting_data = substr($data,5,2).'/'.substr($data,0,4);
			} else {
				$sting_data = $sting_data;
			}
		}
		return $sting_data;
	}

	function tipo_end_extenco($tipo_end) {
		if ( $tipo_end == 'av' ) {
			return 'Avenida';
		} else if ( $tipo_end == 'qd' ) {
			return 'Quadra';
		} else if ( $tipo_end == 'est' ) {
			return 'Estrada';
		} else if ( $tipo_end == 'tv' ) {
			return 'Travessa';
		} else if ( $tipo_end == 'al' ) {
			return 'Alameda';
		} else if ( $tipo_end == "pc" ) {
			return 'Praça';
		} else if ( $tipo_end == 'r' ) {
			return 'Rua';
		} else if ( $tipo_end == 'condo' ) {
			return 'Condominio';
		} else if ( $tipo_end == 'vl' ) {
			return 'Viela';
		} else if ( $tipo_end == 'rod' ) {
			return 'Rodovia';
		} else {
			return  $tipo_end;
		}
	}
	function formatar_endereco($uf,$cidade,$cep,$bairro,$endereco,$tipo_end,$numero,$compl,$tipo_compl1,$tipo_compl2,$tipo_compl3,$compl1,$compl2,$compl3,$exibir_cidade) {
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		
		$this->var_end = explode(' ', $endereco);
		$this->end_total = count($this->var_end);
		
		for ( $z = 0; $z < $this->end_total; $z++ ) {
			if ( $this->validacoes->detectar_artigo('sem_romanos',$this->var_end[$z]) == 1 ) {
				$this->string_end .= ' '.strtolower($this->var_end[$z]);
			} else {
				$this->string_end .= ' '.ucfirst($this->formatacoes->converter_termo($this->var_end[$z], '0-first'));
			}
		}
		
		$this->msg .= $this->formatacoes->tipo_end_extenco($tipo_end).$this->string_end.', ';
		//	Apagando variáveis.
		unset($this->string_end, $this->var_end, $this->end_total);
		
		if ( !empty($numero) ) {
			$this->msg .= $numero.', ';
		}
		
		if ( !empty($tipo_compl1) ) {
			//	Caso exista um valor nos compl1 ao 3 não utilizar o campo compl (antigo).
			$this->campo_compl = 'nao';
			$this->msg .= ucfirst($this->formatacoes->converter_termo($tipo_compl1, 0)).' '.$compl1.', ';
		}
		if ( !empty($tipo_compl2) ) {
			//	Caso exista um valor nos compl1 ao 3 não utilizar o campo compl (antigo).
			$this->campo_compl = 'nao';
			$this->msg .= ucfirst($this->formatacoes->converter_termo($tipo_compl2, 0)).' '.$compl2.', ';
		}
		if ( !empty($tipo_compl3) ) {
			//	Caso exista um valor nos compl1 ao 3 não utilizar o campo compl (antigo).
			$this->campo_compl = 'nao';
			$this->msg .= ucfirst($this->formatacoes->converter_termo($tipo_compl3, 0)).' '.$compl3.', ';
		}
		
		if ( !empty($compl) and $this->campo_compl != 'nao' ){
			$this->msg .= $compl.', ';
		}
		
		//	Apaga variável.
		unset($this->campo_compl);
		
		$this->var_end = explode(' ', $bairro);
		$this->end_total = count($this->var_end);
		for ( $z = 0; $z < $this->end_total; $z++ ) {
			if ( $this->validacoes->detectar_artigo('sem_romanos',$this->var_end[$z]) == 1 ) {
				$this->msg .= strtolower($this->var_end[$z]).' ';
			} else {
				$this->msg .= ucfirst($this->formatacoes->converter_termo($this->var_end[$z], '0-first')).' ';
			}
		}
		//	Apagando variáveis.
		unset($this->string_end, $this->var_end, $this->end_total);
		
		$this->msg .= '<br />';
		if ( $exibir_cidade == 'sim' ) { $this->retorno = $this->msg.$this->formatacoes->nome_cidade($cidade).'/'.$uf.', '.substr($cep,0,5).'-'.substr($cep,5,3); } else if ( $exibir_cidade == 'nao' ) {
			$this->retorno = $this->msg;
		}
		
		unset($this->msg);
		return $this->retorno;
		
	}

	function formatar_data($separacao,$data) {
		$this->dia = substr($data,8,2);
        $this->mes = substr($data,5,2);
        $this->ano = substr($data,0,4);
        return $this->dia.$separacao.$this->mes.$separacao.$this->ano;
	}
	function retira_acentos($str, $enc = "UTF-8") {
		$this->acentos = array(
		'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
		'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
		'C' => '/&Ccedil;/',
		'c' => '/&ccedil;/',
		'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
		'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
		'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
		'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
		'N' => '/&Ntilde;/',
		'n' => '/&ntilde;/',
		'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
		'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
		'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
		'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
		'Y' => '/&Yacute;/',
		'y' => '/&yacute;|&yuml;/',
		'a.' => '/&ordf;/',
		'o.' => '/&ordm;/');
	
		return preg_replace($this->acentos, array_keys($this->acentos), htmlentities($str,ENT_NOQUOTES, $enc));
	}
	//	Retirar os simbolos da string.
	function retira_simbolos($string) {
		$this->string = trim($string);
	    $this->string = strtolower($this->string);
	
	    $this->string = str_replace( '-', '', $this->string);
	    $this->string = str_replace( '\\', '', $this->string);
	    $this->string = str_replace( '/', '', $this->string);
	    $this->string = str_replace( '´', '', $this->string);
	    $this->string = str_replace( '`', '', $this->string);
	    $this->string = str_replace( '~', '', $this->string);
	    $this->string = str_replace( '^', '', $this->string);
	    $this->string = str_replace( '¨', '', $this->string);
	    
	    $this->string = str_replace( '!', '', $this->string);
	    $this->string = str_replace( '@', '', $this->string);
	    $this->string = str_replace( '#', '', $this->string);
	    $this->string = str_replace( '$', '', $this->string);
	    $this->string = str_replace( '%', '', $this->string);
	    $this->string = str_replace( '¨', '', $this->string);
	    $this->string = str_replace( '*', '', $this->string);
	    $this->string = str_replace( '(', '', $this->string);
	    $this->string = str_replace( ')', '', $this->string);
	    $this->string = str_replace( '_', '', $this->string);
	    $this->string = str_replace( '+', '', $this->string);
	    $this->string = str_replace( '=', '', $this->string);
	    $this->string = str_replace( '{', '', $this->string);
	    $this->string = str_replace( '[', '', $this->string);
	    $this->string = str_replace( '}', '', $this->string);
	    $this->string = str_replace( ']', '', $this->string);
	    $this->string = str_replace( '|', '', $this->string);
	    $this->string = str_replace( '<', '', $this->string);
	    $this->string = str_replace( '>', '', $this->string);
	    $this->string = str_replace( '.', '', $this->string);
	    $this->string = str_replace( ':', '', $this->string);
	    $this->string = str_replace( ';', '', $this->string);
	    $this->string = str_replace( '?', '', $this->string);
	    $this->string = str_replace( '¹', '', $this->string);
	    $this->string = str_replace( '²', '', $this->string);
	    $this->string = str_replace( '³', '', $this->string);
	    $this->string = str_replace( '£', '', $this->string);
	    $this->string = str_replace( '¢', '', $this->string);
	    $this->string = str_replace( '¬', '', $this->string);
	    $this->string = str_replace( '§', '', $this->string);
	    $this->string = str_replace( 'ª', '', $this->string);
	    $this->string = str_replace( 'º', '', $this->string);
	    $this->string = str_replace( '"', '', $this->string);
	    $this->string = str_replace( "'", '', $this->string);
	    return $this->string;
	}
	//	Retirar artigos da string.
	function retira_artigos($string) {
	    $this->string = trim($string);
	    $this->string = strtolower($this->string);
	
	    $this->palavras = explode(" ", $this->string);
		$this->t_palavras = count($this->palavras);
		
		$this->string = null;
		for($i = 0; $i < $this->t_palavras; $i++){
	
		    $this->palavras[$i] = str_replace( ' da ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' de ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' do ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' das ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' dos ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' a ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' e ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' i ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' o ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' u ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' as ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' os ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' em ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' para ', '', ' '.$this->palavras[$i].' ');
		    $this->palavras[$i] = str_replace( ' pra ', '', ' '.$this->palavras[$i].' ');
			
			$this->nova_string .= ' '.trim($this->palavras[$i]);
		
		}
		return $this->nova_string;
		//	Apagando variável.
		unset($this->nova_string);
	}
	
	//	Converte o termo tanto para maíusculo, quanto para minúsculo.
	function converter_termo($termo, $tipo) { 
	    if ( $tipo == '1') {
	    	$this->palavra = str_replace( '\\', '', strtr(strtoupper($termo),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß"));
	    } else if ( $tipo == '0') {
	    	$this->palavra = str_replace( '\\', '', strtr(strtolower($termo),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ")); 
	    } else if ( $tipo == '0-first') {
	    	if ( $this->retira_acentos(substr($termo,0,1)) == NULL ) {
	    		$this->l_maiuscula = str_replace( '\\', '', strtr(substr($termo,0,2), "àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß"));
	    	
		    	$this->palavra = str_replace( '\\', '', $this->l_maiuscula.strtr(strtolower(substr($termo,2,strlen($termo))),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ"));
	    	} else {
	    		$this->palavra = str_replace( '\\', '', ucfirst(strtr(strtolower($termo),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ")));
	    	}
	    	//	Correção de exibição para números romanos.
    		$this->explodir	= explode(' ', trim($this->palavra));
			$this->t_palavras = count($this->explodir);
			$this->n_romanos = array('X','IX','VIII','VII','VI','V','IV','III','II','I');
			
			for($this->i = 0; $this->i < $this->t_palavras; $this->i++){
				if ( in_array($this->converter_termo($this->explodir[$this->i],1), $this->n_romanos) ) {
	    			$this->palavra2 .= ' '.$this->converter_termo($this->explodir[$this->i],1);
    			} else {
    				$this->palavra2 .= ' '.$this->explodir[$this->i];
    			}
    		}
    		$this->palavra = trim($this->palavra2);
    		//	Apagando variável.
    		unset($this->explodir,$this->t_palavras,$this->palavra2);
    		//---
		}
	    return $this->palavra;
	    //	Apagando variável.
	    unset($this->palavra);
	}
	//	Formatação padrão do nome de cidade.
	function nome_cidade($cidade) {
		$this->formatacoes = new formatacoes;
		$this->validacoes = new validacoes;
		$this->cidade = explode(' ', $cidade);
		$this->t_palavras =	count($this->cidade);
		for($this->i = 0; $this->i < $this->t_palavras; $this->i++){
			if ( $this->i == 0 ) {
				$this->nome_cidade = ucfirst($this->formatacoes->converter_termo($this->cidade[$this->i], 0));
			} else {
				if ( $this->validacoes->detectar_artigo('sem_romanos',$this->cidade[$this->i]) == 1 ) {
					$this->nome_cidade = $this->nome_cidade.' '.strtolower($this->cidade[$this->i]);
				} else {
					$this->nome_cidade = $this->nome_cidade.' '.ucfirst($this->formatacoes->converter_termo($this->cidade[$this->i], 0));
				}
			}
		}
		return $this->nome_cidade;
	}
	//	Delimitar visualização do texto e adiciona uma opção de visualização completa
	function longo_texto($id_publicacao,$texto) {
		if ( strlen(nl2br($texto)) <= 400 ) {
			return nl2br($texto); 
		} else {
			$n_texto	=	strlen(nl2br($texto))-400;
			if ( (INT)$id_publicacao >= 0 ) {
				return substr(nl2br($texto), 0,400)."<span id='tres_pontos$id_publicacao'>...</span><script>$('#ler_mais$id_publicacao').show();$('#ler_mais$id_publicacao').click(function(){ $('#mais_texto$id_publicacao').show();$('#tres_pontos$id_publicacao').hide();$('#ler_mais$id_publicacao').hide(); });</script><span id='ler_mais$id_publicacao' class='ler_mais' style='display:none;'>Ler mais</span><span id='mais_texto$id_publicacao' style='display:none;'>".substr(nl2br($texto), 400,$n_texto)."</span>";
			}
		}
	}
	
	// Autor: Uêmerson Alves
	// Data: 04/11/2011 ás 01:17
	// Objetivo: Simplificar leitura de data dos posts.
	function string_tempo_post($datapost, $dataatual) {
		
		$this->obj	=	new FuncDataHora();
		$this->valor	=	$this->obj->difDataHora($datapost, $dataatual,"s");
		
		$this->aa		=	"há,hora,horas,Ontem,às,de";
		$this->a		=	explode(',', $this->aa);		
		$this->pp		=	"segundo,segundos,minuto,minutos,hora,horas";
		$this->p		=	explode(',', $this->pp);
		
		if ( $this->valor >= 1 and $this->valor < 60 ) {
			$this->tipo	=	's';
		} else if ( $this->valor >= 60 and $this->valor < 3600 ) {
			$this->tipo	=	'm';
			$this->valor	=	round($this->valor/60);
		} else if ( $this->valor >= 3600 and $this->valor < 86400 ) {
			$this->tipo	=	'h';
			$this->valor	=	round($this->valor/3600);
		} else if ( $this->valor >= 86400) {
			$this->tipo	=	'd';
			$this->valor	=	round($this->valor/86400);
		}
		
		switch ( substr($datapost,5,2) ){
			case 1: $this->messtring = "Janeiro"; break;
			case 2: $this->messtring = "Fevereiro"; break;
			case 3: $this->messtring = "Mar&ccedil;o"; break;
			case 4: $this->messtring = "Abril"; break;
			case 5: $this->messtring = "Maio"; break;
			case 6: $this->messtring = "Junho"; break;
			case 7: $this->messtring = "Julho"; break;
			case 8: $this->messtring = "Agosto"; break;
			case 9: $this->messtring = "Setembro"; break;
			case 10: $this->messtring = "Outubro"; break;
			case 11: $this->messtring = "Novembro"; break;
			case 12: $this->messtring = "Dezembro"; break;
		}
		
		if ( $this->valor > 1 and $this->valor < 60 and $this->tipo == 's' ) {
			return $this->a[0].'&nbsp;'.$this->valor.'&nbsp;'.$this->p[1];
		} else if (  $this->valor == 1 and $this->tipo == 's' ) {
			return $this->a[0].'&nbsp;'.$this->valor.'&nbsp;'.$this->p[0];
		} else if ( $this->valor > 1 and $this->valor < 60 and $this->tipo == 'm' ) {
			return $this->a[0].'&nbsp;'.$this->valor.'&nbsp;'.$this->p[3];
		} else if (  $this->valor == 1 and $this->tipo == 'm' ) {
			return $this->a[0].'&nbsp;'.$this->valor.'&nbsp;'.$this->p[2];
		} else if (  $this->valor > 1 and $this->valor < 24 and $this->tipo == 'h' ) {
			return $this->a[0].'&nbsp;'.$this->valor.'&nbsp;'.$this->p[5];
		} else if (  $this->valor == 1 and $this->tipo == 'h' ) {
			return $this->a[0].'&nbsp;'.$this->valor.'&nbsp;'.$this->p[4];
		} else if (  $this->valor == 1 and $this->tipo == 'd' ) {
			return $this->a[3].'&nbsp;'.$this->a[4].'&nbsp;'.substr($datapost,11,5).'&nbsp;';
		} else if (  $this->valor > 1 and $this->tipo == 'd' and substr($datapost,0,4) == substr($dataatual,0,4) ) {
			return substr($datapost,8,2).'&nbsp;'.$this->a[5].'&nbsp;'.$this->messtring.'&nbsp;'.$this->a[4].'&nbsp;'.substr($datapost,11,5);
		} else if (  $this->valor > 1 and $this->tipo == 'd' and substr($datapost,0,4) < substr($dataatual,0,4) ) {
			return substr($datapost,8,2).'&nbsp;'.$this->a[5].'&nbsp;'.$this->messtring.'&nbsp;'.$this->a[5].'&nbsp;'.substr($datapost,0,4).'&nbsp;'.$this->a[4].'&nbsp;'.substr($datapost,11,5);
		}
		//return $this->valor;
	}
	//	Criptografia padrão.
	function criptografia($valor, $tipo, $acao){
		if ( $tipo == 'base64' ) {
			if ( $acao == 'encode' ) {
				return base64_encode(base64_encode(base64_encode('apisuporte'.$valor)));
			} else if ( $acao == 'decode' ) {
				return substr(base64_decode(base64_decode(base64_decode($valor))),10,11);
			}
		}
	}
	//	Retorna o termo no singular e plural.
	function singular_plural($termo){
	    // Here is the list of rules. To add a scenario,
	    // Add the plural ending as the key and the singular
	    // ending as the value for that key. This could be
	    // turned into a preg_replace and probably will be
	    // eventually, but for now, this is what it is.
	    //
	    // Note: The first rule has a value of false since
	    // we don't want to mess with words that end with
	    // double 's'. We normally wouldn't have to create
	    // rules for words we don't want to mess with, but
	    // the last rule (s) would catch double (ss) words
	    // if we didn't stop before it got to that rule. 
	    
	    $this->palavras = explode(' ', $termo);
		$this->n_palavras = count($this->palavras);
		
		for ($this->i=0; $this->i <= $this->n_palavras; $this->i++) {
		    $this->rules = array(
		    	'ais' => 'al',
		    	'al' => 'ais',
		    	
		    	'res' => 'r',
		    	'r' => 'res',
		    	
				'coes' => 'cao',
		    	'cao' => 'coes',
		    	
				'hoes' => 'hao',
		    	'hao' => 'hoes',
				
				'roes' => 'rao',
		    	'rao' => 'roes',
		    	
				'ioes' => 'iao',
		    	'iao' => 'ioes',
		    	
				'soes' => 'sao',
		    	'sao' => 'soes',
				
		    	'oes' => 'ao',
		    	
		    	'aes' => 'ao',
		    	'ao' => 'aes',
		    	
				'ais' => 'ai',
		    	'ai' => 'ais',
		    	
				'eis' => 'el',
		    	'el' => 'eis',
		    	
		    	'nis' => 'nil',
		    	'il' => 'nis',
		    	
		    	'ns' => 'm',
		    	'm' => 'ns',
		    	
		    	'il' => 'is',
		    	'is' => 'il',
		    	
		    	'is' => 'l',
		    	'l' => 'is',
		    	
		    	'a' => 'as',
		    	'as' => 'a',
		    	
		    	'es' => 'e',
		    	'e' => 'es',
		    	
				'is' => 'i',
		    	'i' => 'is',
		    	
		    	'os' => 'o',
		    	'o' => 'os',
		    	
				'us' => 'u',
		    	'u' => 'us',
		    	
		    	's' => '');
		    // Loop through all the rules and do the replacement. 
		    foreach(array_keys($this->rules) as $this->key){
		        // If the end of the word doesn't match the key,
		        // it's not a candidate for replacement. Move on
		        // to the next plural ending. 
		        if(substr($this->palavras[$this->i], (strlen($this->key) * -1)) != $this->key) 
		            continue;
		        // If the value of the key is false, stop looping
		        // and return the original version of the word. 
		        if($this->key === false) 
		            return $termo;
		        // We've made it this far, so we can do the
		        // replacement. 
		        $this->resultado = $this->resultado.' '.substr($this->palavras[$this->i], 0, strlen($this->palavras[$this->i]) - strlen($this->key)) . $this->rules[$this->key]; 
		    }
	    }
	    return $termo.' '.$this->resultado;
	}
}