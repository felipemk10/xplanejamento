<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$funcoes = new funcoes;
$validacoes = new validacoes;
$func_analise = new func_analise;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento, 7 = loteamento
$tipoprocesso = (int)$_GET['tipoprocesso'];
//  Função para includes reconhecer a página e modificar sua exibição.
$aprovacaologs = true;

$id_pro = (int)$_GET['id_pro'];

//  Se o usuário for analista, impede que ele edite a edição da análise.
 $consultaAnalista = $configuracoes->consulta("SELECT cg.nome, usuarios.tipousuario FROM geral.cg 
        INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg
      
      WHERE cg.id_cg = ".$formatacoes->criptografia($_SESSION['id_usuario'] ,'base64','decode')." and usuarios.ativo = true and usuarios.tipousuario = 'an'");

if ( $id_pro > 0  ) {
  //  Listando informações sobre o processo.
  $consulta = "SELECT 
    processos_analise.id_ana,
    processos_analise.id_pro";

    if ( $consultaAnalista->rowCount() == 0 ) {

      $consulta .=",
      processos_analise.tipo,
      processos_analise.situacaoprojeto,
      processos_analise.obsgerais,
      processos_analise.valor1,   
      processos_analise.valor2,   
      processos_analise.valor3,   
      processos_analise.valor4,   
      processos_analise.valor5,   
      processos_analise.valor6,   
      processos_analise.valor7,   
      processos_analise.valor8,   
      processos_analise.valor9,   
      processos_analise.valor10,  
      processos_analise.valor11,  
      processos_analise.valor12,  
      processos_analise.valor13,  
      processos_analise.valor14,  
      processos_analise.valor15,  
      processos_analise.valor16,  
      processos_analise.valor17,  
      processos_analise.valor18,  
      processos_analise.valor19,  
      processos_analise.valor20,  
      processos_analise.valor21,  
      processos_analise.valor22,  
      processos_analise.valor23,  
      processos_analise.valor24,  
      processos_analise.valor25,  
      processos_analise.valor26,  
      processos_analise.valor27,  
      processos_analise.valor28,  
      processos_analise.valor29,  
      processos_analise.valor30,  
      processos_analise.valor31,  
      processos_analise.valor32,  
      processos_analise.valor33,  
      processos_analise.valor34,  
      processos_analise.valor35,  
      processos_analise.valor36,  
      processos_analise.valor37,  
      processos_analise.valor38,  
      processos_analise.valor39,  
      processos_analise.valor40,  
      processos_analise.valor41,  
      processos_analise.valor42,  
      processos_analise.valor43,  
      processos_analise.valor44,  
      processos_analise.valor45,  
      processos_analise.valor46,  
      processos_analise.valor47,  
      processos_analise.valor48,  
      processos_analise.valor49,  
      processos_analise.valor50";
    }
                           
    $consulta .= "
    FROM 
    processos.processos_analise

    WHERE processos.processos_analise.id_pro = $id_pro and tipo = 'aa'";

    $consulta = $configuracoes->consulta($consulta);

    $linha = $consulta->fetch();

    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
    processos.id_pro,
    processos.endereco,
    processos.quadra,
    processos.lote,
    processos.numero,
    processos.bairro,
    processos.cidade,
    processos.estado,
    processos.tipoprocesso,
    processos.datahora, 
    processos.situacaoprojeto 

    FROM 

    processos.processos 

    WHERE processos.id_pro = $id_pro and processos.ativo = true");

    $linha2 = $consulta->fetch();
}

if ( $_POST['formulario'] == 'ok' ) {
  $id_ana = (int)$_POST['id_ana'];

  $situacaoprojeto = $_POST['situacaoprojeto'];
  $obsgerais = $_POST['obsgerais'];
  $valor1 = $_POST['valor1'];
  $valor2 = $_POST['valor2'];
  $valor3 = $_POST['valor3'];
  $valor4 = $_POST['valor4'];
  $valor5 = $_POST['valor5'];
  $valor6 = $_POST['valor6'];
  $valor7 = $_POST['valor7'];
  $valor8 = $_POST['valor8'];
  $valor9 = $_POST['valor9'];
  $valor10 = $_POST['valor10'];
  $valor11 = $_POST['valor11'];
  $valor12 = $_POST['valor12'];
  $valor13 = $_POST['valor13'];
  $valor14 = $_POST['valor14'];
  $valor15 = $_POST['valor15'];
  $valor16 = $_POST['valor16'];
  $valor17 = $_POST['valor17'];
  $valor18 = $_POST['valor18'];
  $valor19 = $_POST['valor19'];
  $valor20 = $_POST['valor20'];
  $valor21 = $_POST['valor21'];
  $valor22 = $_POST['valor22'];
  $valor23 = $_POST['valor23'];
  $valor24 = $_POST['valor24'];
  $valor25 = $_POST['valor25'];
  $valor26 = $_POST['valor26'];
  $valor27 = $_POST['valor27'];
  $valor28 = $_POST['valor28'];
  $valor29 = $_POST['valor29'];
  $valor30 = $_POST['valor30'];
  $valor31 = $_POST['valor31'];
  $valor32 = $_POST['valor32'];
  $valor33 = $_POST['valor33'];
  $valor34 = $_POST['valor34'];
  $valor35 = $_POST['valor35'];
  $valor36 = $_POST['valor36'];
  $valor37 = $_POST['valor37'];
  $valor38 = $_POST['valor38'];
  $valor39 = $_POST['valor39'];
  $valor40 = $_POST['valor40'];
  $valor41 = $_POST['valor41'];
  $valor42 = $_POST['valor42'];
  $valor43 = $_POST['valor43'];
  $valor44 = $_POST['valor44'];
  $valor45 = $_POST['valor45'];
  $valor46 = $_POST['valor46'];
  $valor47 = $_POST['valor47'];
  $valor48 = $_POST['valor48'];
  $valor49 = $_POST['valor49'];
  $valor50 = $_POST['valor50'];
  


  if ( $id_ana > 0 ) {
    $formulario = 'alteracao';
  } else {
    $formulario = 'cadastro';
  }

  
      $id_ana2 = $func_analise->manipulacoes(
      $id_ana,
      $formatacoes->criptografia($_SESSION['id_usuario'],'base64','decode'),
      $id_pro,
      'aa',
      $situacaoprojeto,
      $obsgerais,
      '',   
      '',   
      '',   
      '',   
      '',
      $valor1,   
      $valor2,   
      $valor3,   
      $valor4,   
      $valor5,   
      $valor6,   
      $valor7,   
      $valor8,   
      $valor9,   
      $valor10,  
      $valor11,  
      $valor12,  
      $valor13,  
      $valor14,  
      $valor15,  
      $valor16,  
      $valor17,  
      $valor18,  
      $valor19,  
      $valor20,  
      $valor21,  
      $valor22,  
      $valor23,  
      $valor24,  
      $valor25,  
      $valor26,  
      $valor27,  
      $valor28,  
      $valor29,  
      $valor30,  
      $valor31,  
      $valor32,  
      $valor33,  
      $valor34,  
      $valor35,  
      $valor36,  
      $valor37,  
      $valor38,  
      $valor39,  
      $valor40,  
      $valor41,  
      $valor42,  
      $valor43,  
      $valor44,  
      $valor45,  
      $valor46,  
      $valor47,  
      $valor48,  
      $valor49,  
      $valor50,
      $formulario);
 
    if ( $id_ana == 0 ) {
      $id_ana = $id_ana2;
    }

    //  Pega nome e e-mail do cliente e usuário.
    $tusuario = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos

      INNER JOIN geral.cg ON cg.id_cg = processos.id_cg
      WHERE processos.id_pro = $id_pro");
    $tusuario = $tusuario->fetch();

    $tautor = $configuracoes->consulta("SELECT cg.nome, cg.email FROM processos.processos_analise

      INNER JOIN geral.cg ON cg.id_cg = processos_analise.id_cg
      WHERE processos_analise.id_ana = $id_ana");
    $tautor = $tautor->fetch();

    //--Procedimento envio de e-mail (usando Mailjet)--

    if ( $tipoprocesso == 1 ) {
      $descr_tipoprocesso = 'Alvará de Construção';
    } else if ( $tipoprocesso == 2 ) {
      $descr_tipoprocesso = 'Alvará de Regularização de Obras';
    } else if ( $tipoprocesso == 3 ) {
      $descr_tipoprocesso = 'Alvará de Acréscimo de Área';
    } else if ( $tipoprocesso == 4 ) {
      $descr_tipoprocesso = 'Condomínio Edilício';
    } else if ( $tipoprocesso == 5 ) {
      $descr_tipoprocesso = 'Redimensionamento';
    } else if ( $tipoprocesso == 7 ) {//Irving
      $descr_tipoprocesso = 'Loteamento';
    }

    /*
      1 - A ser analisado = white, 
      2 - Em análise = #ddd, 
      3 - Pendência de documentos ou correção = #c4d79b, 
      4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
      5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
      6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
      7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
      8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
      9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
      10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
      11 - Aprovado = #eada6d, 
      12 - Dispensar Vistoria
    */

    if ( $situacaoprojeto == 1 ) {
      $descr_situacaoprojeto = 'A ser analisado '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 2 ) {
      $descr_situacaoprojeto = 'Documentos checados e encaminhados para vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 3 ) {
      $descr_situacaoprojeto = 'Insuficiente - Pendência de Documentos ou correção - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 4 ) {
      $descr_situacaoprojeto = 'Reprovado - Processo não permitido ou reprovado na análise/vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 5 ) {
      $descr_situacaoprojeto = 'Processo encaminhado à procuradoria - Dúvida na vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 6 ) {
      $descr_situacaoprojeto = 'Processo encaminhado ao departamento imobiliário - Dúvida na vistoria - '.date('d/m/Y H:i');
    } else if ( $situacaoprojeto == 7 ) {
       $descr_situacaoprojeto = 'Aprovado - Pendente de pagamento de taxa para elaboração do decreto - '.date('d/m/Y H:i');
       $descr_situacaoprojeto2 = 'Entre em contato ou compareça na Secretária de Planejamento!';
    } else if ( $situacaoprojeto == 8 ) {
      $descr_situacaoprojeto = 'Aprovado - Fazer decreto e pegar assinatura - com taxa paga - '.date('d/m/Y H:i');
      $descr_situacaoprojeto2 = 'Entre em contato ou compareça na Secretária de Planejamento!';
    } else if ( $situacaoprojeto == 9 ) {
      $descr_situacaoprojeto = 'Aprovado - Decreto assinado - para entregar - '.date('d/m/Y H:i');
      $descr_situacaoprojeto2 = 'Entre em contato ou compareça na Secretária de Planejamento!';
    } else if ( $situacaoprojeto == 10 ) {
      $descr_situacaoprojeto = 'Aprovado - Processo finalizado e decreto entregue'.date('d/m/Y H:i');
      $descr_situacaoprojeto2 = 'Entre em contato ou compareça na Secretária de Planejamento!';
    } else if ( $situacaoprojeto == 11 ) {
      $descr_situacaoprojeto = 'Aprovado - '.date('d/m/Y H:i');
      $descr_situacaoprojeto2 = 'Entre em contato ou compareça na Secretária de Planejamento!';
    } else if ( $situacaoprojeto == 12 ) {
      $descr_situacaoprojeto = 'Documentos encaminhados para análise - Vistoria dispensada - '.date('d/m/Y H:i');
    }
    
    require 'vendor/autoload.php';

    $resources = new \Mailjet\Resources();

    $api1 = 'f74843b63a568c5d7116cff5d971eb45';
    $api2 = '06fed94a47fa0688ef15fd36453ec365';

    $mj = new \Mailjet\Client($api1, $api2);
    $body = [
        'FromEmail' => "nao-responda@valleteclab.com",
        'FromName' => "Prefeitura de Luís Eduardo Magalhães - XPlanejamento",
        'Subject' => 'Análise de Processo [ '.$descr_tipoprocesso.' ]',
        'Html-part' => '[ E-mail resposta, por favor não responda esse e-mail ] <br />
          Prezado(a) Senhor(a),<br />
          <br /><br />
          Status de processo de <strong>'.$descr_tipoprocesso.'</strong>, Protocolo: '.$id_pro.'<br /> 
          <strong>Autor da análise:</strong> '.$tautor['nome'].'<br />
          <strong>Situação do projeto:</strong><br />
          '.$descr_situacaoprojeto.'
          <br />
          '.$descr_situacaoprojeto2.'
          <br />
          <strong>Observações</strong><br />
          '.$obsgerais.'
          <br />
          <br />
          www.luiseduardomagalhaes.ba.gov.br/planejamento <br />
          <br /><br />
          Agradecemos o contato!',
        'Recipients' => [
            [
                'Email' => "".$tusuario['email'].""
            ]
        ]
    ];
    $response = $mj->post($resources::$Email, ['body' => $body]);
    $response->success() && var_dump($response->getData());

    ?>
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
      $(function () {
        setTimeout(function(){
              alert('Procedimento efetuado com sucesso!');
              window.open('aprovacao.php?id_pro=<?php echo $id_pro; ?>&tipoprocesso=<?php echo $tipoprocesso; ?>','_self');
          }, 1000);
        });
    </script>
    <?php
}

?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia" />
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

</head>

<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">

  <!-- Start: Main -->
  <div id="main">

    <!-- Start: Header -->
    <?php include('includes/header.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="dashboard.html">Alvará de Loteamento</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <!-- Begin: Content Header -->

            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <form method="post" id="admin-form">
                  <input type="hidden" name="formulario" value="ok">
                  <input type="hidden" name="id_ana" value="<?php echo $linha['id_ana']; ?>">

                  <div class="panel-body bg-light">

                    <!-- .section-divider -->
                    <!-- inicio formulário -->
                    <div class="section row">
                      <div class="topbar-left">
                        <ol class="breadcrumb">
                          <li class="crumb-icon">
                            <a href="dashboard.html">
                              <span class="glyphicon-lg"></span>
                            </a>
                          </li>
                          <li class="crumb-active">
                            <a><h2>Análise</h3></a>
                        </ol>
                      </div>
                    </div>

                    <div class="panel-body bg-light">

                    <?php //include_once('includes/info_loteamento.php'); ?>


                      <hr />
                      <div class="section row">
                        <div class="col-md-8">
                          <strong>Consta nos documentos entregues para análise:</strong>
                        </div>
                      </div>

                  <!---->

                  <div class="section row">
                      <i class="fa fa-exclamation-circle text-info fa-lg pr10"></i> Os itens abaixo estão em conformidade com as normas ? 
                   </div>
                  
                      <div class="section row">
                        <div class="col-md-8">
                          <strong>(A) Carimbo (ou quadro) - Espaço reservado no canto inferior direito das pranchas</strong>
                        </div>
                      </div>

                    <div class="section row">
                      <div class="col-md-6"><strong><em>Marcar | Desmarcar todos;</em></strong></div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="selectall" id="selecionarTodosSim" type="radio" value="t">
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="selectall" id="selecionarTodosNao" type="radio" value="f">
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                     <div class="section row">
                      <div class="col-md-6">Identificação da empresa e/ou do profissional responsável pelo projeto;</div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor1" class="yes" type="radio" value="t" <?php if ( $linha['valor1'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor1" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor1'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Identificação da finalidade do projeto (aprovação para construção, reforma com ou sem acréscimo de área, modificação de projeto aprovado, regularização);
                      </div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor2" class="yes" type="radio" value="t" <?php if ( $linha['valor2'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor2" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor2'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Indicação da finalidade da obra (residencial, comercial, MISTO, industrial, etc) e natureza da obra (alvenaria, madeira, etc.);</div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor3" class="yes" type="radio" value="t" <?php if ( $linha['valor3'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor3" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor3'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Nome do Proprietário da Obra;</div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor4" class="yes" type="radio" value="t" <?php if ( $linha['valor4'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor4" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor4'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Endereço da Obra;</div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor5" class="yes" type="radio" value="t" <?php if ( $linha['valor5'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor5" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor5'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Conteúdo gráfico existente em cada prancha;</div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor6" class="yes" type="radio" value="t" <?php if ( $linha['valor6'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor6" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor6'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                    </div>
                    <div class="section row">
                      <div class="col-md-6">Indicação de escala;</div>
                        <div class="col-md-2">                       
                          <label class="option option-primary">
                              <input name="valor7" class="yes" type="radio" value="t" <?php if ( $linha['valor7'] == true ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Sim</label>
                        </div>
                        <div class="col-md-2">
                          <label class="option option-primary">
                              <input name="valor7" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor7'] == false ) { echo "checked='checked'"; } ?>>
                              <span class="radio"></span>Não</label>
                        </div> 
                      </div>

                         <div class="section row">
                          <div class="col-md-6">Indicação sequencial do projeto (número ou letras das pranchas);</div>
                            <div class="col-md-2">                       
                              <label class="option option-primary">
                                  <input name="valor8" class="yes" type="radio" value="t" <?php if ( $linha['valor8'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                  <input name="valor8" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor8'] == false ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Não</label>
                            </div> 
                        </div>
                         <div class="section row">
                          <div class="col-md-6">Data;</div>
                            <div class="col-md-2">                       
                              <label class="option option-primary">
                                  <input name="valor9" class="yes" type="radio" value="t" <?php if ( $linha['valor9'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                  <input name="valor9" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor9'] == false ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Não</label>
                            </div> 
                        </div>
                        <div class="section row">
                          <div class="col-md-6">Quadro de áreas (área do terreno, área a ser construída (separada por pavimento e total), área permeável taxa de ocupação);
                          </div>
                            <div class="col-md-2">                       
                              <label class="option option-primary">
                                  <input name="valor10" class="yes" type="radio" value="t" <?php if ( $linha['valor10'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                  <input name="valor10" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor10'] == false ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Não</label>
                            </div> 
                        </div>
                        <div class="section row">
                          <div class="col-md-6">Quadro de assinaturas (autoria do projeto, responsabilidade técnica, proprietário da obra);</div>
                            <div class="col-md-2">                       
                              <label class="option option-primary">
                                  <input name="valor11" class="yes" type="radio" value="t" <?php if ( $linha['valor11'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                  <input name="valor11" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor11'] == false ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Não</label>
                            </div> 
                        </div>
                        <div class="section row">
                          <div class="col-md-6">Á reservada para aprovação;</div>
                            <div class="col-md-2">                       
                              <label class="option option-primary">
                                  <input name="valor12" class="yes" type="radio" value="t" <?php if ( $linha['valor12'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                  <input name="valor12" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor12'] == false ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Não</label>
                            </div> 
                        </div>
                        <div class="section row">
                          <div class="col-md-6">CPF do Proprietário ou CNPJ;</div>
                            <div class="col-md-2">                       
                              <label class="option option-primary">
                                <input name="valor13" class="yes" type="radio" value="t" <?php if ( $linha['valor13'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor13" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor13'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>
                          <div class="section row">
                            <div class="col-md-6"><strong>(B) Planta de situação - poderá estar no carimbo e s/ escala</strong></div>
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Indicação Norte;  </div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                <input name="valor14" class="yes" type="radio" value="t" <?php if ( $linha['valor14'] == true ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor14" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor14'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Número de Quadra e Lote(s);</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                <input name="valor15" class="yes" type="radio" value="t" <?php if ( $linha['valor15'] == true ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                                <label class="option option-primary">
                                <input name="valor15" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor15'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Dimensões das divisas do lote e distância à esquina mais próxima.;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                <input name="valor16" class="yes" type="radio" value="t" <?php if ( $linha['valor16'] == true ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Sim</label>
                            </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor16" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor16'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Nome das Ruas (da fachada frontal e a perpendicular mais próxima).</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor17" class="yes" type="radio" value="t" <?php if ( $linha['valor17'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor17" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor17'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>
                          <div class="section row">
                            <div class="col-md-6"><strong>(C) Locação- poderá conter a planta de cobertura (com suas respectivas exigências)</strong>
                            </div>
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Projeção da edificação (com medidas do seu perímetro)..</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor18" class="yes" type="radio" value="t" <?php if ( $linha['valor18'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor18" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor18'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Distância da edificação em relação às divisas do terreno e a outras edificações;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor19" class="yes" type="radio" value="t" <?php if ( $linha['valor19'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor19" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor19'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Indicação dos acessos, muros de vedação e portões;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor20" class="yes" type="radio" value="t" <?php if ( $linha['valor20'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor20" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor20'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Indicação da posição da(s) via(s) pública(s) que faz (em) limite com terreno, passeio público.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor21" class="yes" type="radio" value="t" <?php if ( $linha['valor21'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor21" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor21'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Localização da fossa séptica.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor22" class="yes" type="radio" value="t" <?php if ( $linha['valor22'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor22" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor22'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Escala 1/100 ou 1/200.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor23" class="yes" type="radio" value="t" <?php if ( $linha['valor23'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor23" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor23'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                             <div class="section row">
                            <div class="col-md-6">Garagem / Estacionamento (Dimensões mínimas das vagas, número mínimo de vagas);</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor24" class="yes" type="radio" value="t" <?php if ( $linha['valor24'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor24" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor24'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                           <div class="section row">
                            <div class="col-md-6">Calçada nos padrões definidos pela prefeitura;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor25" class="yes" type="radio" value="t" <?php if ( $linha['valor25'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor25" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor25'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6"><strong>(D) Planta Baixa</strong>
                            </div>
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Planta(s) da edificação (de cada pavimento não repetido;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor26" class="yes" type="radio" value="t" <?php if ( $linha['valor26'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor26" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor26'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Caracterização dos elementos do projeto: fechamento externos e internos, acessos, circulações verticais e horizontais,áreas de serviço e demais elementos significativos;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor27" class="yes" type="radio" value="t" <?php if ( $linha['valor27'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor27" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor27'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Indicação dos nomes dos compartimentos e suas respectivas áreas;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor28" class="yes" type="radio" value="t" <?php if ( $linha['valor28'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor28" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor28'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Cotas de níveis principais;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor29" class="yes" type="radio" value="t" <?php if ( $linha['valor29'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor29" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor29'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Cotas gerais;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor30" class="yes" type="radio" value="t" <?php if ( $linha['valor30'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor30" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor30'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Notas gerais;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor31" class="yes" type="radio" value="t" <?php if ( $linha['valor31'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor31" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor31'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Indicação das aberturas com larguras, altura e peitoril (estas informações poderão estar num quadro de esquadrias, com seus respectivos códigos indicados nas plantas);</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor32" class="yes" type="radio" value="t" <?php if ( $linha['valor32'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor32" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor32'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Quando existir escada, indicar números de degraus e sentido da mesma (sobe / desce);</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor33" class="yes" type="radio" value="t" <?php if ( $linha['valor33'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor33" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor33'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Escala 1/50, 1/75 ou 1/100.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor34" class="yes" type="radio" value="t" <?php if ( $linha['valor34'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor34" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor34'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6"><strong>(D) Planta de Cobertura</strong>
                            </div>
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Cotas Gerais;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor35" class="yes" type="radio" value="t" <?php if ( $linha['valor35'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor35" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor35'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Caracterização dos elementos da cobertura: indição do tipo, inclinação e sentido do telhado, calhas, laje impermeabilizada, poços de luz, dutos de ventilação, chaminés, etc.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor36" class="yes" type="radio" value="t" <?php if ( $linha['valor36'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor36" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor36'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Projeção das paredes que compõem o perímetro da edificação;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor37" class="yes" type="radio" value="t" <?php if ( $linha['valor37'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor37" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor37'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Escala 1/100 ou 1/200</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor38" class="yes" type="radio" value="t" <?php if ( $linha['valor38'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor38" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor38'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6"><strong>(F) Cortes</strong>
                            </div>
                          </div>

                          <div class="section row">
                            <div class="col-md-6">No mínimo um longitudinal em um transversal.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor39" class="yes" type="radio" value="t" <?php if ( $linha['valor39'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor39" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor39'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Um dos cortes deverá passar longitudinalmente pela escada (quando existir);</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor40" class="yes" type="radio" value="t" <?php if ( $linha['valor40'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor40" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor40'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Caracterização dos elementos do projeto: fechamentos e externos e internos, acessos, circulações verticais e demais elementos significativos;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor41" class="yes" type="radio" value="t" <?php if ( $linha['valor41'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor41" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor41'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Indicação dos nomes dos compartimentos;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor42" class="yes" type="radio" value="t" <?php if ( $linha['valor42'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor42" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor42'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Cotas de níveis principais;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor43" class="yes" type="radio" value="t" <?php if ( $linha['valor43'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor43" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor43'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Cotas gerais (inclusive pé-direito);</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor44" class="yes" type="radio" value="t" <?php if ( $linha['valor44'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor44" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor44'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Notas Gerais;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor45" class="yes" type="radio" value="t" <?php if ( $linha['valor45'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor45" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor45'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Escala 1/50, 1/75 ou 1/100.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor46" class="yes" type="radio" value="t" <?php if ( $linha['valor46'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor46" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor46'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6"><strong>(F) Fachada</strong>
                            </div>
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Todas as que estiverem voltadas para as vias públicas;</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor47" class="yes" type="radio" value="t" <?php if ( $linha['valor47'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor47" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor47'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Escala 1/50, 1/75 ou 1/100.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor48" class="yes" type="radio" value="t" <?php if ( $linha['valor48'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor48" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor48'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6"><strong>(G) Legislação Municipal</strong>
                            </div>
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Projeto em conformidade com as exigências contidas na legislação municipal.</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor49" class="yes" type="radio" value="t" <?php if ( $linha['valor49'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor49" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor49'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>

                          <div class="section row">
                            <div class="col-md-6">Segue abaixo relação dos itens a serem adequados quanto à legislação municipal vigente:</div>
                              <div class="col-md-2">                       
                                <label class="option option-primary">
                                  <input name="valor50" class="yes" type="radio" value="t" <?php if ( $linha['valor50'] == true ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Sim</label>
                              </div>
                            <div class="col-md-2">
                              <label class="option option-primary">
                                <input name="valor50" class="no" type="radio" value="f" <?php if ( $linha['id_ana'] > 0 and $consultaAnalista->rowCount() == 0 and $linha['valor50'] == false ) { echo "checked='checked'"; } ?>>
                                <span class="radio"></span>Não</label>
                            </div> 
                          </div>


                      <!--Status do processo -->
                        <div class="section row">
                          <div class="col-md-4">
                            <strong>Situação do projeto:</strong>
                          </div>
                        </div>
                        <div class="section row">
                          <div class=" pull-left leg-sitdr-5"></div>
                            <div class="col-md-3">                       
                              <label class="option option-primary">
                                  <input name="situacaoprojeto" type="radio" value="2" <?php if ( $linha['situacaoprojeto'] == 2 ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span>Em estado de análise:</label>
                            </div>
                        </div>
                        <div class="section row">
                          <div class=" pull-left leg-sitdr-8"></div>
                            <div class="col-md-6">                       
                              <label class="option option-primary">
                                  <input name="situacaoprojeto" type="radio" value="3" <?php if ( $linha['situacaoprojeto'] == 3 ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span><strong>Insuficiente</strong> - Pendência de documentos ou correção;</label>
                            </div>
                        </div>
                        <div class="section row">
                          <div class=" pull-left leg-sitdr-2"></div>
                            <div class="col-md-6">                       
                              <label class="option option-primary">
                                  <input name="situacaoprojeto" type="radio" value="4" <?php if ( $linha['situacaoprojeto'] == 4 ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span><strong>Reprovado</strong> - Processo não permitido ou reprovado na análise/vistoria</label>
                            </div>
                        </div>
                        <div class="section row">
                          <div class=" pull-left leg-sitdr-2"></div>
                            <div class="col-md-6">                       
                              <label class="option option-primary">
                                  <input name="situacaoprojeto" type="radio" value="4" <?php if ( $linha['situacaoprojeto'] == 4 ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span><strong>Concedida admissibilidade técnica</strong> - Processo encaminhado à Procuradoria</label>
                            </div>
                        </div>
                        <div class="section row">
                          <div class=" pull-left leg-sitdr-11"></div>
                            <div class="col-md-6">                       
                              <label class="option option-primary">
                                  <input name="situacaoprojeto" type="radio" value="11" <?php if ( $linha['situacaoprojeto'] == 11 ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span><strong>Aprovado</strong></label>
                            </div>
                        </div>
                        <div class="section row">
                          <div class=" pull-left leg-sitdr-2"></div>
                            <div class="col-md-6">                       
                              <label class="option option-primary">
                                  <input name="situacaoprojeto" type="radio" value="4" <?php if ( $linha['situacaoprojeto'] == 4 ) { echo "checked='checked'"; } ?>>
                                  <span class="radio"></span><strong>Processo finalizado e entregue</strong>
                              </label>
                            </div>
                        </div>


                        <div class="section" id="spy3">
                        <label for="comment" class="field prepend-icon">
                          <textarea class="gui-textarea" id="obsgerais" name="obsgerais"><?php echo $linha['obsgerais']; ?></textarea>
                          <label for="comment" class="field-icon">
                            <i class="fa fa-comments"></i>
                          </label>
                        </label>
                      </div>
                </div>
                       <div class="panel-footer text-left">
                          <button type="submit" class="button btn-success">Atualizar</button>
                        </div>
            </div>
            <div id="sidebar-right-tab2" class="tab-pane"></div>
            <div id="sidebar-right-tab3" class="tab-pane"></div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style type="text/css">
 /*LEGENDAS  
Legendas Listar processos -> Desmenbramento, Remembramento */
.leg-sitdr-1, .leg-sitdr-2, .leg-sitdr-3, .leg-sitdr-4, .leg-sitdr-5, .leg-sitdr-6, .leg-sitdr-7, .leg-sitdr-8, .leg-sitdr-9,  
.leg-sitdr-10, .leg-sitdr-11  {
  width: 40px;
  height: 23px;

}
  .leg-sitdr-1 {
  background-color: white; 

}
.leg-sitdr-2 {
  background-color: #c00000; 

}
.leg-sitdr-3 {
  background-color: #ffff00; 

}
.leg-sitdr-4 {
  background-color: #da9694; 

}
.leg-sitdr-5 {
  background-color: #ddd; 

}
.leg-sitdr-6 {
  background-color: #00b050; 

}
.leg-sitdr-7 {
  background-color: #7030a0; 

}
.leg-sitdr-8 {
  background-color: #c4d79b; 

}
.leg-sitdr-9 {
  background-color: #0070c0; 

} 

.leg-sitdr-10 {
  background-color: #fabf8f; 


}
.leg-sitdr-11 {
  background-color: #eada6d;
}
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/funcao_checkall.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
    function ap30(){ 
      if ( $('input[name=checkocultdetalhes]').is(':checked')) {
        $("#camposocultosmaisdetana").fadeIn(500);
      }else{
        $("#camposocultosmaisdetana").css("display","none");
      } 
    }
    function aplogs($acionador,$objeto){ 
      if ( $('input[name='+$acionador+']').is(':checked')) {
        $("#"+$objeto).fadeIn(500);
      }else{
        $("#"+$objeto).css("display","none");
      } 
    }
    function apfotos(){ 
      if ( $('input[name=checkocultofotos]').is(':checked')) {
        $("#boxfotos").fadeIn(500);
      }else{
        $("#boxfotos").css("display","none");
      } 
    }
  </script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        valor1: {
          required: true
        },
        valor2: {
          required: true
        },
        valor3: {
          required: true
        },
        situacaoprojeto: {
          required: true
        }

      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        valor1: {
          required: 'Marque uma opção'
        },
        valor2: {
          required: 'Marque uma opção'
        },
        valor3: {
          required: 'Marque uma opção'
        },
        situacaoprojeto: {
          required: 'Escolha a situação'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>