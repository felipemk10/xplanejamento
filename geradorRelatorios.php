<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
//$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
//$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

if ( $_GET['relatorio'] == 'alvaras' ) { 


?>
<table border="0" align="center" width="100%">
  <tr>
    <td align="center"><h1>RELATÓRIO GERAL DE ALVARÁ/HABITE-SE - 2018</h1> </td>
  </tr>
  <tr>
    <td align="center"><span style="font-size:9px;">XPlanejamento.valleteclab.com</span> <hr /></td>
  </tr>
</table>
<?php

$alvaras = $configuracoes->consulta("SELECT 
  processos_alvarahabitese.id_cg,
  processos_alvarahabitese.id_pro,
  processos_alvarahabitese.datahora,   
  tipo,   
  processos_alvarahabitese.numero,
  ano,
  processos.tipoprocesso

  FROM 
  processos.processos_alvarahabitese

  INNER JOIN processos.processos ON processos.id_pro = processos_alvarahabitese.id_pro

  WHERE processos_alvarahabitese.id_pro > 0 and date_part('year', processos_alvarahabitese.datahora) = date_part('year', CURRENT_DATE) ORDER BY processos_alvarahabitese.numero ASC");
  
  foreach ( $alvaras as $listagem_alvarahabite ) { 
    $contagemTipoDoc[$listagem_alvarahabite['tipo']]++;
    if ( $listagem_alvarahabite['tipo'] != 2 ) {
      $contagemTipoAlvaras[$listagem_alvarahabite['tipoprocesso']]++;
    }
  }
  ?>

  <table border="1" align="center">
    <tr>
      <td>
        <h4>HABITE-SE EMITIDOS DURANTE O ANO (2018): <?php echo (int)$contagemTipoDoc[2]; ?></h4>
      </td>
    </tr>
  </table>
   <table border="1" align="center"> 
    <tr>
      <td>
        <h4>ALVARÁS EMITIDOS DURANTE O ANO (2018): <?php echo (int)$contagemTipoDoc[1]+$contagemTipoDoc[3]; ?></h4>
      </td>
    </tr>
    <tr>
      <td>
        <h4>Alvará de Construção: <?php echo (int)$contagemTipoAlvaras[1]; ?></h4>
      </td>
    </tr>
    <tr>
      <td>
        <h4>Alvará de Regularização de Obras: <?php echo (int)$contagemTipoAlvaras[2]; ?></h4>
      </td>
    </tr>
    <tr>
      <td>
        <h4>Alvará de Acréscimo de Área: <?php echo (int)$contagemTipoAlvaras[3]; ?></h4>
      </td>
    </tr>
    <tr>
      <td>
        <h4>Condomínio Edilício: <?php echo (int)$contagemTipoAlvaras[4]; ?></h4>
      </td>
    </tr>

  </table>
  <table border="0" align="center" width="100%">
    <tr>
      <td>Emitido: <strong><?php echo date('d/m/Y'); ?><strong></td>
    </tr>
    <tr>
      <td align="center"><span style="font-size:9px;">XPlanejamento.valleteclab.com</span></td>
    </tr>
  </table>
  <?php
}

//  PAMELA FABIULA DOS REIS LOPES 
//$analista = 144758;
//  EDUARDA JUNQUEIRA
//$analista = 144554;
//  ANA CAROLINA PASSOS MACIEL
//$analista = 144649;
//  RAFAELA ZILIO FAEDO
//$analista = 144773;
//  DIEGO DIAS
//$analista = 144815;
//  TAIANE TIZIA ZABEL DE SOUZA
//$analista = 144853;
//  EVERTON TELES DA SILVA
//$analista = 144753;
//  CARLOS OTONI
//$analista = 144564;
//  JOÃO FRARE
//$analista = 147962;

if ( $_GET['relatorio'] == 'analistas' ) { 

?>

<table border="0" align="center" width="100%">
  <tr>
    <td align="center"><h1>RELATÓRIO GERAL DE ANÁLISE - 2018</h1> </td>
  </tr>
  <tr>
    <td align="center"><span style="font-size:9px;">XPlanejamento.valleteclab.com</span> <hr /></td>
  </tr>
</table>
<div id="boxlogsAA">
  <table class="table table-hover" style="font-size: 12px;">
    <thead>
    <tr>
      <th></th>
      <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    //  Listando informações sobre as unidades.
    $con_listagem_log = $configuracoes->consulta("SELECT 
      log_analise.id_cg,
      log_analise.id_pro,
      log_analise.datahora,   
      log_analise.situacaoprojeto,   
      log_analise.obsgerais,
      log_analise.tipo,
      cg.nome

      FROM 
      logs.log_analise

      LEFT JOIN geral.cg ON cg.id_cg = log_analise.id_cg

      WHERE logs.log_analise.id_cg IN (144758,144554,144649,144773,144815,144853,144753,144564,147962) and log_analise.tipo = 'aa' and date_part('year', log_analise.datahora) = date_part('year', CURRENT_DATE) ORDER BY logs.log_analise.datahora ASC");
      $log_id = 0;

      
      foreach ( $con_listagem_log as $listagem_log ) { 
        $log_id++;

        /*
          1 - A ser analisado = white, 
          2 - Em análise = #ddd, 
          3 - Pendência de documentos ou correção = #c4d79b, 
          4 - Processo não permitido ou reprovado na análise/vistoria = #c00000, 
          5 - Processo encaminhado à procuradoria - Dúvida na vistoria = #00b050, 
          6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria = #0070c0, 
          7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto = #ffff00, 
          8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga = #7030a0, 
          9 - Aprovado - Decreto assinado - para entregar = #fabf8f, 
          10 - Aprovado - Processo finalizado e decreto entregue = #da9694, 
          11 - Aprovado = #eada6d, 
          12 - Dispensar Vistoria
        */
          $contador[0]++;
          $contador[$listagem_log['situacaoprojeto']]++;
          $analista[$listagem_log['id_cg']][$listagem_log['situacaoprojeto']]++;

          if ( $listagem_log['situacaoprojeto'] == 1 ) {
            $log_situacao = "A ser analisado";
          } else if ( $listagem_log['situacaoprojeto'] == 2 ) {
            $log_situacao = "Em análise";
          } else if ( $listagem_log['situacaoprojeto'] == 3 ) {
            $log_situacao = "Pendência de documentos ou correção";
          } else if ( $listagem_log['situacaoprojeto'] == 4 ) {
            $log_situacao = "Processo não permitido ou reprovado na análise/vistoria";
          } else if ( $listagem_log['situacaoprojeto'] == 5 ) {
            $log_situacao = "Processo encaminhado à procuradoria - Dúvida na vistoria";
          } else if ( $listagem_log['situacaoprojeto'] == 6 ) {
            $log_situacao = "Processo encaminhado ao departamento imobiliário - Dúvida na vistoria";
          } else if ( $listagem_log['situacaoprojeto'] == 7 ) {
            $log_situacao = "Aprovado - Pendente de pagamento de taxa para elaboração do decreto";
          } else if ( $listagem_log['situacaoprojeto'] == 8 ) {
            $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
          } else if ( $listagem_log['situacaoprojeto'] == 9 ) {
            $log_situacao = "Aprovado - Fazer decreto e pegar assinatura - com taxa paga";
          } else if ( $listagem_log['situacaoprojeto'] == 10 ) {
            $log_situacao = "Aprovado - Processo finalizado e decreto entregue";
          } else if ( $listagem_log['situacaoprojeto'] == 11 ) {
            $log_situacao = "Aprovado";
          } else if ( $listagem_log['situacaoprojeto'] == 12 ) {
            $log_situacao = "Vistoria dispensada - Encaminhada para análise";
          } else if ( $listagem_log['situacaoprojeto'] == 13 ) {
            $log_situacao = 'Documentos retirados';
          } else if ( $listagem_log['situacaoprojeto'] == 14 ) {
            $log_situacao = 'Documentos entregues';
          } else if ( $listagem_log['situacaoprojeto'] == 15 ) {
            $log_situacao = 'Processo não encaminhado para análise: Inconsistência na vistoria';
          }
        ?>
          <tr>
            <td align="left" colspan="2">Protocolo: <strong><?php echo $listagem_log['id_pro']; ?></strong></td>
          </tr>
          <tr style="border-top:1px solid green; ">
            <td align="left">Tipo:</td>
            <td align="left"><?php 
              if ( $listagem_log['tipo'] == 'ch' ) {
                echo 'Checagem';
              } else if ( $listagem_log['tipo'] == 'po' ) {
                echo 'Protocolo';
              } else if ( $listagem_log['tipo'] == 'fi' ) {
                echo 'Fiscalização';
              } else if ( $listagem_log['tipo'] == 'aa' ) {
                echo 'Análise';
              }

             ?></td>
          </tr>
          <tr>
            <td align="left"><?php echo $log_id; ?><sup>a</sup> Etapa:</td>
            <td align="left"><?php echo $formatacoes->formatar_datahora('/',$listagem_log['datahora']); ?></td>
          </tr>
          <tr>
            <td align="left">Situação:</td>
            <td align="left"><?php echo $log_situacao; ?></td>
          </tr>
          <tr>
            <td align="left">Autor:</td>
            <td align="left"><?php echo $listagem_log['nome']; ?></td>
          </tr>
          <tr>
            <td align="left">Observações:</td>
            <td align="left"><?php echo $listagem_log['obsgerais']; ?></td>
          </tr>
          <tr>
            <td colspan="2"><hr /></td>
          </tr>
          
      <?php } ?>
      </tbody>

      <footer>
        <tr>
          <th></th>
          <th></th>
        </tr>
      </footer>
    </table>
</div>

<table border="1" align="center">
  <tr>
    <td>
      <h4>ANÁLISES DURANTE O ANO (2018): <?php echo (int)$contador[0]; ?></h4>
    </td>
  </tr>
  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$contador[1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$contador[2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$contador[3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$contador[4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$contador[5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$contador[6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$contador[7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$contador[8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$contador[9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$contador[10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$contador[11]; ?></strong>
    </td>
  </tr>
</table>



<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - PAMELA FABIULA DOS REIS LOPES<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144758][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144758][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144758][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144758][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144758][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144758][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144758][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144758][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144758][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144758][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144758][11]; ?></strong>
    </td>
  </tr>
</table>


<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - EDUARDA JUNQUEIRA<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144554][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144554][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144554][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144554][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144554][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144554][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144554][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144554][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144554][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144554][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144554][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - ANA CAROLINA PASSOS MACIEL<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144649][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144649][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144649][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144649][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144649][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144649][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144649][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144649][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144649][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144649][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144649][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - RAFAELA ZILIO FAEDO<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144773][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144773][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144773][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144773][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144773][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144773][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144773][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144773][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144773][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144773][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144773][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - DIEGO DIAS<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144815][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144815][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144815][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144815][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144815][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144815][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144815][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144815][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144815][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144815][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144815][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - TAIANE TIZIA ZABEL DE SOUZA<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144853][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144853][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144853][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144853][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144853][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144853][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144853][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144853][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144853][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144853][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144853][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - EVERTON TELES DA SILVA<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144753][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144753][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144753][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144753][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144753][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144753][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144753][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144753][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144753][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144753][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144753][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - CARLOS OTONI<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144564][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144564][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144564][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144564][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144564][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144564][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144564][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144564][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144564][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144564][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144564][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - CARLOS OTONI<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[144564][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[144564][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[144564][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[144564][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144564][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[144564][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[144564][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[144564][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[144564][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[144564][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[144564][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="1" width="100%">
  <tr>
    <td>
      <h4>ANALISTA - JOÃO FRARE<h4/>
    </td>
  </tr>

  <tr>
    <td>
      A ser analisado: 
      <strong><?php echo (int)$analista[147962][1]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Em análise: 
      <strong><?php echo (int)$analista[147962][2]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Pendência de documentos ou correção: 
      <strong><?php echo (int)$analista[147962][3]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo não permitido ou reprovado na análise/vistoria: 
      <strong><?php echo (int)$analista[147962][4]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado à procuradoria - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[147962][5]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Processo encaminhado ao departamento imobiliário - Dúvida na vistoria: 
      <strong><?php echo (int)$analista[147962][6]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Pendente de pagamento de taxa para elaboração do decreto: 
      <strong><?php echo (int)$analista[147962][7]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Fazer decreto e pegar assinatura - com taxa paga: 
      <strong><?php echo (int)$analista[147962][8]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Decreto assinado - para entregar: 
      <strong><?php echo (int)$analista[147962][9]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado - Processo finalizado e decreto entregue: 
      <strong><?php echo (int)$analista[147962][10]; ?></strong>
    </td>
  </tr>
  <tr>
    <td>
      Aprovado: 
      <strong><?php echo (int)$analista[147962][11]; ?></strong>
    </td>
  </tr>
</table>

<table border="0" align="center" width="100%">
  <tr>
    <td>Emitido: <strong><?php echo date('d/m/Y'); ?><strong></td>
  </tr>
  <tr>
    <td align="center"><span style="font-size:9px;">XPlanejamento.valleteclab.com</span></td>
  </tr>
</table>
<?php } ?>