<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

$id_pro = (int)$_GET['id_pro'];
$id_ah = (int)$_GET['id_ah'];

// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

if ( $id_pro > 0 ) {
    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();

      $consulta_alvarahabitese = $configuracoes->consulta("SELECT 
        id_pro,
        --  1 = Alvará, 2 = Habite-se, 3 Alvará Regularização
        tipo,
        numero,   
        id_cg,
        id_que,
        ano,
        substituir,
        datahora

        FROM 

        processos.processos_alvarahabitese 

        WHERE  processos_alvarahabitese.id_pro = $id_pro and processos_alvarahabitese.id_ah = $id_ah"); //Irving: incluido id_pro = $id_pro igual no habitese_impressao.php
      
      
      $linha_alvarahabitese = $consulta_alvarahabitese->fetch();
}

include("biblioteca/mpdf60/mpdf.php");

$html = "
<body style='text-align:center;font-family:Helvetica;color:#393939;'>
	<table width='800' align='center' border='0' style='font-size:15px;border-style:dashed;'>
		<tr>
			<td colspan='4' align='center'>
				<h2 align='center' style='padding-top:20px;padding-bottom:20px;font-size:30px;'>ALVARÁ DE LICENÇA PARA&nbsp;";

					if ( $linha2['tipoprocesso'] == 1 ) {
					$html .= 'CONSTRUÇÃO';
					} else if ( $linha2['tipoprocesso'] == 2 ) {
					$html .= 'REGULARIZAÇÃO DE OBRAS';
					} else if ( $linha2['tipoprocesso'] == 3 ) {
					$html .= 'ACRÉSCIMO DE ÁREAS';
					} else if ( $linha2['tipoprocesso'] == 4 ) {
					$con_que = $configuracoes->consulta("SELECT 
					      id_que,   
					      areacontruir,   
					      areaexistente,  
					      areausoexclusivo,   
					      areacomumproporcional,  
					      tipoprocesso

					      FROM 
					      processos.processos_que

					      WHERE processos_que.id_que = ".$linha_alvarahabitese['id_que']."");
					$linha_que = $con_que->fetch();

					if ( $linha_que['tipoprocesso'] == 'c' ) {
					  $html .= 'CONSTRUÇÃO';
					} else if ( $linha_que['tipoprocesso'] == 'r' ) {
					  $html .= 'REGULARIZAÇÃO';
					} else if ( $linha_que['tipoprocesso'] == 'a' ) {
					  $html .= 'ACRÉSCIMO DE ÁREAS';
					}
					} else if ( $linha2['tipoprocesso'] == 5 ) {
						$html .= 'REDIMENSIONAMENTO';
					} else if ( $linha2['tipoprocesso'] == 6 ) {
						$html .= 'PROJETO';
					}

$html .= "</h2> <br /><br />
			</td>
		</tr>
			<tr style='font-size:18px;'>
				<td align='left' width='50' style='padding-top:5px;padding-bottom:5px;font-size:12px;'>EMISSÃO:</td>
				<td align='left' style='padding-top:5px;padding-bottom:5px;'><strong>";


				$html .= $formatacoes->formatar_data('/',$linha_alvarahabitese['datahora']);

$html .=		"</strong></td>
				<td align='right' style='padding-top:5px;padding-bottom:5px;font-size:12px;'>NÚMERO:</td>
				<td width='170' align='right' style='font-size:20px;font-style:italic;'><strong>";

		if ( $linha2['tipoprocesso'] == 2 or $linha_que['tipoprocesso'] == 'r' ) { 
			$html .= 'R.'; 
		} 
		$html .= $linha_alvarahabitese['numero'].' / '.$linha_alvarahabitese['ano'];


$html .= "			</strong></td>
			</tr>";

	if ( (int)$linha_alvarahabitese['substituir'] > 0 ) {

        $substituto = $configuracoes->consulta("SELECT ano FROM processos.processos_alvarahabitese WHERE processos_alvarahabitese.numero = ".$linha_alvarahabitese['substituir']." and tipo = ".$linha_alvarahabitese['tipo']." and id_pro != 0 ORDER BY id_ah DESC")->fetch();//Irving modif.18/09/2020
$html .= "<tr>
			<td colspan='4' align='center' style='background:#EEEED1;'><strong>Este documento substitui o alvará existente de número:</strong></td>
		 </tr>
		 <tr>
			<td align='center' colspan='4' style='background:#EEEED1;'><strong>";

			if ( $linha2['tipoprocesso'] == 2 ) { 
				$html .= 'R.'; 
			} 
			$html .= $linha_alvarahabitese['substituir'].' / '.$substituto['ano'];


$html .= "			</strong></td>
		</tr>";
	}		
$html .= "		
		<tr align='center'>
			<td width='150' colspan='3' style='padding-top:5px;padding-bottom:5px;border-style:dashed;border-color:#EEEED1;'><strong>DADOS DO PROPRIETÁRIO</strong></td>
		</tr>";

//  Listando informações sobre os proprietários.
$con_listagem_proprietario = $configuracoes->consulta("SELECT 
cg.nome,
cg.cpfcnpj

FROM 
processos.processos_proprietario

INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

foreach ( $con_listagem_proprietario as $listagem_proprietario ) {

$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td align='right' width='20'><b>NOME:</b></td>
						<td width='4'></td>
						<td align='left' width='400'>";
						$html .= strtoupper($listagem_proprietario['nome']);
						$html .= "</td>
						<td align='4' width='20'><b>";
						if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
				        	$html .= 'CPF';
				        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
				          	$html .= 'CNPJ';
				        }

						$html .= "</b></td>
						<td width='4'></td>
						<td align='left'>"; 
						if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 11 ) {
				          $html .= $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'###.###.###-##');
				        } else if ( strlen($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj'])) == 14 ) {
				          $html .= $formatacoes->mask($formatacoes->retira_simbolos($listagem_proprietario['cpfcnpj']),'##.###.###/####-##');
				        }
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$html .= "		
		<tr align='center'>
			<td width='150' colspan='3' style='padding-top:5px;padding-bottom:5px;border-style:dashed;border-color:#EEEED1;'><strong>DADOS DO AUTOR/RESP. TÉCNICO</strong></td>
		</tr>";

$con_listagem_autor = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");
foreach ( $con_listagem_autor as $listagem_autor ) { 
	$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td align='right' width='200'><b>AUTOR:</b></td>
						<td width='4'></td>
						<td align='left' width='300'>";
						$html .= strtoupper($listagem_autor['nome']);
						$html .= "</td>
						<td align='4' width='20'><b>";
						if ( $listagem_autor['tipoprofissional'] == 'e' ) { 
							$html .= 'CREA'; 
						} else if ( $listagem_autor['tipoprofissional'] == 'a' ) { 
							$html .= 'CAU'; 
						}

						$html .= "</b></td>
						<td width='4'></td>
						<td align='left'>"; 
						$html .= $listagem_autor['creacau'];
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$con_listagem_resptecnico = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");
foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) {
	$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td align='right' width='200'><b>RESP. TÉCNICO:</b></td>
						<td width='4'></td>
						<td align='left' width='300'>";
						$html .= strtoupper($listagem_resptecnico['nome']);
						$html .= "</td>
						<td align='right' width='20'><b>";
						if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { 
							$html .= 'CREA'; 
						} else if ( $listagem_resptecnico['tipoprofissional'] == 'a' ) { 
							$html .= 'CAU'; 
						}

						$html .= "</b></td>
						<td width='4'></td>
						<td align='left'>"; 
						$html .= $listagem_resptecnico['creacau'];
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$html .= "<tr align='center'>
			<td width='150' colspan='3' style='padding-top:5px;padding-bottom:5px;border-style:dashed;border-color:#EEEED1;'><strong>DADOS DO PROJETO</strong></td>
		</tr>";

//  Listando informações sobre autor.
$con_listagem_autor = $configuracoes->consulta("SELECT 
  cg.nome,
  cg.creacau,
  -- e = engenheiro, a = arquiteto
  processos_profissional.tipoprofissional

  FROM 
  processos.processos_profissional

  INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

  WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

foreach ( $con_listagem_autor as $listagem_autor ) {

$html .= "<tr>
			<td colspan='4'>
				<table style='padding-top:8px;padding-bottom:8px;border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td width='20'><b>AUTORIA:</b></td>
						<td width='2'></td>
						<td>";
						$html .= strtoupper($listagem_autor['nome']);
						$html .= "</td>
						<td align='right' width='50'><b>";
						if ( $listagem_autor['tipoprofissional'] == 'e' ) { 
							$html .= 'CREA'; 
						} else if ( $listagem_autor['tipoprofissional'] == 'a' ) { 
							$html .= 'CAU'; 
						}
						$html .= "</b></td>
						<td width='150'>";
						$html .= $listagem_autor['creacau'];
						$html .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
}

$html .= "<tr>
			<td colspan='4'>
				<table style='border-style:dashed;border-color:#EEEED1;width:100%;font-size:13px;'>
					<tr>
						<td width='20'><b>ENDEREÇO:</b></td>
						<td width='2'></td>
						<td align='left'>";
						$html .= $linha2['endereco'];
						$html .= "</td>
						<td align='right' width='50' colspan='2'><b>NÚMERO:</b></td>
						<td width='150'>";
						$html .= $linha2['numero'];
						$html .= "</td>
					</tr>
					<tr>
						<td width='20' align='right'><b>QUADRA:</b></td>
						<td></td>
						<td>";
						$html .= $linha2['quadra'];
						$html .= "</td>
						<td align='right' width='50' colspan='2'><b>LOTE:</b></td>
						<td width='150'>";
						$html .= $linha2['lote'];
						$html .= "</td>
					</tr>
					<tr>
						<td width='20' align='right'><b>BAIRRO:</b></td>
						<td></td>
						<td>"; 
						$html .= $linha2['bairro'];
						$html .= "</td>
						<td align='right' width='50'><b></b></td>
						<td align='right' width='50'><b>CIDADE:</b></td>
						<td width='300'>LUÍS EDUARDO MAGALHÃES - BA</td>
					</tr>";

					if ( $tipoprocesso >= 1 and $tipoprocesso <= 3 or $tipoprocesso == 6 ) { 
			        	include_once('includes/impressao/info_alvara_r.php');
			        } else if ( $tipoprocesso == 4 and $_GET['condominio_pavimentos'] != 't' ) { 
			        	include_once('includes/impressao/info_condominio_r.php');
			        } else if ( $tipoprocesso == 4 and $_GET['condominio_pavimentos'] == 't' ) { 
			        	include_once('includes/impressao/info_condominio_pavimentos.php');
			        }					

				$html .= "</table>
	 			</td>
	 		</tr>
		</table>
	</body>";


$mpdf = new mPDF(); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->setHTMLFooter($rodape);

$mpdf->AddPage('', // L - landscape, P - portrait 
	'', '', '', '',
	15, // margin_left
	15, // margin right
	55, // margin top
	0, // margin bottom
	10, // margin header
	25  // margin footer
); 
//$css = file_get_contents("css/estilo.css");
//$mpdf->WriteHTML($css,1);
//$mpdf->SetHTMLHeader($header);
$mpdf->WriteHTML($html);
$mpdf->Output();

 //exit;
