<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require 'vendor/autoload.php';
session_start();

$autenticacao   = new \Biblioteca\autenticacao;
$autenticacao->v1($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));
$usuarios       = new \Biblioteca\usuarios;

$manipuladores  = new Biblioteca\manipuladores;

$processos      = new \Biblioteca\processos;

//  Listando informações sobre o processo.
$consulta = $processos->consulta("SELECT 
  cg.id_cg,
  cg.nome,
  cg.email,
  cg.endereco,
  cg.bairro,
  cg.telefone,
  cg.cpfcnpj,
  usuarios.tipousuario,
  usuarios.departamento,
  usuarios.cargofuncao

  FROM 

  geral.cg

  INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg

  WHERE usuarios.id_cg = ".$manipuladores->criptografia($_SESSION['id_usuario'],'base64','decode')."");
  $linha = $consulta->fetch();
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia" />
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="TemplateMonster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>
<body class="admin-validation-page" data-spy="scroll" data-target="#nav-spy" data-offset="200">
  <!-- Start: Main -->
  <div id="main">
    <!-- Start: Header -->
    <?php include('includes/header2.php'); ?>
    <!-- End: Header -->
    <!-- Start: Sidebar -->
    <?php include('includes/sidebar.php'); ?>
    <!-- End: Sidebar -->

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="">Configurações</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border">

                <form method="post" id="admin-form" action="javascript: formularios_dinamicos('#admin-form', 'backend/configuracao.php', '#carregando', '#formResult', 's', '');" method="post">
                <input type="hidden" name="formulario" value="ok">
                <div class="panel-body bg-light">

                    <div class="section-divider mt20 mb40">
                      <span> Dados pessoais </span>
                    </div>
                    <!-- .section-divider -->
                    <!-- .Início do formulário -->  
                   
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="nome" class="field prepend-icon">
                          <input type="text" name="nome" id="nome" class="gui-input" placeholder="Nome..." value="<?php echo $linha->nome; ?>">
                          <label for="nome" class="field-icon">
                            <i class="fa fa-user"></i>
                          </label>
                        </label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-6">
                        <label for="email" class="field prepend-icon">
    	                    <input type="email" name="email" id="email" class="gui-input" placeholder="E-mail" value="<?php echo $linha->email; ?>">
    	                    <label for="email" class="field-icon">
    	                      <i class="fa fa-envelope"></i>
    	                    </label>
    	                  </label>
                      </div>
                    </div>

                   	<div class="section row">
	                   	<div class="col-md-3">
	                      <label for="cpf" class="field">CPF:
	                      <input name="cpfcnpj" id="cpfcnpj" class="gui-input" type="text" value="<?php echo $linha->cpfcnpj; ?>"></label>
            		  	</div>
            		</div>  	
                  <div class="section-divider col-md-12"><span> Dados de Endereço/Contato</span></div>
	                <div class="section row">    
	                    <div class="col-md-3">
                        <label ">Estado atual:</label>
                        <label for="estado" class="field select">
                          <select id="estado" name="estado">
                            <option value="ba">Bahia</option>
                          </select>
                          <i class="arrow"></i>
                        </label>
	                  </div>
                      <div class="col-md-4">
                          <label for="">Cidade atual:</label>
                          <label for="cidade" class="field select">
                            <select id="cidade" name="cidade">
                              <option value="2919553">Luis Eduardo Magalhães</option>
                            </select>
                            <i class="arrow"></i>
                          </label>
                        </div>

                    </div> 
	                <div class="section row">
                      <div class="col-md-4">  
                        <label class="field" for="endereco">Endereço
                        <input name="endereco" id="endereco" class="gui-input" type="text" value="<?php echo $linha->endereco; ?>"></label>
                      </div>
                      <div class="col-md-3">
                        <label class="field" for="bairro">Bairro
                        <input name="bairro" id="bairro" class="gui-input" type="text" value="<?php echo $linha->bairro; ?>"></label>
                      </div>
                      <div class="col-md-2">
                        <label class="field" for="telefone">Telefone
                        <input type="tel" name="telefone" id="telefone" class="gui-input phone-group" placeholder="Telefone" aria-invalid="false" value="<?php echo $linha->telefone; ?>"></label>

                      </div>
	                </div>
                  <?php 
                    //  Impede que o cliente utilize recursos do sistema.
                    if ( $infousuario['tipousuario'] != 'cl' ) { ?>
	                <div class="section-divider col-md-12"><span> Dados de Função</span></div>
	                <div class="section row">
	                	<div class="col-md-4">  
	                        <label class="field" for="departamento">Departamento
	                        <input name="departamento" class="gui-input" type="text" value="<?php echo $linha->departamento; ?>"></label>
	                    </div>
	                    <div class="col-md-3">  
	                        <label class="field" for="cargofuncao">Cargo/Função
	                        <input name="cargofuncao" class="gui-input" type="text" value="<?php echo $linha->cargofuncao; ?>"></label>
	                    </div>
	                    <div class="section-divider col-md-12"><span> Dados de Usuário</span></div>
	                    <?php /*<div class="col-md-4">
                        	<label for="">Categoria de usuário:</label>
	                        <label class="field select">
	                          <select id="tipousuario" name="tipousuario">
	                            <option value="">Selecione</option>
	                            <option <?php if ( $linha->tipousuario == 'co' ) { ?>selected<?php } ?> value="co">Coordenador</option>
	                            <option <?php if ( $linha->tipousuario == 'an' ) { ?>selected<?php } ?> value="an">Analista</option>
	                            <option <?php if ( $linha->tipousuario == 'fi' ) { ?>selected<?php } ?> value="fi">Fiscal</option>
	                            <option <?php if ( $linha->tipousuario == 'ad' ) { ?>selected<?php } ?> value="ad">Administrador</option>
	                          </select>
	                          <i class="arrow"></i>
	                        </label>
        			   	</div>
*/ ?>
                    </div><!-- fim da row -->
                  <?php } ?>
                  <div class="col-md-4">
                    <label class="option option-primary">
                        <input type="checkbox" name="alterarsenha" onclick="apalterarsenha();">
                        <span class="checkbox"></span>Alterar Senha</label>
                  </div>
                  <div id="boxalterarsenha" style="display: none;">
                    <div class="section row">
                      <div class="col-md-3"></div>
                    </div>
                    <div class="section row">
                      <div class="col-md-3">
                        <label class="field" for="senha">Senha:
                        <input name="senha" id="senha" class="gui-input" type="password"></label>
                      </div>
                    </div>
                    <div class="section row">
                      <div class="col-md-3">
                        <label class="field" for="repitasenha">Repita a senha:
                        <input name="repitasenha" class="gui-input" type="password"></label>
                      </div>
                    </div>
                  </div>
                </div>
			          <div class="panel-footer text-left">
                <button type="submit" class="button btn-success">Atualizar</button>
              </div>
            </div>
          </div>
          <!-- end: .tab-content -->
        </div>
      </section>
      </div>
    </aside>
    <!-- End: Right Sidebar -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="js/jquery.maskedinput.min.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    $("#cpfcnpj").mask("999.999.999-99");
    $("#telefone").mask("(99) 99999-9999");
    
    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#admin-form").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        nome: {
          required: true
        },
        email: {
          required: true,
          email: true
        },
        cpfcnpj: {
          required: true
        },
        estado: {
          required: true
        },
        cidade: {
          required: true
        },
        endereco: {
          required: true
        },
        bairro: {
          required: true
        },
        departamento: {
          required: true
        },
        cargofuncao: {
          required: true
        },
        tipousuario: {
          required: true
        },
        telefone: {
          require_from_group: [1, ".phone-group"]
        },
        senha: {
          required: function(element) { 
            if ($('input[name=alterarsenha]').is(':checked')){
              required: true
            }
            
          }
        },
        repitasenha: {
          required: true,
          minlength: 6,
          maxlength: 16,
          equalTo: '#senha'
        }
      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        nome: {
          required: 'Informe o seu nome'
        },
        email: {
          required: 'Informe um e-mail válido',
          email: 'Informe um e-mail válido'
        },
        cpfcnpj: {
          required: 'Informe seu CPF'
        },
        estado: {
          required: 'Informe o Estado'
        },
        cidade: {
          required: 'Informe a Cidade'
        },
        endereco: {
          required: 'Informe seu endereço'
        },
        bairro: {
          required: 'Informe o bairro onde mora'
        },
        departamento: {
          required: 'Informe seu Departamento'
        },
        cargofuncao: {
          required: 'Informe seu Cargo/Função'
        },
        tipousuario: {
          required: 'Informe seu Tipo de Usuário'
        },
        telefone: {
          require_from_group: 'Informe seu telefone'
        },
        senha: {
          required: 'Informe sua senha'
        },
        repitasenha: {
          required: 'Insira a mesma senha informada acima',
          equalTo: 'Senhas não estão iguais'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
