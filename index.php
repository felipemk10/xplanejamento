<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$funcoes = new funcoes;
$login = new login;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));
//---

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
  if ( $_POST['formulario'] == 'login' ) {
    //  Rodando anti-injecgtion nas variáveis.
    $_POST['loginemail'] = $validacoes->anti_injection($_POST['loginemail']);
    $_POST['loginsenha'] = $validacoes->anti_injection($_POST['loginsenha']);
    //---
    
    $msg_formulario = $login->logar($_POST['loginemail'],$_POST['loginsenha']);
  }

  if ( $_POST['formulario'] == 'consulta' ) {
    //  Consultando processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro
                             

      FROM 
      processos.processos

      INNER JOIN processos.processos_proprietario ON processos_proprietario.id_pro = processos.id_pro
      INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg 


      WHERE processos.id_pro = ".(int)$_POST['protocolo']." and cg.cpfcnpj = '".$validacoes->anti_injection($_POST['cpfcnpj'])."'");

      if ( $consulta->rowCount() > 0 ) {
        $linhaProtocolo = $consulta->fetch();
        ?>
        <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript">
          window.open('detalhesProtocoloCliente.php?id_pro=<?php echo $linhaProtocolo['id_pro']; ?>','_self');
        </script>
        <?php
      } else {
        $msg_formulario = 'Processo não encontrado.';
      } 
  }
}
?>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
    <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
    <meta name="description" content="Sistema de Planejamento">
    <meta name="author" content="TemplateMonster">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  </head>

<body class="admin-validation-page sb-l-o sb-r-c onload-check sb-l-m sb-l-disable-animation" data-spy="scroll" data-target="#nav-spy" data-offset="200" style="min-height: 94px;background-color: #4d8dce;">


  <!-- Start: Main -->
  <div id="main">

    

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

     <!-- Begin: Content -->
      <section id="content" class="table-layout">

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="height: 294px;">
            <!-- Validation Example -->
            <div class="admin-form theme-primary mw1000 center-block" style="padding-bottom: 175px;">

              <div class="panel heading-border panel-system" style="border: 0;">

                

                  <div class="panel-body bg-light alinha-conteudo">
                    <!-- .section-divider -->
                    <div class="section row">
                      <div class="col-md-3">
                        <img class="logo-prefeitura" src="img/logo_prefeitura_home.png" height="150">
                      </div>
                      <div class="col-md-9 top-titulo"><img src="img/logo-planejamento.png"></div>
                    </div>
                    <div class="section row  menu">
                      <?php include_once('includes/menuindex.php'); ?>
                    </div>
                    <div class="section row">
                      <div class="col-md-7">
                      <p class="texto-info">
                      Sistema de Planejamento, Orçamento e Gestão tem a finalidade de virtualizar todo o processo e etapas de elaboração de processos de Alvará de Construção, Regularização, Acréscimo de Área, Desmembramento, Remembramento e Redimensionamento.
                      <p class="texto-info">Todo o processo será cadastro pelo sistema, em seguida um protocolo será gerado para consultas de andamento do processo.</p>

                      <p class="texto-info"><strong>Secretária de Planejamento, Orçamento e Gestão</strong></p>

                      <p class="texto-info">A SMPOG tem por objetivo desenvolver ações relacionadas ao planejamento municipal, gestão e acompanhamento da execução do Plano de Governo, bem como acompanhar e executar o planejamento orçamentário, propondo medidas que busquem a efetividade e eficácia das ações de Governo.</p>

                      <p class="texto-info">A Secretaria de Planejamento Subdivide-se em duas Diretorias. Uma ligada ao Planejamento Orçamentário e Acompanhamento de Convênios e uma ligada ao Planejamento e Uso do Solo, ou seja, a organização urbanística da cidade.</p>

                      <p class="texto-info">O Organograma da Secretaria de Planejamento, Orçamento e Gestão é composta de Funções Nomeadas, ocupadas por funcionários concursados ou não, e Funções Técnicas ocupadas por profissionais concursados.</p>
                  </div>
                    <div class="col-md-offset-1 col-md-4 menu-login">
                        <form method="post" action="" id="formlogin">
                          <input type="hidden" name="formulario" value="login">
                          <div class="section row">
                            <div class="panel-body bg-light p25 pb15">
                              <!-- Divider -->
                              <div class="section-divider mv30">
                                <span>Área do profissional</span>
                              </div>

                              <!-- Username Input -->
                              <div class="section">
                                <label class="field-label  fs18 mb10">E-mail</label>
                                <label for="loginemail" class="field prepend-icon">
                                  <input name="loginemail" id="loginemail" class="gui-input" placeholder="Entre com o e-mail" type="text" value="<?php if ( $_SERVER['REQUEST_METHOD'] == 'POST' and $_POST['formulario'] == 'login' ) { echo $_POST['loginemail']; } ?>">
                                  <label for="username" class="field-icon">
                                    <i class="fa fa-user"></i>
                                  </label>
                                </label>
                              </div>

                              <!-- Password Input -->
                              <div class="section">
                                <label class="field-label  fs18 mb10">Senha</label>
                                <label for="loginsenha" class="field prepend-icon">
                                  <input type="password" name="loginsenha" id="loginsenha" class="gui-input" placeholder="Entre com a senha">
                                  <label for="password" class="field-icon">
                                    <i class="fa fa-lock"></i>
                                  </label>
                                </label>
                              </div>
                              <?php if ( $_POST['formulario'] == 'login' and !empty($msg_formulario) ) { ?>
                                <div class="panel-footer clearfix" style="text-align:center;">
                                  <a href="#" class="btn btn-danger mb10 mr5 notification" data-note-style="danger"><?php echo $msg_formulario; ?></a>
                                </div>
                                <?php } ?>
                                <a href="esqueceuasenha.php">Não consegue acessar sua conta?</a>
                              <div class="col-md-1" style="  margin-left: 210px;">
                               <button type="submit" class="button btn-primary" >Entrar</button>
                                <label class="switch ib switch-primary mt10">
                                  <input name="remember" id="remember" checked="" type="checkbox">
                                </label>
                              </div>
                          </div>
                        </form>
                        </div>
                        <form method="post" action="" id="formconsulta">
                              <input type="hidden" name="formulario" value="consulta">
                              <!--Login profissional -->
                              <div class="section row">
                                 <div class="panel-body bg-light p25 pb15" style="margin-top: -67px;">
                              
                                  <div class="section-divider mv30">
                                    <span>Área do cliente - Consulta</span>
                                  </div>
                                  <div class="section">
                                    <label for="protocolo" class="field-label  fs18 mb10">Protocolo</label>
                                    <label for="protocolo" class="field prepend-icon">
                                      <input name="protocolo" id="protocolo" class="gui-input" type="text">
                                      <label for="useprotocolorname" class="field-icon">
                                        <i class="fa fa-code"></i>
                                      </label>
                                    </label>
                                  </div>
                                  <div class="section">
                                    <label for="cpfcnpj" class="field-label  fs18 mb10">CPF/CPNJ - Proprietário</label>
                                    <label for="cpfcnpj" class="field prepend-icon">
                                      <input name="cpfcnpj" id="cpfcnpj" class="gui-input" type="text">
                                      <label for="cpfcnpj" class="field-icon">
                                        <i class="fa fa-lock"></i>
                                      </label>
                                    </label>
                                    <span>(somente números)</span>
                                  </div>
                                </div>
                                <?php if ( $_POST['formulario'] == 'consulta' and !empty($msg_formulario) ) { ?>
                                <div class="panel-footer clearfix" style="text-align:center;">
                                  <a href="#" class="btn btn-danger mb10 mr5 notification" data-note-style="danger"><?php echo $msg_formulario; ?></a>
                                </div>
                                <?php } ?>
                                <div class="col-md-1" style="  margin-left: 230px;">
                                 <button type="submit" class="button btn-primary" >Consultar</button>
                                  <label class="switch ib switch-primary mt10">
                                    <input name="remember" id="remember" checked="" type="checkbox">
                                  </label>
                                </div>
                          </div>
                       </form>
                       <div class="modal fade" id="myModal">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"></span></button>
                                <h1 class="modal-title">Sejam bem-vindos!</h4>
                              </div>
                              
                              <div class="modal-body">
                                <h2 style="color: red;">Nota:</h2>
                            
                                <p style="font-size: 18px;">O sistema de planejamento mudou e agora poderá ser acessado através deste novo endereço "13.58.238.67/xplanejamento". Agora para acessar <b>é usado o email</b> e não mais o seu nome de usuário, se esse é sua primeira vez click neste <a class= "text-danger mn" href="http://13.58.238.67/xplanejamento/esqueceuasenha.php" style="color: blue;">link</a> para redefinir sua senha e ter acesso novamente.</p>
                               
                               
                              </div>
                              
                              <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                              </div>
                            
                            </div><!--/.modal-content-->
                          </div><!-- /.modal-dialog-->
                        </div><!-- /.modal -->
                  </div><!-- conteudo-->
                  </div>
                    <!-- end .section row section -->
                  
                  <?php include('includes/rodapeindex.php'); ?>
          </div>

        </div>
        <!-- end: .admin-form -->

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <style>

  .logo-prefeitura {
    padding-top: 45px;
    padding-left:90px;
  }
  .top-titulo {
    font-size: 28px;
  font-weight: 600;
  margin-top: 32px
  }
  .logo-legenda {
    font-size: 15px;
  }
  .button-direita {
  
  }

    .menu-login {
      font-size: 11px;
      margin-top: 5px; 
    }
    .profissional {
      padding: 22px;



    }
   .col-md-menu {
    padding: 5px;
   }
   div.main {
    background-color: rgba(211,211,211, 0.5)
   }
   .menu-botao{
    border-left: 1px solid #A9A9A9; 
    margin-left: -30px;
    padding-left: 10px;
   }
   .menu-botao-inf {
    border-left: 1px solid #A9A9A9; 

   }

   .menu {
    border: 1px solid rgba(169,169,169, 1.5); 
    border-radius: 3px;
    padding: 20px;
    font-size: 14px;
    color: black;
    background-color: rgba(211,211,211, 0.2)
   }
   .alinha-conteudo{
    margin-top: 40px;


  }
  .texto-info {
    text-align: justify;
    font-size: 13px;
    margin-top: 15px;
    color: #545353;
  }
  .titulo-info {
    font-size: 25px;
    font-weight: bold;
    color: #545353;
  }

  
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
  }
  </style>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- jQuery Validate Plugin-->
  <script src="assets/admin-tools/admin-forms/js/jquery.validate.min.js"></script>

  <!-- jQuery Validate Addon -->
  <script src="assets/admin-tools/admin-forms/js/additional-methods.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {
    //$('#myModal').modal('show');

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS     
    Demo.init();

    /* @custom validation method (smartCaptcha) 
    ------------------------------------------------------------------ */

    $.validator.methods.smartCaptcha = function(value, element, param) {
      return value == param;
    };

    $("#formlogin").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        loginemail: {
          required: true
        },
        loginsenha: {
          required: true
        }

      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        loginemail: {
          required: 'Digite o e-mail'
        },
        loginsenha: {
          required: 'Digite a senha'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });

    $("#formconsulta").validate({

      /* @validation states + elements 
      ------------------------------------------- */

      errorClass: "state-error",
      validClass: "state-success",
      errorElement: "em",

      /* @validation rules 
      ------------------------------------------ */

      rules: {
        protocolo: {
          required: true
        },
        cpfcnpj: {
          required: true
        }

      },

      /* @validation error messages 
      ---------------------------------------------- */

      messages: {
        protocolo: {
          required: 'Digite o protocolo'
        },
        cpfcnpj: {
          required: 'Digite o CPF/CNPJ'
        }

      },

      /* @validation highlighting + error placement  
      ---------------------------------------------------- */

      highlight: function(element, errorClass, validClass) {
        $(element).closest('.field').addClass(errorClass).removeClass(validClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('.field').removeClass(errorClass).addClass(validClass);
      },
      errorPlacement: function(error, element) {
        if (element.is(":radio") || element.is(":checkbox")) {
          element.closest('.option-group').after(error);
        } else {
          error.insertAfter(element.parent());
        }
      }

    });


    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark'
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark'
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });

      // var sliders = $('.ui-timepicker-div', adminForm).find('.ui-slider');
      $('body').find('.ui-slider').each(function(i, e) {
        $(e).addClass('slider-primary');
      });

      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);



  });
  </script>
  <!-- END: PAGE SCRIPTS -->




</body></html>