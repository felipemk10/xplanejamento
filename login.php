<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$funcoes = new funcoes;
$login = new login;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'bloquear',pathinfo( __FILE__ ));
//---

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
  if ( $_POST['formulario'] == 'login' ) {
    //  Rodando anti-injecgtion nas variáveis.
    $_POST['email'] = $validacoes->anti_injection($_POST['email']);
    $_POST['senha'] = $validacoes->anti_injection($_POST['senha']);
    //---
    
    $msg_formulario = $login->logar($_POST['email'],$_POST['senha']);
  }
}
?>
<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
  <meta name="author" content="uasgeek">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
 <!-- <link rel="shortcut icon" href="assets/img/"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page external-alt sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content">

        <div class="admin-form theme-primary mw500" id="login">

          <!-- Login Logo -->
          <div class="row table-layout">
            <a href="dashboard.html" title="Return to Dashboard">
              <img src="img/vale.jpeg" title="XPlanejamento Logo" class="center-block img-responsive" style="max-width: 275px;">
            </a>
          </div>

          <!-- Login Panel/Form -->
          <div class="panel mt30 mb25">

            <form method="post" id="contact">
              <input type="hidden" name="formulario" value="login" />
              <div class="panel-body bg-light p25 pb15">         

                <!-- Username Input -->
                <div class="section">
                  <label for="username" class="field-label  fs18 mb10">E-mail</label>
                  <label for="username" class="field prepend-icon">
                    <input type="text" name="email" id="email" class="gui-input" placeholder="Insira o e-mail">
                    <label for="username" class="field-icon">
                      <i class="fa fa-user"></i>
                    </label>
                  </label>
                </div>

                <!-- Password Input -->
                <div class="section">
                  <label for="username" class="field-label  fs18 mb10">Senha</label>
                  <label for="password" class="field prepend-icon">
                    <input type="password" name="senha" id="senha" class="gui-input" placeholder="Insira a Senha">
                    <label for="password" class="field-icon">
                      <i class="fa fa-lock"></i>
                    </label>
                  </label>
                </div>
              </div>
              <?php if ( !empty($msg_formulario) ) { ?>
              <div class="panel-footer clearfix" style="text-align:center;">
                <a href="#" class="btn btn-danger mb10 mr5 notification" data-note-style="danger"><?php echo $msg_formulario; ?></a>
              </div>
              <?php } ?>
              <div class="panel-footer clearfix" style="text-align:center;">
                <button type="submit" class="button btn-primary mr10">Entrar</button>
                <!--<label class="switch ib switch-primary mt10">
                  <input type="checkbox" name="remember" id="remember" checked>
                  <label for="remember" data-on="YES" data-off="NO"></label>
                  <span>Remember me</span>
                </label>-->
              </div>
              
            </form>
          </div>

          <!-- Registration Links -->
          <div class="login-links">
            <p>
               <a href="esqueceuasenha.php" class="" title="Sign In">Esqueceu a Senha?</a>
            <!--</p>Haven't yet Registered?
              <a href="pages_register(alt).html" title="Sign In">Sign up here</a>
            </p>-->
          </div>
          
          <!-- Registration Links(alt) -->
          <div class="login-links hidden">
            <a href="pages_login-alt.html" class="active" title="Sign In">Sign In</a> |
            <a href="pages_register-alt.html" class="" title="Register">Register</a>
          </div>

        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->


  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    //Demo.init();

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>
