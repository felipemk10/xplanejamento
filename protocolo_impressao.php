<?php
//  A função autoload é utilizada no PHP para fazer o carregamento automático das classes.
require_once('libs/autoload.php');

$conexao = new conexao;
$configuracoes = new configuracoes;
$formatacoes = new formatacoes;
$validacoes = new validacoes;
$autenticar_usuario = new autenticar_usuario;
//  Autenticando usuário
$autenticar_usuario->autenticar($_SESSION['id_usuario'],'permitir',pathinfo( __FILE__ ));

$id_pro = (int)$_GET['id_pro'];

// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
$tipoprocesso = (int)$_GET['tipoprocesso'];

//  Função para includes reconhecer a página e modificar sua exibição.
$emissaoalvarahabitese  = false;

if ( $id_pro > 0 ) {
    //  Listando informações sobre o processo.
    $consulta = $configuracoes->consulta("SELECT 
      processos.id_pro,
      processos.endereco,
      processos.quadra,
      processos.lote,
      processos.numero,
      processos.bairro,
      processos.cidade,
      processos.estado,
      processos.tipoprocesso,
      processos.datahora, 
      processos.situacaoprojeto 

      FROM 

      processos.processos 

      WHERE processos.id_pro = $id_pro and processos.ativo = true");
      $linha2 = $consulta->fetch();
}
?>
<!DOCTYPE html>
<html>
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title>XPlanejamento ver.1.0.0beta</title>
  <meta name="keywords" content="planejamento, prefeitura, luís eduardo magalhães, bahia">
  <meta name="description" content="Sistema de Planejamento">
    <meta name="author" content="TemplateMonster">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->



  </head>
  <body class="impressao">
    <br>
      <div class="section row">
        <div class="col-xs-offset-8 col-xs-2 print-no-view">
          <button type="button" onclick="window.print();">Imprimir</button>
        </div>
      </div>
      <div class="section row">
        <div class="col-xs-12 text-center"><br><br><br>

          <div class=" col-xs-3 ">
            <div class="logo-relatorio"></div>
          </div>
        
          <br>
          <br>

          <span class="titulo alinha-espaco"><b>SECRETARIA DE PLANEJAMENTO, ORÇAMENTO E GESTÃO</b></span>
        </div>
      </div><!-- fim da section row -->
      <br>
      <br>
      <div class="section row">
        <div class="col-xs-offset-2 col-xs-6 titulo tituloPrint" style="text-align:center;"><b>ALVARÁ DE LICENÇA PARA <?php
          if ( $linha2['tipoprocesso'] == 1 ) {
            echo 'CONSTRUÇÃO';
          } else if ( $linha2['tipoprocesso'] == 2 ) {
            echo 'REGULARIZAÇÃO DE OBRAS';
          } else if ( $linha2['tipoprocesso'] == 3 ) {
            echo 'ACRÉSCIMO DE ÁREAS';
          } else if ( $linha2['tipoprocesso'] == 4 ) {
            echo 'CONDOMÍNIO';
          } else if ( $linha2['tipoprocesso'] == 5 ) {
            echo 'REDIMENSIONAMENTO';
          }
        ?></b></div>
      </div>
      <br>
      <div class="section row">
        <div class="c col-xs-2 text-right subtitulo">Protocolo:</div>
        <div class="col-xs-3 text-left campo-texto"><h4><?php echo $id_pro; ?></h4></div>

        <div class="c col-xs-2 text-right subtitulo">Data de emissão:</div>
        <div class="col-xs-3 text-left campo-texto"><h4><?php 

        if ( !empty($_GET['dataemissao']) ) {
          echo $formatacoes->formatar_data('/',$_GET['dataemissao']);
        } else {
          echo $formatacoes->formatar_data('/',date('Y-m-d'));
        }

         ?></h4></div>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-3 subtitulo ">Dados do proprietário</div>
      </div>
      </div>
      <div class="section row">
      <?php 
      //  Listando informações sobre os proprietários.
      $con_listagem_proprietario = $configuracoes->consulta("SELECT 
        cg.nome,
        cg.cpfcnpj

        FROM 
        processos.processos_proprietario

        INNER JOIN geral.cg ON cg.id_cg = processos_proprietario.id_cg                                    

        WHERE processos.processos_proprietario.id_pro = $id_pro ORDER BY processos.processos_proprietario.id_pro ASC"); 

        foreach ( $con_listagem_proprietario as $listagem_proprietario ) { 
        ?>
      <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Nome:</div>
      <div class="col-xs-6 text-left campo-texto"><?php echo strtoupper($listagem_proprietario['nome']); ?></div>   
      <div class="col-xs-1 text-right subtitulo"><?php 
        if ( strlen($listagem_proprietario['cpfcnpj']) == 11 ) {
          echo 'CPF';
        } else if ( strlen($listagem_proprietario['cpfcnpj']) == 14 ) {
          echo 'CNPJ';
        }
        ?>:</div>
      <div class="col-xs-2 text-left campo-texto"><?php 

        if ( strlen($listagem_proprietario['cpfcnpj']) == 11 ) {
          echo $formatacoes->mask($listagem_proprietario['cpfcnpj'],'###.###.###-##');
        } else if ( strlen($listagem_proprietario['cpfcnpj']) == 14 ) {
          echo $formatacoes->mask($listagem_proprietario['cpfcnpj'],'##.###.###/####-##');
        }

        ?></div>   
      <?php } ?>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-2 text-right subtitulo">Dados do projeto</div>
        <br>
      </div>
      <div class="section row">
      <?php 
        //  Listando informações sobre autor.
        $con_listagem_autor = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'a'");

        foreach ( $con_listagem_autor as $listagem_autor ) { ?>
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Autoria:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo strtoupper($listagem_autor['nome']); ?></div>
        <div class="col-xs-1 text-right subtitulo"><?php if ( $listagem_autor['tipoprofissional'] == 'e' ) { echo 'CREA'; } else if ( $listagem_autor['tipoprofissional'] == 'a' ) { echo 'CAU'; } ?></div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_autor['creacau']; ?></div> 

      <?php } ?>
      </div>
      <div class="section row">
      <?php 
        //  Listando informações sobre autor.
        $con_listagem_resptecnico = $configuracoes->consulta("SELECT 
          cg.nome,
          cg.creacau,
          -- e = engenheiro, a = arquiteto
          processos_profissional.tipoprofissional

          FROM 
          processos.processos_profissional

          INNER JOIN geral.cg ON cg.id_cg = processos_profissional.id_cg                                    

          WHERE processos.processos_profissional.id_pro = $id_pro and processos.processos_profissional.tipo = 'r'");

        foreach ( $con_listagem_resptecnico as $listagem_resptecnico ) { ?>
        <div class="col-xs-2 text-right subtitulo">Resposável Técnico:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo strtoupper($listagem_resptecnico['nome']); ?></div>
        <div class="col-xs-1 text-right subtitulo"><?php if ( $listagem_resptecnico['tipoprofissional'] == 'e' ) { echo 'CREA'; } else if ( $listagem_resptecnico['tipoprofissional'] == 'a' ) { echo 'CAU'; } ?></div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $listagem_resptecnico['creacau']; ?></div>  
      <?php } ?>
      </div>
      <br>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Endereço:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo $linha2['endereco']; ?></div>
        <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
        <div class="col-xs-1 text-right subtitulo">Quadra:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha2['quadra']; ?></div>
        <?php } ?>
      </div>

      <div class="section row">
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Bairro:</div>
        <div class="col-xs-6 text-left campo-texto"><?php echo $linha2['bairro']; ?></div>
        <?php if ( !empty($linha2['quadra']) and !empty($linha2['lote']) ) { ?>
        <div class="col-xs-1 text-right subtitulo">Lote:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha2['lote']; ?></div>
        <?php } ?>
      </div>
      <div class="section row">
        <div class="col-xs-offset-1 col-xs-1 text-right subtitulo">Cidade:</div>
        <div class="col-xs-6 text-left campo-texto">Luís Eduardo Magalhães - BA</div>
        <?php if ( $linha2['numero'] > 0 ) { ?>
        <div class="col-xs-1 text-right subtitulo">Número:</div>
        <div class="col-xs-2 text-left campo-texto"><?php echo $linha2['numero']; ?></div>
        <?php } ?>
      </div>
      <div class="section row">
      <br>

      <?php  
        if ( $tipoprocesso >= 1 and $tipoprocesso <= 3 ) { 
          include_once('includes/impressao/info_alvaraProtocolo.php');
        } else if ( $tipoprocesso == 4 and $_GET['condominio_pavimentos'] != 't' ) { 
          include_once('includes/impressao/info_condominioProtocolo.php');
        } else if ( $tipoprocesso == 4 and $_GET['condominio_pavimentos'] == 't' ) { 
          include_once('includes/impressao/info_condominioProtocolo_pavimentos.php');
        } else if ( $tipoprocesso == 5 ) { 
          include_once('includes/impressao/info_alvaraRedimensionamentoProtocolo.php');
        }
      ?>
  </body>
    <style>
   @media print {
    .hidden-print {
      display: none !important;
    }
      
    .subtitulo {
      font-weight: bold;
    }

    .titulo, .campo-texto {
      font-size: 10px!important;
      font-weight: normal!important;
    }

    .titulo, .subtitulo, .campo-texto {
      font-size: 10px!important;
      font-weight: normal!important;

    }
    .impressao {
      margin-top: -70px;
    }

     .nota {
      border-style: none!important;
      font-size: 10px!important;
      text-align: justify;
    }

    .conteudo-1{border-style: none!important;}
    .campo-texto { border-style: none!important;}
    .print-no-view {display: none!important; }
    .assinatura { border-bottom: 1px solid black;
      border-bottom: 1px solid black; }
    .text-area {
      width: 540px!important;
      height: 50px!important;
    }
  }
    .text-area {
      width: 1060px;
      height: 100px;

    }
  
    .tituloPrint {
      margin-left:300px;
    }    
    @media print {
      .logo-relatorio {
       content:url('img/logo_prefeitura_home.png');
       z-index: 2;
       position: absolute;
       width: 80px;
       height: 80px;
      }
      .tituloPrint {
        margin-top:-10px;
        margin-left:180px;
      }
    }

    .impressao {
    padding-left: 60px;
    background: #fff;
    color: #393939;
    font-size: 16px;


    }
    .alinha-espaco {
      padding-top: -1000px;
    }

    .titulo {
      font-weight: 600;
      font-size: 15px;

      
    }

    .titulo-sec {
        font-weight: 600;
        font-size: 12px;
        margin-left: 64px;

    }
    .subtitulo {
        font-size: 14px;
        font-weight: bold;
        margin-top: 10px; 


    }
    .assinatura { border-bottom: 1px solid black;}

    .campo-texto {
      font-size: 14px;
      border-bottom: #666 1px solid;
      margin-top: 10px;  
    }
    .text-area {
      border: 1px solid black;
    }
    .nota {

      font-size: 13px;
      text-align: justify;
    }


      
    </style>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <script src="js/functions.js"></script>
    <script type="text/javascript">
      function aplogs($acionador,$objeto){ 
        if ( $('input[name='+$acionador+']').is(':checked')) {
          $("#"+$objeto).fadeIn(500);
        }else{
          $("#"+$objeto).css("display","none");
        } 
      }
      function ap30(){ 
          if ( $('input[name=checkocultdetalhes]').is(':checked')) {
              $("#camposocultosmaisdetana").fadeIn(500);
            }else{
              $("#camposocultosmaisdetana").css("display","none");
            } 
        }
        function apobs(){ 
          if ( $('input[name=checkocultobs]').is(':checked')) {
              $("#boxobs").fadeIn(500);
            }else{
              $("#boxobs").css("display","none");
            } 
        }
    </script>
</html>