<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;

class autenticacao extends crud {

	function v1($id_usuario,$tipo,$pagina) {

		$id_usuario = (INT)manipuladores::criptografia($id_usuario,'base64','decode');
		
		$this->temposessao = 10800; // Obs: Media em segundos.

		//  Verifica se é cliente; caso seja, o sistema irá impedi-lo de ver as informações de outros usuários.
		$this->tcliente = db::prepare("SELECT id_cg FROM geral.usuarios WHERE id_cg = $id_usuario and tipousuario = 'cl'");
		$this->tcliente->execute();
		$this->tcliente = $this->tcliente->rowCount();
		//---

		//	Páginas que o Cliente não pode ter acesso.
		if ( $pagina['basename'] == 'aprovacao.php' or 
   			$pagina['basename'] == 'aprovacao_redimensionamento.php' or
   			$pagina['basename'] == 'checagem.php' or  
   			$pagina['basename'] == 'checagem_condominio.php' or
   			$pagina['basename'] == 'checagem_redimensionamento.php' or
   			$pagina['basename'] == 'fiscalizacao.php' ) {
   			$this->pginvalida = true;
   		}

		if (  $tipo == 'bloquear' and $id_usuario > 0 ) {
			?><script language='javascript' type='text/javascript'>window.open('listar_processos.php','_self');</script><?php
		} else if ( $tipo == 'permitir' and (INT)$id_usuario == 0 or 
		$tipo == 'permitir' and !empty($_SESSION["sessiontime"]) and $_SESSION["sessiontime"] < (time() - $this->temposessao) ) {

			//	Apagando todas as sessões.
			session_unset();
			
			?><script language='javascript' type='text/javascript'>window.open('login.php','_self');</script><?php
		} else if ( $this->tcliente > 0 and $this->pginvalida == true ) {
			?><script language='javascript' type='text/javascript'>window.open('login.php','_self');</script><?php
		} else if ( $tipo == 'permitir' and (INT)$_SESSION['id_usuario'] > 0 ) {
			$_SESSION["sessiontime"] = time();
		}
	}
}