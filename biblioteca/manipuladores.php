<?php
namespace Biblioteca;

class manipuladores {
	//	Evitar SQL injection
	public function anti_injection($sql) {
		//Remove palavras que contenham sintaxe sql
		$sql = preg_replace("/(from|select|insert|delete|where|drop table|show tables|#|\*|--|\\\\)/i", '',$sql);
		$sql = addslashes($sql);//Adiciona barras invertidas a uma string
		$sql = get_magic_quotes_gpc() ? stripslashes($sql) : $sql;
		//$sql = function_exists('mysql_real_escape_string') ? mysql_real_escape_string($sql) : mysql_escape_string($sql);
		return $sql;
	}
	//---
	public function formatar_data($separacao,$data) {
		$this->dia = substr($data,8,2);
        $this->mes = substr($data,5,2);
        $this->ano = substr($data,0,4);
        return $this->dia.$separacao.$this->mes.$separacao.$this->ano;
	}
	public function formatar_datahora($separacao,$data) {
		$this->dia = substr($data,8,2);
        $this->mes = substr($data,5,2);
        $this->ano = substr($data,0,4);

        $this->hora = substr($data,11,5);
        return $this->dia.$separacao.$this->mes.$separacao.$this->ano.' - '.$this->hora;
	}
	public function retira_acentos($str, $enc = "UTF-8") {
		$this->acentos = array(
		'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
		'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
		'C' => '/&Ccedil;/',
		'c' => '/&ccedil;/',
		'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
		'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
		'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
		'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
		'N' => '/&Ntilde;/',
		'n' => '/&ntilde;/',
		'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
		'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
		'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
		'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
		'Y' => '/&Yacute;/',
		'y' => '/&yacute;|&yuml;/',
		'a.' => '/&ordf;/',
		'o.' => '/&ordm;/');
	
		return preg_replace($this->acentos, array_keys($this->acentos), htmlentities($str,ENT_NOQUOTES, $enc));
	}
	//	Retirar os simbolos da string.
	public function retira_simbolos($string) {
		$this->string = trim($string);
	    $this->string = strtolower($this->string);
	
	    $this->string = str_replace( '-', '', $this->string);
	    $this->string = str_replace( '\\', '', $this->string);
	    $this->string = str_replace( '/', '', $this->string);
	    $this->string = str_replace( '´', '', $this->string);
	    $this->string = str_replace( '`', '', $this->string);
	    $this->string = str_replace( '~', '', $this->string);
	    $this->string = str_replace( '^', '', $this->string);
	    $this->string = str_replace( '¨', '', $this->string);
	    
	    $this->string = str_replace( '!', '', $this->string);
	    $this->string = str_replace( '@', '', $this->string);
	    $this->string = str_replace( '#', '', $this->string);
	    $this->string = str_replace( '$', '', $this->string);
	    $this->string = str_replace( '%', '', $this->string);
	    $this->string = str_replace( '¨', '', $this->string);
	    $this->string = str_replace( '*', '', $this->string);
	    $this->string = str_replace( '(', '', $this->string);
	    $this->string = str_replace( ')', '', $this->string);
	    $this->string = str_replace( '_', '', $this->string);
	    $this->string = str_replace( '+', '', $this->string);
	    $this->string = str_replace( '=', '', $this->string);
	    $this->string = str_replace( '{', '', $this->string);
	    $this->string = str_replace( '[', '', $this->string);
	    $this->string = str_replace( '}', '', $this->string);
	    $this->string = str_replace( ']', '', $this->string);
	    $this->string = str_replace( '|', '', $this->string);
	    $this->string = str_replace( '<', '', $this->string);
	    $this->string = str_replace( '>', '', $this->string);
	    $this->string = str_replace( '.', '', $this->string);
	    $this->string = str_replace( ':', '', $this->string);
	    $this->string = str_replace( ';', '', $this->string);
	    $this->string = str_replace( '?', '', $this->string);
	    $this->string = str_replace( '¹', '', $this->string);
	    $this->string = str_replace( '²', '', $this->string);
	    $this->string = str_replace( '³', '', $this->string);
	    $this->string = str_replace( '£', '', $this->string);
	    $this->string = str_replace( '¢', '', $this->string);
	    $this->string = str_replace( '¬', '', $this->string);
	    $this->string = str_replace( '§', '', $this->string);
	    $this->string = str_replace( 'ª', '', $this->string);
	    $this->string = str_replace( 'º', '', $this->string);
	    $this->string = str_replace( '"', '', $this->string);
	    $this->string = str_replace( "'", '', $this->string);
	    return $this->string;
	}
	
	//	Criptografia padrão.
	public function criptografia($valor, $tipo, $acao){
		if ( $tipo == 'base64' ) {
			if ( $acao == 'encode' ) {
				return base64_encode(base64_encode(base64_encode('apisuporte'.$valor)));
			} else if ( $acao == 'decode' ) {
				return substr(base64_decode(base64_decode(base64_decode($valor))),10,11);
			}
		}
	}

	public function numerodouble($modo,$numero) {
		if ( empty($numero) ) {
			return 0;
		} else {
			if ( $modo == 'ajuste' ) {
				return str_replace( ',', '.', $numero);
			} else if ( $modo == 'imprimir' ) {
				return str_replace( '.', ',', $numero);
			}
		}
	}

	public function numeroarea($numero) { //Irving
		if ( empty($numero) ) {
			return '0,00';
		} else if ($numero == floor($numero)) {
			return $numero.',00';
		} else if ($numero*10 == floor($numero*10)) {
			return str_replace( '.', ',', $numero.'0');
		} else {
				return str_replace( '.', ',', $numero);
		}	
	}

	public function mask($val, $mask) {
	 $maskared = '';
	 $k = 0;
	 for($i = 0; $i<=strlen($mask)-1; $i++)
	 {
	   if($mask[$i] == '#')
	   {
	      if(isset($val[$k]))
	       $maskared .= $val[$k++];
	   }
	   else
	 {
	     if(isset($mask[$i]))
	     $maskared .= $mask[$i];
	     }
	 }
	 return $maskared;
	}
}