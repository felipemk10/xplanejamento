<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_pavimentacao extends crud {
	function manipulacoes($id_pav, $id_alv, $tipo, $area, $formulario) {
		//	tipo: pat = Pavimento Terreo, sbc = área sub solo a contruir, are = área Existente,  sbe = área sub solo existente, adk = área Deck, apa = área Piscina 
		$tipo = manipuladores::anti_injection($tipo);


		if ( $formulario != 'delete' and $tipo != 'pat' and 
			$formulario != 'delete' and $tipo != 'sbc' and 
			$formulario != 'delete' and $tipo != 'are' and 
			$formulario != 'delete' and $tipo != 'sbe' and 
			$formulario != 'delete' and $tipo != 'adk' and 
			$formulario != 'delete' and $tipo != 'apa' ) {
			return 'Tipo inválido';
		} else if ( $formulario != 'delete' and empty($area) ) {
			return 'Preencha a área do terreno';
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_pavimentacao (
	            		id_alv,	 
						area,	 
						tipo,
						datahora
	            	) 


	            	VALUES (?,?,?,now())");	
					$sql->bindValue(1, $id_alv);
					$sql->bindValue(2, $area);
					$sql->bindValue(3, $tipo);
					
					$sql->execute();

					return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos_pavimentacao
	            		SET 
		            		id_alv	 		=	$id_alv,
							area	 		=	$area,
							tipo	=	'".$tipo."'
							
	            		WHERE id_pav = ".$id_pav."");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$sql = db::prepare("DELETE FROM processos.processos_pavimentacao WHERE id_pav = $id_pav");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}