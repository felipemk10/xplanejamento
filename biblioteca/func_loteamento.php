<?php 
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_loteamento extends crud {
	function manipulacoes($id_lot, $id_pro, $id_cg, $loteamentoproposto, $matricula, $areatotal, $areaviaspublicas, $areaverde, $areaequipamentopublico, $arealotes, $quantidade_lotes, $lot_ativo, $formulario) { 
		 
		$loteamentoproposto = manipuladores::anti_injection($loteamentoproposto);

		if ( empty($loteamentoproposto) ) {
			return 'Preencha o nome do loteamento proposto';
		} else if ( empty($areatotal) ) {
			return 'Preencha a área total do loteamento';
		} else if ( empty($areaviaspublicas) ) {
			return 'Preencha a área de vias públicas do loteamento';
		} else if ( empty($areaverde) ) {
      	  	return 'Preencha a área verde do loteamento';
      	} else if ( empty($areaequipamentopublico) ) {
			return 'Preencha a área de equipamentos públicos do loteamento'; 
		} else if ( empty($arealotes) ) {
        	return 'Preencha a área de lotes do loteamento';
        } else if ( empty($quantidade_lotes) ) {
        	return 'Preencha a quantidade de lotes do loteamento';
        } else if ( empty($matricula) ) {
        	return 'Preencha o número de matrícula do loteamento';
		} else {
			
			if ( $formulario == 'cadastro' ){ 
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_loteamento (
	            		id_pro,	 
						id_cg,
						nome,	 
						areatotal,	 
						areaviaspublicas,	 
						areaverde,
						areaequipamentopublico,
						arealotes,
						datahora,
						ativo,
						quantlotes,
						matricula
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,now(),false,?,?)");
					$sql->bindValue(1, $id_pro);
					$sql->bindValue(2, $id_cg);
					$sql->bindValue(3, $loteamentoproposto);
					$sql->bindValue(4, $areatotal);
					$sql->bindValue(5, $areaviaspublicas);
					$sql->bindValue(6, $areaverde);
					$sql->bindValue(7, $areaequipamentopublico);
					$sql->bindValue(8, $arealotes);
					$sql->bindValue(9, $quantidade_lotes);
					$sql->bindValue(10, $matricula);
					
					$sql->execute();
					return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				//if($lot_ativo=='on') $lot_ativo = true; else $lot_ativo = false;

				$sql = db::prepare("UPDATE processos.processos_loteamento
	            		SET 
		            		id_pro	 				=	$id_pro,
							id_cg	 				=	$id_cg,
							nome					=	'$loteamentoproposto',
							areatotal	 			=	$areatotal,
							areaviaspublicas		=	$areaviaspublicas,
							areaverde				=	$areaverde,
							areaequipamentopublico 	=	$areaequipamentopublico,
							arealotes				=	$arealotes,
							ativo 					=	'$lot_ativo',
							quantlotes              =   $quantidade_lotes,
							matricula				=	'$matricula'

	            		WHERE id_lot = $id_lot");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}