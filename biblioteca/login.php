<?php
namespace Biblioteca;

use Biblioteca\crud;

class login extends crud {
  protected $table = 'geral.cg';
  private $nome;
  private $email;

  public function setNome($nome) {
    $this->nome = $nome;
  }

  public function getNome() {
    return $this->nome;
  }

  public function setEmail($email) {
    $this->email = $email;
  }

  public function insert() {
    $sql  = "INSERT INTO $this->table (nome, email), VALUES (:nome, :email)";
    $stmt = DB::prepare($sql);
    $stmt->bindParam(':nome', $this->nome);
    $stmt->bindParam(':email', $this->email);
    return $stmt->execute();
  }

}
