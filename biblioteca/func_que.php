<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_que extends crud {
	function manipulacoes($id_que, $id_con, $areacontruir, $areaexistente, $areausoexclusivo, $areacomumproporcional, $tipoprocesso, $formulario) {
		//	tipoprocesso: c = Construção, R = Regularização, A = Acréscimo de área
		$tipoprocesso = manipuladores::anti_injection($tipoprocesso);

		if ( $formulario != 'delete' and $tipoprocesso != 'c' and $tipoprocesso != 'r' and $tipoprocesso != 'a' ) {
			return 'Tipo processo inválido2'.$tipoprocesso.'--';
		} else if ( $formulario != 'delete' and $areacontruir < 0 ) {
			return 'Preencha a área a construir';
		} else if ( $formulario != 'delete' and $areaexistente < 0 ) {
			return 'Preencha a área existente';
		} else if ( $formulario != 'delete' and $areausoexclusivo < 0 ) {
			return 'Preencha a área de uso exclusivo';
		} else if ( $formulario != 'delete' and $areacomumproporcional < 0 ) {
			return 'Preencha a área comum proporcional';
		} else {
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_que (
	            		id_con,	 
						areacontruir,	
						areaexistente,	
						areausoexclusivo,	
						areacomumproporcional,	
						tipoprocesso
	            	) 


	            	VALUES (?,?,?,?,?,?)");	
					$sql->bindValue(1, $id_con);
					$sql->bindValue(2, $areacontruir);
					$sql->bindValue(3, $areaexistente);
					$sql->bindValue(4, $areausoexclusivo);
					$sql->bindValue(5, $areacomumproporcional);
					$sql->bindValue(6, $tipoprocesso);
					
					$sql->execute();

					return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos_que
	            		SET 
		            		id_con 					= $id_con,	 
							areacontruir			= ".(double)$areacontruir.",
							areaexistente			= ".(double)$areaexistente.",
							areausoexclusivo		= ".(double)$areausoexclusivo.",
							areacomumproporcional	= ".(double)$areacomumproporcional.",
							tipoprocesso			= '".$tipoprocesso."'
							
	            		WHERE id_que = ".$id_que."");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$sql = db::prepare("DELETE FROM processos.processos_que WHERE id_que = $id_que");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}