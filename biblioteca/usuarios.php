<?php
namespace Biblioteca;

use \PDO;

class usuarios extends crud {
  protected $table = 'geral.cg';

  private $tipoUsuario;
  private $tipoProfissional;
  private static $UsuarioLogado;
  
  // cl = Cliente, co = Coordenador, an = Analista, fi = Fiscal, ad = Administrador
  public function setTipoUsuario($tipoUsuario) {
    $this->tipoUsuario = $tipoUsuario;
  }
  // 1 = Engenheiro, 2 = Técnico, 3 = Arquiteto
  public function setTipoProfissional($tipoProfissional) {
    $this->tipoProfissional = $tipoProfissional;
  }
  
  public function findAll() {
    $sql  = "SELECT 
      cg.id_cg,
      cg.nome,  
      cg.endereco, 
      cg.bairro, 
      cg.datahora,
      usuarios.departamento,
      usuarios.cargofuncao,
      usuarios.tipoprofissional,
      cg.ativo

      FROM $this->table 

        INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg

      WHERE ";

      if ( !empty($this->tipoUsuario) ) {
        $sql = $sql." usuarios.tipousuario   = '$this->tipoUsuario'";
      }
      if ( $this->tipoProfissional  > 0 ) {
        $sql = $sql." and usuarios.tipoprofissional   =  :tipoprofissional";
      }

    $stmt = db::prepare($sql);
    
    if ( $this->tipoProfissional  > 0 ) {
      $stmt->bindParam(':tipoprofissional', $this->tipoProfissional, PDO::PARAM_INT);
    }
    
    $stmt->execute();
    //print_r($stmt);
    $stmt2[0] = $stmt->rowCount();
    $stmt2[1] = $stmt->fetchAll();

    return $stmt2;
  }

  //  Imprime nome do usuário.
  public static function getUsuarioLogado($id) {
    if (!isset(self::$UsuarioLogado)) {
      $sql  = "SELECT cg.nome, usuarios.tipousuario, usuarios.bi FROM geral.cg 
        INNER JOIN geral.usuarios ON usuarios.id_cg = cg.id_cg
      WHERE cg.id_cg = :id";
      $stmt = db::prepare($sql);
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);
      $stmt->execute();
      self::$UsuarioLogado = $stmt->fetch();
    }
    return self::$UsuarioLogado;
  }
}