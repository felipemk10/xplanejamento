<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_condominio extends crud  {
	function manipulacoes($id_con,$id_pro,$id_cg, $finalidadeobra, $areaterreno, $situacaoterreno, $desmembramento, $desmembramento_entregue, $formulario) {
		// finalidadeobra: re = residencial, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial 
		$finalidadeobra 	= manipuladores::anti_injection($finalidadeobra);
		// situacaoterreno: m = meio de quadra, e = esquina
		$situacaoterreno 	= manipuladores::anti_injection($situacaoterreno); 
		
		if ( $finalidadeobra != 'rc' and $finalidadeobra != 'cm' ) {
			return 'Finalidade de Obra inválida';
		} else if ( $situacaoterreno != 'm' and $situacaoterreno != 'e' ) {
			return 'Situação do Terreno inválida';
		} else if ( isset($desmembramento) and empty($desmembramento_entregue) ) {
      	  	return 'Informe a opção de Entregue'; 
		} else if ( empty($areaterreno) ) {
			return 'Preencha a área do terreno';
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_condominio (
	            		id_pro,	 
						id_cg,	 
						areaterreno,	 
						situacaoterreno,	 
						desmembramento,
						desmembramento_entregue,
						finalidadeobra,
						datahora
	            	) 


	            	VALUES (?,?,?,?,?,?,?,now())");	
					$sql->bindValue(1, $id_pro);
					$sql->bindValue(2, $id_cg);
					$sql->bindValue(3, $areaterreno);
					$sql->bindValue(4, $situacaoterreno);
					$sql->bindValue(5, $desmembramento);
					$sql->bindValue(6, $desmembramento_entregue);
					$sql->bindValue(7, $finalidadeobra);
					
					$sql->execute();

					return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos_condominio
	            		SET 
		            		id_pro	 		=	$id_pro,
							id_cg	 		=	$id_cg,
							areaterreno	 	=	$areaterreno,
							situacaoterreno	=	'".$situacaoterreno."',
							desmembramento	=	'".$desmembramento."',
							desmembramento_entregue	=	'".$desmembramento_entregue."',
							finalidadeobra	=	'".$finalidadeobra."'

	            		WHERE id_con = ".$id_con."");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}