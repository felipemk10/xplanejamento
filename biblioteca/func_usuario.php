<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_usuario extends crud {
		 
	function manipulacoes(
		$id_cg, 
		$nome,	 
		$email,	 
		$endereco,	 
		$bairro,	 
		$cidade,
		$estado,	 
		$telefone,	 
		$cpfcnpj,	 
		$creacau,
		//	1 = usuario, 2 = cadastro do sistema
		$tipo, 
		$formulario) {
			
		if ( $formulario == 'cadastro' ){
			if ( $consulta = processos::consulta("SELECT email FROM geral.cg WHERE email = '".$email."'")->rowCount() > 0 and $tipo == 1 ) {
				return 'E-mail '.$email.' já está em uso';
			} else {
				$sql = db::prepare("INSERT INTO geral.cg (
            		nome,	 
					email,	 
					endereco,	 
					bairro,	 
					cidade,
					estado,
					telefone,	 
					cpfcnpj,	 
					creacau,
					tipo,
					datahora
            	) 


            	VALUES (?,?,?,?,?,?,?,?,?,?,now())");	
				$sql->bindValue(1, $nome);
				$sql->bindValue(2, $email);
				$sql->bindValue(3, $endereco);
				$sql->bindValue(4, $bairro);
				$sql->bindValue(5, $cidade);
				$sql->bindValue(6, $estado);
				$sql->bindValue(7, $telefone);
				$sql->bindValue(8, $cpfcnpj);
				$sql->bindValue(9, $creacau);
				$sql->bindValue(10, $tipo);

				$sql->execute();

				return db::lastInsertId();
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
				

			//	Redirecionamento
			//header("Location: index.php");
		} else if ( $formulario == 'alteracao' ) {
			$sql = db::prepare("UPDATE geral.cg SET 
					nome = '".$nome."',	 
					email = '".$email."',	 
					endereco = '".$endereco."',	 
					bairro = '".$bairro."',	 
					cidade = ".$cidade.",
					estado = '".$estado."',
					telefone = '".$telefone."',	 
					cpfcnpj = '".$cpfcnpj."',	 
					creacau = '".$creacau."',
					tipo = $tipo
				WHERE 
					id_cg = $id_cg");
			$sql->execute();
			
			// Mostrar possíveis erros.
			//print_r($sql->errorInfo());
		} else if ( $formulario == 'desativa' ) {
			$sql = db::prepare("UPDATE geral.cg SET ativo = false WHERE id_cg = ".$id_cg."");
			
			$sql->execute();
			
			// Mostrar possíveis erros.
			//print_r($sql->errorInfo());
		} else if ( $formulario == 'ativa' ) {
			$sql = db::prepare("UPDATE geral.cg SET ativo = true WHERE id_cg = ".$id_cg."");
			
			$sql->execute();
			
			// Mostrar possíveis erros.
			//print_r($sql->errorInfo());
		}
	}
}
