<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_alvara extends crud {
	function manipulacoes($id_alv,$id_pro,$id_cg, $finalidadeobra, $areaterreno, $situacaoterreno, $desmembramento, $desmembramento_entregue, $taxapermeabilidade,$taxaocupacao, $formulario) {
		// finalidadeobra: re = residencial, rc = residencial em condomínio, co = comercial, mi = MISTO, is = institucional, ga = galpao, id = industrial 
		$finalidadeobra 	= manipuladores::anti_injection($finalidadeobra);
		// situacaoterreno: m = meio de quadra, e = esquina
		$situacaoterreno 	= manipuladores::anti_injection($situacaoterreno); 
		

		if ( $finalidadeobra != 're' and $finalidadeobra != 'rc' and $finalidadeobra != 'co' and $finalidadeobra != 'mi' and $finalidadeobra != 'is' and $finalidadeobra != 'ga' and $finalidadeobra != 'id' ) { //Irving - acrescentado rc
			return 'Finalidade de Obra inválida';
		} else if ( $situacaoterreno != 'm' and $situacaoterreno != 'e' ) {
			return 'Situação do Terreno inválida';
		} else if ( isset($desmembramento) and empty($desmembramento_entregue) ) {
      	  	return 'Informe a opção de Entregue'; 
		} else if ( empty($areaterreno) ) {
			return 'Preencha a área do terreno';
		/*} else if ( empty($taxapermeabilidade) ) {
        	return 'Preencha a Taxa de permeabilidade';*/
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_alvara (
	            		id_pro,	 
						id_cg,	 
						areaterreno,	 
						situacaoterreno,	 
						desmembramento,
						desmembramento_entregue,
						finalidadeobra,
						taxapermeabilidade,
						taxaocupacao,
						datahora
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,?,now())");	
					$sql->bindValue(1, $id_pro);
					$sql->bindValue(2, $id_cg);
					$sql->bindValue(3, $areaterreno);
					$sql->bindValue(4, $situacaoterreno);
					$sql->bindValue(5, $desmembramento);
					$sql->bindValue(6, $desmembramento_entregue);
					$sql->bindValue(7, $finalidadeobra);
					$sql->bindValue(8, $taxapermeabilidade);
					$sql->bindValue(9, $taxaocupacao);
					
					$sql->execute();

					return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos_alvara
	            		SET 
		            		id_pro	 		=	$id_pro,
							id_cg	 		=	$id_cg,
							areaterreno	 	=	$areaterreno,
							situacaoterreno	=	'".$situacaoterreno."',
							desmembramento	=	'".$desmembramento."',
							desmembramento_entregue		=	'".$desmembramento_entregue."',
							finalidadeobra				=	'".$finalidadeobra."',
							taxapermeabilidade 			=	$taxapermeabilidade,
							taxaocupacao 	   			=	$taxaocupacao

	            		WHERE id_alv = ".$id_alv."");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}