<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_profissional extends crud {
	function manipulacoes($id_pro, $id_cg, $tipoprofissional, $tipo, $formulario) {
		//	e = engenheiro, a = arquiteto
		$tipoprofissional 	= manipuladores::anti_injection($tipoprofissional);
		//	a = autor, r = responsável técnico
		$tipo 				= manipuladores::anti_injection($tipo); 
		//---
		
		
		if ( $tipoprofissional != 'e' and $tipoprofissional != 'a' ) {
			return 'Tipo profissional inválido';
		} else if ( $tipo != 'a' and $tipo != 'r' ) {
			return 'Tipo inválido';
		} else {
			
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $this->sql = db::prepare("INSERT INTO processos.processos_profissional (
	            		id_pro,	
						id_cg,
						tipoprofissional,	
						tipo
	            	) 


	            	VALUES (?,?,?,?)");	
					$this->sql->bindValue(1, $id_pro);
					$this->sql->bindValue(2, $id_cg);
					$this->sql->bindValue(3, $tipoprofissional);
					$this->sql->bindValue(4, $tipo);
					
					$this->sql->execute();

					return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($this->sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$this->sql = db::prepare("UPDATE processos.processos_profissional
	            		SET 
		            		id_pro = $id_pro,	
							id_cg = $id_cg,
							tipoprofissional = '".$tipoprofissional."',	
							tipo = '".$tipo."'

	            		WHERE id_pro = ".$id_pro." and id_cg = ".$id_cg."");
				
				$this->sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($this->sql->errorInfo());
			}
		}
	}
}