<?php
namespace Biblioteca;

abstract class crud extends db {
  protected $table;
  
  public function delete($id) {
    $sql  = "DELETE FROM $this->table WHERE id = :id";
    $stmt = db::prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
  }
}