<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_processo extends crud {
	function manipulacoes($id_pro,$tipoprocesso,$situacaoprojeto,$endereco,$bairro,$quadra,$lote,$numero,$cidade,$estado,$formulario) {
		
		// 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
		$tipoprocesso = (int)$tipoprocesso;
		

		$endereco 	= manipuladores::anti_injection($endereco);
		$bairro 	= manipuladores::anti_injection($bairro);
		$quadra 	= manipuladores::anti_injection($quadra);
		$lote 		= manipuladores::anti_injection($lote);
		$numero 	= (int)$numero;
		$cidade 	= manipuladores::anti_injection($cidade);
		$estado 	= manipuladores::anti_injection($estado);
		//---
		
		/* Legenda 'Situação Projeto'
		  1 - A ser analisado,
		  2 - Em análise,
		  3 - Pendência de documentos ou correção, 
		  
		  4 - Processo não permitido ou reprovado na análise/vistoria, 
		  5 - Processo encaminhado à procuradoria - Dúvida na vistoria,
		  6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria, 
		  
		  7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto,
		  8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga, 
		  9 - Aprovado - Decreto assinado - para entregar,
		  10 - Aprovado - Processo finalizado e decreto entregue, 

		  11 - Aprovado
		*/

		if ( $tipoprocesso < 1 or $tipoprocesso > 5 and $tipoprocesso != 7) {//Irving - tipoprocesso de 1 a 5 ou 7
			return 'Processo inválido.';
		} else if ( $situacaoprojeto < 1 and $situacaoprojeto > 11 ) {
			return 'Situação do projeto inválido';
		} else if ( empty($endereco) ) {
			return 'Preencha o endereço';
		} else if ( $numero == 0 and empty($quadra) and empty($lote) ) {
			return 'Preencha quadra/lote ou o número do local';
		} else if ( $numero == 0 and empty($quadra) ) {
			return 'Preencha a quadra';
		} else if ( $numero == 0 and empty($lote) ) {
			return 'Preencha a lote';
		} else if ( empty($bairro) ) {
			return 'Preencha o bairro';
		} else if ( empty($estado) ) {
			return 'Escolha o estado';
		} else if ( empty($cidade) ) {
			return 'Escolha a cidade';
		} else {
			
			$sql_usuario = processos::consulta("SELECT cg.id_cg FROM geral.cg WHERE cg.id_cg = ".manipuladores::criptografia($_SESSION['id_usuario'],'base64','decode')." and cg.ativo = true");

	    	$linha_usu = $sql_usuario->fetch();

		    if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos (
	            		tipoprocesso, 
	            		situacaoprojeto, 
	            		endereco,
	            		bairro,
	            		quadra,
	            		lote,
	            		numero,
	            		cidade,
	            		estado,
	            		id_cg,
	            		datahora,
	            		ativo
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,?,?,now(),true)");	
					$sql->bindValue(1, $tipoprocesso);
					$sql->bindValue(2, $situacaoprojeto);
					$sql->bindValue(3, $endereco);
					$sql->bindValue(4, $bairro);
					$sql->bindValue(5, $quadra);
					$sql->bindValue(6, $lote);
					$sql->bindValue(7, $numero);
					$sql->bindValue(8, $cidade);
					$sql->bindValue(9, $estado);
					$sql->bindValue(10, $linha_usu->id_cg);
					
					$sql->execute();

					$id_pro = db::lastInsertId();

					// Cadastra análise
					$sql = db::prepare("INSERT INTO processos.processos_analise (
	            		id_pro,
	            		id_cg,
	            		tipo,
	            		situacaoprojeto,
	            		datahora
	            	)

	            	VALUES (?,?,?,?,now())");	
					$sql->bindValue(1, $id_pro);
					$sql->bindValue(2, manipuladores::criptografia($_SESSION['id_usuario'],'base64','decode'));
					$sql->bindValue(3, 'ca');
					$sql->bindValue(4, 1);
					
					$sql->execute();
					
					return $id_pro;

					// Mostrar possíveis erros.
					print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos
	            		SET 
	            		tipoprocesso = ".$tipoprocesso.", 
	            		endereco = '".$endereco."',
	            		bairro = '".$bairro."',
	            		quadra = '".$quadra."',
	            		lote = '".$lote."',
	            		numero = ".$numero.",
	            		cidade = ".$cidade.",
	            		estado = '".$estado."',
	            		id_cg = ".manipuladores::criptografia($_SESSION['id_usuario'],'base64','decode')."

	            		WHERE id_pro = ".$id_pro."");
				$sql->execute();
				
				return $id_pro;

				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}