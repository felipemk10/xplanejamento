<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_que_pavimentos extends crud {
	function manipulacoes($id_qp, $id_que, $id_con, $areacontruir, $areaexistente, $areausoexclusivo, $areacomumproporcional, $formulario) {
		
		if ( $formulario != 'delete' and $areacontruir < 0 ) {
			return 'Preencha a área a construir';
		} else if ( $formulario != 'delete' and $areaexistente < 0 ) {
			return 'Preencha a área existente';
		} else if ( $formulario != 'delete' and $areausoexclusivo < 0 ) {
			return 'Preencha a área de uso exclusivo';
		} else if ( $formulario != 'delete' and $areacomumproporcional < 0 ) {
			return 'Preencha a área comum proporcional';
		} else {
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_que_pavimentos (
	            		id_con,
	            		id_que,	 
						areacontruir,	
						areaexistente,	
						areausoexclusivo,	
						areacomumproporcional
	            	) 


	            	VALUES (?,?,?,?,?,?)");	
					$sql->bindValue(1, $id_con);
					$sql->bindValue(2, $id_que);
					$sql->bindValue(3, $areacontruir);
					$sql->bindValue(4, $areaexistente);
					$sql->bindValue(5, $areausoexclusivo);
					$sql->bindValue(6, $areacomumproporcional);
					
					$sql->execute();

					//return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos_que_pavimentos
	            		SET  
							areacontruir			= ".(double)$areacontruir.",
							areaexistente			= ".(double)$areaexistente.",
							areausoexclusivo		= ".(double)$areausoexclusivo.",
							areacomumproporcional	= ".(double)$areacomumproporcional."
							
	            		WHERE id_qp = ".$id_qp."");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$sql = db::prepare("DELETE FROM processos.processos_que_pavimentos WHERE id_qp = $id_qp");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			}
		}
	}
}