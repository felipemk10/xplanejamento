<?php
namespace Biblioteca;

class parametros {
	//	Tempo de execusão de script zerado, assim não haverá o erro maximum 30 seconds
	private function getTempoExecucao($tempo) {
		return set_time_limit($tempo);
	}
	
	private function getData() {
		return date("Y-m-d H:i:s");
	}
	
	//	Modo on-line ou off-line.
	private function getUrlAcesso() {
		if ( $_SERVER['SERVER_NAME'] == '13.58.238.67' ) {
			return 'http://13.58.238.67/xplanejamento/';
		} else {
			return 'http://localhost:8080/xplanejamento/';
		}
	}
	
	private function getUrlAtual() {
		$this->config_protocolo    = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false) ? 'http' : 'https';
		$this->config_host         = $_SERVER['HTTP_HOST'];
		$this->config_script       = $_SERVER['SCRIPT_NAME'];
		$this->config_parametros   = $_SERVER['QUERY_STRING'];
		return $this->config_urlatual     = $this->config_protocolo . '://' . $this->config_host . $this->config_script . '?' . $this->config_parametros;
	}
}
