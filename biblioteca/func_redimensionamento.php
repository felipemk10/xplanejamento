<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\manipuladores;
use Biblioteca\processos;

class func_redimensionamento extends crud {
	function manipulacoes($id_red, $id_pro, $areatotalterreno, $sa_qtdlotes, $sp_qtdlotes, $descricaolotes, $requerente, $requerentetelefone, $tipo, $formulario) {
		
		$descricaolotes 	= manipuladores::anti_injection($descricaolotes);
		$requerente 		= manipuladores::anti_injection($requerente);
		$requerentetelefone = manipuladores::anti_injection($requerentetelefone);


		if ( empty($sa_qtdlotes) ) {
			return 'Preencha a quantidade dos lotes - Situação Atual';
		} else if ( empty($sp_qtdlotes) ) {
			return 'Preencha a quantidade dos lotes - Situação Pretendida';
		} else if ( empty($descricaolotes) ) {
			return 'Digite a descrição dos lotes';
		} else if ( empty($requerente) ) {
			return 'Digite o nome do requerente';
		} else if ( empty($requerentetelefone) ) {
			return 'Preencha a área de uso exclusivo';
		} else if ( empty($areatotalterreno) ) {
			return 'Preencha a área comum proporcional';
		} else if ( $tipo < 1 and $tipo > 2 ) {
			return 'Tipo de Redimensionamento inválido';
		} else {
			if ( $formulario == 'cadastro' ){
		    	// Cadastra processo
	            $sql = db::prepare("INSERT INTO processos.processos_redimensionamento (
	            		id_pro,	 
						areatotalterreno,	
						requerente,	
						requerentetelefone,	
						descricaolotes,
						sa_qtdlotes,
						sp_qtdlotes,
						tipo,
						datahora
	            	) 


	            	VALUES (?,?,?,?,?,?,?,?,now())");	
					$sql->bindValue(1, $id_pro);
					$sql->bindValue(2, $areatotalterreno);
					$sql->bindValue(3, $requerente);
					$sql->bindValue(4, $requerentetelefone);
					$sql->bindValue(5, $descricaolotes);
					$sql->bindValue(6, $sa_qtdlotes);
					$sql->bindValue(7, $sp_qtdlotes);
					$sql->bindValue(8, $tipo);
					
					$sql->execute();

					//return db::lastInsertId();

					// Mostrar possíveis erros.
					//print_r($sql->errorInfo());

				//	Redirecionamento
				//header("Location: index.php");
			} else if ( $formulario == 'alteracao' ) {
				$sql = db::prepare("UPDATE processos.processos_redimensionamento
	            		SET 
		            		id_pro 					= $id_pro,	 
							areatotalterreno		= $areatotalterreno,
							requerente				= '".$requerente."',
							requerentetelefone		= '".$requerentetelefone."',
							descricaolotes			= '".$descricaolotes."',
							sa_qtdlotes				= $sa_qtdlotes,
							sp_qtdlotes				= $sp_qtdlotes,
							tipo  					= $tipo
							
	            		WHERE id_red = ".$id_red."");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
			} else if ( $formulario == 'delete' ) {
				$sql = db::prepare("DELETE FROM processos.processos_redimensionamento WHERE id_red = $id_red");
				
				$sql->execute();
				
				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());
		}
		}
	}
}