<?php
namespace Biblioteca;

use Biblioteca\crud;
use Biblioteca\processos;

class func_usuario_sis extends crud {
		 
	function manipulacoes(
		$id_cg, 
		$senha,	 
		$tipousuario,	 
		$departamento,	 
		$cargofuncao,
		$tipoprofissional,
		$formulario) {
		
		if ( $formulario == 'cadastro' ){
	    	// Cadastra processo
            $sql = db::prepare("INSERT INTO geral.usuarios (
            		id_cg,	 
					senha,
					tipousuario,
					departamento,	 
					cargofuncao,
					tipoprofissional
            	) 


            	VALUES (?,?,?,?,?,?)");	
				$sql->bindValue(1, $id_cg);
				$sql->bindValue(2, md5($senha));
				$sql->bindValue(3, $tipousuario);
				$sql->bindValue(4, $departamento);
				$sql->bindValue(5, $cargofuncao);
				$sql->bindValue(6, $tipoprofissional);

				$sql->execute();

				return db::lastInsertId();

				// Mostrar possíveis erros.
				//print_r($sql->errorInfo());

			//	Redirecionamento
			//header("Location: index.php");
		} else if ( $formulario == 'alteracao' ) {
			$sql = "UPDATE geral.usuarios SET";

			if ( !empty($senha) ) {
				$sql = $sql." senha = '".md5($senha)."',";
			}
			
			if ( !empty($tipoprofissional) ) {
				$vartipoprofissional = ',tipoprofissional = '.$tipoprofissional.' ';
			}
			$sql = $sql."
					departamento	= '".$departamento."',
					cargofuncao 	= '".$cargofuncao."'
					".$vartipoprofissional."

				WHERE 
					id_cg = $id_cg";
			
			$sql = db::prepare($sql);

			$sql->execute();
			
			// Mostrar possíveis erros.
			//print_r($sql->errorInfo());
		} else if ( $formulario == 'desativa' ) {
			$sql = db::prepare("UPDATE geral.cg SET ativo = false WHERE id_cg = ".$id_cg."");
			
			$sql->execute();
			
			// Mostrar possíveis erros.
			//print_r($sql->errorInfo());
		}
	}
}
