<?php
namespace Biblioteca;

use Biblioteca\crud;

class processos extends crud {
  protected $table;
  
  private $tipoProcesso;
  private $tipoSituacaoProjeto;
  
  // 1 = Alvará de Construção, 2 = Alvará de Regularização de Obras, 3 = Alvará de Acréscimo de Área, 4 = Condomínio Edilício, 5 = Redimensionamento
  public function setTipoProcesso($tipoprocesso) {
    if ( $tipoprocesso == 0 ) {
      $this->tipoProcesso = $tipoprocesso = 1;
    } else {
      $this->tipoProcesso = $tipoprocesso;
    }
  }
  
  /*1 - A ser analisado, 2 - Em análise, 3 - Pendência de documentos ou correção, 4 - Processo não permitido ou reprovado na análise/vistoria, 5 - Processo encaminhado à procuradoria - Dúvida na vistoria, 6 - Processo encaminhado ao departamento imobiliário - Dúvida na vistoria, 7 - Aprovado - Pendente de pagamento de taxa para elaboração do decreto, 8 - Aprovado - Fazer decreto e pegar assinatura - com taxa paga, 9 - Aprovado - Decreto assinado - para entregar, 10 - Aprovado - Processo finalizado e decreto entregue, 11 - Aprovado, 12 - Dispensar Vistoria*/
  public function setSituacaoProjeto($situacaoprojeto) {
    $this->tipoSituacaoProjeto = $situacaoprojeto;
  }

  public function getTipoProcesso() {
    return $this->tipoProcesso;
  }
  public function getSituacaoProjeto() {
    return $this->tipoSituacaoProjeto;
  }

  public function consulta($consulta) {
    $valor = db::prepare($consulta);
    $valor->execute();
    //$valor2[0] = $valor->rowCount();
    //$valor2[1] = $valor->fetchAll();

    return $valor;
  }
  public function altTipoProcesso($id_pro,$tipoprocesso) {
    $sql = db::prepare("UPDATE processos.processos
              SET 
              tipoprocesso = ".$tipoprocesso."

              WHERE id_pro = ".$id_pro."");
    $sql->execute();
    
    print_r($sql->errorInfo());    
  }
}
